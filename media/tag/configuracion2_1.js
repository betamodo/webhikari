$(document).ready(function () {
    var sampleTags = ['c++', 'java', 'php', 'coldfusion', 'javascript', 'asp', 'ruby', 'python', 'c', 'scala', 'groovy', 'haskell', 'perl', 'erlang', 'apl', 'cobol', 'go', 'lua'];
    var input = $("input#text");
    var instance = $("<ul class=\"tags\"></ul>");
    $('#myULTags').tagit({
        availableTags: sampleTags, // this param is of course optional. it's for autocomplete.
        // configure the name of the input field (will be submitted with form), default: item[tags]
        tagSource: function (request, response) {
            $.ajax({
                url: "../json_buscar_tag",
                dataType: "json",
                type: 'POST',
                data: {
                    term: 'ruby'
                },
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.value
                        }
                    }));
                }

            });
        },
        itemName: 'item',
        fieldName: 'tags'
    });

});