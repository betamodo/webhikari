$(document).ready(function () {
	$(document).on("click", ".dlgdetalle", function () {
		var idlocal = $("#cbolocal").val();
        var idarea = $("#cbolabor").val();
        var finicio = $("#finicio").val();
        var ffin = $("#ffin").val();
		var dia = $(this).attr("data-dia");
		var hora = $(this).attr("data-hora");
		var total = $(this).attr("data-total");
        $("#divtareo").append("<div class='cargando'></div>");
        $.post("tareo/json_detalle_tareo", {total:total, hora:hora,dia:dia, idlocal: idlocal, idarea: idarea, finicio: finicio, ffin: ffin}, function (data) {
            $("#bodydetalletareo").html(data);
        });
		$('#modaldetalle').modal('show');
		$(".cargando").remove();
	});
	
	$(document).on("click", "#btnGuardarSimulacion", function () {
		var idlocal = $("#cbolocal").val();
		var idlabor = $("#cbolabor").val();
		var finicio = $("#finicio").val();
		var ffin = $("#ffin").val();
		
		var formData = new FormData();
		formData.append('idlocal', idlocal);
		formData.append('idarea', idlabor);
		formData.append('finicio', $("#finicio").val());
		formData.append('ffin', $("#ffin").val());
		formData.append('dia', $("#nomdia").val());
		formData.append('hrcomun', $("#hrcomun").val());
		var i=0;
        $( ".cboLaborSimu select option:selected" ).each(function() {
			formData.append('key_'+ i, $(this).attr("data-id"));
			formData.append('dia_'+ $(this).attr("data-id"), $(this).val());
			i++;
		});
		
		$( ".cboHoraSimu select option:selected" ).each(function() {
			formData.append('hora_'+ $(this).attr("data-id"), $(this).val());
			i++;
		});
		
		$.ajax({
            url: 'tareo/json_grabar_simu',
            type: 'POST',
            contentType: false,
            data: formData,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $("#footer_modal").append("<div id='cargando' class='cargando'></div>");
            },
            success: function (data) {
				if (data.msj == "") {
					var idlocal = $("#cbolocal").val();
					var idlabor = $("#cbolabor").val();
					var finicio = $("#finicio").val();
					var ffin = $("#ffin").val();
					$('#modaldetalleaux').modal('hide');
					$("#divtareoaux").html("<div class='cargando'></div>");
					$.post("tareo/json_tareo_aux", {idlocal: idlocal, idlabor: idlabor, finicio: finicio, ffin: ffin}, function (data) {
							$("#divtareoaux").html(data);
					});
					$(".cargando").remove();
                }else{
					alert(data.msj);
					$('#modaldetalleaux').modal('hide');
				}
                
	
            },
            error: function () {
                $("#cargador").remove();
            }
        }).done(function (msg) {

        });
	});
	
	$(document).on("click", ".dlgdetalleaux", function () {
		var idlocal = $("#cbolocal").val();
        var idarea = $("#cbolabor").val();
        var finicio = $("#finicio").val();
        var ffin = $("#ffin").val();
		var dia = $(this).attr("data-dia");
		var hora = $(this).attr("data-hora");
		var total = $(this).attr("data-total");
        $("#divtareoaux").append("<div class='cargando'></div>");
        $.post("tareo/json_detalle_tareo_aux", {total:total, hora:hora,dia:dia, idlocal: idlocal, idarea: idarea, finicio: finicio, ffin: ffin}, function (data) {
            $("#bodydetalletareoaux").html(data);
        });
		$('#modaldetalleaux').modal('show');
		$(".cargando").remove();
	});
	
    $(document).on("click", "#btnvertareo", function () {
        var idlocal = $("#cbolocal").val();
        var idlabor = $("#cbolabor").val();
        var finicio = $("#finicio").val();
        var ffin = $("#ffin").val();
        $("#divtareo").html("<div class='cargando'></div>");
		$("#divtareoaux").html("<div class='cargando'></div>");
        $.post("tareo/json_tareo", {idlocal: idlocal, idlabor: idlabor, finicio: finicio, ffin: ffin}, function (data) {
            $("#divtareo").html(data);
        });
		$.post("tareo/json_tareo_aux", {idlocal: idlocal, idlabor: idlabor, finicio: finicio, ffin: ffin}, function (data) {
            $("#divtareoaux").html(data);
        });
    });
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
});
 