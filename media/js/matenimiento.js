$(document).ready(function () {

    $("#btnCrearUsuario").click(function (e) {
        e.preventDefault();

        var msj = "";
        // Recogiendo datos
        var nombre = $("#txtnombre").val();
        if (nombre == "") {
            msj += "Debe escribir el nombre\n";
        }

        var username = $("#txtusername").val();
        if (username == "") {
            msj += "Debe escribir el username\n";
        }
        var correo = $("#txtcorreo").val();
        if (correo == "") {
            msj += "Debe escribir el correo\n";
        }

        var clave = $("#txtclave").val();
        if (clave == "") {
            msj += "Debe escribir el clave\n";
        }

        var rol = $("#cborol").val();
        if (rol == "") {
            msj += "Debe seleccionar el rol\n";
        }
        var estado = $("#cboEstado").val();
        if (estado == "") {
            msj += "Debe seleccionar el estado\n";
        }

        if (msj != "") {
            alert(msj);
            return false;
        }
        var formData = new FormData();
        formData.append('nombre', nombre);
        formData.append('username', username);
        formData.append('clave', clave);
        formData.append('rol', rol);
        formData.append('correo', correo);
        formData.append('estado', estado);

        $("input[type=file]").each(function (index)
        {
            console.log(index);
            if ($('#file_' + index).length) {
                formData.append('archivo' + index, $('#file_' + index)[0].files[0]);
                $('#file_' + index).remove();
            }
        });

        $.ajax({
            url: 'json_crear_usuario',
            type: 'POST',
            contentType: false,
            data: formData,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $("#panelFormulario").append("<div id='cargando' class='cargando'></div>");
            },
            success: function (data) {
                if (data.msj == "") {
                    location.href = "../usuario/lista";
                } else {
                    $("#mensajeaux").append('<div class="alert alert-danger fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            data.msj +
                            '</div>');
							$("#cargando").remove();    
                }
                $("#cargador").remove();                

            },
            error: function () {
                $("#cargador").remove();
                $("#files_list").html("");
                //alert("No se pudo registrar, se sugiere actualizar la página");
            }
        }).done(function (msg) {

        });
    });

    $("#btnEditarUsuario").click(function (e) {
        e.preventDefault();

        var msj = "";
        // Recogiendo datos
        var idusuario = $("#txtusuario").val();
        var nombre = $("#txtnombre").val();
        if (nombre == "") {
            msj += "Debe escribir el nombre\n";
        }

        var username = $("#txtusername").val();
        if (username == "") {
            msj += "Debe escribir el username\n";
        }
        var correo = $("#txtcorreo").val();
        if (correo == "") {
            msj += "Debe escribir el correo\n";
        }

        var clave = $("#txtclave").val();
        if (clave == "") {
            msj += "Debe escribir el clave\n";
        }

        var rol = $("#cborol").val();
        if (rol == "") {
            msj += "Debe seleccionar el rol\n";
        }
        var estado = $("#cboEstado").val();
        if (estado == "") {
            msj += "Debe seleccionar el estado\n";
        }

        if (msj != "") {
            alert(msj);
            return false;
        }
        var formData = new FormData();
        formData.append('nombre', nombre);
        formData.append('username', username);
        formData.append('clave', clave);
        formData.append('rol', rol);
        formData.append('correo', correo);
        formData.append('estado', estado);
        formData.append('idusuario', idusuario);

        $("input[type=file]").each(function (index)
        {
            console.log(index);
            if ($('#file_' + index).length) {
                formData.append('archivo' + index, $('#file_' + index)[0].files[0]);
                $('#file_' + index).remove();
            }
        });

        $.ajax({
            url: '../json_editar_usuario',
            type: 'POST',
            contentType: false,
            data: formData,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $("#panelFormulario").append("<div id='cargador' class='cargando'><div>");
            },
            success: function (data) {
                if (data.msj == "") {
                    location.href = "../../usuario/lista";
                } else {
                    $("#cargador").remove();
                    $("#mensajeaux").html('<div class="alert alert-danger fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            '<strong></strong> ' + data.msj +
                            '</div>');

                }
            },
            error: function () {
                $("#cargador").remove();
                $("#files_list").html("");
                //alert("No se pudo registrar, se sugiere actualizar la página");
            }
        }).done(function (msg) {

        });
    });

    /* ----------------------------------------------------- */
    $("#btnCrearLocal").click(function (e) {
        e.preventDefault();

        var msj = "";
        // Recogiendo datos
        var nombre = $("#txtnombre").val();
        if (nombre == "") {
            msj += "Debe escribir el nombre\n";
        }

        var idusuario = $("#cboUsuario").val();
        if (idusuario == "") {
            msj += "Debe seleccionar el usurio responsable\n";
        }
        var estado = $("#cboEstado").val();
        if (estado == "") {
            msj += "Debe seleccionar el estado\n";
        }

        if (msj != "") {
            alert(msj);
            return false;
        }

        var formData = new FormData();
        formData.append('nombre', nombre);
        formData.append('idusuario', idusuario);
        formData.append('estado', estado);

        $.ajax({
            url: 'json_crear_local',
            type: 'POST',
            contentType: false,
            data: formData,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $("#panelFormulario").append("<div id='cargando' class='cargando'><div>");
            },
            success: function (data) {
                if (data.msj == "") {
                    location.href = "../local/lista";
                } else {
                    $("#mensajeaux").append('<div class="alert alert-danger fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            '<strong></strong> ' + data.msj +
                            '</div>');
                    $("#cargando").remove();
                }

            },
            error: function () {
                $("#cargador").remove();
            }
        }).done(function (msg) {

        });
    });

    $("#btnEditarLocal").click(function (e) {
        e.preventDefault();

        var msj = "";
        // Recogiendo datos
        var idlocal = $("#txtidlocal").val();

        var nombre = $("#txtnombre").val();
        if (nombre == "") {
            msj += "Debe escribir el nombre\n";
        }

        var idusuario = $("#cboUsuario").val();
        if (idusuario == "") {
            msj += "Debe seleccionar el usurio responsable\n";
        }
        var estado = $("#cboEstado").val();
        if (estado == "") {
            msj += "Debe seleccionar el estado\n";
        }

        if (msj != "") {
            alert(msj);
            return false;
        }

        var formData = new FormData();
        formData.append('idlocal', idlocal);
        formData.append('nombre', nombre);
        formData.append('idusuario', idusuario);
        formData.append('estado', estado);

        $.ajax({
            url: '../json_editar_local',
            type: 'POST',
            contentType: false,
            data: formData,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $("#panelFormulario").append("<div id='cargador' class='cargando'><div>");
            },
            success: function (data) {
                if (data.msj == "") {
                    location.href = "../../local/lista";
                } else {
                    $("#cargador").remove();
                    $("#mensajeaux").html('<div class="alert alert-danger fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            '<strong></strong> ' + data.msj +
                            '</div>');

                }
            },
            error: function () {
                $("#cargador").remove();                
            }
        }).done(function (msg) {

        });
    });

/* ----------------------------------------------------- */

$("#btnCrearCategoria").click(function (e) {
        e.preventDefault();

        var msj = "";
        // Recogiendo datos
        var nombre = $("#txtnombre").val();
        if (nombre == "") {
            msj += "Debe escribir el nombre\n";
        }

        var idusuario = $("#cboUsuario").val();
        if (idusuario == "") {
            msj += "Debe seleccionar el usurio responsable\n";
        }
        var estado = $("#cboEstado").val();
        if (estado == "") {
            msj += "Debe seleccionar el estado\n";
        }

        if (msj != "") {
            alert(msj);
            return false;
        }

        var formData = new FormData();
        formData.append('nombre', nombre);
        formData.append('idusuario', idusuario);
        formData.append('estado', estado);

        $.ajax({
            url: 'json_crear_categoria',
            type: 'POST',
            contentType: false,
            data: formData,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $("#panelFormulario").append("<div id='cargando' class='cargando'><div>");
            },
            success: function (data) {
                if (data.msj == "") {
                    location.href = "../categoria/lista";
                } else {
                    $("#mensajeaux").html('<div class="alert alert-error fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            '<strong></strong> ' + data.msj +
                            '</div>');
                    $("#cargador").remove();
                }

            },
            error: function () {
                $("#cargador").remove();
            }
        }).done(function (msg) {

        });
    });

$("#btnEditarCategoria").click(function (e) {
        e.preventDefault();

        var msj = "";
        // Recogiendo datos
        var idcategoria = $("#txtidcategoria").val();

        var nombre = $("#txtnombre").val();
        if (nombre == "") {
            msj += "Debe escribir el nombre\n";
        }

        var idusuario = $("#cboUsuario").val();
        if (idusuario == "") {
            msj += "Debe seleccionar el usurio responsable\n";
        }
        var estado = $("#cboEstado").val();
        if (estado == "") {
            msj += "Debe seleccionar el estado\n";
        }

        if (msj != "") {
            alert(msj);
            return false;
        }

        var formData = new FormData();
        formData.append('idcategoria', idcategoria);
        formData.append('nombre', nombre);
        formData.append('idusuario', idusuario);
        formData.append('estado', estado);

        $.ajax({
            url: '../json_editar_categoria',
            type: 'POST',
            contentType: false,
            data: formData,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $("#panelFormulario").append("<div id='cargador' class='cargando'><div>");
            },
            success: function (data) {
                if (data.msj == "") {
                    location.href = "../../categoria/lista";
                } else {
                    $("#cargador").remove();
                    $("#mensajeaux").html('<div class="alert alert-danger fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            '<strong></strong> ' + data.msj +
                            '</div>');

                }
            },
            error: function () {
                $("#cargador").remove();                
            }
        }).done(function (msg) {

        });
    });

});
 