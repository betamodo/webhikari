$(document).ready(function () {

    $(".dataconfig").click(function (e) {
        e.preventDefault();
        var id = $(this).attr("data-id");
        var valor = $("#config_" + id).val();
        var msj = "";
        // Recogiendo datos

        if (valor == "") {
            alert("Debe escribir el nombre");
            return false;
        }

        var formData = new FormData();
        formData.append('id', id);
        formData.append('valor', valor);

        $.ajax({
            url: 'json_guardar_config',
            type: 'POST',
            contentType: false,
            data: formData,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $("#panelFormulario").append("<div id='cargando' class='cargando'></div>");
            },
            success: function (data) {
                if (data.msj == "") {
                    location.href = "lista";
                } else {
                    $("#mensajeaux").append('<div class="alert alert-danger fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            data.msj +
                            '</div>');
                    $("#cargando").remove();
                }
                $("#cargador").remove();

            },
            error: function () {
                $("#cargador").remove();

            }
        }).done(function (msg) {

        });
    });

});
 