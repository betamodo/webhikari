$(document).ready(function () {
	/*Editar*/
	$("#btnEditarstand").click(function (e) {
        e.preventDefault();

        var msj = "";
        // Recogiendo datos
        var idstand = $("#txtidstand").val();

        var codigo = $("#txtnombre").val();
        if (codigo == "") {
            msj += "Debe escribir el nombre\n";
        }

        var estado = $("#cboestado").val();
        if (estado == "") {
            msj += "Debe seleccionar el estado\n";
        }

        if (msj != "") {
            alert(msj);
            return false;
        }
		var descripcion=$("#txtdescripcion").val();

        var formData = new FormData();
        formData.append('idstand', idstand);
        formData.append('codigo', codigo);        
        formData.append('estado', estado);
		formData.append('descripcion', descripcion);

        $.ajax({
            url: '../json_editar_stand',
            type: 'POST',
            contentType: false,
            data: formData,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $("#panelFormulario").append("<div id='cargador' class='cargando'><div>");
            },
            success: function (data) {
                if (data.msj == "") {
                    location.href = "../../stand/lista";
                } else {
                    $("#cargador").remove();
                    $("#mensajeaux").html('<div class="alert alert-danger fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            '<strong></strong> ' + data.msj +
                            '</div>');

                }
            },
            error: function () {
                $("#cargador").remove();                
            }
        }).done(function (msg) {

        });
    });
	
	
	$("#btnCrearstand").click(function (e) {
        e.preventDefault();

        var msj = "";
        // Recogiendo datos
        var codigo = $("#txtnombre").val();
        if (codigo == "") {
            msj += "Debe escribir el nombre\n";
        }

        var estado = $("#cboestado").val();
        if (estado == "") {
            msj += "Debe seleccionar el estado\n";
        }

        if (msj != "") {
            alert(msj);
            return false;
        }
		var descripcion=$("#txtdescripcion").val();
        var formData = new FormData();
        formData.append('codigo', codigo);
        formData.append('estado', estado);
		formData.append('descripcion', descripcion);
        $.ajax({
            url: 'json_crear_stand',
            type: 'POST',
            contentType: false,
            data: formData,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $("#panelFormulario").append("<div id='cargando' class='cargando'><div>");
            },
            success: function (data) {
                if (data.msj == "") {
                    location.href = "../stand/lista";
                } else {
                    $("#mensajeaux").html('<div class="alert alert-error fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            '<strong></strong> ' + data.msj +
                            '</div>');
                    $("#cargador").remove();
                }

            },
            error: function () {
                $("#cargador").remove();
            }
        }).done(function (msg) {

        });
    });
	
	
});