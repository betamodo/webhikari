$(document).ready(function () {
    $("input").keypress(function (e) {
        if (e.which == 13) {
            $("#entrar").trigger("click");
        }
    });

    $("#entrar").click(function (e) {
        e.preventDefault();
        var msj = validar();
        if (msj == "") {
            //boxLogin = new ajaxLoader($("html"));
            var formData = new FormData();
            formData.append('nomusuario', $("#nomusuario").val());
            formData.append('clave', $("#clave").val());
            $.ajax({
                url: 'usuario/entrar',
                type: 'POST',
                contentType: false,
                data: formData,
                processData: false,
                cache: false
            }).done(function (msg) {
                if (parseInt(msg) == 0) {
                    $("#loginMsj").css("display", "");
                } else {
                    
                    if (parseInt(msg) == 2) {
                        location.href = "panel";
                    } else {
						if(parseInt(msg) == 3){
							//location.href = "documento/lista";
						}else{
							//location.href = "tarea/lista";
						}
                        
                    }
                }
            });
        } else {
            alert(msj);
            //if (boxLogin) boxLogin.remove();
        }
        msj = "";
    });


    function validar() {
        var msj = "";
        if ($("#nomusuario").val() == "") {
            msj += "Debe ingresar su usuario \n";
        }
        if ($("#clave").val() == "") {
            msj += "Debe ingresar su clave \n";
        }
        return msj;
    }
    /*
     $("#test-upload").on('fileloaded', function(event, file, previewId, index) {
     alert('i = ' + index + ', id = ' + previewId + ', file = ' + file.name);
     });
     */
});
 