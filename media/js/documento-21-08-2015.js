$(document).ready(function () {
    $(document).on("clic", ".derivar", function () {
        $('#myModal').modal('show');
    });
    $("#cboTipo").change(function (e) {
        e.preventDefault();

        if ($(this).val() == 1) {
            document.getElementById("txtbox").placeholder = "Código del STANTD";
            $("#txtbox").prop("disabled", false);
        } else if ($(this).val() == 2) {
            document.getElementById("txtbox").placeholder = "Código del BOX";
            $("#txtbox").prop("disabled", false);
        } else {
            document.getElementById("txtbox").placeholder = "";
            $("#txtbox").val("");
            $("#txtbox").prop("disabled", true);
        }

    });

    $("#btnCrearDoc").click(function (e) {
        e.preventDefault();
        var tags = $("input#text").val();
        var aTags = tags.split(",");

        var msj = "";
        // Recogiendo datos

        if (tags == "") {
            msj += "Debe escribir como mínimo una palabra clave\n";
        }

        var asunto = $("#txtasunto").val();
        if (asunto == "") {
            msj += "Debe escribir el asunto\n";
        }

        var origen = $("#txtorigen").val();
        if (origen == "") {
            msj += "Debe escribir el nombre\n";
        }

        var idtipodoc = $("#cbotipodoc").val();
        if (idtipodoc == "") {
            msj += "Debe seleccionar el tipo de documento\n";
        }

        var identificador = $("#txtidentificador").val();
        if (identificador == "") {
            msj += "Debe escribir el identificador\n";
        }

        var idlugar = $("#cboLugar").val();
        if (idlugar == "") {
            msj += "Debe seleccionar el lugar\n";
        }
        var codfile = $("#txtfile").val();
        if (codfile == "") {
            msj += "Debe escribir el código del file\n";
        }
        var idtipo = $("#cboTipo").val();
        var codbox = $("#txtbox").val();
        var descripcion = $("#txtdescripcion").val();
        if (codbox == "") {
            msj += "Debe escribir el código del box o stand\n";
        }


        var formData = new FormData();
        formData.append('idlugar', idlugar);
        formData.append('asunto', asunto);
        formData.append('origen', origen);
        formData.append('idtipodoc', idtipodoc);
        formData.append('identificador', identificador);
        formData.append('codfile', codfile);
        formData.append('idtipo', idtipo);
        formData.append('tags', tags);
        formData.append('codbox', codbox);
        formData.append('descripcion', descripcion);
        var cont = 0;
        $("input[type=file]").each(function (index)
        {
            console.log(index);
            if ($('#file_' + index).length) {
                formData.append('archivo' + index, $('#file_' + index)[0].files[0]);
                //$('#file_' + index).remove();
                cont++;
            }
        });
        if (cont == 0) {
            msj += "Debe adjuntar una imagen o archivo del documento\n";
        }
        if (msj != "") {
            alert(msj);
            return false;
        }
        $.ajax({
            url: 'json_crear_documento',
            type: 'POST',
            contentType: false,
            data: formData,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $("#panelFormulario").append("<div id='cargando' class='cargando'></div>");
            },
            success: function (data) {
                if (data.msj == "") {
                    location.href = "../documento/lista";
                } else {
                    $("#mensajeaux").append('<div class="alert alert-danger fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            data.msj +
                            '</div>');
                    $("#cargando").remove();
                }
                $("#cargador").remove();

            },
            error: function () {
                $("#cargador").remove();
                $("#files_list").html("");
                //alert("No se pudo registrar, se sugiere actualizar la página");
            }
        }).done(function (msg) {

        });
    });

    $("#btnBuscarDoc").click(function () {
        if ($("#busquedaAvanzada").html() == "") {
            $("#busquedaAvanzada").html("<div class='cargando'></div>").load("get_div_busqueda");
        } else {
            $("#busquedaAvanzada").html("");
        }
    });

    $("#btnMoverDoc").click(function (e) {
        e.preventDefault();
        var msj = "";

        var idlugar = $("#cboLugar").val();
        if (idlugar == "") {
            msj += "Debe seleccionar el lugar\n";
        }
        var codfile = $("#txtfile").val();
        if (codfile == "") {
            msj += "Debe escribir el código del file\n";
        }
        var codbox = $("#txtbox").val();
        var iddoc = $("#txtiddoc").val();
        var formData = new FormData();
        formData.append('iddoc', iddoc);
        formData.append('idlugar', idlugar);
        formData.append('codfile', codfile);
        formData.append('codbox', codbox);

        if (msj != "") {
            alert(msj);
            return false;
        }
        $.ajax({
            url: '../json_mover_documento',
            type: 'POST',
            contentType: false,
            data: formData,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $("#panelFormulario").append("<div id='cargando' class='cargando'></div>");
            },
            success: function (data) {
                if (data.msj == "") {
                    location.href = "../documento/lista";
                } else {
                    $("#mensajeaux").append('<div class="alert alert-danger fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            data.msj +
                            '</div>');
                    $("#cargando").remove();
                }
                $("#cargador").remove();

            },
            error: function () {
                $("#cargador").remove();
                $("#files_list").html("");
                //alert("No se pudo registrar, se sugiere actualizar la página");
            }
        }).done(function (msg) {

        });
    });

    $("#btnEditarDoc").click(function (e) {
        e.preventDefault();
        var tags = $("#text").val();
        var msj = "";
        // Recogiendo datos

        if (tags == "") {
            msj += "Debe escribir como mínimo una palabra clave\n";
        }

        var asunto = $("#txtasunto").val();
        if (asunto == "") {
            msj += "Debe escribir el asunto\n";
        }

        var origen = $("#txtorigen").val();
        if (origen == "") {
            msj += "Debe escribir el nombre\n";
        }

        var idtipodoc = $("#cbotipodoc").val();
        if (idtipodoc == "") {
            msj += "Debe seleccionar el tipo de documento\n";
        }

        var identificador = $("#txtidentificador").val();
        if (identificador == "") {
            msj += "Debe escribir el identificador\n";
        }

        var idlugar = $("#cboLugar").val();
        if (idlugar == "") {
            msj += "Debe seleccionar el lugar\n";
        }
        var codfile = $("#txtfile").val();
        if (codfile == "") {
            msj += "Debe escribir el código del file\n";
        }
        var idtipo = $("#cboTipo").val();
        var iddoc = $("#txtiddoc").val();
        var codbox = $("#txtbox").val();
        var descripcion = $("#txtdescripcion").val();
        if (codbox == "") {
            msj += "Debe escribir el código del box o stand\n";
        }


        var formData = new FormData();
        formData.append('iddoc', iddoc);
        formData.append('idlugar', idlugar);
        formData.append('asunto', asunto);
        formData.append('origen', origen);
        formData.append('idtipodoc', idtipodoc);
        formData.append('identificador', identificador);
        formData.append('codfile', codfile);
        formData.append('idtipo', idtipo);
        formData.append('tags', tags);
        formData.append('codbox', codbox);
        formData.append('descripcion', descripcion);
        var cont = 0;
        $("input[type=file]").each(function (index)
        {
            console.log(index);
            if ($('#file_' + index).length) {
                formData.append('archivo' + index, $('#file_' + index)[0].files[0]);
                //$('#file_' + index).remove();
                cont++;
            }
        });
        if (cont == 0) {
            msj += "Debe adjuntar una imagen o archivo del documento\n";
        }
        if (msj != "") {
            alert(msj);
            return false;
        }
        $.ajax({
            url: '../json_editar_documento',
            type: 'POST',
            contentType: false,
            data: formData,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $("#panelFormulario").append("<div id='cargando' class='cargando'></div>");
            },
            success: function (data) {
                if (data.msj == "") {
                    location.href = "../documento/lista";
                } else {
                    $("#mensajeaux").append('<div class="alert alert-danger fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            data.msj +
                            '</div>');
                    $("#cargando").remove();
                }
                $("#cargador").remove();

            },
            error: function () {
                $("#cargador").remove();
                $("#files_list").html("");
                //alert("No se pudo registrar, se sugiere actualizar la página");
            }
        }).done(function (msg) {

        });
    });
    $("#finicio").datepicker();
    $("#ffin").datepicker();
    $("#btnlimpiar").click(function () {
        $("#select_local").val("");
        $("#finicio").val("");
        $("#ffin").val("");
        $("#montoinicial").val("");
        $("#montofinal").val("");
        $("#montoigual").val("");
    });
    $("#btnBuscar").click(function () {
        var idalmacen = $("#select_almacen").val();
        var file = $("#bus_file").val();
        var box = $("#bus_box").val();
        var idtipo = $("#select_tipodoc").val();
        var codigo = $("#bus_codigo").val();
        var asunto = $("#bus_asunto").val();
        var descripcion = $("#bus_descripcion").val();
        var finicio = $("#finicio").val();
        var ffin = $("#ffin").val();


        var formData = new FormData();
        formData.append('idalmacen', idalmacen);
        formData.append('file', file);
        formData.append('box', box);
        formData.append('idtipo', idtipo);
        formData.append('codigo', codigo);
        formData.append('asunto', asunto);
        formData.append('descripcion', descripcion);
        formData.append('finicio', finicio);
        formData.append('ffin', ffin);

        $("#listaTareas").html("<div class='cargando'></div>");
        $.post("json_buscar", {idalmacen: idalmacen, file: file, box: box, finicio: finicio, ffin: ffin, idtipo: idtipo, codigo: codigo, asunto: asunto, descripcion: descripcion}, function (data) {
            $("#listaTareas").html(data);
        });
    });
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
});
 