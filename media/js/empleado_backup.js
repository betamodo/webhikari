$(document).ready(function () {
	/*Editar*/
	$("#btnEditarempleado").click(function (e) {
        e.preventDefault();

        var msj = "";
        // Recogiendo datos
        var idtipodoc = $("#txtidtipodoc").val();

        var nombre = $("#txtnombre").val();
        if (nombre == "") {
            msj += "Debe escribir el nombre\n";
        }

        var estado = $("#cboestado").val();
        if (estado == "") {
            msj += "Debe seleccionar el estado\n";
        }

        if (msj != "") {
            alert(msj);
            return false;
        }

        var formData = new FormData();
        formData.append('idtipodoc', idtipodoc);
        formData.append('nombre', nombre);        
        formData.append('estado', estado);

        $.ajax({
            url: '../json_editar_empleado',
            type: 'POST',
            contentType: false,
            data: formData,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $("#panelFormulario").append("<div id='cargador' class='cargando'><div>");
            },
            success: function (data) {
                if (data.msj == "") {
                    location.href = "../../empleado/lista";
                } else {
                    $("#cargador").remove();
                    $("#mensajeaux").html('<div class="alert alert-danger fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            '<strong></strong> ' + data.msj +
                            '</div>');

                }
            },
            error: function () {
                $("#cargador").remove();                
            }
        }).done(function (msg) {

        });
    });
	
	
	$("#btnCrearempleado").click(function (e) {
        e.preventDefault();

        var msj = "";
        // Recogiendo datos 
        var nombre = $("#txtnombre").val()+" "+$("#txtapellidos").val();
		var direccion = $("#txtdireccion").val();
		var dni = $("#txtdni").val();
		var sexo = $("#cbosexo").val();
		var regimen = $("#cboregimen").val();
        /*******************NUEVO*************************/
        var horasExtrasCheck = $("#cbohorasextrascheck").is(":checked");
        var horasExtras = $("#cbohorasextras").val();
        /*****************FIN-NUEVO***********************/
		var sueldo = $("#txtsueldo").val();
		var idlabor = $("#cbolabor").val();
		var telefono = $("#txttelefono").val();
		var email = $("#txtemail").val();
		var estado = $("#cboEstado").val();
		
		var lu_hora_inicio = $("#txt_lu_hora_inicio").val();
		var lu_hora_fin = $("#txt_lu_hora_fin").val();
		
		var ma_hora_inicio = $("#txt_ma_hora_inicio").val();
		var ma_hora_fin = $("#txt_ma_hora_fin").val();
		
		var mi_hora_inicio = $("#txt_mi_hora_inicio").val();
		var mi_hora_fin = $("#txt_mi_hora_fin").val();
		
		var ju_hora_inicio = $("#txt_ju_hora_inicio").val();
		var ju_hora_fin = $("#txt_ju_hora_fin").val();
		
		var vi_hora_inicio = $("#txt_vi_hora_inicio").val();
		var vi_hora_fin = $("#txt_vi_hora_fin").val();
		
		var sa_hora_inicio = $("#txt_sa_hora_inicio").val();
		var sa_hora_fin = $("#txt_sa_hora_fin").val();
		
		var do_hora_inicio = $("#txt_do_hora_inicio").val();
		var do_hora_fin = $("#txt_do_hora_fin").val();
		
		var finicio = $("#finicio").val();
		var ffin = $("#ffin").val();
		
		var idlocal = $("#cbolocal").val();
	
		
        if (msj != "") {
            alert(msj);
            return false;
        }

        var formData = new FormData();
		formData.append('finicio', finicio);
		formData.append('ffin', ffin);
		formData.append('idlocal', idlocal);
        formData.append('nombre', nombre);
        formData.append('direccion', direccion);
		formData.append('dni', dni);
        formData.append('regimen', regimen);
        /*******************NUEVO*************************/
        formData.append('horasExtrasCheck', horasExtrasCheck);
        formData.append('horasExtras', horasExtras);
        /*****************FIN-NUEVO***********************/
		formData.append('sexo', sexo);
        formData.append('estado', estado);
		formData.append('sueldo', sueldo);
        formData.append('idlabor', idlabor);
		formData.append('telefono', telefono);
        formData.append('email', email);
		
		formData.append('lu_hora_inicio', lu_hora_inicio);
        formData.append('lu_hora_fin', lu_hora_fin);
		
		formData.append('ma_hora_inicio', ma_hora_inicio);
        formData.append('ma_hora_fin', ma_hora_fin);
		
		formData.append('mi_hora_inicio', mi_hora_inicio);
        formData.append('mi_hora_fin', mi_hora_fin);
		
		formData.append('ju_hora_inicio', ju_hora_inicio);
        formData.append('ju_hora_fin', ju_hora_fin);
		
		formData.append('vi_hora_inicio', vi_hora_inicio);
        formData.append('vi_hora_fin', vi_hora_fin);
		
		formData.append('sa_hora_inicio', sa_hora_inicio);
        formData.append('sa_hora_fin', sa_hora_fin);
		
		formData.append('do_hora_inicio', do_hora_inicio);
        formData.append('do_hora_fin', do_hora_fin);

        $.ajax({
            url: 'json_crear_empleado',
            type: 'POST',
            contentType: false,
            data: formData,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $("#panelFormulario").append("<div id='cargando' class='cargando'><div>");
            },
            success: function (data) {
                if (data.msj == "") {
                    location.href = "../empleado/lista";
                } else {
                    $("#mensajeaux").html('<div class="alert alert-error fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            '<strong></strong> ' + data.msj +
                            '</div>');
                    $("#cargador").remove();
                }

            },
            error: function () {
                $("#cargador").remove();
            }
        }).done(function (msg) {

        });
    });
	
	
});