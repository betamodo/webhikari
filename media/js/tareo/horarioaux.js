Array.prototype.equals = function (array) {
    // if the other array is a falsy value, return
    if (!array)
        return false;

    // compare lengths - can save a lot of time 
    if (this.length != array.length)
        return false;

    for (var i = 0, l=this.length; i < l; i++) {
        // Check if we have nested arrays
        if (this[i] instanceof Array && array[i] instanceof Array) {
            // recurse into the nested arrays
            if (!this[i].equals(array[i]))
                return false;       
        }           
        else if (this[i] != array[i]) { 
            // Warning - two different object instances will never be equal: {x:20} != {x:20}
            return false;   
        }           
    }       
    return true;
}

$(function () {
    var diasSumados         = $('span.dia-sumado').length, 
        diasRestados        = $('span.dia-restado').length,
        regimen             = parseInt(empleadoContrato.regimen),
        horasExtras         = parseInt(empleadoContrato.horas_extras),
        horasExtrasDisponibles = 0;
        diasDescanso        = empleadoDescanso,
        vacacionesInicio    = empleadoVacaciones.fecha_inicio,
        vacacionesFin       = empleadoVacaciones.fecha_fin,
        horarioInicial      = getHorarioActual();
    var seleccionados = [];

    $('td.numeric').sortable({
        items: 'a.arrastre > span',
        connectWith: "td.numeric",
        cursor: "move",
        start: function( event, ui ) {
            seleccionados[0] = ui.item.attr("id");
            seleccionados[1] = ui.item.parent();
            // console.log("inicio");
        },
        stop: function( event, ui ) {
            var h = $('#'+seleccionados[0]).siblings('span');

            if (h.length == 1 && esIgualAHora(seleccionados[0], h.attr('id'))) {
                // No registrarlo si seleccionan una hora que sea igual a otra
                ui.item.appendTo(seleccionados[1]);
                return;
            }

            fijarIntercambio(seleccionados[0], seleccionados[1]);
        }
    });



    if (horasExtras > 0) {
        var selectorPadre, selectorHijo;
        $('a#selector-l').parent().on('click.botonL', function(event) {
            event.preventDefault();
            if ($(this).find('a#selector-l').hasClass('disabled') && horasExtrasDisponibles == 0) {
                alert("No hay horas extras disponibles para agregar.");
            };
        });
        $('a#selector-l, a#selector-n').each(function(index, el) {
            $(this).parent().sortable({
                items: 'a > span',
                containment: "table",
                connectWith: "td.numeric",
                cursor: "move", 
                helper: function() {
                    var t = $(this), letra = t.text(), clase = letra == "L" ? "success" : "danger";
                    selectorHijo = t.clone(true);
                    return $('<span class="label label-'+clase+'">'+letra+'</span>');
                },
                start: function( event, ui ) {
                    ui.helper.css({
                        "font-size": "100%",
                        "font-weight": "700",
                        "height" : "auto",
                        "width"  : "auto",
                        "margin"   : "0",
                        "z-index" : "10000",
                        "position" : "absolute"
                    });
                    selectorPadre = ui.item.parent();
                    selectorHijo.appendTo(selectorPadre);
                },
                stop: function(event, ui) {
                    var padre = ui.item.parent(), t = padre.find('span').not(ui.item), letra = ui.item.text(), activo = letra == "L" ? true:false;
                    // Elimino el clon 
                    ui.item.remove();
                    // Si hay una hora valida
                    if (t.length) {
                        // Hay una hora valida para cambiar
                        // si tiene horas extras disponibles
                        // si no es su dia de descanso
                        // si no esta dentro de sus vacaciones
                        if (esHoraNoLaborada(t) && letra == "L" && (isHoraEnDiaDescanso(t) || isHoraEnDiaVacaciones(t))) {
                            // Quieren colocarle labor en un dia de descanso o de vacaciones
                            return;
                        };
                        if ((esHoraLaborada(t) || esHoraNoLaboradaSumada(t)) && !activo) {
                            // Permitir poner una hora a laborar por una no laborada
                            if (esHoraLaborada(t)) {
                                // Pasar a hora laborada restada
                                hacerHoraLaboradaRestada(t);
                            } else {
                                // Pasar a hora no laborada
                                hacerHoraNoLaborada(t);
                            }
                            t.text(letra);
                            horasExtrasDisponibles++;
                        } else if ((esHoraNoLaborada(t) || esHoraLaboradaRestada(t)) && activo) {
                            // Permitir poner una hora a no laborar por una a laborar
                            if (esHoraNoLaborada(t)) {
                                // Pasar a hora no laborada  sumada
                                hacerHoraNoLaboradaSumada(t);
                            } else {
                                // Pasar a hora laborada
                                hacerHoraLaborada(t);
                            }
                            t.text(letra);
                            horasExtrasDisponibles--;
                        }
                        calculateAnShowHorasExtrasDisponibles();
                    };
                }
            });
        });
    };

    $("a#guardar").click(function(event) {
        event.preventDefault();
        // Guardar cuando los horarios son diferentes
        var horarioActual = getHorarioActual();
        $.ajax({
            url: '../../../json_horario_aux',
            type: 'POST',
            dataType: 'json',
            data: {"idEmpleado": empleadoContrato.idempleado, "horarioActual": horarioActual},
            beforeSend: function () {
                $("#panelFormulario").append("<div id='cargador' class='cargando'><div>");
            },
            success: function (data) {
                if (data.msj == "") {
                    location.href = location.href;
                } else {
                    $("#mensajeaux").html('<div class="alert alert-danger fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            '<strong></strong> ' + data.msj +
                            '</div>');

                }
            }
        })
        .always(function() {
            // console.log("complete");
            $("div#cargador").remove();
        });
    });


    calculateAnShowHorasExtrasDisponibles();

    function esIgualAHora(previa, nueva) {
        p = $('#'+previa), n = $('#'+nueva);
        return (p.hasClass('label-success') && n.hasClass('label-success')) || (p.hasClass('label-danger') && n.hasClass('label-danger'));
    }

    function hacerHoraNormal(objeto) {
        objeto.addClass('horario-normal');
    }

    function hacerHoraSumada(objeto) {
        objeto.addClass('dia-sumado');
    }

    function hacerHoraRestada(objeto) {
        objeto.addClass('dia-restado');
    }

    function removerHoraSumada(objeto) {
        objeto.removeClass('dia-sumado');
    }

    function removerHoraRestada(objeto) {
        objeto.removeClass('dia-restado');
    }

    function esHoraLaborada(objeto) {
        return (objeto.hasClass('horario-normal') & objeto.hasClass('label-success'));
    }

    function esHoraNoLaboradaSumada(objeto) {
        return (objeto.hasClass('dia-sumado') & objeto.hasClass('label-success'));
    }

    function esHoraLaboradaRestada(objeto) {
        return (objeto.hasClass('dia-restado') & objeto.hasClass('horario-normal') & objeto.hasClass('label-danger'));
    }

    function esHoraNoLaborada(objeto) {
        return (objeto.hasClass('label-danger'));
    }

    function hacerHoraLaborada(objeto) {
        objeto.addClass('horario-normal')
            .addClass('label-success')
            .removeClass('dia-sumado')
            .removeClass('dia-restado')
            .removeClass('label-danger');
    }

    function hacerHoraNoLaboradaSumada(objeto) {
        objeto.addClass('dia-sumado')
            .addClass('label-success')
            .removeClass('horario-normal')
            .removeClass('dia-restado')
            .removeClass('label-danger');
    }

    function hacerHoraLaboradaRestada(objeto) {
        objeto.addClass('horario-normal')
            .addClass('label-danger')
            .addClass('dia-restado')
            .removeClass('dia-sumado')
            .removeClass('label-success');
    }

    function hacerHoraNoLaborada(objeto) {
        objeto.addClass('label-danger')
            .removeClass('horario-normal')
            .removeClass('dia-sumado')
            .removeClass('dia-restado')
            .removeClass('label-success');
    }

    /**
     * Verifica si la hora seleccionada pertenece a un dia de descanso
     * 
     * @param Object objeto Hora a verificar
     * 
     * @return Boolean
     */
    function isHoraEnDiaDescanso(objeto) {
        var id = objeto.attr('id');
        var diaHora = id.split("-");
        if (diasDescanso) {
            for (var i = diasDescanso.length - 1; i >= 0; i--) {
                if (diaHora[0] == diasDescanso[i].dia_semana) {
                    alert("Es un día de descanso.");
                    return true;
                };
            };
        }
        
        return false;
    }

    /**
     * Verifica si la hora seleccionada pertenece a un dia de vacaciones
     * 
     * @param Object objeto Hora a verificar
     * 
     * @return Boolean
     */
    function isHoraEnDiaVacaciones(objeto) {
        if (vacacionesInicio != undefined && vacacionesFin != undefined) {
            // Tiene vacaciones asignadas
            var id = objeto.attr('id'),
                diaHora = id.split("-"),
                primerDiaRango = Date.parse(rangoHorario.inicio),
                vacacionesI    = Date.parse(vacacionesInicio),
                vacacionesF    = Date.parse(vacacionesFin),
                indiceDiaSeleccionado = objeto.data('indicedia'),
                diaSeleccionado = Date.parse(rangoHorario.inicio).add(parseInt(indiceDiaSeleccionado)).days();
                
                if ((vacacionesI <= diaSeleccionado) && (diaSeleccionado <= vacacionesF)) {
                    alert("Es un día de vacaciones.");
                    return true;
                }
        }

        return false;
    }

    /**
     * Intercambiar los dias cuando se hace drag and drop
     * 
     * @param String idA       Id del objeto que se esta arrastrando
     * @param Object padreOldA Padre anterior del objeto que se arrastro
     * 
     * @return void
     */
    function fijarIntercambio(idA, padreOldA) {
        var a = $('#'+idA), b = a.siblings('span'), padre1 = padreOldA, padre2 = b.parent();
        if(b.length) {
            // if (((esHoraNoLaborada(b) || esHoraLaboradaRestada(b)) && (esHoraLaborada(a) || esHoraNoLaboradaSumada(a))) && (isHoraEnDiaDescanso(b) || isHoraEnDiaVacaciones(b))) {
            if ((isHoraEnDiaDescanso(b) || isHoraEnDiaVacaciones(b))) {
                // Quieren colocarle un dia a laborar en un dia de descanso o de vacaciones
                a.appendTo(padre1);
                return;
            };
            // Intercambiar spans
            b.appendTo(padre1);
            // Limpiar los seleccionados
            seleccionados = [];
            // Intercambiar IDs
            a.attr('id', b.attr('id'));
            b.attr('id', idA);
            // Para la primera fecha seleccionada
            if (esHoraLaborada(a)) { // Hora laborada L
                if (esHoraLaboradaRestada(b)) {
                    // Hora laborada pero es restada para no laborar (L -> LR) y (LR -> L)
                    hacerHoraLaboradaRestada(b);
                    hacerHoraLaborada(a);
                } else if (esHoraNoLaborada(b)) {
                    // Hora no laborada (L -> LR) y (N -> NS)
                    hacerHoraLaboradaRestada(b);
                    hacerHoraNoLaboradaSumada(a);
                }
            } else if (esHoraNoLaboradaSumada(a)) { // Hora no laborada pero es sumada para laborar NS
                if (esHoraLaboradaRestada(b)) {
                    // Hora laborada pero es restada para no laborar (NS -> N) y (LR -> L)
                    hacerHoraNoLaborada(b);
                    hacerHoraLaborada(a);
                } else if (esHoraNoLaborada(b)) {
                    // Hora no laborada (NS -> N) y (N -> NS)
                    hacerHoraNoLaborada(b);
                    hacerHoraNoLaboradaSumada(a);
                }
            } else if (esHoraLaboradaRestada(a)) { // Hora laborada pero es restada para no laborar LR
                if (esHoraLaborada(b)) {
                    // Hora laborada (LR -> L) y (L -> LR)
                    hacerHoraLaborada(b);
                    hacerHoraLaboradaRestada(a);
                } else if (esHoraNoLaboradaSumada(b)) {
                    // Hora no laborada pero es sumada para laborar (LR -> L) y (NS -> N)
                    hacerHoraLaborada(b);
                    hacerHoraNoLaborada(a);
                }
            } else if (esHoraNoLaborada(a)) { // Hora no laborada N
                if (esHoraLaborada(b)) {
                    // Hora laborada (N -> L) y (L -> N)
                    hacerHoraLaborada(b);
                    hacerHoraNoLaborada(a);
                } else if (esHoraNoLaboradaSumada(b)) {
                    // Hora no laborada pero es sumada para laborar (N -> NS) y (NS -> N)
                    hacerHoraNoLaboradaSumada(b);
                    hacerHoraNoLaborada(a);
                }
            }
        }
        calculateAnShowHorasExtrasDisponibles();
    }

    /**
     * Calcula las horas extras disponibles
     * Y ejecuta la funcionisHorarioDiferente
     * 
     * @return void
     */
    function calculateAnShowHorasExtrasDisponibles() {
        var horasNoLaboradasSumadas = $("span.label-success.dia-sumado").length, horasLaboradasRestadas = $("span.label-danger.horario-normal.dia-restado").length,
            botonL = $("a#selector-l");
        horasExtrasDisponibles = horasExtras - horasNoLaboradasSumadas + horasLaboradasRestadas;
        if (horasExtrasDisponibles <= 0) {
            // Solo permitir quitar horas
            botonL.addClass('disabled');
            // botonL.draggable("disable");
            botonL.parent().sortable("disable");
        } else {
            botonL.removeClass('disabled');
            // botonL.draggable("enable");
            botonL.parent().sortable("enable");
        }
        $('#horas-extras-disponibles').text(horasExtrasDisponibles);
        isHorarioDiferente();
    }

    /**
     * Horario actual en array
     *
     * @return Array [0] => id (en formato dia-hora), [1] => labora?, [2] => tipo (1 resta, 2 suma, 0 es normal)
     */
    function getHorarioActual() {
        var horario = [];
        $("a.arrastre span").each(function(index, el) {
            var t = $(this), id = t.attr('id'), labora = t.hasClass('label-success');
            var tipo = 0;
            if (esHoraLaboradaRestada(t)) {
                tipo = 1;
            } else if (esHoraNoLaboradaSumada(t)) {
                tipo = 2;
            }

            // horario.push({"id": id, "labora": labora});
            horario.push([id, labora, tipo]);
        });
        return horario;
    }

    /**
     * Comparar si hay modificaciones en el horario
     *
     * @return void
     */
    function isHorarioDiferente() {
        horarioFinal = getHorarioActual();
        if (!horarioInicial.equals(horarioFinal)) {
            $("a#guardar").removeClass('disabled');
        } else {
            $("a#guardar").addClass('disabled');
        }
    }
});