$(document).ready(function () {
    /*******************NUEVO*************************/
    // Checkbox de horas extras
    $("#cbohorasextrascheck").click(function(event) {
        //Si seleccionan horas extras o no
        var t = $(this), horas = $("#cbohorasextras");
        if (t.is(':checked')) { //Seleccionaron
            horas.removeAttr('disabled');
            horas.attr('min', '1');
            horas.val("1");
            horas.focus();
        } else { //No seleccionaron
            horas.attr('min', '0');
            horas.val("0");
            horas.attr('disabled', 'disabled');
        };
    });

    // Select de dias de descanso
    $("#cbodiasdescanso").multiselect({
        nonSelectedText: 'Días de descanso',
        nSelectedText: ' - Días descanso',
        allSelectedText: 'Todos los días descanso',
        buttonWidth: '100%',
        buttonText: function(options, select) {
            return 'Días de descanso';
        },
        onChange: function(option, checked, select) {
            var caja = $("div#dia-"+$(option).val()), primerHorario = caja.find('select').first(), segundoHorario = caja.find('select').last();
            if (checked) {
                primerHorario.find('option:selected').removeAttr('selected');
                segundoHorario.find('option:selected').removeAttr('selected');
                caja.hide();
            } else{
                caja.show();
            };
        }
    });
    // Ocultar los días en carga para los de descanso
    $("#cbodiasdescanso").children('option').each(function(index, el) {
        var t = $(this), seleccionado = $(this).is(':selected');
        if (seleccionado) {
            $("div#dia-"+t.val()).hide();
        }
    });
    /*****************FIN-NUEVO***********************/

	/*Editar*/
	$("#btnEditarempleado").click(function (e) {
        e.preventDefault();
		//location.href = "../../empleado/lista";
        var msj = "";
        // Recogiendo datos 
		var idempleado = $("#txtidempleado").val();
        var nombre = $("#txtnombre").val();
		var direccion = $("#txtdireccion").val();
		var dni = $("#txtdni").val();
		var sexo = $("#cbosexo").val();
		var regimen = $("#cboregimen").val();
        /*******************NUEVO*************************/
        var horasExtrasCheck = $("#cbohorasextrascheck").is(":checked");
        var horasExtras = $("#cbohorasextras").val();
        var diasDescanso = $("#cbodiasdescanso").val();
        /*****************FIN-NUEVO***********************/
		var sueldo = $("#txtsueldo").val();
		var idlabor = $("#cbolabor").val();
		var telefono = $("#txttelefono").val();
		var email = $("#txtemail").val();
		var estado = $("#cboEstado").val();
		
		var lu_hora_inicio = $("#txt_lu_hora_inicio").val();
		var lu_hora_fin = $("#txt_lu_hora_fin").val();
		
		var ma_hora_inicio = $("#txt_ma_hora_inicio").val();
		var ma_hora_fin = $("#txt_ma_hora_fin").val();
		
		var mi_hora_inicio = $("#txt_mi_hora_inicio").val();
		var mi_hora_fin = $("#txt_mi_hora_fin").val();
		
		var ju_hora_inicio = $("#txt_ju_hora_inicio").val();
		var ju_hora_fin = $("#txt_ju_hora_fin").val();
		
		var vi_hora_inicio = $("#txt_vi_hora_inicio").val();
		var vi_hora_fin = $("#txt_vi_hora_fin").val();
		
		var sa_hora_inicio = $("#txt_sa_hora_inicio").val();
		var sa_hora_fin = $("#txt_sa_hora_fin").val();
		
		var do_hora_inicio = $("#txt_do_hora_inicio").val();
		var do_hora_fin = $("#txt_do_hora_fin").val();
		
		var finicio = $("#finicio").val();
		var ffin = $("#ffin").val();
		
		var idlocal = $("#cbolocal").val();
	
		
        if (msj != "") {
            alert(msj);
            return false;
        }

        var formData = new FormData();
		formData.append('finicio', finicio);
		formData.append('idempleado', idempleado);
		formData.append('ffin', ffin);
		formData.append('idlocal', idlocal);
        formData.append('nombre', nombre);
        formData.append('direccion', direccion);
		formData.append('dni', dni);
        formData.append('regimen', regimen);
        /*******************NUEVO*************************/
        formData.append('horasExtrasCheck', horasExtrasCheck);
        formData.append('horasExtras', horasExtras);
        formData.append('diasDescanso', diasDescanso);
        /*****************FIN-NUEVO***********************/
		formData.append('sexo', sexo);
        formData.append('estado', estado);
		formData.append('sueldo', sueldo);
        formData.append('idlabor', idlabor);
		formData.append('telefono', telefono);
        formData.append('email', email);
		
		formData.append('lu_hora_inicio', lu_hora_inicio);
        formData.append('lu_hora_fin', lu_hora_fin);
		
		formData.append('ma_hora_inicio', ma_hora_inicio);
        formData.append('ma_hora_fin', ma_hora_fin);
		
		formData.append('mi_hora_inicio', mi_hora_inicio);
        formData.append('mi_hora_fin', mi_hora_fin);
		
		formData.append('ju_hora_inicio', ju_hora_inicio);
        formData.append('ju_hora_fin', ju_hora_fin);
		
		formData.append('vi_hora_inicio', vi_hora_inicio);
        formData.append('vi_hora_fin', vi_hora_fin);
		
		formData.append('sa_hora_inicio', sa_hora_inicio);
        formData.append('sa_hora_fin', sa_hora_fin);
		
		formData.append('do_hora_inicio', do_hora_inicio);
        formData.append('do_hora_fin', do_hora_fin);

        $.ajax({
            url: '../json_editar_empleado',
            type: 'POST',
            contentType: false,
            data: formData,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $("#panelFormulario").append("<div id='cargador' class='cargando'><div>");
            },
            success: function (data) {
                if (data.msj == "") {
                    location.href = "../../empleado/lista";
                } else {
                    $("#cargador").remove();
                    $("#mensajeaux").html('<div class="alert alert-danger fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            '<strong></strong> ' + data.msj +
                            '</div>');

                }
            },
            error: function () {
                $("#cargador").remove();                
            }
        }).done(function (msg) {

        });
    });
	
	
	$("#btnCrearempleado").click(function (e) {
        e.preventDefault();

        var msj = "";
        // Recogiendo datos 
        var nombre = $("#txtnombre").val()+" "+$("#txtapellidos").val();
		var direccion = $("#txtdireccion").val();
		var dni = $("#txtdni").val();
		var sexo = $("#cbosexo").val();
		var regimen = $("#cboregimen").val();
        /*******************NUEVO*************************/
        var horasExtrasCheck = $("#cbohorasextrascheck").is(":checked");
        var horasExtras = $("#cbohorasextras").val();
        var diasDescanso = $("#cbodiasdescanso").val();
        /*****************FIN-NUEVO***********************/
		var sueldo = $("#txtsueldo").val();
		var idlabor = $("#cbolabor").val();
		var telefono = $("#txttelefono").val();
		var email = $("#txtemail").val();
		var estado = $("#cboEstado").val();
		
		var lu_hora_inicio = $("#txt_lu_hora_inicio").val();
		var lu_hora_fin = $("#txt_lu_hora_fin").val();
		
		var ma_hora_inicio = $("#txt_ma_hora_inicio").val();
		var ma_hora_fin = $("#txt_ma_hora_fin").val();
		
		var mi_hora_inicio = $("#txt_mi_hora_inicio").val();
		var mi_hora_fin = $("#txt_mi_hora_fin").val();
		
		var ju_hora_inicio = $("#txt_ju_hora_inicio").val();
		var ju_hora_fin = $("#txt_ju_hora_fin").val();
		
		var vi_hora_inicio = $("#txt_vi_hora_inicio").val();
		var vi_hora_fin = $("#txt_vi_hora_fin").val();
		
		var sa_hora_inicio = $("#txt_sa_hora_inicio").val();
		var sa_hora_fin = $("#txt_sa_hora_fin").val();
		
		var do_hora_inicio = $("#txt_do_hora_inicio").val();
		var do_hora_fin = $("#txt_do_hora_fin").val();
		
		var finicio = $("#finicio").val();
		var ffin = $("#ffin").val();
		
		var idlocal = $("#cbolocal").val();
	
		
        if (msj != "") {
            alert(msj);
            return false;
        }

        var formData = new FormData();
		formData.append('finicio', finicio);
		formData.append('ffin', ffin);
		formData.append('idlocal', idlocal);
        formData.append('nombre', nombre);
        formData.append('direccion', direccion);
		formData.append('dni', dni);
        formData.append('regimen', regimen);
        /*******************NUEVO*************************/
        formData.append('horasExtrasCheck', horasExtrasCheck);
        formData.append('horasExtras', horasExtras);
        formData.append('diasDescanso', diasDescanso);
        /*****************FIN-NUEVO***********************/
		formData.append('sexo', sexo);
        formData.append('estado', estado);
		formData.append('sueldo', sueldo);
        formData.append('idlabor', idlabor);
		formData.append('telefono', telefono);
        formData.append('email', email);
		
		formData.append('lu_hora_inicio', lu_hora_inicio);
        formData.append('lu_hora_fin', lu_hora_fin);
		
		formData.append('ma_hora_inicio', ma_hora_inicio);
        formData.append('ma_hora_fin', ma_hora_fin);
		
		formData.append('mi_hora_inicio', mi_hora_inicio);
        formData.append('mi_hora_fin', mi_hora_fin);
		
		formData.append('ju_hora_inicio', ju_hora_inicio);
        formData.append('ju_hora_fin', ju_hora_fin);
		
		formData.append('vi_hora_inicio', vi_hora_inicio);
        formData.append('vi_hora_fin', vi_hora_fin);
		
		formData.append('sa_hora_inicio', sa_hora_inicio);
        formData.append('sa_hora_fin', sa_hora_fin);
		
		formData.append('do_hora_inicio', do_hora_inicio);
        formData.append('do_hora_fin', do_hora_fin);

        $.ajax({
            url: 'json_crear_empleado',
            type: 'POST',
            contentType: false,
            data: formData,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $("#panelFormulario").append("<div id='cargando' class='cargando'><div>");
            },
            success: function (data) {
                if (data.msj == "") {
                    location.href = "../empleado/lista";
                } else {
                    $("#mensajeaux").html('<div class="alert alert-error fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            '<strong></strong> ' + data.msj +
                            '</div>');
                    $("#cargador").remove();
                }

            },
            error: function () {
                $("#cargador").remove();
            }
        }).done(function (msg) {

        });
    });
	
	
});