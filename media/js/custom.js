$(document).ready(function () {
	var arrFiles = [];
    var nro_files = 0;
    jQuery('.numbersOnly').keyup(function () {
        this.value = this.value.replace(/[^0-9\.]/g, '');
    });

    $(document).on("click", "#btncambiaestado", function () {
        var idtarea = $(this).attr("data-idtarea");
        var idestado = $("#select_estado").val();
        $.post("../json_cambiaestado", {idtarea: idtarea, idestado: idestado}, function (div) {
        });
        location.href = "../tarea/lista";        
    });
    $(document).on("click", ".btnresponder", function () {
        var id = $(this).attr("data-id");
        var descripcion = $("#texto_" + id).val();
        $("#chat_respuestas_" + id).append("<div class='text-center'><img id='carga' src='../../media/img/carga.gif'></div>");
        $.post("../json_responder", {id: id, descripcion: descripcion}, function (div) {
            $("#chat_respuestas_" + id).append(div);
            $("#carga").remove();
        });
        $("#texto_" + id).val("");

        $("#form_respuesta_" + id).html("");
    });

    $(document).on("click", ".cerrarresponder", function () {
        var id = $(this).attr("data-id");
        $("#form_respuesta_" + id).html("");
    });

    $(".link-responder").click(function () {
        var id = $(this).attr("data-id");
        var html = '<div class="responder clearifx">' +
                '<span class = "spn-avatar pull-left media-object" href = "#"> ' +
                '<i class = "glyphicon glyphicon-user" ></i>' +
                '</span>' +
                '<div class="media-body" > ' +
                '<form action=""> ' +
                '<div class="form-group">' +
                '<textarea id="texto_' + id + '" class = "form-control" ></textarea>' +
                '</div>' +
                '<a href="javascript:void(0);" class="btnresponder btn btn-sm btn-default pull-right" data-tipo="cerrar" data-id=' + id + ' >Enviar</a>' +
                '<a href="javascript:void(0);" class="cerrarresponder btn btn-sm btn-link pull-right" data-tipo="cerrar" data-id=' + id + ' >Cerrar</a>' +
                '</form>' +
                '</div>    ' +
                '</div>';
        $("#form_respuesta_" + id).html("");
        $("#form_respuesta_" + id).html(html);
    });
    $(".rechazardir").click(function (e) {
        var id = $(this).attr("data-id");
        var idgrupo = $(this).attr("data-idgrupo");
		/*var contcheck=$("input:checkbox").size();
		alert(contcheck);
		return false;*/
        var dialog = $('<p>¿Realmente deseas rechazar?<br>La tarea regresará a esperar a ser atendida</p>').dialog({
            buttons: {
                "Si": function () {
                    $(this).remove();
                    $.post("../json_rechazar", {idtarea: id});
                    location.href = "../evaluargrupo/" + idgrupo;
                },
                "No": function () {
                    $(this).remove();
                    return false;
                }
            }
        });
    });
    $('#btnAgregarArchivo').click(function (event) {
        nro_files++;
        var newhtml = '<br id="br_' + nro_files + '"><div id="remove_' + nro_files + '" style="margin-right: 4px;" class="btn btn-sm btn-default btn_remover_files" data-id="' + nro_files + '"><span data-id="file_' + nro_files + '" class="glyphicon glyphicon-minus-sign btn_remover_files"></span></div>';
        newhtml += '<input type="file" name="file_' + nro_files + '" id="file_' + nro_files + '" class="custom-file-input">';
        $('#subir2').append(newhtml);
        contarInputFiles();
    });
    $(document).on("click", ".btn_remover_files", function () {
        var id = $(this).attr("data-id");
        $("#file_" + id).remove();
        $("#remove_" + id).remove();
        $("#br_" + id).remove();
    });
    function contarInputFiles() {
        arrFiles = [];
        var i = 0;
        $(".custom-file-input").each(function (index) {
            arrFiles.push($(this).attr("name"));
            i++;
        });
        $("#hid_input_files").val(arrFiles);
    }

    $("#btnexportar").click(function () {
        var idlocal = $("#select_local").val();
        if (idlocal == "" || idlocal == undefined) {
            idlocal = -1;
        }
        var finicio = $("#finicio").val();
        if (finicio == "" || finicio == undefined) {
            finicio = -1;
        }
        var ffin = $("#ffin").val();
        if (ffin == "" || ffin == undefined) {
            ffin = -1;
        }
        var montoinicial = $("#montoinicial").val();
        if (montoinicial == "" || montoinicial == undefined) {
            montoinicial = -1;
        }
        var montofinal = $("#montofinal").val();
        if (montofinal == "" || montofinal == undefined) {
            montofinal = -1;
        }
        var montoigual = $("#montoigual").val();
        if (montoigual == "" || montoigual == undefined) {
            montoigual = -1;
        }

        var url = "json_exportarPresupuestos/" + idlocal + "/" + finicio + "/" + ffin + "/" + montoinicial + "/" + montofinal + "/" + montoigual;
        location.href = url;
    });
    $("#btnbuscaropcion").click(function () {
        if ($("#busquedaAvanzada").html() == "") {
            $("#busquedaAvanzada").html("<div class='cargando'></div>").load("get_div_busqueda");
        } else {
            $("#busquedaAvanzada").html("");
        }
    });
    $("#txtbuscar").keypress(function (e) {
        var nombre = $("#txtbuscar").val();
        var formData = new FormData();
        formData.append('nombre', nombre);
        if (e.which == 13) {
            $.ajax({
                url: 'json_buscar',
                type: 'POST',
                contentType: false,
                data: formData,
                processData: false,
                cache: false,
                dataType: "json",
                beforeSend: function () {
                    $("#preloader").css("display", "block");
                    $("#status").css("display", "block");
                },
                success: function (data) {
                    if (data.msj == "") {
                        location.href = "../tarea/lista";
                    } else {
                        $("#mensajeaux").html('<div class="alert alert-danger fade in">' +
                                '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                                data.msj +
                                '</div>');
                    }
                    $("#cargando").remove();
                },
                error: function () {
                    $(".cargando").remove();
                    //alert("No se pudo registrar, se sugiere actualizar la página");
                }
            }).done(function (msg) {

            });
        }
    });
    $('#radioBtn a').on('click', function () {
        var sel = $(this).data('title');
        var tog = $(this).data('toggle');
        $('#' + tog).prop('value', sel);
        $('a[data-toggle="' + tog + '"]').not('[data-title="' + sel + '"]').removeClass('active').addClass('notActive');
        $('a[data-toggle="' + tog + '"][data-title="' + sel + '"]').removeClass('notActive').addClass('active');
    });
    $("#btnCrearTarea").click(function (e) {
        e.preventDefault();
        var msj = "";
        // Recogiendo datos
        var prioridad = $('input:radio[name=rbPrioridad]:checked').val();
        var nombre = $("#txtnombre").val();
        if (nombre == "") {
            msj += "Debe escribir el nombre de la tarea\n";
        }

        var descripcion = $("#txtdescripcion").val();
        var idcategoria = $("#cbocategoria").val();
        if (idcategoria == "") {
            msj += "Debe seleccionar una categoria\n";
        }

        var idlocal = $("#cbolocal").val();
        if ($('#cbolocal').length) {
            if (idlocal == "") {
                msj += "Debe seleccionar un local\n";
            }
        } else {
            idlocal = "";
        }

        var ckadmin = $("#admin").is(':checked');
        var admin = 0;
        if (ckadmin == true) {
            admin = 1;
        }

        if (msj != "") {
            alert(msj);
            return false;
        }
        var formData = new FormData();
        formData.append('nombre', nombre);
        formData.append('prioridad', prioridad);
        formData.append('descripcion', descripcion);
        formData.append('idcategoria', idcategoria);
        formData.append('admin', admin);
        formData.append('idlocal', idlocal);
        $("input[type=file]").each(function (index)
        {
            console.log(index);
            if ($('#file_' + index).length) {
                formData.append('archivo' + index, $('#file_' + index)[0].files[0]);
                $('#file_' + index).remove();
            }

        });
        $.ajax({
            url: 'json_registrar',
            type: 'POST',
            contentType: false,
            data: formData,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $("#panelFormulario").append("<div class='cargando'></div>");
            },
            success: function (data) {
                if (data.msj == "") {
                    location.href = "../tarea/lista";
                } else {
                    $("#mensajeaux").html('<div class="alert alert-danger fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            data.msj +
                            '</div>');
                }
                $(".cargando").remove();
            },
            error: function () {
                $(".cargando").remove();
                //alert("No se pudo registrar, se sugiere actualizar la página");
            }
        }).done(function (msg) {

        });
    });
    $("input:checkbox").click(function (e) {
        var valor = $(this).attr("value");
        var presu = $(this).attr("data-presu");
        var montoActual = $("#totalaprobado").html();
        var montoNuevo = 0;
        if (valor == 1) {
            valor = 0;
            montoNuevo = parseFloat(montoActual) - parseFloat(presu);
        } else {
            valor = 1;
            montoNuevo = parseFloat(presu) + parseFloat(montoActual);
        }
        $("#totalaprobado").html("");
        $("#totalaprobado").html(montoNuevo.toFixed(2));
        $(this).attr("value", valor);
    });
    $("#btnProgramar").click(function (e) {
        e.preventDefault();
        var lunes = $("#lunes").val();
        var martes = $("#martes").val();
        var miercoles = $("#miercoles").val();
        var jueves = $("#jueves").val();
        var viernes = $("#viernes").val();
        var sabado = $("#sabado").val();
        var domingo = $("#domingo").val();
        var ffin = $("#ffin").val();
        var finicio = $("#finicio").val();
        var msj = "";
        var repeticion = $("#cborepeticion").val();
        if (repeticion == "") {
            msj += "Debe seleccionar la repetición\n";
        }


// Recogiendo datos
        var prioridad = $('input:radio[name=rbPrioridad]:checked').val();
        var nombre = $("#txtnombre").val();
        if (nombre == "") {
            msj += "Debe escribir el nombre de la tarea\n";
        }

		var proveedor = $("#txtproveedor").val();
        if (proveedor == "") {
            msj += "Debe escribir el nombre del proveedor\n";
        }
		
		var presupuesto = $("#txtpresupuesto").val();
        if (presupuesto == "") {
            msj += "Debe escribir el monto del presupuesto\n";
        }
		
        var descripcion = $("#txtdescripcion").val();
        var idcategoria = $("#cbocategoria").val();
        if (idcategoria == "") {
            msj += "Debe seleccionar una categoria\n";
        }

        var idlocal = $("#cbolocal").val();
        if ($('#cbolocal').length) {
            if (idlocal == "") {
                msj += "Debe seleccionar un local\n";
            }
        } else {
            idlocal = "";
        }

        var ckadmin = $("#admin").is(':checked');
        var admin = 0;
        if (ckadmin == true) {
            admin = 1;
        }

        if (msj != "") {
            alert(msj);
            return false;
        }
        var formData = new FormData();
        formData.append('nombre', nombre);
        formData.append('prioridad', prioridad);
        formData.append('descripcion', descripcion);
        formData.append('idcategoria', idcategoria);
        formData.append('admin', admin);
		formData.append('presupuesto', presupuesto);
		formData.append('proveedor', proveedor);
        formData.append('idlocal', idlocal);
        formData.append('lunes', lunes);
        formData.append('martes', martes);
        formData.append('miercoles', miercoles);
        formData.append('jueves', jueves);
        formData.append('viernes', viernes);
        formData.append('sabado', sabado);
        formData.append('domingo', domingo);
        formData.append('ffin', ffin);
        formData.append('finicio', finicio);
        formData.append('repeticion', repeticion);
        $("input[type=file]").each(function (index)
        {
            console.log(index);
            if ($('#file_' + index).length) {
                formData.append('archivo' + index, $('#file_' + index)[0].files[0]);
                $('#file_' + index).remove();
            }

        });
        $.ajax({
            url: 'json_programar',
            type: 'POST',
            contentType: false,
            data: formData,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $("#panelFormulario").append("<div class='cargando'></div>");
            },
            success: function (data) {
                if (data.msj == "") { 
                    location.href = "../tarea/listaprogramadas";
                } else {
                    $("#mensajeaux").html('<div class="alert alert-danger fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            data.msj +
                            '</div>');
                }
                $(".cargando").remove();
            },
            error: function () {
                $(".cargando").remove();
                //alert("No se pudo registrar, se sugiere actualizar la página");
            }
        }).done(function (msg) {

        });
    });
    $("#btnCorregirEjecutar").click(function () {
        var accion = $(this).attr("data-id");
        var descripcion = $("#txtdescripcion").val();
        var proveedor = $("#txtproveedor").val();
        var presupuesto = $("#txtpresupuesto").val();
        var idtarea = $("#txtidtarea").val();
        var formData = new FormData();
        formData.append('descripcion', descripcion);
        formData.append('accion', accion);
        formData.append('idtarea', idtarea);
        formData.append('proveedor', proveedor);
        formData.append('presupuesto', presupuesto);
        $("input[type=file]").each(function (index)
        {
            console.log(index);
            if ($('#file_' + index).length) {
                formData.append('archivo' + index, $('#file_' + index)[0].files[0]);
                $('#file_' + index).remove();
            }

        });
        $.ajax({
            url: '../json_corregir_ejecutar',
            type: 'POST',
            contentType: false,
            data: formData,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $("#panelFormulario").append("<div class='cargando'></div>");
            },
            success: function (data) {
                if (data.msj == "") {
                    location.href = "../tarea/lista";
                } else {
                    $("#mensajeaux").html('<div class="alert alert-danger fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            data.msj +
                            '</div>');
                    $(".cargando").remove();
                }
                $(".cargando").remove();
            },
            error: function () {
                $(".cargando").remove();
                //alert("No se pudo registrar, se sugiere actualizar la página");
            }
        }).done(function (msg) {

        });
    });
    $("#btnEditarProgramar").click(function (e) {
        e.preventDefault();
        var lunes = $("#lunes").val();
        var martes = $("#martes").val();
        var miercoles = $("#miercoles").val();
        var jueves = $("#jueves").val();
        var viernes = $("#viernes").val();
        var sabado = $("#sabado").val();
        var domingo = $("#domingo").val();
        var ffin = $("#ffin").val();
        var finicio = $("#finicio").val();
        var msj = "";
        var repeticion = $("#cborepeticion").val();
        if (repeticion == "") {
            msj += "Debe seleccionar la repetición\n";
        }


// Recogiendo datos
        var prioridad = $('input:radio[name=rbPrioridad]:checked').val();
        var nombre = $("#txtnombre").val();
        if (nombre == "") {
            msj += "Debe escribir el nombre de la tarea\n";
        }
		
		var proveedor = $("#txtproveedor").val();
        if (proveedor == "") {
            msj += "Debe escribir el nombre del proveedor\n";
        }
		
		var presupuesto = $("#txtpresupuesto").val();
        if (presupuesto == "") {
            msj += "Debe escribir el monto del presupuesto\n";
        }

        var descripcion = $("#txtdescripcion").val();
        var idcategoria = $("#cbocategoria").val();
        if (idcategoria == "") {
            msj += "Debe seleccionar una categoria\n";
        }

        var idlocal = $("#cbolocal").val();
        if ($('#cbolocal').length) {
            if (idlocal == "") {
                msj += "Debe seleccionar un local\n";
            }
        } else {
            idlocal = "";
        }

        var ckadmin = $("#admin").is(':checked');
        var admin = 0;
        if (ckadmin == true) {
            admin = 1;
        }

        if (msj != "") {
            alert(msj);
            return false;
        }
        var formData = new FormData();
        formData.append('nombre', nombre);
        formData.append('prioridad', prioridad);
        formData.append('descripcion', descripcion);
		formData.append('presupuesto', presupuesto);
		formData.append('proveedor', proveedor);
        formData.append('idcategoria', idcategoria);
        formData.append('admin', admin);
        formData.append('idlocal', idlocal);
        formData.append('lunes', lunes);
        formData.append('martes', martes);
        formData.append('miercoles', miercoles);
        formData.append('jueves', jueves);
        formData.append('viernes', viernes);
        formData.append('sabado', sabado);
        formData.append('domingo', domingo);
        formData.append('ffin', ffin);
        formData.append('finicio', finicio);
        formData.append('repeticion', repeticion);
        $("input[type=file]").each(function (index)
        {
            console.log(index);
            if ($('#file_' + index).length) {
                formData.append('archivo' + index, $('#file_' + index)[0].files[0]);
                $('#file_' + index).remove();
            }

        });
        $.ajax({
            url: 'json_programar',
            type: 'POST',
            contentType: false,
            data: formData,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $("#panelFormulario").append("<div class='cargando'></div>");
            },
            success: function (data) {
                if (data.msj == "") {
                    location.href = "../tarea/listaprogramadas";
                } else {
                    $("#mensajeaux").html('<div class="alert alert-danger fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            data.msj +
                            '</div>');
                }
                $(".cargando").remove();
            },
            error: function () {
                $(".cargando").remove();
                //alert("No se pudo registrar, se sugiere actualizar la página");
            }
        }).done(function (msg) {

        });
    });
    $("#btnEditarTarea").click(function () {
        var msj = "";
        // Recogiendo datos
        var prioridad = $('input:radio[name=rbPrioridad]:checked').val();
        var idtarea = $("#txtidtarea").val();
        var proveedor = $("#txtproveedor").val();
        var presupuesto = $("#txtpresupuesto").val();
        var nombre = $("#txtnombre").val();
        if (nombre == "") {
            msj += "Debe escribir el nombre de la tarea\n";
        }

        var descripcion = $("#txtdescripcion").val();
        var idcategoria = $("#cbocategoria").val();
        if (idcategoria == "") {
            msj += "Debe seleccionar una categoria\n";
        }

        var idlocal = $("#cbolocal").val();
        if ($('#cbolocal').length) {
            if (idlocal == "") {
                msj += "Debe seleccionar un local\n";
            }
        } else {
            idlocal = "";
        }

        var ckadmin = $("#admin").is(':checked');
        var admin = 0;
        if (ckadmin == true) {
            admin = 1;
        }

        if (msj != "") {
            alert(msj);
            return false;
        }
        var formData = new FormData();
        formData.append('nombre', nombre);
        formData.append('prioridad', prioridad);
        formData.append('presupuesto', presupuesto);
        formData.append('proveedor', proveedor);
        formData.append('descripcion', descripcion);
        formData.append('idcategoria', idcategoria);
        formData.append('admin', admin);
        formData.append('idtarea', idtarea);
        formData.append('idlocal', idlocal);
        $("input[type=file]").each(function (index)
        {
            console.log(index);
            if ($('#file_' + index).length) {
                formData.append('archivo' + index, $('#file_' + index)[0].files[0]);
                $('#file_' + index).remove();
            }

        });
        $.ajax({
            url: '../json_editar',
            type: 'POST',
            contentType: false,
            data: formData,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $("#panelFormulario").append("<div class='cargando'></div>");
            },
            success: function (data) {
                if (data.msj == "") {
                    location.href = "../tarea/lista";
                } else {
                    $("#mensajeaux").html('<div class="alert alert-danger fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            data.msj +
                            '</div>');
                }
                $(".cargando").remove();
            },
            error: function () {
                $(".cargando").remove();
                //alert("No se pudo registrar, se sugiere actualizar la página");
            }
        }).done(function (msg) {

        });
    });
    $("#btnCorregirTarea").click(function () {
        var msj = "";
        // Recogiendo datos
        var prioridad = $('input:radio[name=rbPrioridad]:checked').val();
        var idtarea = $("#txtidtarea").val();
        var proveedor = $("#txtproveedor").val();
        var presupuesto = $("#txtpresupuesto").val();
        var nombre = $("#txtnombre").val();
        if (nombre == "") {
            msj += "Debe escribir el nombre de la tarea\n";
        }

        var descripcion = $("#txtdescripcion").val();
        var idcategoria = $("#cbocategoria").val();
        if (idcategoria == "") {
            msj += "Debe seleccionar una categoria\n";
        }

        var idlocal = $("#cbolocal").val();
        if ($('#cbolocal').length) {
            if (idlocal == "") {
                msj += "Debe seleccionar un local\n";
            }
        } else {
            idlocal = "";
        }

        var ckadmin = $("#admin").is(':checked');
        var admin = 0;
        if (ckadmin == true) {
            admin = 1;
        }

        if (msj != "") {
            alert(msj);
            return false;
        }
        var formData = new FormData();
        formData.append('nombre', nombre);
        formData.append('prioridad', prioridad);
        formData.append('presupuesto', presupuesto);
        formData.append('proveedor', proveedor);
        formData.append('descripcion', descripcion);
        formData.append('idcategoria', idcategoria);
        formData.append('admin', admin);
        formData.append('idtarea', idtarea);
        formData.append('idlocal', idlocal);
        $("input[type=file]").each(function (index)
        {
            console.log(index);
            if ($('#file_' + index).length) {
                formData.append('archivo' + index, $('#file_' + index)[0].files[0]);
                $('#file_' + index).remove();
            }

        });
        $.ajax({
            url: '../json_corregir_tarea',
            type: 'POST',
            contentType: false,
            data: formData,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $("#panelFormulario").append("<div class='cargando'></div>");
            },
            success: function (data) {
                if (data.msj == "") {
                    location.href = "../tarea/lista";
                } else {
                    $("#mensajeaux").html('<div class="alert alert-danger fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            data.msj +
                            '</div>');
                }
                $(".cargando").remove();
            },
            error: function () {
                $(".cargando").remove();
                //alert("No se pudo registrar, se sugiere actualizar la página");
            }
        }).done(function (msg) {

        });
    });
    $("#btnPorAtender").click(function () {
        var presupuesto = $("#txtpresupuesto").val();
        if (presupuesto == "") {
            presupuesto = 0;
        }
        var monto_minimo = $("#monto_minimo").val();
        var accion = $(this).attr("data-id");
        var paraadmin = $("#param_paraadmin").val();
        var idrol = $("#rol").val();
        if (idrol == 1 && paraadmin == 1) {
            accion = 1;
            Poratender(accion);
            return false;
        }
        if (parseFloat(presupuesto) <= parseFloat(monto_minimo)) {
            var dialog = $('<p>¿Deseas preparar presupuesto para aprobación?</p>').dialog({
                buttons: {
                    "Si, deseo preparar ppto": function () {

                        accion = 2;
                        //alert("2");
                        Poratender(accion);
                    },
                    "No, deseo iniciar tarea": function () {
                        accion = 1;
                        //alert("1");
                        Poratender(accion);
                    },
                    "Cancelar": function () {
                        $(this).remove();
                        return false;
                    }
                }
            });
        } else {
            accion = 2;
            Poratender(accion);
            //alert("Pasas defrente a evaluar presupuesto");
        }

    });
    function Poratender(accion) {
        var descripcion = $("#txtdescripcion").val();
        var proveedor = $("#txtproveedor").val();
        var presupuesto = $("#txtpresupuesto").val();
        var idtarea = $("#txtidtarea").val();
        var msj = "";
        if (accion == 2) {
//            if (presupuesto == "" || proveedor == "" || presupuesto == "0") {
//                msj = "Debe escrbir datos del pesupuesto";
//            }
        }
        if (msj != "") {
            alert(msj);
            return false;
        }


        var formData = new FormData();
        formData.append('descripcion', descripcion);
        formData.append('accion', accion);
        formData.append('proveedor', proveedor);
        formData.append('presupuesto', presupuesto);
        formData.append('idtarea', idtarea);
        $("input[type=file]").each(function (index)
        {
            console.log($(this).attr("id"));
            if ($('#file_' + index).length) {
                formData.append('archivo' + index, $('#file_' + index)[0].files[0]);
                $('#file_' + index).remove();
            }

        });
        $.ajax({
            url: '../json_por_atender',
            type: 'POST',
            contentType: false,
            data: formData,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $("#panelFormulario").append("<div class='cargando'></div>");
            },
            success: function (data) {
                if (data.msj == "") {
                    location.href = "../tarea/lista";
                } else {
                    $("#mensajeaux").html('<div class="alert alert-danger fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            data.msj +
                            '</div>');
                }
                $(".cargando").remove();
            },
            error: function () {
                $(".cargando").remove();
                //alert("No se pudo registrar, se sugiere actualizar la página");
            }
        }).done(function (msg) {

        });
    }

    $("#btnRechazar, #btnPorAtenderCancelar, #btnPorEvaluar").click(function () {
        var accion = $(this).attr("data-id");
        Poratender(accion);
    });
    $("#btnCorregirPorEvaluar, #btnCorregirRechazar").click(function () {
        var accion = $(this).attr("data-id");
        var descripcion = $("#txtdescripcion").val();
        var proveedor = $("#txtproveedor").val();
        var presupuesto = $("#txtpresupuesto").val();
        var idtarea = $("#txtidtarea").val();
        var msj = "";
        if (accion == 2) {
            if (presupuesto == "" || proveedor == "" || presupuesto == "0") {
                msj = "Debe escrbir datos del pesupuesto";
            }
        }
        if (msj != "") {
            alert(msj);
            return false;
        }
        var formData = new FormData();
        formData.append('descripcion', descripcion);
        formData.append('accion', accion);
        formData.append('proveedor', proveedor);
        formData.append('presupuesto', presupuesto);
        formData.append('idtarea', idtarea);
        $("input[type=file]").each(function (index)
        {
            console.log($(this).attr("id"));
            if ($('#file_' + index).length) {
                formData.append('archivo' + index, $('#file_' + index)[0].files[0]);
                $('#file_' + index).remove();
            }

        });
        $.ajax({
            url: '../json_por_corregirppto',
            type: 'POST',
            contentType: false,
            data: formData,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $("#panelFormulario").append("<div class='cargando'></div>");
            },
            success: function (data) {
                if (data.msj == "") {
                    location.href = "../tarea/lista";
                } else {
                    $("#mensajeaux").html('<div class="alert alert-danger fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            data.msj +
                            '</div>');
                }
                $(".cargando").remove();
            },
            error: function () {
                $(".cargando").remove();
                //alert("No se pudo registrar, se sugiere actualizar la página");
            }
        }).done(function (msg) {

        });
    });
    $("#btnEjecutar, #btnRechazarEje").click(function () {
        var accion = $(this).attr("data-id");
        var descripcion = $("#txtdescripcion").val();
        var proveedor = $("#txtproveedor").val();
        var presupuesto = $("#txtpresupuesto").val();
        var idtarea = $("#txtidtarea").val();
        var formData = new FormData();
        formData.append('descripcion', descripcion);
        formData.append('accion', accion);
        formData.append('idtarea', idtarea);
        formData.append('proveedor', proveedor);
        formData.append('presupuesto', presupuesto);
        $("input[type=file]").each(function (index)
        {
            console.log(index);
            if ($('#file_' + index).length) {
                formData.append('archivo' + index, $('#file_' + index)[0].files[0]);
                $('#file_' + index).remove();
            }

        });
        $.ajax({
            url: '../json_ejecutar',
            type: 'POST',
            contentType: false,
            data: formData,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $("#panelFormulario").append("<div class='cargando'></div>");
            },
            success: function (data) {
                if (data.msj == "") {
                    location.href = "../tarea/lista";
                } else {
                    $("#mensajeaux").html('<div class="alert alert-danger fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            data.msj +
                            '</div>');
                }
                $(".cargando").remove();
            },
            error: function () {
                $(".cargando").remove();
                //alert("No se pudo registrar, se sugiere actualizar la página");
            }
        }).done(function (msg) {

        });
    });
    function Evaluar() {
        var idgrupo = $("#idgrupo").val();
        var ids = "";
        var monto = $("#totalaprobado").html();
        $("input:checkbox:checked").each(function () {
            ids += $(this).attr("data-id") + ",";
        });
        $("#ids").val(ids);
        var formData = new FormData();
        formData.append('ids', ids);
        formData.append('idgrupo', idgrupo);
        formData.append('monto', monto);
        $.ajax({
            url: '../json_evaluar',
            type: 'POST',
            contentType: false,
            data: formData,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $("#panelFormulario").append("<div class='cargando'></div>");
            },
            success: function (data) {
                if (data.msj == "") {
                    location.href = "../listapresu";
                } else {
                    $("#mensajeaux").html('<div class="alert alert-danger fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            data.msj +
                            '</div>');
                }
                $(".cargando").remove();
            },
            error: function () {
                $(".cargando").remove();
                //alert("No se pudo registrar, se sugiere actualizar la página");
            }
        }).done(function (msg) {

        });
    }
    $("#btnEvaluarAprob").click(function () {

        var monto = $("#totalaprobado").html();
        monto = parseFloat(monto);
        var dialog = $('<p>Se procederá a aprobar presupuesto de S/. <strong>' + monto.toFixed(2) + '</strong> <br> Los presupuestos no seleccionados irán al ejecutor de la tarea para corregir el presupuesto<br><br> ¿Desea continuar?</p>').dialog({
            buttons: {
                "Si": function () {
                    Evaluar();
                    $(this).remove();
                },
                "No": function () {
                    $(this).remove();
                    return false;
                }
            }
        });
    });
    $(document).on("keydown keyup", ".inputpresu", function (e) {
        SumarPresupuestos();
    });
    function SumarPresupuestos() {
        var sum = 0;
        var textpresu = "";
        //iterate through each textboxes and add the values
        $(".form-control.inputpresu").each(function () {
            //add only if the value is number
            if (!isNaN(this.value) && this.value.length != 0) {
                sum += parseFloat(this.value);
                textpresu += this.value + ",";
                $(this).css("background-color", "#FEFFB0");
                console.log(sum);
            } else if (this.value.length != 0) {
                textpresu += "0" + ",";
                $(this).css("background-color", "red");
            } else if (this.value == "") {
                textpresu += "0" + ",";
            }
        });
        $("#totalgrupo").html(sum.toFixed(2));
        $("#presuTareas").val(textpresu);
    }

    $("#btnEnviappto").click(function (e) {
        e.preventDefault();
        var ids = "";
        var presus = "";
        var html = '<form id="frmDatos"><div class="row"><div class="col-md-10"><span class="cabeceragrupo">TAREA</span></div><div class="col-md-2"><span class="cabeceragrupo">PRESUPUESTO</span></div></div><div class="divisorgrupo"></div>';
        var montoTotal = 0;
        $("input:checkbox:checked").each(function () {
            ids += $(this).attr("data-id") + ",";
            presus += $(this).attr("data-presu") + ",";
            html += '<div class="row"><div class="col-md-10" style="line-height: 32px;"><strong>' + $(this).attr("data-codigo") + '</strong>- ' + $(this).attr("data-nombre") + '</div><div class="col-md-2"><input type="text" class="form-control inputpresu" value="' + $(this).attr("data-presu") + '"></div></div><div class="divisorgrupo"></div>';
            montoTotal = parseFloat($(this).attr("data-presu")) + parseFloat(montoTotal);
        });
        html += '<div class="row"><div class="col-md-10"></div><div class="col-md-2">S/. <span class="totalgrupo" id="totalgrupo">' + montoTotal + '</span></div></div></form>';
        html += '<div class="divisorgrupo"></div><br><div class="row"><div class="col-md-12"><textarea id="txtdescripcion" class="form-control" rows="3" placeholder="Escribir comentario"></textarea></div>';
        if (ids != "") {
            $('#myModal').modal('show');
        } else {
            alert("No hay tareas seleccionadas para crear el grupo");
            return false;
        }

        $("#idsTareas").val(ids);
        $("#presuTareas").val(presus);
        $("#panelentrada").html(html);
    });
    $("#btnEnviarPresu").click(function () {
        var ids = $("#idsTareas").val();
        var presus = $("#presuTareas").val();
        var descripcion = $("#txtdescripcion").val();
        var nomGrupo = $("#nomGrupo").val();
        if (nomGrupo == "") {
            alert("Debe ingresar un nombre al grupo");
            return false;
        }
        var formData = new FormData();
        formData.append('nombre', nomGrupo);
        formData.append('descripcion', descripcion);
        formData.append('ids', ids);
        formData.append('presus', presus);
        $.ajax({
            url: 'json_por_enviarpresu',
            type: 'POST',
            contentType: false,
            data: formData,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $("#panelFormulario").append("<div class='cargando'></div>");
            },
            success: function (data) {
                if (data.msj == "") {
                    location.href = "../tarea/lista";
                } else {
                    $("#mensajeaux").html('<div class="alert alert-danger fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            data.msj +
                            '</div>');
                }
                $(".cargando").remove();
            },
            error: function () {
                $(".cargando").remove();
            }
        }).done(function (msg) {

        });
    });
    $("#btnConforme, #btnNoConforme").click(function () {

        var accion = $(this).attr("data-id");
        var descripcion = $("#txtdescripcion").val();
        var idtarea = $("#txtidtarea").val();
        var formData = new FormData();
        formData.append('descripcion', descripcion);
        formData.append('accion', accion);
        formData.append('idtarea', idtarea);
        $("input[type=file]").each(function (index)
        {
            console.log(index);
            if ($('#file_' + index).length) {
                formData.append('archivo' + index, $('#file_' + index)[0].files[0]);
                $('#file_' + index).remove();
            }

        });
        $.ajax({
            url: '../json_confirmar',
            type: 'POST',
            contentType: false,
            data: formData,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $("#panelFormulario").append("<div class='cargando'></div>");
            },
            success: function (data) {
                if (data.msj == "") {
                    location.href = "../tarea/lista";
                } else {
                    $("#mensajeaux").html('<div class="alert alert-danger fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            data.msj +
                            '</div>');
                }
                $(".cargando").remove();
            },
            error: function () {
                $(".cargando").remove();
            }
        }).done(function (msg) {

        });
    });
});
 