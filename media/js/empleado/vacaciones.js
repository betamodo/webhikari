$(function(){
    $('input.datepicker').datepicker();

    // Enviar datos
    $("button#btnGuardarVacaciones").click(function(event) {
        var datos = $('div#datos-enviar input').serialize();
        if (validarFechas($('#fecha-inicio').val(), $('#fecha-fin').val())) {
            //Fechas son validas
            $.ajax({
                url: '../vacaciones_json',
                type: 'POST',
                dataType: 'json',
                data: datos,
                beforeSend: function () {
                    $("#panelFormulario").append("<div id='cargador' class='cargando'><div>");
                },
                success: function (data) {
                    if (data.msj == "") {
                        location.href = "../../empleado/lista";
                    } else {
                        $("#mensajeaux").html('<div class="alert alert-danger fade in">' +
                                '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                                '<strong></strong> ' + data.msj +
                                '</div>');

                    }
                }
            })
            .always(function() {
                console.log("complete");
                $("div#cargador").remove();
            });
        };
    });

    /**
     * Valida si la fecha menor es en realidad menor a la mayor. Formato "dd/mm/yy"
     * 
     * @param  string menor
     * @param  string mayor
     * @return boolean
     */
    function validarFechas(menor, mayor) {
        if (menor != "" && mayor != "") {
            var min = $.datepicker.parseDate( "dd/mm/yy", menor ), max = $.datepicker.parseDate( "dd/mm/yy", mayor );
            if (min >= max) return false;

            return true;
        } else {
            return false;
        }
    }
});