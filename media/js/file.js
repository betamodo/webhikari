$(document).ready(function () {
	/*Editar*/
	$("#btnEditarfile").click(function (e) {
        e.preventDefault();

        var msj = "";
        // Recogiendo datos
        var idfile = $("#txtidfile").val();

        var codigo = $("#txtnombre").val();
        if (codigo == "") {
            msj += "Debe escribir el nombre\n";
        }

        var estado = $("#cboestado").val();
        if (estado == "") {
            msj += "Debe seleccionar el estado\n";
        }

        if (msj != "") {
            alert(msj);
            return false;
        }
		var descripcion=$("#txtdescripcion").val();

        var formData = new FormData();
        formData.append('idfile', idfile);
        formData.append('codigo', codigo);        
        formData.append('estado', estado);
		formData.append('descripcion', descripcion);

        $.ajax({
            url: '../json_editar_file',
            type: 'POST',
            contentType: false,
            data: formData,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $("#panelFormulario").append("<div id='cargador' class='cargando'><div>");
            },
            success: function (data) {
                if (data.msj == "") {
                    location.href = "../../file/lista";
                } else {
                    $("#cargador").remove();
                    $("#mensajeaux").html('<div class="alert alert-danger fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            '<strong></strong> ' + data.msj +
                            '</div>');

                }
            },
            error: function () {
                $("#cargador").remove();                
            }
        }).done(function (msg) {

        });
    });
	
	
	$("#btnCrearfile").click(function (e) {
        e.preventDefault();

        var msj = "";
        // Recogiendo datos
        var codigo = $("#txtnombre").val();
        if (codigo == "") {
            msj += "Debe escribir el nombre\n";
        }

        var estado = $("#cboestado").val();
        if (estado == "") {
            msj += "Debe seleccionar el estado\n";
        }

        if (msj != "") {
            alert(msj);
            return false;
        }
		var descripcion=$("#txtdescripcion").val();
        var formData = new FormData();
        formData.append('codigo', codigo);
        formData.append('estado', estado);
		formData.append('descripcion', descripcion);
        $.ajax({
            url: 'json_crear_file',
            type: 'POST',
            contentType: false,
            data: formData,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $("#panelFormulario").append("<div id='cargando' class='cargando'><div>");
            },
            success: function (data) {
                if (data.msj == "") {
                    location.href = "../file/lista";
                } else {
                    $("#mensajeaux").html('<div class="alert alert-error fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            '<strong></strong> ' + data.msj +
                            '</div>');
                    $("#cargador").remove();
                }

            },
            error: function () {
                $("#cargador").remove();
            }
        }).done(function (msg) {

        });
    });
	
	
});