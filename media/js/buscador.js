$(document).ready(function () {

    $(document).on("change", "#cbocampos_filtro", function () {
        var tipo = $('#cbocampos_filtro option:selected').attr("data-tipo");
        $("#divvalor").html("");
        $("#divvalor").html("<div class='cargando'></div>").load("get_div_valor/" + tipo);
    });
    $(document).on("click", ".delete_campo", function () {
        var campo = $(this).attr("data-id");
        $("#campo_" + campo).remove();
    });
    $("#btnAgregar_filtro").click(function (e) {
        var campo = $("#cbocampos_filtro").val();
        var valor = $("#txtbuscador_filtro").val();
        var control = $('#cbocampos_filtro option:selected').attr("data-control");
        var valorlabel = valor;
        if (control == "texto") {
            valorlabel = valor;
        } else if (control == "combo") {
            valorlabel = $('#txtbuscador_filtro option:selected').text();
        } else if (control == "fecha") {
            var inicio = $("#finicio").val();
            var aInicio = inicio.split("/");
            var finicio = aInicio[2] + "-" + aInicio[1] + "-" + aInicio[0];
            var fin = $("#ffin").val();
            var afin = fin.split("/");
            var ffin = afin[2] + "-" + afin[1] + "-" + afin[0];
            valor = finicio + "@" + ffin;
            valorlabel = "Del " + $("#finicio").val() + " hasta " + $("#ffin").val();
        }
        if (valor == "") {
            alert("Debe ingresar un valor de búsqueda");
            return false;
        }
        var label = $('#cbocampos_filtro option:selected').text();
        valor = valor.replace('"', "'");
        valor = valor.replace('"', "'");
        if ($('#campo_' + campo).length > 0) {
            alert("Ya existe");
        } else {
            var html = '<span class="campo" data-control="' + control + '" id="campo_' + campo + '" data-value="' + valor + '" data-campo="' + campo + '">' +
                    '<span class="delete_campo" data-id="' + campo + '" >X</span> | ' +
                    '<strong>' + label + '</strong>: ' + valorlabel +
                    '</span>';
            $("#criterios").append(html);
        }

    });
    $("#btnBuscar_filtro").click(function () {
        var where = "";
        $(".campo").each(function (index) {
            if ($(this).attr("data-control") == "fecha") {
                var aFechas = $(this).attr("data-value").split("@");
                where += " AND date(d." + $(this).attr("data-campo") + ") BETWEEN '" + aFechas[0] + "' and '" + aFechas[1] + "' ";
            } else if ($(this).attr("data-control") == "texto") {
                where += " and d." + $(this).attr("data-campo") + " like '%" + $(this).attr("data-value") + "%'";
            } else if ($(this).attr("data-control") == "combo") {
                where += " and d." + $(this).attr("data-campo") + " = " + $(this).attr("data-value");
            }
        });
        var htmlbusqueda = $("#criterios").html();
        if (where != false) {
            $.post("json_buscar", {where: where, filtro: htmlbusqueda}, function (data) {
                if (data == "session") {
                    location.href = "../../hikari";
                } else {
                    $("#listaTareas").html(data);
                }

            });
        }

    });
});

$(document).ready(function () {
    $("#btnBuscar_filtro").trigger("click");
});
