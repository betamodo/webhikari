$(document).ready(function () {
	/*Editar*/
	$("#btnEditartipodoc").click(function (e) {
        e.preventDefault();

        var msj = "";
        // Recogiendo datos
        var idtipodoc = $("#txtidtipodoc").val();

        var nombre = $("#txtnombre").val();
        if (nombre == "") {
            msj += "Debe escribir el nombre\n";
        }

        var estado = $("#cboestado").val();
        if (estado == "") {
            msj += "Debe seleccionar el estado\n";
        }

        if (msj != "") {
            alert(msj);
            return false;
        }

        var formData = new FormData();
        formData.append('idtipodoc', idtipodoc);
        formData.append('nombre', nombre);        
        formData.append('estado', estado);

        $.ajax({
            url: '../json_editar_tipodoc',
            type: 'POST',
            contentType: false,
            data: formData,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $("#panelFormulario").append("<div id='cargador' class='cargando'><div>");
            },
            success: function (data) {
                if (data.msj == "") {
                    location.href = "../../tipodoc/lista";
                } else {
                    $("#cargador").remove();
                    $("#mensajeaux").html('<div class="alert alert-danger fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            '<strong></strong> ' + data.msj +
                            '</div>');

                }
            },
            error: function () {
                $("#cargador").remove();                
            }
        }).done(function (msg) {

        });
    });
	
	
	$("#btnCreartipodoc").click(function (e) {
        e.preventDefault();

        var msj = "";
        // Recogiendo datos
        var nombre = $("#txtnombre").val();
        if (nombre == "") {
            msj += "Debe escribir el nombre\n";
        }

        var estado = $("#cboestado").val();
        if (estado == "") {
            msj += "Debe seleccionar el estado\n";
        }

        if (msj != "") {
            alert(msj);
            return false;
        }

        var formData = new FormData();
        formData.append('nombre', nombre);
        formData.append('estado', estado);

        $.ajax({
            url: 'json_crear_tipodoc',
            type: 'POST',
            contentType: false,
            data: formData,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $("#panelFormulario").append("<div id='cargando' class='cargando'><div>");
            },
            success: function (data) {
                if (data.msj == "") {
                    location.href = "../tipodoc/lista";
                } else {
                    $("#mensajeaux").html('<div class="alert alert-error fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            '<strong></strong> ' + data.msj +
                            '</div>');
                    $("#cargador").remove();
                }

            },
            error: function () {
                $("#cargador").remove();
            }
        }).done(function (msg) {

        });
    });
	
	
});