$(document).ready(function () {
	var nro_files = 0;
	 $(document).on("click", "#btnAgregarArchivo", function () {
        nro_files++;
        var newhtml = '<br id="br_' + nro_files + '"><div id="remove_' + nro_files + '" style="margin-right: 4px;" class="btn btn-sm btn-default btn_remover_files" data-id="' + nro_files + '"><span data-id="file_' + nro_files + '" class="glyphicon glyphicon-minus-sign btn_remover_files"></span></div>';
        newhtml += '<input type="file" name="file_' + nro_files + '" id="file_' + nro_files + '" class="custom-file-input">';
        $('#subir2').append(newhtml);
        contarInputFiles();
    });
	function contarInputFiles() {
        arrFiles = [];
        var i = 0;
        $(".custom-file-input").each(function (index) {
            arrFiles.push($(this).attr("name"));
            i++;
        });
        $("#hid_input_files").val(arrFiles);
    }
	
	$(document).on("click", "#btnmeterbox", function () {
		var ids="";
		var msj="";
		var idlugar=$("#cboalmacenbox").val();
		if(idlugar==""){
			msj += "Debe seleccionar un almacén \n";;
		}
		var box =$("#txtboxbox").val();
		if(box==""){
			msj += "Debe escribir el número de box \n";
		}
		if(msj!=""){
			alert(msj);
			return false;
		}
		
		$("input:checkbox:checked").each(function () {
            ids += $(this).attr("data-id") + ",";
        });
		 $.post("json_mover_documento", {ids: ids, idlugar: idlugar,  box: box}, function (data) {            
        });				
	});
	
	$(document).on("click", "input:checkbox", function (e) {
	    var valor = $(this).attr("value");
        if (valor == 1) {
            valor = 0;
        } else {
            valor = 1;
        }
        $(this).attr("value", valor);
    });
	
	$(document).on("click", ".btnaddbox", function () {
		var msj="";
		var ids="";
		$("input:checkbox:checked").each(function () {
            ids += $(this).attr("data-id") + ",";			
        }); 
		if(msj!=""){
			alert(msj);
		}else{
			$("#contenido_box").html("<div class='cargando'></div>").load("get_div_box");
			$('#dlgbox').modal('show');
		}
		
	});
    $(document).on("click", ".derivar", function () {
        var id = $(this).attr("data-id");
        $("#contenido_modal").html("");
        $("#contenido_modal").html("<div class='cargando'></div>").load("get_div_derivar/" + id);
        $('#myModal').modal('show');
    });
    $(document).on("click", "#btnDerivardoc", function () {
        var idcategoria = $("#cbocategoria").val();
        var descripcion = $("#descripcion").val();
        var iddoc = $("#iddoc").val();
        var formData = new FormData();
        formData.append('iddoc', iddoc);
        formData.append('descripcion', descripcion);
        formData.append('idcategoria', idcategoria);
        $.ajax({
            url: 'json_derivar_documento',
            type: 'POST',
            contentType: false,
            data: formData,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $("#footer_modal").append("<div id='cargando' class='cargando'></div>");
            },
            success: function (data) {
                if (data.msj == "") {
                    location.href = "../documento/lista";
                } else {
                    $("#mensajeaux").append('<div class="alert alert-danger fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            data.msj +
                            '</div>');
                    $("#cargando").remove();
                }
                $("#cargador").remove();

            },
            error: function () {
                $("#cargador").remove();
            }
        }).done(function (msg) {

        });
    });

    $("#cboTipo").change(function (e) {
        e.preventDefault();

        if ($(this).val() == 1) {
            document.getElementById("txtbox").placeholder = "Código del STANTD";
            $("#txtbox").prop("disabled", false);
        } else if ($(this).val() == 2) {
            document.getElementById("txtbox").placeholder = "Código del BOX";
            $("#txtbox").prop("disabled", false);
        } else {
            document.getElementById("txtbox").placeholder = "";
            $("#txtbox").val("");
            $("#txtbox").prop("disabled", true);
        }

    });

    $("#btnTransportar").click(function (e) {
        e.preventDefault();
        var msj = "";
        // Recogiendo datos

        var fecha_envio = $("#ffin").val();
        if (fecha_envio == "") {
            msj += "Debe seleccionar una fecha de envío\n";
        }

        var modalidad = $("#cbomodalidad").val();
        if (modalidad == "") {
            msj += "Debe escribir la modalidad de envío\n";
        }

        var descripcion = $("#txtdescripcion").val();
        var responsable = $("#txtresponsable").val();
        var iddocumento = $("#txtid").val();
        var idlugar = $("#cboLugar").val();
        var formData = new FormData();
        formData.append('responsable', responsable);
        formData.append('fecha_envio', fecha_envio);
        formData.append('iddocumento', iddocumento);
        formData.append('idlugar', idlugar);
        formData.append('modalidad', modalidad);
        formData.append('descripcion', descripcion);
        var cont = 0;
        $("input[type=file]").each(function (index)
        {
            console.log(index);
            if ($('#file_' + index).length) {
                formData.append('archivo' + index, $('#file_' + index)[0].files[0]);
                cont++;
            }
        });

        if (msj != "") {
            alert(msj);
            return false;
        }
        $.ajax({
            url: '../json_transportar_documento',
            type: 'POST',
            contentType: false,
            data: formData,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $("#panelFormulario").append("<div id='cargando' class='cargando'></div>");
            },
            success: function (data) {
                if (data.msj == "") {
                    location.href = "../documento/lista";
                } else {
                    $("#mensajeaux").append('<div class="alert alert-danger fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            data.msj +
                            '</div>');
                    $("#cargando").remove();
                }
                $("#cargador").remove();

            },
            error: function () {
                $("#cargador").remove();
                $("#files_list").html("");
                //alert("No se pudo registrar, se sugiere actualizar la página");
            }
        }).done(function (msg) {

        });
    });

    $("#btnCrearDoc").click(function (e) {
       $("#panelFormulario").append("<div id='cargador' class='cargando'></div>");
	    var msj = "";

        var identificador = $("#txtidentificador").val();
        var finicio = $("#finicio").val();
        if (finicio == "") {
            msj += "Debe seleccionar la fecha de recepción\n";
        }

        var origen = $("#txtorigen").val();
        if (origen == "") {
            msj += "Debe escribir el nombre\n";
        }

        var idcategoria = $("#cbocategoria").val();
        if (idcategoria == "") {
            msj += "Debe seleccionar el área destinada\n";
        }

        var asunto = $("#txtasunto").val();
        if (asunto == "") {
            msj += "Debe escribir el asunto\n";
        }

        var idtipodoc = $("#cbotipodoc").val();
        if (idtipodoc == "") {
            msj += "Debe seleccionar el tipo de documento\n";
        }
		/*------ BORRAR DESPUES DE MIGRA --------------*/
		var idfile = $("#cbofile").val();
        if (idfile == "") {
            msj += "Debe seleccionar un file\n";
        }
		
		var idstand = $("#cbostand").val();
        if (idstand == "") {
            msj += "Debe seleccionar un stand\n";
        }
				
		var idalmacen = $("#cboLugar").val();
        if (idalmacen == "") {
            msj += "Debe seleccionar un almacén\n";
        }
		if (msj != "") {
			alert(msj);
			$("#cargador").css("display","none");
			$("#cargador").remove();           
            return false;
			e.preventDefault();
			
        }
    });

    $("#btnBuscarDoc").click(function () {
        if ($("#busquedaAvanzada").html() == "") {
            $("#busquedaAvanzada").html("<div class='cargando'></div>").load("get_div_busqueda");
        } else {
            $("#busquedaAvanzada").html("");
        }
    });

    $("#btnAlmacenaDeriva, #btnAlmacena").click(function (e) {
        e.preventDefault();
        var msj = "";
        var accion = $(this).attr("data-id");

        var fecha_recepcion = $("#ffin").val();
        if (fecha_recepcion == "") {
            msj += "Debe seleccionar una fecha de recepción\n";
        }

        var idfile = $("#cbofile").val();
        var idstand = $("#cbostand").val();
        var descripcion = $("#txtdescripcion").val();
        var codbox = $("#txtbox").val();
        var iddoc = $("#txtid").val();
        var formData = new FormData();
        formData.append('iddoc', iddoc);
        formData.append('accion', accion);
        formData.append('fecha_recepcion', fecha_recepcion);
        formData.append('idfile', idfile);
        formData.append('idstand', idstand);
        formData.append('codbox', codbox);
        formData.append('descripcion', descripcion);
        if (msj != "") {
            alert(msj);
            return false;
        }
        $.ajax({
            url: '../json_almacenar_documento',
            type: 'POST',
            contentType: false,
            data: formData,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $("#panelFormulario").append("<div id='cargando' class='cargando'></div>");
            },
            success: function (data) {
                if (data.msj == "") {
                    location.href = "../documento/lista";
                } else {
                    $("#mensajeaux").append('<div class="alert alert-danger fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            data.msj +
                            '</div>');
                    $("#cargando").remove();
                }
                $("#cargador").remove();

            },
            error: function () {
                $("#cargador").remove();
                $("#files_list").html("");
                //alert("No se pudo registrar, se sugiere actualizar la página");
            }
        }).done(function (msg) {

        });
    });

    $("#btnEditarDoc").click(function (e) {
        e.preventDefault();

        var msj = "";

        var iddoc = $("#txtiddoc").val();
        var identificador = $("#txtidentificador").val();
        var frecepcion = $("#finicio").val();
        var origen = $("#txtorigen").val();
        var idcategoria = $("#cbocategoria").val();
        var idtipodoc = $("#cbotipodoc").val();
        var asunto = $("#txtasunto").val();
        var descripcion = $("#txtdescripcion").val();

        var fenvio = $("#ffin").val();
        var idlugar = $("#cboLugar").val();
        var modalidad = $("#cbomodalidad").val();
        var responsable = $("#txtresponsable").val();
        var comentario_traslado = $("#txtdescripcion_transporte").val();

        var fecha_almacen = $("#fauxiliar").val();
        var comentario_almacen = $("#txtdescripcion_almacen").val();
        var idfile = $("#cbofile").val();
        var idstand = $("#cbostand").val();
        var codbox = $("#txtbox").val();


        var formData = new FormData();
        formData.append('iddoc', iddoc);
        formData.append('identificador', identificador);
        formData.append('frecepcion', frecepcion);
        formData.append('remitente', origen);
        formData.append('idcategoria', idcategoria);
        formData.append('idtipodoc', idtipodoc);
        formData.append('asunto', asunto);
        formData.append('descripcion', descripcion);

        formData.append('fenvio', fenvio);
        formData.append('idlugar', idlugar);
        formData.append('modalidad', modalidad);
        formData.append('responsable', responsable);
        formData.append('comentario_traslado', comentario_traslado);

        formData.append('fecha_almacen', fecha_almacen);
        formData.append('idfile', idfile);
        formData.append('idstand', idstand);
        formData.append('codbox', codbox);
        formData.append('comentario_almacen', comentario_almacen);

        var cont = 0;
        $("input[type=file]").each(function (index)
        {
            console.log(index);
            if ($('#file_' + index).length) {
                formData.append('archivo' + index, $('#file_' + index)[0].files[0]);
                //$('#file_' + index).remove();
                cont++;
            }
        });
        if (cont == 0) {
            msj += "Debe adjuntar una imagen o archivo del documento\n";
        }
        if (msj != "") {
            alert(msj);
            return false;
        }
        $.ajax({
            url: '../json_editar_documento',
            type: 'POST',
            contentType: false,
            data: formData,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $("#panelFormulario").append("<div id='cargando' class='cargando'></div>");
            },
            success: function (data) {
                if (data.msj == "") {
                    location.href = "../documento/lista";
                } else {
                    $("#mensajeaux").append('<div class="alert alert-danger fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            data.msj +
                            '</div>');
                    $("#cargando").remove();
                }
                $("#cargador").remove();

            },
            error: function () {
                $("#cargador").remove();
                $("#files_list").html("");
                //alert("No se pudo registrar, se sugiere actualizar la página");
            }
        }).done(function (msg) {

        });
    });

    $("#finicio").datepicker();
    $("#ffin").datepicker();
    $("#btnlimpiar").click(function () {
        $("#select_local").val("");
		$("input").val("");
        $("#finicio").val("");
        $("#ffin").val("");
        $("#montoinicial").val("");
        $("#montofinal").val("");
        $("#montoigual").val("");
    });
    $("#btnBuscar").click(function () {
        var idalmacen = $("#select_almacen").val();
        var file = $("#bus_file").val();
        var stand= $("#bus_stand").val();
		var box = "";
		var estado = $("#bus_estado").val();
		var idtipo = $("#select_tipodoc").val();
		var codigo = $("#bus_codigo").val();
		var codbox = $("#bus_codigobox").val();
		var asunto = $("#bus_asunto").val();
		var remitente = $("#bus_remitente").val();
		var finicio = $("#finicio").val();
        var ffin = $("#ffin").val();
		var idcategoria = $("#select_cate").val();


        var formData = new FormData();
        formData.append('idalmacen', idalmacen);
        formData.append('file', file);
		formData.append('idcategoria', idcategoria);
		formData.append('stand', stand);
        formData.append('box', box);
		formData.append('estado', estado);
        formData.append('idtipo', idtipo);
        formData.append('codigo', codigo);
		formData.append('codbox', codbox);
        formData.append('asunto', asunto);
        formData.append('finicio', finicio);
        formData.append('ffin', ffin);

        $("#listaTareas").html("<div class='cargando'></div>");
        $.post("json_buscar", {idalmacen: idalmacen,idcategoria:idcategoria, remitente:remitente,file: file,  stand: stand, box: box,  estado: estado,idtipo: idtipo, codigo: codigo, codbox: codbox, asunto: asunto, finicio: finicio, ffin: ffin}, function (data) {
			
			if(data.toString()=="session"){
				location.href="../documento/lista";
			}else{
				$("#listaTareas").html(data);
			}
            
        });
    });
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
	$("#btnBuscar").trigger("click");
});
 