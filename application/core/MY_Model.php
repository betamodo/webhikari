<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class MY_Model extends CI_Model {

    protected $table;
    protected $pk;

    public function __construct() {
        parent::__construct();
        date_default_timezone_set("America/Lima");
    }

    public function getdata($columnas = null, $where = null, $limit = 100, $offset = 0) {
        $this->db->select($columnas);
        $query = $this->db->get_where($this->table, $where, $limit, $offset);
        return ($limit === 1) ? $query->row() : $query->result();
    }
 
	 public function editarWhere($where = null, $data) {

        if (!empty($where))
            $this->db->where($where);
        $this->db->update($this->table, $data);
        return ($this->db->affected_rows() > 0) ? true : false;
    }
    public function getExiste($campo = "", $valorComprar = "", $menosID = "", $limit = 1, $offset = 0) {
        if ($campo == "") {
            $campo = $this->pk;
        }
        $where = "";
        $where = $campo . " = '" . $valorComprar . "' and " . $this->pk . " != " . $menosID;
        $this->db->select("count(" . $campo . ") as cantidad");
        $query = $this->db->get_where($this->table, $where, $limit, $offset);
        return ($limit === 1) ? $query->row() : $query->result();
    }

    public function getdataOrdenado($columnas = null, $where = null, $limit = 100, $offset = 0) {
        $query = $this->db->select($columnas)
                ->from($this->table)
                ->where($where)
                ->order_by($this->pk, 'desc')
                ->limit($limit)
                ->get();
        return ($limit === 1) ? $query->row() : $query->result();
    }

    public function guardar($data) {
        $this->db->insert($this->table, $data);
        return ($this->db->affected_rows() > 0) ? $this->db->insert_id() : 0;
    }
	
	public function eliminar($id) {
		$this->db->where($this->pk,$id);
		return $this->db->delete($this->table);
    }

    public function insert_batch($data) {
        $this->db->insert_batch($this->table, $data);
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    public function editar($id, $data, $where = null, $where_in = null) {
        if (!empty($id)) {
            $this->db->where($this->pk, $id);
        } else {
            if (!empty($where))
                $this->db->where($where);
            if (is_array($where_in))
                $this->db->where_in($this->pk, $where_in);
        }
        $this->db->update($this->table, $data);
        return ($this->db->affected_rows() > 0) ? true : false;
    }

    public function countAll() {
        return $this->db->count_all($this->table);
    }

    public function countAllResult($where = null, $like = null) {
        if ($where !== null)
            $this->db->from($where);
        if ($like !== null)
            $this->db->like($like);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

}
