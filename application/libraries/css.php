<?php

Class Css {

    private $styleIn = "\t<link rel='stylesheet' type='text/css' media='all' href='";
    private $styleOut = "'/>\n";
    private $style = array();
    private $msj = array("0" => "Error al devolver estilo\n",
        "1" => "Error Al enviar la posicion y el estilo para modificar");

    public function AgregarEstilo($filename) {
        $this->style[] = $this->styleIn . $filename . $this->styleOut;
    }

    public function DevolverEstilo() {

        if (is_array($this->style)) {
            return $this->style;
        } else {
            return $this->msj[0];
        }
    }

    public function CambiarEstilo($posicion=null, $filename=null) {
        if ($posicion != null && $filename != null) {
            $this->style[$posicion] = $this->styleIn . $filename . $this->styleOut;
        } else {
            return $this->msj[1];
        }
    }

}

?>
