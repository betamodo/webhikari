<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
//  AQUI EMPEZAMOS A CONFIGURAR LOS TEMPLATE DE LAS PAGINAS

//******************************************************************
//              TEMPLATE POR DEFECTO LLAMADO default
//******************************************************************
$template['active_template'] = 'default';
$template['default']['template'] = 'template/template';
$template['default']['regions'] = array(
 'header',
 'titleheader',
 'title',
 'content',
 'botones',
 'footer',
);
$template['default']['parser'] = 'parser';
$template['default']['parser_method'] = 'parse';
$template['default']['parse_template'] = FALSE;

//******************************************************************
//      CONFIGURAMOS OTRO TEMPLATE LLAMADO registro
//******************************************************************
$template['registro']['template'] = 'template/registro';
$template['registro']['regions'] = array(
 'header',
 'title',
 'content',
 'sidebar',
 'footer',
);
$template['registro']['parser'] = 'parser';
$template['registro']['parser_method'] = 'parse';
$template['default']['parse_template'] = FALSE;

/* End of file template.php */
/* Location: ./system/application/config/template.php */