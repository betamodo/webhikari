<?php

class libresmodel extends MY_Model {

    protected $table    = "libres";
    protected $pk       = 'idlibres';

    const SUMAR_DIA     = 2;
    const RESTAR_DIA    = 1;

    function __construct() {
        parent::__construct();
        $this->load->helper('funciones');
    }

    /**
     * Buscar los dias que tiene libre o asignados el empleado
     * 
     * @param integer $idEmpleado
     * 
     * @return boolean|Array
     */
    public function getDiasLibreEmpleado($idEmpleado = 0, $ordenar = false)
    {
        if ($idEmpleado > 0) {
            $data   = $this->getdata(array('*'), array('idempleado' => $idEmpleado));
            if ($ordenar) {
                $dias   = getIdDia();
                $orden  = array();
                // Ordernar por array[dia en numero][hora] = tipo
                foreach ($data as $fila) {
                    $orden[$dias[$fila->dia]['iddia']][intval($fila->hora)] = $fila->tipo;
                }
                $data = $orden;
            }

            return $data;
        }

        return false;
    }

    /**
     * Eliminar los dias libres o laborables del empleado
     * 
     * @param integer $idEmpleado
     * 
     * @return boolean
     */
    public function eliminarDiasLibres($idEmpleado = 0)
    {
        if ($idEmpleado > 0) {
            $this->db->where('idempleado', $idEmpleado);
            return $this->db->delete($this->table);
        }

        return false;
    }
} 
