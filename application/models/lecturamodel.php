<?php

class lecturamodel extends MY_Model {

    protected $table = "lectura"; 
    protected $pk = 'idlectura';

    function __construct() {
        parent::__construct();
    }

	public function getLecturasTareaEstado($where="") {
        $query = $this->db->select('l.idlectura')
                ->from("lectura l")
                ->join('tarea_estado t', 't.idtarea_estado=l.idtarea_estado')
                ->where($where)
                ->get();
        //var_dump($this->db->select());exit;
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
	
	public function listaNotificaciones($idusuario = 0) {
        $query = $this->db->select('l.*, t.fecha_registro, t.idtarea, t.comentario')
                ->from("lectura l")
                ->join('tarea_estado t', 't.idtarea_estado=l.idtarea_estado')
                ->where("l.idusuario = " . $idusuario." and leido=0")
                ->get();
        //var_dump($this->db->select());exit;
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
}
