<?php

class archivomodel extends MY_Model {

    protected $table = "archivo";
    protected $pk = 'idarchivo';

    function __construct() {
        parent::__construct();
    }

    function getArchivosTarea($idtarea = 0) {
        //->where('estado_registro', 1)
        $query = $this->db->select('a.url')
                ->from("archivo a")
                ->join('tarea_estado t', 't.idtarea_estado=a.idtarea_estado')
                ->where('t.idtarea = ' . $idtarea . " and t.idestado = 1")
                ->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

}
