<?php

/**
 * Modelo de los dias de descanso del empleado
 * 
* @author  Fernando Paz <ferpaz@beta.pe>
* @version 1.0
*/
class EmpleadoDiasDescansoModel extends MY_Model
{
    /**
     * Nombre de la tabla en la BD
     * @var string
     */
    protected $table = "rh_empleado_dias_descanso";

    /**
     * Columna como indice primario en la BD
     * @var string
     */
    protected $pk = 'id';

    /**
     * Dias de la semana para traducirlos a la BD
     */
    public static $DIAS_SEMANA = array(
        1 => "Lunes",
        2 => "Martes",
        3 => "Miércoles",
        4 => "Jueves",
        5 => "Viernes",
        6 => "Sábado",
        7 => "Domingo",
    );

    function __construct()
    {
        parent::__construct();
    }

    /**
     * Obtiene los dias de descanso de un empleado
     * 
     * @param int $idEmpleado
     * 
     * @return Array
     */
    public function getDiasDescansoEmpleado($idEmpleado) {
        $formateado = array();
        $dias = $this->db
                ->select(array('dia_semana'))
                ->from($this->table)
                ->where("id_empleado", $idEmpleado)
                ->order_by("dia_semana", "ASC")
                ->get()
                ->result_array();
        foreach ($dias as $fila) {
            $formateado[] = $fila['dia_semana'];
        }

        return $formateado;
    }

    /**
     * Insertar los dias de descanso del empleado
     * 
     * @param int    $idEmpleado
     * @param string $diasDescanso Valor de los dias separados por coma
     * 
     * @return boolean
     */
    public function insertarDiasDescanso($idEmpleado, $diasDescanso) {
        $dias = explode(",", $diasDescanso);
        $ids = array();
        foreach ($dias as $valor) {
            $datos = array(
                'id_empleado' => $idEmpleado,
                'dia_semana' => $valor
            );

            $this->db->insert($this->table, $datos);

            if ($this->db->affected_rows() > 0) {
                $ids[] = $this->db->insert_id();
            } else {
                foreach ($ids as $id) {
                    $this->db->where($this->pk, $id);
                    $this->db->delete($this->table);
                }

                return false;
            }
        }

        return true;
    }

    /**
     * Elimina los dias de descanso del empleado
     * 
     * @param int $idEmpleado
     * 
     * @return boolean
     */
    public function eliminarDiasDescanso($idEmpleado) {
        $this->db->where('id_empleado', $idEmpleado);

        return $this->db->delete($this->table);
    }
}