<?php

class progpersonalmodel extends MY_Model {

    protected $table = "progpersonal";
    protected $pk = 'idprogpersonal';

    function __construct() {
        parent::__construct();
    }

    function getProgPersonal($where = "") {
        $query = $this->db->select("p.idprogpersonal, p.nombre, p.fecha_inicio, p.fecha_fin, p.activo")
                ->from("progpersonal p")
                ->join('local l', 'l.idlocal = p.idlocal', 'inner')
                ->join('labor a', 'a.idlabor = p.idlabor', 'inner')
                ->where($where)
                ->get();
        
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

}
