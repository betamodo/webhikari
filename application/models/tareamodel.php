<?php

class tareamodel extends MY_Model {

    protected $table = "tarea";
    protected $pk = 'idtarea';

    function __construct() {
        parent::__construct();
    }

    public function getDataPresupuesto($aColumnas = array("*")) {
        $fields = $this->db->field_data("nombre");
        $query = $this->db->select($aColumnas)->get('tarea');
        return array("fields" => $fields, "query" => $query);
    }

    function getMatrizLocalesEstado($objLocales, $objEstados) {
        $mResultados = array();
        foreach ($objLocales as $itemUsuario) {
            foreach ($objEstados as $itemEstado) {
                $where = "estado_registro = 1 and idlocal = " . $itemUsuario->idlocal . " and ultimoestado = " . $itemEstado->idestado . "";
                $query = $this->db->select('count(idtarea) as cantidad')
                        ->from("tarea")
                        ->where($where)
                        ->limit(1)
                        ->get();
                $objTarea = $query->row();
                $mResultados[$itemUsuario->idlocal][$itemEstado->idestado] = $objTarea->cantidad;
            }
        }

        return $mResultados;
    }

    function getMatrizUsuariEstado($objUsuarios, $objEstados) {
        $mResultados = array();
        foreach ($objUsuarios as $itemUsuario) {
            foreach ($objEstados as $itemEstado) {
                $where = "estado_registro = 1 and idusuario_rol_ejecutor = " . $itemUsuario->idusuario . " and ultimoestado = " . $itemEstado->idestado;
                if ($itemEstado->idestado == 3 || $itemEstado->idestado == 4) {
                    $where = "idusuario_rol_creador = " . $itemUsuario->idusuario . " and ultimoestado = " . $itemEstado->idestado;
                }
                $query = $this->db->select('count(idtarea) as cantidad')
                        ->from("tarea")
                        ->where($where)
                        ->limit(1)
                        ->get();
                $objTarea = $query->row();
                
                $mResultados[$itemUsuario->idusuario][$itemEstado->idestado] = $objTarea->cantidad;
            }
        }
        
        return $mResultados;
    }

    function getListaTareas($where, $limit = 100) {
		//echo $where;exit;
		if($where == "t.estado_registro = 1"){
			$where.=" AND t.ultimoestado != 5";
		}
		$rol=intval($this->session->userdata('rol'));
		if($limit != 1){
		if($rol==1){
			$cadena_buscada   = 't.idlocal';
			$posicion_coincidencia = strpos($where, $cadena_buscada);	
			$cant_ands=substr_count($where, ' AND');
			if ($posicion_coincidencia != false && $cant_ands == 1) {
				$where.=" AND t.ultimoestado != 5";
			}else{
				//$where.=" AND t.idlocal = " . $this->session->userdata('idlocal') . "";
			}
			//echo $posicion_coincidencia."--".$cant_ands;exit;
		}
		if($rol==2){
			$where.=" AND idusuario_rol_ejecutor = " . $this->session->userdata('idusuario') . " or idusuario_rol_creador = " . $this->session->userdata('idusuario');
		}
	}
		//echo $where;exit;
        $query = $this->db->select("t.idtarea, t.idusuario_creador,t.numrechazos, t.ppto_aprobado,t.presupuesto ,t.proveedor ,t.descripcion ,t.codigo, t.idcategoria, t.nombre,t.paraadmin, t.ultimoestado, 
t.fecha_actualizacion, t.fecha_registro, t.prioridad, t.idusuario_rol_creador, t.fecha_registro, 
t.idusuario_rol_ejecutor, t.idlocal, l.nombre as nomlocal")
                ->from("tarea t")
                ->join('local l', 't.idlocal = l.idlocal')
                ->where($where)
                ->order_by('t.idtarea', 'desc')
                ->get();
				//var_dump( $this->db->select());exit;
        if ($limit == 1) {
            return $query->row();
        } else {
            return $query->result();
        }
    }

    function getListaGrupos($where, $limit = 100) {
        $query = $this->db->select("g.*, u.nombre as nomcreador")
                ->from("grupo g")
                ->join('usuario u', 'u.idusuario = g.idusuario_creador','left')
                ->where($where)
                ->order_by("estado_registro, idgrupo", 'desc')
                ->get();

        if ($limit == 1) {
            return $query->row();
        } else {

            return $query->result();
        }
    }

}
