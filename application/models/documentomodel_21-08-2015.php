<?php

class documentomodel extends MY_Model {

    protected $table = "documento";
    protected $pk = 'iddocumento';

    function __construct() {
        parent::__construct();
    }

    function getListaDocumentos($where, $limit = 100) {
        $query = $this->db->select("d.*, a.nombre as nomalmacen")
                ->from("documento d")
                ->join('almacen a', 'a.idalmacen = d.idalmacen', 'left')
                ->where($where)
                ->order_by($this->pk, 'desc')
                ->get();

        if ($limit == 1) {
            return $query->row();
        } else {

            return $query->result();
        }
    }

}
