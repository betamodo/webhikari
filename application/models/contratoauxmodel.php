<?php

class contratoauxmodel extends MY_Model {

    protected $table = "contratoaux";
    protected $pk = 'idcontratoaux';

    function __construct() {
        parent::__construct();
    }

	function getCantidadEmpleados($where, $limit = 100) {
        $query = $this->db->select("count(c.idcontratoaux) as cantidad")
                ->from("contratoaux c")
                ->join('labor l', 'c.idlabor = l.idlabor')
                ->where($where)
                ->order_by($this->pk, 'desc')
                ->get();
        //echo var_dump($this->db->select());exit;
        if ($limit == 1) {
            return $query->row();
        } else {
            return $query->result();
        }
    }
	
	function getEmpleadosContrato($where, $limit = 100) {
        $query = $this->db->select("idempleado")
                ->from("contratoaux c")
                ->join('labor l', 'c.idlabor = l.idlabor')
                ->where($where)
                ->order_by($this->pk, 'desc')
                ->get();
        //echo var_dump($this->db->select());exit;
        if ($limit == 1) {
            return $query->row();
        } else {
            return $query->result();
        }
    }
}
