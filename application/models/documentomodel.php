<?php

class documentomodel extends MY_Model {

    protected $table = "documento";
    protected $pk = 'iddocumento';

    function __construct() {
        parent::__construct();
    }

    function getListaDocumentos($where, $limit = 100) {
		if($where == "d.estado_registro = 1"){
			$where.=" and d.estado != 4";
		}
		$query = $this->db->select("d.*, a.nombre as nomalmacen")
                ->from("documento d")
                ->join('almacen a', 'a.idalmacen = d.idalmacen', 'left')
                ->where($where)
                ->order_by($this->pk, 'desc')
				->limit($limit)
                ->get();

        if ($limit == 1) {
            return $query->row();
        } else {

            return $query->result();
        }
    }

}
