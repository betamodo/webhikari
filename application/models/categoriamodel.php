<?php

class categoriamodel extends MY_Model {

    protected $table = "categoria";
    protected $pk = 'idcategoria';

    function __construct() {
        parent::__construct();
    }

    function getaCateUsuario() {
        //->where('estado_registro', 1)
        $query = $this->db->select('c.idcategoria, u.idusuario ,c.nombre as nomcategoria, u.nombre as nomresponsable')
                ->from("categoria c")
                ->join('usuario u', 'c.idusuario_responsable=u.idusuario')
                ->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $fila) {
                $data[$fila->idcategoria] = array("idusuario" => $fila->idusuario, "nomcategoria" => $fila->nomcategoria, "nomresponsable" => $fila->nomresponsable);
            }
            return $data;
        } else {
            return false;
        }
    }

    function getListaCategorias($where, $limit = 100) {
        $query = $this->db->select("c.idcategoria, c.nombre, u.nombre as nomresponsable, c.estado_registro")
                ->from("categoria c")
                ->join('usuario u', 'c.idusuario_responsable = u.idusuario', 'left')
                ->where($where)
                ->order_by($this->pk, 'desc')
                ->get();

        if ($limit == 1) {
            return $query->row();
        } else {
            return $query->result();
        }
    }

}
