<?php

class rh_empleadomodel extends MY_Model {

    protected $table = "rh_empleado";
    protected $pk = 'idrh_empleado';

    function __construct() {
        parent::__construct();
    }
}
