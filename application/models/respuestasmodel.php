<?php

class respuestasmodel extends MY_Model {

    protected $table = "respuestas";
    protected $pk = 'idrespuesta';

    function __construct() {
        parent::__construct();
    }

	function getUsuariosRespuestas($where, $limit = 100) {
        $query = $this->db->distinct()
				->select("r.idusuario_creador")
                ->from("respuestas r")
                ->join('tarea_estado  t', 't.idtarea_estado = r.idtarea_estado')
                ->where($where)
                ->order_by($this->pk, 'desc')
                ->get();
        //echo var_dump($this->db->select());exit;
        if ($limit == 1) {
            return $query->row();
        } else {
            return $query->result();
        } 
    }
}
