<?php

class Programacionmodel extends MY_Model {

    protected $table = "programacion";
    protected $pk = 'idprogramacion';

    function __construct() {
        parent::__construct();
    }

    public function getProgramacion() {
        $query = $this->db->select('`p.*, t.codigo, t.nombre, t.presupuesto, t.proveedor, t.idcategoria, t.idusuario_rol_creador, t.idlocal, t.fecha_ultima_creada')
                ->from("programacion p")
                ->join('tareaprog t', 'p.idtareaprog=t.idtareaprog')
                ->order_by($this->pk, 'desc')
                ->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

}
