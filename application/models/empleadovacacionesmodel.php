<?php

/**
 * Modelo de las vacaciones del empleado
 * 
* @author  Fernando Paz <ferpaz@beta.pe>
* @version 1.0
*/
class EmpleadoVacacionesModel extends MY_Model
{
    /**
     * Nombre de la tabla en la BD
     * @var string
     */
    protected $table = "rh_empleado_vacaciones";

    /**
     * Columna como indice primario en la BD
     * @var string
     */
    protected $pk = 'id';

    function __construct()
    {
        parent::__construct();
    }

    /**
     * Obtiene las vacaciones de un empleado
     * 
     * @param int $idEmpleado
     * 
     * @return Array
     */
    public function getVacacionesEmpleado($idEmpleado) {
        return $this->getdata(array('fecha_inicio', 'fecha_fin'), array("id_empleado" => $idEmpleado), 1);
        /*return $this->db
                ->select(array('fecha_inicio', 'fecha_fin'))
                ->from($this->table)
                ->where("id_empleado", $idEmpleado)
                ->get()
                ->row_array();*/
    }

    /**
     * Si tiene vacaciones asignadas o no
     * @param int  $idEmpleado
     * 
     * @return boolean
     */
    public function hasVacaciones($idEmpleado)
    {
        $fila = $this->getVacacionesEmpleado($idEmpleado);
        if (isset($fila) && !empty($fila)) {
            return true;
        }

        return false;
    }
}