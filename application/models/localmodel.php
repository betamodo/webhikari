<?php

class localmodel extends MY_Model {

    protected $table = "local";
    protected $pk = 'idlocal';

    function __construct() {
        parent::__construct();
    }

    function getLocales() {
        //->where('estado_registro', 1)
        $query = $this->db->select('idlocal, nombre')
                ->from("local")
                ->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $fila) {
                $data[$fila->idlocal] = array("idlocal" => $fila->idlocal, "nombre" => $fila->nombre);
            }
            return $data;
        } else {
            return false;
        }
    }

    function getListaLocales($where, $limit = 100) {
        $query = $this->db->select("l.idlocal, l.nombre, u.nombre as nomresponsable, l.estado_registro")
                ->from("local l")
                ->join('usuario u', 'l.idusuario_responsable = u.idusuario', 'left')
                ->where($where)
                ->order_by($this->pk, 'desc')
                ->get();

        if ($limit == 1) {
            return $query->row();
        } else {
            return $query->result();
        }
    }

}
