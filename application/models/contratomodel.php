<?php

class contratomodel extends MY_Model {

    protected $table = "contrato";
    protected $pk = 'idcontrato';

    function __construct() {
        parent::__construct();
    }

	function getCantidadEmpleados($where, $limit = 100) {
        $query = $this->db->select("count(c.idcontrato) as cantidad")
                ->from("contrato c")
                ->join('labor l', 'c.idlabor = l.idlabor')
                ->where($where)
                ->order_by($this->pk, 'desc')
                ->get();
        //echo var_dump($this->db->select());exit;
        if ($limit == 1) {
            return $query->row();
        } else {
            return $query->result();
        }
    }
}
