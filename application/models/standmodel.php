<?php

class standmodel extends MY_Model {

    protected $table = "stand";
    protected $pk = 'idstand';

    function __construct() {
        parent::__construct();
    }

    function getStand() {
        $query = $this->db->select('idstand, codigo')
                ->from("stand")
                ->get();
        $data[0] = array("codigo" => "");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $fila) {
                $data[$fila->idstand] = array("codigo" => $fila->codigo);
            }
            return $data;
        } else {
            return false;
        }
    }

}
