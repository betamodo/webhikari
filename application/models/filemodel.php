<?php

class filemodel extends MY_Model {

    protected $table = "file";
    protected $pk = 'idfile';

    function __construct() {
        parent::__construct();
    }

    function getFiles() {
        $query = $this->db->select('idfile, codigo')
                ->from("file")
                ->get();
        $data[0] = array("codigo" => "");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $fila) {
                $data[$fila->idfile] = array("codigo" => $fila->codigo);
            }
            return $data;
        } else {
            return false;
        }
    }

}
