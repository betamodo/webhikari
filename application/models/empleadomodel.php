<?php

class empleadomodel extends MY_Model {

    protected $table = "rh_empleado";
    protected $pk = 'idrh_empleado';

    function __construct() {
        parent::__construct();
    }

		function getEmpleadosContrato($where, $limit = 100) {
        $query = $this->db->select("c.*, e.idrh_empleado, e.rh_nombre")
                ->from("rh_empleado e")
                ->join('contrato c', 'c.idempleado = e.idrh_empleado')
                ->where($where)
                ->order_by($this->pk, 'desc')
                ->get();
        //echo var_dump($query->result());exit;
        if ($limit == 1) {
            return $query->row();
        } else {
            return $query->result();
        }
    }
	function getTodosEmpleadosContrato($where, $limit = 100) {
        $query = $this->db->select("c.*, e.*")
                ->from("rh_empleado e")
                ->join('contrato c', 'c.idempleado = e.idrh_empleado')
                ->where($where)
                ->order_by($this->pk, 'desc')
                ->get();
        //echo var_dump($this->db->select());exit;
        if ($limit == 1) {
            return $query->row();
        } else {
            return $query->result();
        }
    }
	
	//*********************** PARA CONTRATO SIMULACION ********************************
	
	function getEmpleadosContratoAux($where, $limit = 100) {
        $query = $this->db->select("c.*, e.idrh_empleado, e.rh_nombre")
                ->from("rh_empleado e")
                ->join('contratoaux c', 'c.idempleado = e.idrh_empleado')
                ->where($where)
                ->order_by($this->pk, 'desc')
                ->get();
        //echo var_dump($query->result());exit;
        if ($limit == 1) {
            return $query->row();
        } else {
            return $query->result();
        }
    }
	
	function getEmpleadoLaborAux($where, $limit = 100) {
        $query = $this->db->select("idarea")
                ->from("rh_empleado e")
                ->join('labor l', 'l.idlabor = e.idlabor')
                ->where($where)                
                ->get();
        //echo var_dump($query->result());exit;
        if ($limit == 1) {
            return $query->row();
        } else {
            return $query->result();
        }
    }
	
	function getEmpleadosContratoAux2($where="", $whereIn="",$limit = 100) {
		$this->db->select("c.*, e.idrh_empleado, e.rh_nombre")
                ->from("rh_empleado e")
                ->join('contratoaux c', 'c.idempleado = e.idrh_empleado')
                ->where($where)                
                ->get();
		$query1 = $this->db->last_query();		
		
		if($whereIn!=""){
			$this->db->select("c.*, e.idrh_empleado, e.rh_nombre")
					->from("rh_empleado e")
					->join('contratoaux c', 'c.idempleado = e.idrh_empleado')
					->where($whereIn)                
					->get();
			$query2 = $this->db->last_query();			
			$query = $this->db->query($query1." UNION ".$query2);	
		}else{
			$query = $this->db->query($query1);	
		}
		
		
		//echo var_dump($query->result());exit;
        if ($limit == 1) {
            return $query->row();
        } else {
            return $query->result();
        }
    }
	
	function getEmpleadosContratoAux3($where="", $whereIn="",$limit = 100) {
        $this->db->select("c.idempleado")
            ->from("contratoaux c")
            ->join('labor l', 'c.idlabor = l.idlabor')
            ->where($where)
            ->get();
        $query1 = $this->db->last_query();      
        
        if($whereIn!=""){
            $this->db->select("c.idempleado")
                ->from("contratoaux c")
                ->join('labor l', 'c.idlabor = l.idlabor')
                ->where($whereIn)                
                ->get();
            $query2 = $this->db->last_query();          
            $query = $this->db->query($query1." UNION ".$query2);   
        }else{
            $query = $this->db->query($query1); 
        }	
		
		//echo var_dump($query->result());exit;
        if ($limit == 1) {
            return $query->row();
        } else {
            return $query->result();
        }
    }
	
	function getTodosEmpleadosContratoAux($where, $limit = 100) {
        $query = $this->db->select("c.*, e.*")
                ->from("rh_empleado e")
                ->join('contratoaux c', 'c.idempleado = e.idrh_empleado')
                ->where($where)
                ->order_by($this->pk, 'desc')
                ->get();
        //echo var_dump($this->db->select());exit;
        if ($limit == 1) {
            return $query->row();
        } else {
            return $query->result();
        }
    }
}
