<?php

class Usuariomodel extends MY_Model {

    protected $table = "usuario";
    protected $pk = 'idusuario';

    function __construct() {
        parent::__construct();
    }

    function getUsuarios() {
//->where('estado_registro', 1)
        $query = $this->db->select('idusuario, nombre, correo, foto')
                ->from("usuario")
                ->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $fila) {
                $data[$fila->idusuario] = array("idusuario" => $fila->idusuario, "nombre" => $fila->nombre, "correo" => $fila->correo, "foto" => $fila->foto);
            }
            return $data;
        } else {
            return false;
        }
    }

    public function login($nomusuario, $clave) {
        $query = $this->db->select('u.*, l.idlocal, l.nombre as nomlocal')
                ->from("usuario u")
                ->join('local l', 'u.idusuario=l.idusuario_responsable', 'left')
                ->where("u.nomusuario = '" . $nomusuario . "' and u.clave = '" . $clave . "' and u.estado_registro = 1")
                ->limit(1)
                ->get();
        //var_dump($this->db->select());exit;
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function logincuenta($idusuario = 0) {
        $query = $this->db->select('u.*, l.idlocal, l.nombre as nomlocal')
                ->from("usuario u")
                ->join('local l', 'u.idusuario=l.idusuario_responsable', 'left')
                ->where("u.idusuario = " . $idusuario)
                ->limit(1)
                ->get();
        //var_dump($this->db->select());exit;
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    function getListaUsuarios($where, $limit = 100) {
        $query = $this->db->select("u.idusuario, u.nombre, u.rol, u.foto, u.estado_registro, u.correo, l.nombre as nomlocal")
                ->from("usuario u")
                ->join('local l', 'l.idusuario_responsable = u.idusuario', 'left')
                ->where($where)
                ->order_by($this->pk, 'desc')
                ->get();

        if ($limit == 1) {
            return $query->row();
        } else {
            return $query->result();
        }
    }

}

?>
