<?php

class almacenmodel extends MY_Model {

    protected $table = "almacen";
    protected $pk = 'idalmacen';

    function __construct() {
        parent::__construct();
    }

    function getUsuarioAlmacen($where, $limit = 100) {
        $query = $this->db->select("u.nombre, correo")
                ->from("usuario u")
                ->join('almacen a', 'a.idusuario_responsable = u.idusuario')
                ->where($where)
                ->order_by($this->pk, 'desc')
                ->get();

        if ($limit == 1) {
            return $query->row();
        } else {
            return $query->result();
        }
    }
	
	function getdataAlmacen($where, $limit = 100) {
        $query = $this->db->select("a.*, u.nombre as nomresponsable")
                ->from("almacen a")
                ->join('usuario u', 'a.idusuario_responsable = u.idusuario','left')
                ->where($where)
                ->order_by($this->pk, 'desc')
                ->get();

        if ($limit == 1) {
            return $query->row();
        } else {
            return $query->result();
        }
    }

}
