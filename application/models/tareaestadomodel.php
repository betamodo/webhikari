<?php

class tareaestadomodel extends MY_Model {

    protected $table = "tarea_estado";
    protected $pk = 'idtarea_estado';

    function __construct() {
        parent::__construct();
        date_default_timezone_set("America/Lima");
    }

    public function guardar($data) {

        $data["fecha_registro"] = date("Y-m-d H:i:s");
        $this->db->insert($this->table, $data);
        return ($this->db->affected_rows() > 0) ? $this->db->insert_id() : 0;
    }

    function getTareaEstados($idtarea) {
        $rol = $this->session->userdata("rol");
        if ($rol == 1 || $rol == 4) {
            $sql = "SELECT t.*, group_concat(a.url SEPARATOR  '|') as archivos FROM " .
                    "tarea_estado t " .
                    "left join archivo a on t.idtarea_estado=a.idtarea_estado " .
                    "where t.idtarea = " . $idtarea . " " .
                    "group by t.idtarea_estado";
        } else {
            $sql = "SELECT t.*, group_concat(a.url SEPARATOR  '|') as archivos FROM " .
                    "tarea_estado t " .
                    "left join archivo a on t.idtarea_estado=a.idtarea_estado " .
                    "where t.idtarea = " . $idtarea . " " .
                    "group by t.idtarea_estado";
        } 
		if($rol==3){
			$sql = "SELECT t.*, group_concat(a.url SEPARATOR  '|') as archivos FROM " .
                    "tarea_estado t " .
                    "left join archivo a on t.idtarea_estado=a.idtarea_estado " .
                    "where t.idtarea = " . $idtarea .
                    " group by t.idtarea_estado";
		}
        //echo $sql;exit;

        $objData = $this->db->query($sql);
        if ($objData->num_rows() > 0) {
            return $objData->result();
        } else {
            return false;
        }
    }

}
