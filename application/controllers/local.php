<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Local extends MY_Controller {

    private $_fecha_actual;
    private $_idusuario_actual;
    private $_idlocal;
    private $_rol;

    function __construct() {
        parent::__construct();
        $this->_fecha_actual = date("Y-m-d H:i:s");
        $this->load->model('localmodel');
        $this->_idusuario_actual = $this->session->userdata('idusuario');
        $this->_rol = $this->session->userdata('rol');
        $this->_idlocal = $this->session->userdata('idlocal');
        $this->template->set_template('default');
        $this->load->helper(array('url', 'funciones', 'mysql_to_excel_helper'));
    }

    function index() {
        $this->login();
    }

    public function lista() {
        // Trayendo modelos
        $mensaje = $this->session->userdata('mensaje');
        if ($mensaje != "") {
            $mensaje = getHtmlMensaje(1, $mensaje);
        }
        $this->session->set_userdata('mensaje', "");

        $aListaLocales = $this->localmodel->getListaLocales("l.idlocal > 0");
        $data['titulo'] = "Lista de locales";
        $data['mensaje'] = $mensaje;
        $data['aListaLocales'] = $aListaLocales;
        $botones = "";
        $botones .= '<a href = "' . base_url() . 'panel" class = "btn btn-sm btn-default" >Atras</a> ';
        $botones .= '<a href = "' . base_url() . 'local/nuevo" class = "btn btn-sm btn-default" >Nuevo</a> ';
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Locales", TRUE);
        $this->template->write_view('content', 'mantenimiento/local/lista', $data, TRUE);
        $this->template->render();
    }

    public function json_crear_local() {

        $nombre = $this->input->post("nombre");
        $idusuario = $this->input->post("idusuario");
        $estado = $this->input->post("estado");
        $msg_error = "";
        // Validar si existe el id de tarea
        $auxLocal = array();
        $objLocal = array();
        $auxLocal = $this->localmodel->getExiste("nombre", $nombre, 0, 1);

        if ($auxLocal->cantidad == 0) {
            $objLocal = array(
                "nombre" => $nombre,
                "idusuario_responsable" => $idusuario,
                "estado_registro" => $estado
            );
            $idlocal = $this->localmodel->guardar($objLocal);
            // Subimos los archivos si el usuario fue creado exitosamente
            if ($idlocal < 1) {
                $msg_error.="No se pudo registrar";
            }
        } else {
            $msg_error.="El nombre ingresado ya existe";
        }
        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "El local se creó exitosamente");
        }
        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    public function json_editar_local() {

        $idlocal = $this->input->post("idlocal");
        $nombre = $this->input->post("nombre");
        $idusuario = $this->input->post("idusuario");
        $estado = $this->input->post("estado");

        $msg_error = "";
        // Validar si existe el id de tarea
        $auxLocal = array();
        $objLocal = array();
        $auxLocal = $this->localmodel->getExiste("nombre", $nombre, $idlocal, 1);
        if ($auxLocal->cantidad == 0) {
            $objLocal = array(
                "nombre" => $nombre,
                "idusuario_responsable" => $idusuario,
                "estado_registro" => $estado
            );
            $this->localmodel->editar($idlocal, $objLocal);
        } else {
            $msg_error.="El nombre ingresado ya existe";
        }
        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "El local se editó exitosamente");
        }
        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    public function nuevo() {

        // Trayendo modelos
        $this->load->model('usuariomodel');
        $aUsuarios = $this->usuariomodel->getdata(array("idusuario", "nombre"));
        $data['titulo'] = "Nuevo local";
        $data['aUsuarios'] = $aUsuarios;
        $botones = "";
        $botones .= '<a href = "' . base_url() . 'local/lista" class = "btn btn-sm btn-default" >Atras</a> ';
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Nuevo local", TRUE);
        $this->template->write_view('content', 'mantenimiento/local/crear', $data, TRUE);
        $this->template->render();
    }

    public function editar($idlocal) {
        // Trayendo modelos
        $this->load->model('usuariomodel');
        $aUsuarios = $this->usuariomodel->getdata(array("idusuario", "nombre"));

        $objLocal = $this->localmodel->getdata(array("*"), "idlocal = " . $idlocal, 1);
        if ($objLocal == FALSE) {
            $this->session->set_userdata('mensaje', "");
        }

        $data['titulo'] = "Editar local";
        $data['objLocal'] = $objLocal;
        $data['aUsuarios'] = $aUsuarios;
        $botones = "";
        $botones .= '<a href = "' . base_url() . 'local/lista" class = "btn btn-sm btn-default" >Atras</a> ';
        $botones .= '<a href = "' . base_url() . 'local/nuevo" class = "btn btn-sm btn-default" >Nuevo</a> ';
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Editar local", TRUE);
        $this->template->write_view('content', 'mantenimiento/local/editar', $data, TRUE);
        $this->template->render();
    }

}
