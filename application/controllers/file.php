<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class File extends MY_Controller {

    private $_fecha_actual;
    private $_idusuario_actual;
    private $_idlocal;
    private $_rol;

    function __construct() {
        parent::__construct();
        $this->_fecha_actual = date("Y-m-d H:i:s");
        $this->load->model('filemodel');
        $this->_idusuario_actual = $this->session->userdata('idusuario');
        $this->_rol = $this->session->userdata('rol');
        $this->_idlocal = $this->session->userdata('idlocal');
        $this->template->set_template('default');
        $this->load->helper(array('url', 'funciones'));
    }

    function index() {
        $this->login();
    }

    public function lista() {
        // Trayendo modelos
        $mensaje = $this->session->userdata('mensaje');
        if ($mensaje != "") {
            $mensaje = getHtmlMensaje(1, $mensaje);
        }
        $this->session->set_userdata('mensaje', "");

        $aListaFile = $this->filemodel->getdata(array("idfile","codigo","descripcion","estado_registro"));
        $data['titulo'] = "Lista de files";
        $data['mensaje'] = $mensaje;
        $data['aListaFile'] = $aListaFile;
        $botones = "";
        $botones .= '<a href = "' . base_url() . 'panel" class = "btn btn-sm btn-default" >Atras</a> ';
        $botones .= '<a href = "' . base_url() . 'file/nuevo" class = "btn btn-sm btn-default" >Nuevo</a> ';
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "files.", TRUE);
        $this->template->write_view('content', 'mantenimiento/file/lista', $data, TRUE);
        $this->template->render();
    }

    public function json_crear_file() {

        $codigo = $this->input->post("codigo");        
        $estado = $this->input->post("estado");
		$descripcion = $this->input->post("descripcion");
        $msg_error = "";
        // Validar si existe el id de tarea
        $auxfile = array();
        $ofile = array();
        $auxfile = $this->filemodel->getExiste("codigo", $codigo, 0, 1);

        if ($auxfile->cantidad == 0) {
            $ofile = array(
                "codigo" => $codigo,
				"descripcion" => $descripcion,
                "estado_registro" => $estado
            );
            $idfile = $this->filemodel->guardar($ofile);
            // Subimos los archivos si el usuario fue creado exitosamente
            if ($idfile < 1) {
                $msg_error.="No se pudo registrar";
            }
        } else {
            $msg_error.="El codigo ingresado ya existe";
        }
        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "El file se cre� exitosamente");
        }
        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    public function json_editar_file() {

        $idfile = $this->input->post("idfile");
        $codigo = $this->input->post("codigo");
		$descripcion = $this->input->post("descripcion");
        $estado = $this->input->post("estado");

        $msg_error = "";
        $auxfile = array();
        $oFile = array();
        $auxfile = $this->filemodel->getExiste("codigo", $codigo, $idfile, 1);
        if ($auxfile->cantidad == 0) {
            $oFile = array(
                "codigo" => $codigo,                
				"descripcion" => $descripcion,                
                "estado_registro" => $estado
            );
            $this->filemodel->editar($idfile, $oFile);
        } else {
            $msg_error.="El codigo ingresado ya existe";
        }
        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "El file se edit� exitosamente");
        }
        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    public function nuevo() {
        $data['titulo'] = "Nuevo file";
        $botones = "";
        $botones .= '<a href = "' . base_url() . 'file/lista" class = "btn btn-sm btn-default" >Atras</a> ';
		$this->template->add_js('media/js/file.js');
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Nuevo file", TRUE);
        $this->template->write_view('content', 'mantenimiento/file/crear', $data, TRUE);
        $this->template->render();
    }

    public function editar($idfile) {
        // Trayendo modelos 
        $oFile = $this->filemodel->getdata(array("*"), "idfile = " . $idfile, 1);
        if ($oFile == FALSE) {
            $this->session->set_userdata('mensaje', "No existe el file");
        }

        $data['titulo'] = "Editar file";
        $data['oFile'] = $oFile;
        $botones = "";
        $botones .= '<a href = "' . base_url() . 'file/lista" class = "btn btn-sm btn-default" >Atras</a> ';
        $botones .= '<a href = "' . base_url() . 'file/nuevo" class = "btn btn-sm btn-default" >Nuevo</a> ';
		$this->template->add_js('media/js/file.js');
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Editar file", TRUE);
        $this->template->write_view('content', 'mantenimiento/file/editar', $data, TRUE);
        $this->template->render();
    }

}
