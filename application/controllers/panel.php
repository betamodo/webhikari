<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Panel extends My_Controller {

    private $_fecha_actual;
    private $_idusuario_actual;
    private $_rol;

    function __construct() {
        parent::__construct();
        $this->_fecha_actual = date("Y-m-d H:i:s");
        $this->_idusuario_actual = $this->session->userdata('idusuario');
        $this->_rol = $this->session->userdata('rol');
        $this->load->helper(array('url', 'funciones'));
    }

    function index() {
        $this->template->write('botones', '', TRUE);
        $this->template->write('titleheader', "Lista de tareas", TRUE);
        $this->template->write_view('content', 'panel', array("titulo" => "PANEL DE CONTROL"), TRUE);
        $this->template->render();
    }

    public function detallelista($idusuario = 0, $idestado = 0) {
        $this->session->set_userdata('param_idusuario', $idusuario);
        $this->session->set_userdata('param_idestado', $idestado);
        redirect(base_url() . "tarea/lista");
    }

    public function detallelistalocal($idlocal = 0, $idestado = 0) {
        $this->session->set_userdata('param_idlocal', $idlocal);
        $this->session->set_userdata('param_idestadolocal', $idestado);
        redirect(base_url() . "tarea/lista");
    }

    public function resumengestores() {
        //validaSession("formulario");
        // Trayendo modelos 
        $this->load->model('tareamodel');
        $this->load->model('estadomodel');
        $this->load->model('usuariomodel');
        $objEstados = $this->estadomodel->getdata(array("idestado", "nombre"));
        $objUsuarios = $this->usuariomodel->getdata(array("idusuario", "nombre"));
        $objUsuarioAux = $this->usuariomodel->getUsuarios();
        $mResultados = $this->tareamodel->getMatrizUsuariEstado($objUsuarios, $objEstados);
        $aEstados = getEstados();

        $data['titulo'] = "Resumen de gestores";
        $data['mResultados'] = $mResultados;
        $data['aEstados'] = $aEstados;
        $data['objUsuarioAux'] = $objUsuarioAux;
        $botones = "";
        $botones = '<a href = "' . base_url() . 'tarea/lista" class = "btn btn-sm btn-default" >Atras</a> ';
        $botones .= '<a href = "' . base_url() . 'panel/resumengestores" class = "btn btn-sm btn-default active" >Resumen de gestores</a> ';
        $botones .= '<a href = "' . base_url() . 'panel/resumenlocales" class = "btn btn-sm btn-default" >Resumen de Locales</a> ';
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Resumen de gestores", TRUE);
        $this->template->write_view('content', 'dashboard/estadistica1', $data, TRUE);
        $this->template->render();
    }

    public function resumenlocales() {
        //validaSession("formulario");
        // Trayendo modelos
        $this->load->model('tareamodel');
        $this->load->model('estadomodel');
        $this->load->model('localmodel');
        $objEstados = $this->estadomodel->getdata(array("idestado", "nombre"));
        $objLocales = $this->localmodel->getdata(array("idlocal", "nombre"));
        $objLocalesAux = $this->localmodel->getLocales();
        $mResultados = $this->tareamodel->getMatrizLocalesEstado($objLocales, $objEstados);
        $aEstados = getEstados();

        $data['titulo'] = "Resumen de locales";
        $data['mResultados'] = $mResultados;
        $data['aEstados'] = $aEstados;
        $data['objLocales'] = $objLocalesAux;
        $botones = "";
        $botones = '<a href = "' . base_url() . 'tarea/lista" class = "btn btn-sm btn-default" >Atras</a> ';
        $botones .= '<a href = "' . base_url() . 'panel/resumengestores" class = "btn btn-sm btn-default" >Resumen de gestores</a> ';
        $botones .= '<a href = "' . base_url() . 'panel/resumenlocales" class = "btn btn-sm btn-default active" >Resumen de Locales</a> ';
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Resumen de locales", TRUE);
        $this->template->write_view('content', 'dashboard/estadistica2', $data, TRUE);
        $this->template->render();
    }

}
