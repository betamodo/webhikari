<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Tareo extends MY_Controller {

    private $_fecha_actual;
    private $_idusuario_actual;
    private $_idlocal;
    private $_rol;

    function __construct() {
        parent::__construct();
        $this->_fecha_actual = date("Y-m-d H:i:s");
        $this->load->model('documentomodel');
        $this->_idusuario_actual = $this->session->userdata('idusuario');
        $this->_rol = $this->session->userdata('rol');
        $this->_idlocal = $this->session->userdata('idlocal');
        $this->template->set_template('default');
        $this->load->helper(array('url', 'funciones'));
    }

    /**
     * Pagina de inicio del controlador
     * 
     * @return void
     */
    public function index() {
        $this->vertareo();
    }

    function comingsoon() {
        $this->load->view('comingsoon', array());
    }

    public function locales() {
        $this->load->model('localmodel');
        $aLocales = $this->localmodel->getdata(array("idlocal", "nombre"));
        $data["aLocales"] = $aLocales;
        $data["titulo"] = "Locales";
        $botones = "";
        //$botones .= '<a href = "' . base_url() . 'tarea/lista" class = "btn btn-sm btn-default" >Atras</a> ';
        $botones .= '<a href = "' . base_url() . 'documento/nuevo" class = "btn btn-sm btn-default" >Nuevo</a> ';
        $this->template->add_js('media/js/tareo.js');
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Lista de documentos", TRUE);
        $this->template->write_view('content', 'tareo/locales', $data, TRUE);
        $this->template->render();
    }
	
	public function json_grabar_simu(){
		$this->load->model('libresmodel');
		$this->load->model('empleadomodel');
		$this->load->model('labormodel');
		
		$msj="";
		$dia=$this->input->post("dia");
		$idarea=$this->input->post("idarea");
		$idlocal=$this->input->post("idlocal");
		$finicio=$this->input->post("finicio");
		$ffin=$this->input->post("ffin");
		
		$auxDiasPersonalizado=getIdDia();
		foreach($this->input->post() as $nomVariable => $valorVariable){
			if(substr($nomVariable,0,3)=="key"){
				if($this->input->post("hrcomun")!= $this->input->post("hora_".$valorVariable) || $this->input->post("dia")!= $this->input->post("dia_".$valorVariable)){
				   $auxDiasPersonalizado= getIdDia();
				   $hora=$this->input->post("hora_".$valorVariable);
				   $dia=$this->input->post("dia_".$valorVariable);
				   $oLibres = $this->libresmodel->getdata(array("GROUP_CONCAT(idempleado SEPARATOR ',') as ids"),"tipo = 1 and hora = ".$hora." and dia = '".$dia."'",1);
				   $whereLibres="";
				   
				   $oAumento = $this->libresmodel->getdata(array("GROUP_CONCAT(idempleado SEPARATOR ',') as ids"),"tipo = 2 and hora = ".$hora." and dia = '".$dia."'",1);
				   $whereLibres="";
				   $whereIn="";
				   if($oLibres->ids != NULL){
						$whereLibres="AND c.idempleado NOT IN (".$oLibres->ids.")";
					}
					
					if($oAumento->ids != NULL){
						$whereIn=strtolower($dia)." = 1 and c.idlocal = ".$idlocal." and l.idarea = ".$idarea." and ('".  getFechaEn2($finicio)."' >= c.fecha_inicio) and ( '".  getFechaEn2($ffin)."' <= c.fecha_fin) and c.idempleado IN (".$oAumento->ids.")";
					}
				   
					$where="(".intval($hora)." BETWEEN c.".$auxDiasPersonalizado[$dia]["abrev"]."_hora_inicio AND c.".$auxDiasPersonalizado[$dia]["abrev"]."_hora_fin) and ".strtolower($dia)." = 1 and c.idlocal = ".$idlocal." and l.idarea = ".$idarea." and ('".  getFechaEn2($finicio)."' >= c.fecha_inicio) and ( '".  getFechaEn2($ffin)."' <= c.fecha_fin) ".$whereLibres;
					
					$oEmpleados = $this->empleadomodel->getEmpleadosContratoAux3($where,$whereIn); 
					foreach($oEmpleados as $itemEmpleado){
						//echo $itemEmpleado->idempleado."--".$this->input->post($nomVariable)."<br>";
						if($itemEmpleado->idempleado==$this->input->post($nomVariable)){
							$msj.="Este empleado ya labora en este horario \n";							
						}
					}
					//*******************************************************************************
						
					if($msj==""){
						$oLibreAux=$this->libresmodel->getdata(array("idlibres"),"idempleado = ".$this->input->post($nomVariable)." and hora = ".$this->input->post("hrcomun")." and dia = '".$this->input->post("dia")."'",1);
						
						if($oLibreAux!=false){
							$this->libresmodel->eliminar($oLibreAux->idlibres); 						
						}
						
						$oLibre=array(
							"idempleado"=>$this->input->post($nomVariable),
							"dia"=>$this->input->post("dia"),
							"tipo"=>1,							
							"hora"=>$this->input->post("hrcomun")
						);
						$idlibres = $this->libresmodel->guardar($oLibre);
						$oLibre["tipo"]=2;
						$oLibre["dia"]=$dia;
						$oLibre["hora"]=$this->input->post("hora_".$valorVariable);
						$idlibres = $this->libresmodel->guardar($oLibre);
					}
					
					echo json_encode(array("msj" => $msj, "data" => array()));
				}
				
			}
			
		}
	}
	
    public function listaprogramacion() {
        $this->load->model('localmodel');
        $this->load->model('labormodel');
        $this->load->model('progpersonalmodel');
        $this->load->model('detalleprogpersonalmodel');
        $aLocal = array();
        $aLabores = array();
        $aProgPersonal = array();
        $aLocales = $this->localmodel->getdata(array("idlocal", "nombre"), "estado_registro = 1");
        $aLabores = $this->labormodel->getdata(array("idlabor", "nombre"), "estado_registro = 1");
        $aProgPersonal = $this->progpersonalmodel->getProgPersonal("p.idprogpersonal > 0");
        $data["aLabores"] = $aLabores;
        $data["aProgPersonal"] = $aProgPersonal;
        $data["aLocales"] = $aLocales;
        $data["titulo"] = "Programaciones de personal";
        $botones = "";
        $botones .= '<a href = "' . base_url() . 'tareo/programar" class = "btn btn-sm btn-default" >Nuevo</a> ';
		$botones .= '<a href = "' . base_url() . 'tareo" class = "btn btn-sm btn-default" >Ir a tareo</a> ';
        $this->template->add_js('media/js/tareo.js');
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Programaciones de personal", TRUE);
        $this->template->write_view('content', 'tareo/listaprogramacion', $data, TRUE);
        $this->template->render();
    }

    /**
     * Horario del empleado en simulacion o no
     * 
     * @param integer $idempleado
     * 
     * @return void
     */
    public function verhorario($idempleado=0) {
        $this->load->model('contratomodel');
        $this->load->model('empleadomodel'); 
        $horaInicio=5;
        $oCantidades=array();
        $oEmpleado=$this->empleadomodel->getdata(array("rh_nombre as nombre"),"idrh_empleado = ".$idempleado, 1);
        $auxDiasPersonalizado=getDiaSemanaPersonalizado();
        $aDias = array(
            "-1" => "Hora",
            "1" => "Lunes",
            "1" => "Lunes",
            "2" => "Martes",
            "3" => "Miércoles",
            "4" => "Jueves",
            "5" => "Viernes",
            "6" => "Sábado",
            "7" => "Domingo"
        );
        $i=0;
        while ($i < 24) {						
            foreach ($aDias as $key => $itemDias) {
        		if($key !=-1){
        			$where="(".intval($i)." BETWEEN c.".$auxDiasPersonalizado[$key]["abrev"]."_hora_inicio AND c.".$auxDiasPersonalizado[$key]["abrev"]."_hora_fin) and ".strtolower($auxDiasPersonalizado[$key]["campo"])." = 1 and c.idempleado = ".$idempleado;
        			$oContrato=$this->contratomodel->getCantidadEmpleados($where, 1);
        			if($oContrato!=false){
        				$oCantidades[$key][$i]=$oContrato->cantidad;
        			}else{
        				$oCantidades[$key][$i]=-1;
        			}					
        		}									
        	}
            $i++;
        }
        $data["horaInicio"] = $horaInicio;
        $data["aDias"] = $aDias;
        $data["titulo"] = "Horario: ".$oEmpleado->nombre;
        $data["oCantidades"] = $oCantidades;
        $this->template->add_js('media/js/tareo.js');
        $this->template->write('botones', "", TRUE);
        $this->template->write('titleheader', "Horario", TRUE);
        $this->template->write_view('content', 'tareo/horario', $data, TRUE);
        $this->template->render();
    }

    /**
     * Horario simulacion del empleado
     *
     * @param integer $idEmpleado
     * @param string  $fechaInicio Formato yy-mm-dd
     * @param string  $fechaFin    Formato yy-mm-dd
     * 
     * @return void
     */
    public function verhorarioaux($idEmpleado = 0, $fechaInicio, $fechaFin)
    {
        $this->load->model('contratoauxmodel');
        $this->load->model('empleadomodel'); 
        $this->load->model('libresmodel'); 
        $this->load->model('empleadovacacionesmodel'); 
        $this->load->model('empleadodiasdescansomodel');

        $fechaI = date_create_from_format("Y-m-d", $fechaInicio);
        $fechaF = date_create_from_format("Y-m-d", $fechaFin);

        $horaInicio=5;
        $oCantidades=array();
        $oEmpleado=$this->empleadomodel->getdata(array("rh_nombre as nombre"),"idrh_empleado = ".$idEmpleado, 1);
        $auxDiasPersonalizado=getDiaSemanaPersonalizado();

        // Obtiene el array de los dias, comenzando del primero en el rango seleccionado del filtro
        $aDias = array("-1" => "Hora");
        $aDias[date_format($fechaI, 'N')] = EmpleadoDiasDescansoModel::$DIAS_SEMANA[intval(date_format($fechaI, 'N'))];
        for ($i=0; $i < 6; $i++) { 
            $clave = date_format(date_add($fechaI, new \DateInterval('P1D')), 'N');
            $aDias[$clave] = EmpleadoDiasDescansoModel::$DIAS_SEMANA[intval($clave)];
        }
        // Resta los dias sumados para emparejar la fecha
        date_sub($fechaI, date_interval_create_from_date_string('6 days'));

        $i=0;
        while ($i < 24) {                           // Para las 24 horas del dia          
            foreach ($aDias as $key => $itemDias) { // Para cada dia
                if($key != -1){                     // Si son dias de la semana validos (no el nombre hora)
                    $where="(".intval($i)." BETWEEN c.".$auxDiasPersonalizado[$key]["abrev"]."_hora_inicio AND c.".$auxDiasPersonalizado[$key]["abrev"]."_hora_fin) and ".strtolower($auxDiasPersonalizado[$key]["campo"])." = 1 and c.idempleado = ".$idEmpleado;
                    // Busca si el empleado tiene que trabajar un dia ($auxDiasPersonalizado[$key]["campo"]) para una hora especifica intval($i)
                    $oContrato=$this->contratoauxmodel->getCantidadEmpleados($where, 1);
                    if($oContrato != false) {       // Encontro un registro
                        // Lo guarda por el numero de dia 1-7 y la hora 0 - 23
                        $oCantidades[$key][$i] = $oContrato->cantidad;
                    } else {                        // No encontro un registro
                        // Lo guarda por el numero de dia 1-7 y la hora 0 - 23
                        $oCantidades[$key][$i] = -1;
                    }                   
                }                                   
            }
            $i++;
        }

        // Obtener datos del empleado en el contrato
        $data["empleadoContrato"] = $this->contratoauxmodel->getdata(array('*'), array('idempleado' => $idEmpleado), 1);
        // Obtener los dias libres o no segun la tabla
        $data["diasLibres"] = $this->libresmodel->getDiasLibreEmpleado($idEmpleado, true);
        $data["diasLibresJ"] = $this->libresmodel->getDiasLibreEmpleado($idEmpleado); //Para Javascript
        // Obtener los dias de descanso
        $data["empleadoDescanso"] = $this->empleadodiasdescansomodel->getdata(array('dia_semana'), array('id_empleado' => $idEmpleado));
        // Obtener las vacaciones
        $data["empleadoVacaciones"] = $this->empleadovacacionesmodel->getdata(array('fecha_inicio', 'fecha_fin'), array('id_empleado' => $idEmpleado), 1);
        // Rango del horario
        $data['rangoHorario'] = array('inicio' => $fechaInicio, 'fin' => $fechaFin);
        $data["horaInicio"] = $horaInicio;      // Hora en la que comenzar el horario
        $data["aDias"] = $aDias;                // Dias de la semana
        $data["titulo"] = "Horario (".date_format($fechaI, 'd/m/Y')." - ".date_format($fechaF, 'd/m/Y')."): ".htmlentities($oEmpleado->nombre);
        $data["oCantidades"] = $oCantidades;    // Horario del trabajador

        $this->template->add_js('media/js/date.js');
        $this->template->add_js('media/js/tareo/horarioaux.js');
        $this->template->write('botones', "", TRUE);
        $this->template->write('titleheader', "Horario", TRUE);
        $this->template->write_view('content', 'tareo/horarioaux', $data, TRUE);
        $this->template->render();
    }

    /**
     * Recibir por POST los valores a guardar del horario
     * 
     * @param integer $idEmpleado [description]
     * 
     * @return JsonArray
     */
    public function json_horario_aux($idEmpleado = 0)
    {
        $msj = "";
        $data =  array();
        $idEmpleado = $this->input->post("idEmpleado");
        $horarioActual = $this->input->post("horarioActual");

        if (!empty($idEmpleado) && !empty($horarioActual)) {
            // Datos recibidos
            $diasSemana = getDiaSemanaPersonalizado();
            // Cargar los modelos
            $this->load->model('libresmodel'); 
            // Eliminar el horario libre anterior
            if ($this->libresmodel->eliminarDiasLibres($idEmpleado) !== false) {
                foreach ($horarioActual as $hora) {
                    // Para cada hora de cada dia
                    if ($hora[2] != 0) {
                        // Es un tipo valido de hora sumada o restada para agregar
                        $horaDividida = explode("-", $hora[0]);
                        $this->libresmodel->guardar(
                            array(
                                "idempleado"    => intval($idEmpleado),
                                "dia"           => $diasSemana[intval($horaDividida[0])]["campo"],
                                "hora"          => $horaDividida[1],
                                "idlabor"         => 0,
                                "tipo"          => intval($hora[2]),
                            )
                        );
                    }
                }
                $this->session->set_userdata('mensaje', htmlentities("El horario se modificó exitosamente."));
            } else {
                $msj .= "Error al reestructurar el horario.";
            }
        }

        echo json_encode(array("msj" => $msj, "data" => $data));
    }

    public function json_detalle_tareo() {
        $this->load->model('labormodel');
        $this->load->model('localmodel');

        $idlocal = $this->input->post("idlocal");
        $idarea = $this->input->post("idarea");
        $finicio = $this->input->post("finicio");
        $ffin = $this->input->post("ffin");
        $hora= $this->input->post("hora");
        $dia= $this->input->post("dia");
        $total= $this->input->post("total");

        $aLabores=$this->labormodel->getdata(array("idlabor","nombre"),"idarea = ".$idarea);
        $data["total"] = str_pad($total,2,'0', STR_PAD_LEFT);
        $data["hora"] = $hora;
        $data["dia"] = $dia;
        $data["idlocal"] = $idlocal;
        $data["idarea"] = $idarea;
        $data["finicio"] = $finicio;
        $data["ffin"] = $ffin;
        $data["aLabores"] = $aLabores;
        $this->load->view("tareo/divdetalletareo", $data);
    }
	
    /**
     * Detalle del personal en el tareo para una hora especifica
     * 
     * @return void
     */
    public function json_detalle_tareo_aux() {
        $this->load->model('labormodel');
        $this->load->model('localmodel');
        $idlocal = $this->input->post("idlocal");
        $idarea = $this->input->post("idarea");
        $finicio = $this->input->post("finicio");
        $ffin = $this->input->post("ffin");
        $hora= $this->input->post("hora");
        $dia= $this->input->post("dia");
        $total= $this->input->post("total");

        $aLabores=$this->labormodel->getdata(array("idlabor","nombre"),"idarea = ".$idarea);
        $aLaboresTodas=$this->labormodel->getdata(array("idlabor","nombre"),"estado_registro = 1");
        $data["total"] = str_pad($total,2,'0', STR_PAD_LEFT);
        $data["hora"] = $hora;
        $data["dia"] = $dia;
        $data["idlocal"] = $idlocal;
        $data["idarea"] = $idarea;
        $data["finicio"] = $finicio;
        $data["ffin"] = $ffin;
        $data["aLabores"] = $aLabores;
        $data["aLaboresTodas"] = $aLaboresTodas;
        $this->load->view("tareo/divdetalletareoaux", $data);
    }
	
    /**
     * Tareo Simulacion
     *
     * TODO: No mostrar los empleados que esten en vacaciones
     * 
     * @return void
     */
	public function json_tareo_aux() {
        $this->load->model('labormodel');
        $this->load->model('localmodel');
        $this->load->model('libresmodel');
        $this->load->model('contratoauxmodel');
        $this->load->model('progpersonalmodel');
        $this->load->model('detalleprogpersonalmodel');
        $idlocal = $this->input->post("idlocal");
        $idlabor = $this->input->post("idlabor");
        $finicio = $this->input->post("finicio");
        $ffin = $this->input->post("ffin");

        $data['dump'] = array();

        // Declarando variables
        $msj="";
        $aDias=array();
        $oCantidades = array();
        $oCantidadesPersonal = array();
        $oMontos = array();
        $oMontosPersonal = array();
        if($idlocal==""){
        	$msj.= "Debe seleccionar un local <br>";
        }
        if($idlabor==""){
        	$msj.= "Debe seleccionar una labor <br>";
        }
        if($finicio==""){
        	$msj.= "Debe indicar la fecha de inicio <br>";
        }
        if($ffin==""){
        	$msj.= "Debe indicar la fecha de término <br>";
        }
        if($ffin!="" && $finicio!=""){
        	$faux = date('Y-m-d', strtotime('+6 day', strtotime(getFechaEn2($finicio))));
        	if (strcmp(getFechaEn2($ffin), $faux) == 0) {
        		$msj.= "";
        	} else {
        		$msj.= "Las fechas indicadas no suman una semana <br>";
        	}
        }else{
        	$msj.= "Corregir formato de fecha <br>";
        }
        if($msj==""){
        	// Enviando los datos
        	$oProgramacion = array();
        	$oDetalle = array();
        	$oProgramacion = $this->progpersonalmodel->getdata(array("idprogpersonal", "idlocal", "idlabor", "nombre", "fecha_inicio", "fecha_fin"), "idlabor = " . $idlabor." and idlocal = ".$idlocal, 1);
        	
        	if($oProgramacion!=false){
        		$oDetalle = $this->detalleprogpersonalmodel->getdata(array("dia", "hora", "monto", "cantidad"), "idprogpersonal = " . $oProgramacion->idprogpersonal, 200);
        	}else{
        		$msj.="No existe programación";
        	}
        	if($msj==""){
        		$auxDias=getDiaSemana();
        		$auxDiasPersonalizado=getDiaSemanaPersonalizado();				
        		$aDias = array();
        		$aDias[-1]=array("nombre"=>"Hora","campo"=>"Hora");
        		$aLibres=array();
        		foreach ($oDetalle as $itemDetalle) {
        			$oCantidades[$itemDetalle->dia][$itemDetalle->hora] = $itemDetalle->cantidad;
        			$oMontos[$itemDetalle->dia][$itemDetalle->hora] = $itemDetalle->monto; 
        			$where="(".intval($itemDetalle->hora)." BETWEEN c.".$auxDiasPersonalizado[$itemDetalle->dia]["abrev"]."_hora_inicio AND c.".$auxDiasPersonalizado[$itemDetalle->dia]["abrev"]."_hora_fin) and ".strtolower($auxDiasPersonalizado[$itemDetalle->dia]["campo"])." = 1 and c.idlocal = ".$idlocal." and l.idarea = ".$idlabor." and ('".  getFechaEn2($finicio)."' >= c.fecha_inicio) and ( '".  getFechaEn2($ffin)."' <= c.fecha_fin)";
        			//$where="idlocal = ".$idlocal." and idlabor = ".$idlabor." and ('".  getFechaEn2($finicio)."' >= fecha_inicio) and ( '".  getFechaEn2($ffin)."' <= fecha_fin)";
        			//echo $itemDetalle->dia."-".$itemDetalle->hora.":00 -->".$where."<br>";
        			//$oContrato=$this->contratoauxmodel->getEmpleadosContrato($where);
        			$oContrato = getEmpleadosBanAux($itemDetalle->hora,$auxDiasPersonalizado[$itemDetalle->dia]["campo"], $idlocal, $idlabor, $finicio, $ffin, 3, true);
        			$cantEmplContrato = 0;
        			if($oContrato != false){
                        // $data['dump'][] = array("dia" => $itemDetalle->dia, "hora" => $itemDetalle->hora, "where" => $oContrato);
                        
                        
                        $oCantidadesPersonal[$itemDetalle->dia][$itemDetalle->hora] = count($oContrato);
                    }else{
                        // $data['dump'][] = array("dia" => $itemDetalle->dia, "hora" => $itemDetalle->hora, "where" => $oContrato);


                        // Aumentar en este día y en esta hora si tiene un movido a esta hora
        				$oAumento=$this->libresmodel->getdata(array("count(idlibres) as cantidad"),"dia = '".$auxDiasPersonalizado[$itemDetalle->dia]["campo"] . "' and tipo = 2 and hora = ". $itemDetalle->hora,1);							
        				if($oAumento->cantidad > 0){
        					$cantEmplContrato ++;
        				}
        				$oCantidadesPersonal[$itemDetalle->dia][$itemDetalle->hora]=$cantEmplContrato;
        			}
        			
        		}
        		$fechaInicio=strtotime(getFechaEn2($finicio));
        		$fechaFin=strtotime(getFechaEn2($ffin));
        		for($i=$fechaInicio; $i<=$fechaFin; $i+=86400){
        			$diaActual = date("w", strtotime(date("d-m-Y", $i)));
        			$aDias[$auxDias[$diaActual]["iddia"]]=array("nombre"=>$auxDias[$diaActual]["nombre"],"campo"=>$auxDias[$diaActual]["campo"]);					
        		}				
        	}			
        }

        $data["horaInicio"] = 5;
        $data["cantidadHoras"] = 24;
        $data["oCantidadesPersonal"] = $oCantidadesPersonal;
        $data["oCantidades"] = $oCantidades;
        $data["aDias"] = $aDias;
        $data["oMontos"] = $oMontos;
        $data["msj"] = $msj;
        $this->load->view("tareo/divtareoaux", $data);
    }
	
    public function json_tareo() {
		$this->load->model('labormodel');
        $this->load->model('localmodel');
		$this->load->model('contratomodel');
        $this->load->model('progpersonalmodel');
        $this->load->model('detalleprogpersonalmodel');
        $idlocal = $this->input->post("idlocal");
        $idlabor = $this->input->post("idlabor");
        $finicio = $this->input->post("finicio");
        $ffin = $this->input->post("ffin");
		// Declarando variables
		$msj="";
        $aDias=array();
		$oCantidades = array();
		$oCantidadesPersonal = array();
		$oMontos = array();
		$oMontosPersonal = array();
		if($idlocal==""){
			$msj.= "Debe seleccionar un local <br>";
		}
		if($idlabor==""){
			$msj.= "Debe seleccionar una labor <br>";
		}
		if($finicio==""){
			$msj.= "Debe indicar la fecha de inicio <br>";
		}
		if($ffin==""){
			$msj.= "Debe indicar la fecha de término <br>";
		}
		if($ffin!="" && $finicio!=""){
			$faux = date('Y-m-d', strtotime('+6 day', strtotime(getFechaEn2($finicio))));
			if (strcmp(getFechaEn2($ffin), $faux) == 0) {
				$msj.= "";
			} else {
				$msj.= "Las fechas indicadas no suman una semana <br>";
			}
		}else{
			$msj.= "Corregir formato de fecha <br>";
		}
		if($msj==""){
			// Enviando los datos
			$oProgramacion = array();
			$oDetalle = array();
			$oProgramacion = $this->progpersonalmodel->getdata(array("idprogpersonal", "idlocal", "idlabor", "nombre", "fecha_inicio", "fecha_fin"), "idlabor = " . $idlabor." and idlocal = ".$idlocal, 1);
			
			if($oProgramacion!=false){
				$oDetalle = $this->detalleprogpersonalmodel->getdata(array("dia", "hora", "monto", "cantidad"), "idprogpersonal = " . $oProgramacion->idprogpersonal, 200);
			}else{
				$msj.="No existe programación";
			}
			if($msj==""){
				$auxDias=getDiaSemana();
				$auxDiasPersonalizado=getDiaSemanaPersonalizado();				
				$aDias = array();
				$aDias[-1]=array("nombre"=>"Hora","campo"=>"Hora");
				
				foreach ($oDetalle as $itemDetalle) {
					$oCantidades[$itemDetalle->dia][$itemDetalle->hora] = $itemDetalle->cantidad;
					$oMontos[$itemDetalle->dia][$itemDetalle->hora] = $itemDetalle->monto; 
					$where="(".intval($itemDetalle->hora)." BETWEEN c.".$auxDiasPersonalizado[$itemDetalle->dia]["abrev"]."_hora_inicio AND c.".$auxDiasPersonalizado[$itemDetalle->dia]["abrev"]."_hora_fin) and ".strtolower($auxDiasPersonalizado[$itemDetalle->dia]["campo"])." = 1 and c.idlocal = ".$idlocal." and l.idarea = ".$idlabor." and ('".  getFechaEn2($finicio)."' >= c.fecha_inicio) and ( '".  getFechaEn2($ffin)."' <= c.fecha_fin)";
					//$where="idlocal = ".$idlocal." and idlabor = ".$idlabor." and ('".  getFechaEn2($finicio)."' >= fecha_inicio) and ( '".  getFechaEn2($ffin)."' <= fecha_fin)";
					//echo $itemDetalle->dia."-".$itemDetalle->hora.":00 -->".$where."<br>";
					$oContrato=$this->contratomodel->getCantidadEmpleados($where,1);
					if($oContrato!=false){
						$oCantidadesPersonal[$itemDetalle->dia][$itemDetalle->hora]=$oContrato->cantidad;
					}else{
						$oCantidadesPersonal[$itemDetalle->dia][$itemDetalle->hora]=-1;
					}
					
				}
				$fechaInicio=strtotime(getFechaEn2($finicio));
				$fechaFin=strtotime(getFechaEn2($ffin));
				for($i=$fechaInicio; $i<=$fechaFin; $i+=86400){
					$diaActual = date("w", strtotime(date("d-m-Y", $i)));
					$aDias[$auxDias[$diaActual]["iddia"]]=array("nombre"=>$auxDias[$diaActual]["nombre"],"campo"=>$auxDias[$diaActual]["campo"]);					
				}
				
			}			
		}
		
		$data["horaInicio"] = 5;
        $data["cantidadHoras"] = 24;
		$data["oCantidadesPersonal"] = $oCantidadesPersonal;
		$data["oCantidades"] = $oCantidades;
		$data["aDias"] = $aDias;
		$data["oMontos"] = $oMontos;
		$data["msj"] = $msj;
		$this->load->view("tareo/divtareo", $data);
		
    }

    public function vertareo() {
        //$this->load->model('labormodel');
		$this->load->model('areamodel');
        $this->load->model('localmodel');
        $this->load->model('progpersonalmodel');
        $aDias = array(
            "-1" => "Hora",
            "1" => "Lunes",
            "1" => "Lunes",
            "2" => "Martes",
            "3" => "Miércoles",
            "4" => "Jueves",
            "5" => "Viernes",
            "6" => "Sábado",
            "7" => "Domingo"
        );
        $horaInicio = 5;
        $cantidadHoras = 24;
        $aDescanzo = array();
        $data["titulo"] = "Ver Matriz tareo";
        $tituloHeader = "Ver Matriz tareo";
        $botones = "";
        $botones .= '<a href = "' . base_url() . 'tareo/listaprogramacion" class = "btn btn-sm btn-default" >Programaciones Ideales</a> ';
		$botones .= '<a href = "' . base_url() . 'tareo" class = "btn btn-sm btn-default" >Ir a tareo</a> ';
        $aLocal = array();
        $aLabores = array();
        $aLocales = $this->localmodel->getdata(array("idlocal", "nombre"), "estado_registro = 1");
        //$aLabores = $this->labormodel->getdata(array("idlabor", "nombre"), "estado_registro = 1");
		$aArea = $this->areamodel->getdata(array("idarea", "nombre"), "estado_registro = 1");
        $data["aDias"] = $aDias;
        $data["aArea"] = $aArea;
        $data["aLocales"] = $aLocales;
        $data["horaInicio"] = $horaInicio;
        $data["cantidadHoras"] = $cantidadHoras;
        $this->template->add_js('media/js/tareo.js');
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', $tituloHeader, TRUE);
        $this->template->write_view('content', 'tareo/vertareo', $data, TRUE);
        $this->template->render();
    }

    public function editprogramar($idprogramacion = 0) {
        $this->load->model('areamodel');
        $this->load->model('localmodel');
        $this->load->model('progpersonalmodel');
        $this->load->model('detalleprogpersonalmodel');

        $aDias = array(
            "-1" => "Hora",
            "1" => "Lunes",
            "1" => "Lunes",
            "2" => "Martes",
            "3" => "Miércoles",
            "4" => "Jueves",
            "5" => "Viernes",
            "6" => "Sábado",
            "7" => "Domingo"
        );
        $horaInicio = 5;
        $cantidadHoras = 24;
        $aDescanzo = array();
        $tituloHeader = "Editar programación";
        $botones = "";
        $botones .= '<a href = "' . base_url() . 'tareo/listaprogramacion" class = "btn btn-sm btn-default" >Atras</a> ';

        $oProgramacion = array();
        $oDetalle = array();
        $oProgramacion = $this->progpersonalmodel->getdata(array("idprogpersonal", "idlocal", "idlabor", "nombre", "fecha_inicio", "fecha_fin"), "idprogpersonal = " . $idprogramacion, 1);
        $oMontos = array();
        $oDetalle = $this->detalleprogpersonalmodel->getdata(array("dia", "hora", "monto", "cantidad"), "idprogpersonal = " . $idprogramacion, 200);
        $oCantidades = array();
        foreach ($oDetalle as $itemDetalle) {
            $oCantidades[$itemDetalle->dia][$itemDetalle->hora] = $itemDetalle->cantidad;
            $oMontos[$itemDetalle->dia][$itemDetalle->hora] = $itemDetalle->monto;
        }


        $aLocal = array();
        $aLabores = array();
        $aLocales = $this->localmodel->getdata(array("idlocal", "nombre"), "estado_registro = 1");
        $aArea = $this->areamodel->getdata(array("idarea", "nombre"), "estado_registro = 1");
        $data["oProgramacion"] = $oProgramacion;
        $data["oCantidades"] = $oCantidades;
        $data["oMontos"] = $oMontos;
        $data["aDias"] = $aDias;
        $data["titulo"] = "Editar";
        $data["aArea"] = $aArea;
        $data["aLocales"] = $aLocales;
        $data["horaInicio"] = $horaInicio;
        $data["cantidadHoras"] = $cantidadHoras;
        $this->template->add_js('media/js/tareo.js');
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Editar", TRUE);
        $this->template->write_view('content', 'tareo/editprogramar', $data, TRUE);
        $this->template->render();
    }

    public function programar($idprogramacion = 0) {
        $this->load->model('areamodel');
        $this->load->model('localmodel');
        $this->load->model('progpersonalmodel');
        $aDias = array(
            "-1" => "Hora",
            "1" => "Lunes",
            "1" => "Lunes",
            "2" => "Martes",
            "3" => "Miércoles",
            "4" => "Jueves",
            "5" => "Viernes",
            "6" => "Sábado",
            "7" => "Domingo"
        );
        $horaInicio = 5;
        $cantidadHoras = 24;
        $aDescanzo = array();
		$botones = "";
		$botones .= '<a href = "' . base_url() . 'tareo/listaprogramacion" class = "btn btn-sm btn-default" >Atrás</a> ';
        if ($idprogramacion == 0) {
            $data["titulo"] = "Nueva programación";
            $tituloHeader = "Nueva programación";            
            $botones .= '<a href = "' . base_url() . 'documento/nuevo" class = "btn btn-sm btn-default" >Nuevo</a> ';
        } else {
            $tituloHeader = "Editar programación";            
            $botones .= '<a href = "' . base_url() . 'documento/nuevo" class = "btn btn-sm btn-default" >Nuevo</a> ';
        }
		
        $aLocal = array();
        $aLabores = array();
        $aLocales = $this->localmodel->getdata(array("idlocal", "nombre"), "estado_registro = 1");
        $aArea = $this->areamodel->getdata(array("idarea", "nombre"), "estado_registro = 1");
        $data["aDias"] = $aDias;
        $data["aArea"] = $aArea;
        $data["aLocales"] = $aLocales;
        $data["horaInicio"] = $horaInicio;
        $data["cantidadHoras"] = $cantidadHoras;
        $this->template->add_js('media/js/tareo.js');
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', $tituloHeader, TRUE);
        $this->template->write_view('content', 'tareo/programar', $data, TRUE);
        $this->template->render();
    }

    public function grabarprogramacion() {
        $this->load->model('localmodel');
        $this->load->model('areamodel');
        $this->load->model('progpersonalmodel');
        $this->load->model('detalleprogpersonalmodel');
        $idpro_param = $this->input->post("idprogramacion");
        $idlocal = $this->input->post("cbolocal");
        $idlabor = $this->input->post("cbolabor");
        $finicio = $this->input->post("finicio");
        $ffin = $this->input->post("ffin");
        $nombre = "";
        $msj = "";

        // Traemos el nombre del local
        $oAuxLocal = array();
        $oAuxLocal = $this->localmodel->getdata(array("nombre"), "idlocal = " . $idlocal, 1);
        if ($oAuxLocal != false) {
            $nombre.=$oAuxLocal->nombre . "- ";
        } else {
            $msj.="Debes seleccionar un local \n";
        }
        // Traemos el nombre de la labor
        $oAuxLabor = array();
        $oAuxLabor = $this->areamodel->getdata(array("nombre"), "idarea = " . $idlabor, 1);
        if ($oAuxLabor != false) {
            $nombre.=$oAuxLabor->nombre;
        } else {
            $msj.="Debes seleccionar una labor \n";
        }


        // Creamos el objeto programación personal a insertar
        $oProgPersonal = array();
        $oProgPersonal = array(
            "idlocal" => $idlocal,
            "idlabor" => $idlabor,
            "nombre" => $nombre,
            "fecha_inicio" => getFechaEn2($finicio),
            "fecha_fin" => getFechaEn2($ffin)
        );
        if ($idpro_param == 0) {
            $idoProgPersonal = $this->progpersonalmodel->guardar($oProgPersonal);
        } else {
            $idoProgPersonal = $idpro_param;
            $this->progpersonalmodel->editar($idoProgPersonal, $oProgPersonal);
        }

        if ($idoProgPersonal > 0) {
            $i = 0;
            while ($i < 24) {
                for ($j = 1; $j < 8; $j++) {
                    $oDetalleProg = array();
                    $oDetalleProg = array(
                        "idprogpersonal" => $idoProgPersonal,
                        "dia" => $j,
                        "hora" => $i,
                        "cantidad" => intval($this->input->post("txtcantidad_" . $j . "_" . $i . "")),
                        "monto" => doubleval($this->input->post("txtmonto_" . $j . "_" . $i . ""))
                    );
                    if ($idpro_param == 0) {
                        $this->detalleprogpersonalmodel->guardar($oDetalleProg);
                    } else {
                        $this->detalleprogpersonalmodel->editarWhere("idprogpersonal = " . $idoProgPersonal . " and dia = " . $j . " and hora = " . $i, $oDetalleProg);
                    }
                }
                $i++;
            }
        }
        redirect(base_url() . "tareo/listaprogramacion");
    }

    public function nuevopersonal($idlocal = 0) {
        $this->load->model('localmodel');
        $oLocal = $this->localmodel->getdata(array("idlocal", "nombre"), "idlocal = " . $idlocal);
        $data["oLocales"] = $oLocal;
        $data["titulo"] = "Nuevo personal";
        $botones = "";
        //$botones .= '<a href = "' . base_url() . 'tarea/lista" class = "btn btn-sm btn-default" >Atras</a> ';
        $botones .= '<a href = "' . base_url() . 'tareo/programar" class = "btn btn-sm btn-default" >Nuevo</a> ';
        $this->template->add_js('media/js/tareo.js');
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Nuevo personal", TRUE);
        $this->template->write_view('content', 'tareo/nuevopersonal', $data, TRUE);
        $this->template->render();
    }


 }
