<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Tipodoc extends MY_Controller {

    private $_fecha_actual;
    private $_idusuario_actual;
    private $_idlocal;
    private $_rol;

    function __construct() {
        parent::__construct();
        $this->_fecha_actual = date("Y-m-d H:i:s");
        $this->load->model('tipodocmodel');
        $this->_idusuario_actual = $this->session->userdata('idusuario');
        $this->_rol = $this->session->userdata('rol');
        $this->_idlocal = $this->session->userdata('idlocal');
        $this->template->set_template('default');
        $this->load->helper(array('url', 'funciones'));
    }

    function index() {
        $this->login();
    }

    public function lista() {
        // Trayendo modelos
        $mensaje = $this->session->userdata('mensaje');
        if ($mensaje != "") {
            $mensaje = getHtmlMensaje(1, $mensaje);
        }
        $this->session->set_userdata('mensaje', "");

        $aListaTipodoc = $this->tipodocmodel->getdata(array("idtipodoc","nombre","estado_registro"));
        $data['titulo'] = "Lista de tipo de documentos";
        $data['mensaje'] = $mensaje;
        $data['aListaTipodoc'] = $aListaTipodoc;
        $botones = "";
        $botones .= '<a href = "' . base_url() . 'panel" class = "btn btn-sm btn-default" >Atras</a> ';
        $botones .= '<a href = "' . base_url() . 'tipodoc/nuevo" class = "btn btn-sm btn-default" >Nuevo</a> ';
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Tipo docs.", TRUE);
        $this->template->write_view('content', 'mantenimiento/tipodoc/lista', $data, TRUE);
        $this->template->render();
    }

    public function json_crear_tipodoc() {

        $nombre = $this->input->post("nombre");        
        $estado = $this->input->post("estado");
        $msg_error = "";
        // Validar si existe el id de tarea
        $auxTipodoc = array();
        $oTipodoc = array();
        $auxTipodoc = $this->tipodocmodel->getExiste("nombre", $nombre, 0, 1);

        if ($auxTipodoc->cantidad == 0) {
            $oTipodoc = array(
                "nombre" => $nombre,
                "estado_registro" => $estado
            );
            $idtipodoc = $this->tipodocmodel->guardar($oTipodoc);
            // Subimos los archivos si el usuario fue creado exitosamente
            if ($idtipodoc < 1) {
                $msg_error.="No se pudo registrar";
            }
        } else {
            $msg_error.="El nombre ingresado ya existe";
        }
        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "El tipo doc. se cre� exitosamente");
        }
        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    public function json_editar_tipodoc() {

        $idtipodoc = $this->input->post("idtipodoc");
        $nombre = $this->input->post("nombre");
        $estado = $this->input->post("estado");

        $msg_error = "";
        $auxTipodoc = array();
        $oTipodoc = array();
        $auxTipodoc = $this->tipodocmodel->getExiste("nombre", $nombre, $idtipodoc, 1);
        if ($auxTipodoc->cantidad == 0) {
            $oTipodoc = array(
                "nombre" => $nombre,                
                "estado_registro" => $estado
            );
            $this->tipodocmodel->editar($idtipodoc, $oTipodoc);
        } else {
            $msg_error.="El nombre ingresado ya existe";
        }
        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "El tipo doc. se edit� exitosamente");
        }
        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    public function nuevo() {
        $data['titulo'] = "Nuevo tipo doc";
        $botones = "";
        $botones .= '<a href = "' . base_url() . 'categoria/lista" class = "btn btn-sm btn-default" >Atras</a> ';
		$this->template->add_js('media/js/tipodoc.js');
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Nuevo tipo doc.", TRUE);
        $this->template->write_view('content', 'mantenimiento/tipodoc/crear', $data, TRUE);
        $this->template->render();
    }

    public function editar($idtipodoc) {
        // Trayendo modelos
        $oTipodoc = $this->tipodocmodel->getdata(array("*"), "idtipodoc = " . $idtipodoc, 1);
        if ($oTipodoc == FALSE) {
            $this->session->set_userdata('mensaje', "No existe el tipo de documento");
        }

        $data['titulo'] = "Editar tipo doc.";
        $data['oTipodoc'] = $oTipodoc;
        $botones = "";
        $botones .= '<a href = "' . base_url() . 'tipodoc/lista" class = "btn btn-sm btn-default" >Atras</a> ';
        $botones .= '<a href = "' . base_url() . 'tipodoc/nuevo" class = "btn btn-sm btn-default" >Nuevo</a> ';
		$this->template->add_js('media/js/tipodoc.js');
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Editar tipo doc.", TRUE);
        $this->template->write_view('content', 'mantenimiento/tipodoc/editar', $data, TRUE);
        $this->template->render();
    }

}
