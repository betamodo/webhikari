<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Configuracion extends MY_Controller {

    private $_fecha_actual;
    private $_idusuario_actual;
    private $_idlocal;
    private $_rol;

    function __construct() {
        parent::__construct();
        $this->_fecha_actual = date("Y-m-d H:i:s");
        $this->load->model('configuracionmodel');
        $this->_idusuario_actual = $this->session->userdata('idusuario');
        $this->_rol = $this->session->userdata('rol');
        $this->_idlocal = $this->session->userdata('idlocal');
        $this->template->set_template('default');
        $this->load->helper(array('url', 'funciones', 'mysql_to_excel_helper'));
    }

    function index() {
        $this->login();
    }

    public function lista() {
        
        $mensaje = $this->session->userdata('mensaje');
        if ($mensaje != "") {
            $mensaje = getHtmlMensaje(1, $mensaje);
        }
        $data['divmensaje'] = $mensaje;
        $aListaConfig = $this->configuracionmodel->getdata(array("*"));
        $data['titulo'] = "Lista de configuraciones";
        $data['aListaConfig'] = $aListaConfig;
        $botones = "";
        $botones .= '<a href = "' . base_url() . 'panel" class = "btn btn-sm btn-default" >Atras</a> ';
        $this->template->add_js('media/js/configuraciones.js');
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Configuraciones", TRUE);
        $this->template->write_view('content', 'configuracion/lista', $data, TRUE);
        $this->template->render();
    }

    public function json_guardar_config() {

        $id = $this->input->post("id");
        $valor = $this->input->post("valor");

        $msg_error = "";
        // Validar si existe el id de tarea
        $objConfig = array();
        $objConfig = array(
            "valor" => $valor
        );
        $idconfig = $this->configuracionmodel->editar($id, $objConfig);
        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "La configuración se guardó exitosamente");
        }
        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

}
