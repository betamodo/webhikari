<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tarea extends My_Controller {

    private $_fecha_actual;
    private $_idusuario_actual;
    private $_idlocal;
    private $_rol;

    function __construct() {
        parent::__construct();
        date_default_timezone_set("America/Lima");
        $this->_fecha_actual = date("Y-m-d H:i:s");

        $this->_idusuario_actual = $this->session->userdata('idusuario');
        $this->_rol = $this->session->userdata('rol');
        $this->_idlocal = $this->session->userdata('idlocal');

        $this->template->set_template('default');
        $this->load->helper(array('url', 'funciones', 'mysql_to_excel_helper'));
        $this->load->library('My_PHPExcel');
    }

    function index() {
        redirect(base_url() . "tarea/lista", 'refresh');
    }

    public function ejecuta_cron($aProgramacion = array(), $objTareaProg = array()) {
        $this->load->model('tareaprogmodel');
        $this->load->model('programacionmodel');
        $this->load->model('tareamodel');
        $auxoProgramacion = array();
        $idtareaRegistrada = 0;
        $ejecuta = 0;
        $repeticion = $aProgramacion["repeticion"];
        $fechaActual = date("Y-m-d");
        $fechaInicio = $aProgramacion["fecha_inicio"];

        $aDias = array(
            "lunes" => $aProgramacion["lunes"],
            "martes" => $aProgramacion["martes"],
            "miercoles" => $aProgramacion["miercoles"],
            "jueves" => $aProgramacion["jueves"],
            "viernes" => $aProgramacion["viernes"],
            "sabado" => $aProgramacion["sabado"],
            "domingo" => $aProgramacion["domingo"]
        );
        if ($repeticion == 1) {
            $idDiaActual = date("w", strtotime($fechaActual));

            $fechaProxima = getProximoDia($aDias, $fechaInicio, $idDiaActual);
//            echo "Fecha Actual: ".$fechaActual."<br>";
//            echo "Fecha Inicio: ".$fechaInicio."<br>";
//            echo "Fecha Proxima: ".$fechaProxima."<br>";
//            exit;
            if (strcmp($fechaActual, $fechaProxima) == 0) {
                $auxoProgramacion["fecha_actualizacion"] = $fechaActual;
                $fechaSiguiente = date('Y-m-d', strtotime('+1 day', strtotime($fechaActual)));

                $idDiaActualSiguiente = date("w", strtotime($fechaSiguiente));
                $fechaProxima = getProximoDia($aDias, $fechaSiguiente, $idDiaActualSiguiente, 1);

                $auxoProgramacion["fecha_siguiente"] = $fechaProxima;
                // Registra tarea
                $ejecuta = 1;
            } else {
                $auxoProgramacion["fecha_actualizacion"] = $fechaInicio;
                $auxoProgramacion["fecha_siguiente"] = $fechaProxima;
                $ejecuta = 0;
            }
        } elseif ($repeticion == 2) {
            if (strcmp($fechaActual, $fechaInicio) == 0) {
                $auxoProgramacion["fecha_actualizacion"] = $fechaActual;
                $fechaProxima = getProximoSemana($fechaInicio);
                $auxoProgramacion["fecha_siguiente"] = $fechaProxima;
                $ejecuta = 1;
            } else {
                $fechaProxima = getProximoSemana($fechaInicio);
                $auxoProgramacion["fecha_siguiente"] = $fechaProxima;
                $ejecuta = 0;
            }
        } elseif ($repeticion == 3) {
            if (strcmp($fechaActual, $fechaInicio) == 0) {
                $auxoProgramacion["fecha_actualizacion"] = $fechaActual;
                $fechaProxima = getProximoMes($fechaInicio);
                $auxoProgramacion["fecha_siguiente"] = $fechaProxima;
                $ejecuta = 1;
            } else {
                $fechaProxima = getProximoMes($fechaInicio);
                $auxoProgramacion["fecha_siguiente"] = $fechaProxima;
                $ejecuta = 0;
            }
        } elseif ($repeticion == 4) {
            if (strcmp($fechaActual, $fechaInicio) == 0) {
                $auxoProgramacion["fecha_actualizacion"] = $fechaActual;
                $fechaProxima = getProximoAnio($fechaInicio);
                $auxoProgramacion["fecha_siguiente"] = $fechaProxima;
                $ejecuta = 1;
            } else {
                $fechaProxima = getProximoAnio($fechaInicio);
                $auxoProgramacion["fecha_siguiente"] = $fechaProxima;
                $ejecuta = 0;
            }
        }
        if ($ejecuta == 1) {
            $objTarea = array();
            $objTarea["codigo"] = $objTareaProg["codigo"];
            $objTarea["nombre"] = $objTareaProg["nombre"];
            $objTarea["descripcion"] = $objTareaProg["descripcion"];
            $objTarea["presupuesto"] = "";
            $objTarea["proveedor"] = "";
            $objTarea["ultimoestado"] = 1;
            $objTarea["idlocal"] = $objTareaProg["idlocal"];
            $objTarea["idcategoria"] = $objTareaProg["idcategoria"];
            $objTarea["prioridad"] = $objTareaProg["prioridad"];
            $objTarea["paraadmin"] = $objTareaProg["paraadmin"];
            $objTarea["idusuario_rol_creador"] = $objTareaProg["idusuario_rol_creador"];
            $objTarea["idusuario_rol_ejecutor"] = $objTareaProg["idusuario_rol_ejecutor"];
            $objTarea["fecha_actualizacion"] = $this->_fecha_actual;
            $objTarea["fecha_registro"] = $this->_fecha_actual;
            $objTarea["estado_registro"] = 1;
            $this->programacionmodel->editar($aProgramacion["idprogramacion"], $auxoProgramacion);
            $idtareaRegistrada = $this->tareamodel->guardar($objTarea);
        }
//        echo "<pre>";
//        var_dump($objTarea);
//        echo "<pre>";
        return $idtareaRegistrada;
    }

    function json_exportarPresupuestos($idlocal, $finicio, $ffin, $montoinicial, $montofinal, $montoigual) {
        // configuramos las propiedades del documento
        $this->load->model('tareamodel');
        $this->load->model('categoriamodel');

        $where = "t.estado_registro = 1";
        if ($idlocal != "-1") {
            $where.=" AND t.idlocal = " . $idlocal;
        }
        if ($montoigual != "-1") {
            $where.=" AND t.presupuesto = " . doubleval($montoigual);
        }
        if ($montoinicial != "-1" && $montofinal != "-1") {
            $where.=" AND t.presupuesto BETWEEN " . doubleval($montoinicial) . " AND " . doubleval($montofinal) . " ";
        }
        if ($finicio != "-1" && $ffin != "-1") {
            $where.=" AND date(t.fecha_registro) BETWEEN '" . getFechaEn($finicio) . "' and '" . getFechaEn($ffin) . "' ";
        }

        // Trayendo datos
        //$aCategorias = $this->categoriamodel->getdata(array("idcategoria", "nombre"), "estado_registro = 1");
        $aCateUsuario = $this->categoriamodel->getaCateUsuario();
        $auxListaTarea = array();
        if ($this->_rol == 3) {
            $auxListaTarea = $this->tareamodel->getListaTareas($where);
        } else {
            $where.=" and (t.idusuario_rol_ejecutor = " . $this->_idusuario_actual . " or t.idusuario_rol_creador = " . $this->_idusuario_actual . ") ";
            $auxListaTarea = $this->tareamodel->getListaTareas($where);
        }

        $aListaTareas = array();
        $aEstados = getEstados();
        if ($auxListaTarea != false) {
            foreach ($auxListaTarea as $itemTarea) {
                // Saber si se envió al administrados o al gestor
                $responsable = $aCateUsuario[$itemTarea->idcategoria]["nomresponsable"];
                if (intval($itemTarea->paraadmin) == 1) {
                    $responsable = "Administrador";
                }

                $datetime1 = date_create($itemTarea->fecha_registro);
                $datetime2 = date_create(date("Y-m-d H:i:s"));
                $fecha = date_diff($datetime1, $datetime2);
                $tiempoTranscurrido = "";
                if ($fecha->y > 0) {
                    $tiempoTranscurrido = $fecha->y . " A ";
                }
                if ($fecha->m > 0) {
                    $tiempoTranscurrido = $fecha->m . " M ";
                }
                if ($fecha->d > 0) {
                    $tiempoTranscurrido = $fecha->d . " d ";
                }
                if ($fecha->h > 0) {
                    $tiempoTranscurrido = $fecha->h . "h ";
                }
                if ($fecha->i > 0) {
                    $tiempoTranscurrido = $fecha->i . "m ";
                }
                //*******************************
                array_push($aListaTareas, array(
                    "idtarea" => $itemTarea->idtarea,
                    "codigo" => $itemTarea->codigo,
                    "proveedor" => $itemTarea->proveedor,
                    "presupuesto" => $itemTarea->presupuesto,
                    "nombre" => $itemTarea->nombre,
                    "nomlocal" => $itemTarea->nomlocal,
                    "nomestado" => $aEstados[$itemTarea->ultimoestado]["nombre"],
                    "nomcategoria" => $aCateUsuario[$itemTarea->idcategoria]["nomcategoria"],
                    "nomresponsable" => $responsable,
                    "fregistro" => getFechaEs($itemTarea->fecha_registro),
                    "factualizacion" => getFechaEs($itemTarea->fecha_actualizacion),
                    "tipo_transcurrido" => $tiempoTranscurrido,
                    "imagen" => $aEstados[$itemTarea->ultimoestado]["imagen"],
                    "enlace" => getEnlace($itemTarea->ultimoestado, $itemTarea->idtarea, $itemTarea->paraadmin, $itemTarea->idusuario_rol_creador)
                ));
            }
        }

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Arkos Noem Arenom")
                ->setLastModifiedBy("Arkos Noem Arenom")
                ->setTitle("Office 2007 XLSX Test Document")
                ->setSubject("Office 2007 XLSX Test Document")
                ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                ->setKeywords("office 2007 openxml php")
                ->setCategory("Test result file");


        $iHeader = 6;
        // agregamos información a las celdas
        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $iHeader, 'Código')
                ->setCellValue('B' . $iHeader, 'Local')
                ->setCellValue('C' . $iHeader, 'Tarea')
                ->setCellValue('D' . $iHeader, 'Categoría')
                ->setCellValue('E' . $iHeader, 'Responsable')
                ->setCellValue('F' . $iHeader, 'Estado')
                ->setCellValue('G' . $iHeader, 'F. Registro')
                ->setCellValue('H' . $iHeader, 'F. Modificación')
                ->setCellValue('I' . $iHeader, 'Proveedor')
                ->setCellValue('J' . $iHeader, 'Presupuesto');

        $styleArray = array(
            'font' => array(
                'bold' => true,
                'color' => array('rgb' => '030303'),
                'size' => 10,
                'name' => 'Verdana'
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'f5f5f5')
        ));

        $objPHPExcel->getActiveSheet()->getStyle('A' . $iHeader)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('B' . $iHeader)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('C' . $iHeader)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('D' . $iHeader)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('E' . $iHeader)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('F' . $iHeader)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('G' . $iHeader)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('H' . $iHeader)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('I' . $iHeader)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('J' . $iHeader)->applyFromArray($styleArray);

        //Añadir una imagen 
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('ZealImg');
        $objDrawing->setDescription('Image inserted by Zeal');
        $objDrawing->setPath(getcwd() . "/media/img/logo_marca.png");
        $objDrawing->setHeight(65);
        $objDrawing->setCoordinates('B2');
        $objDrawing->setOffsetX(10);
        $objDrawing->getShadow()->setVisible(true);
        $objDrawing->getShadow()->setDirection(36);
        $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
        // Llenando el excel
        $i = 7;
        foreach ($aListaTareas as $itemTarea) {
            // La librería puede manejar la codificación de caracteres UTF-8
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $i, $itemTarea ["codigo"])
                    ->setCellValue('B' . $i, $itemTarea ["nomlocal"])
                    ->setCellValue('C' . $i, $itemTarea ["nombre"])
                    ->setCellValue('D' . $i, $itemTarea ["nomcategoria"])
                    ->setCellValue('E' . $i, $itemTarea ["nomresponsable"])
                    ->setCellValue('F' . $i, $itemTarea ["nomestado"])
                    ->setCellValue('G' . $i, $itemTarea ["fregistro"])
                    ->setCellValue('H' . $i, $itemTarea ["factualizacion"])
                    ->setCellValue('I' . $i, $itemTarea ["proveedor"])
                    ->setCellValue('J' . $i, $itemTarea ["presupuesto"]);
            $i++;
        }

        foreach (range('A', 'Z') as $columnID) {

            $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }
        $objPHPExcel->getActiveSheet()->getCell('C3')->setValue('LISTA DE TAREAS');
        $style = array(
            'font' => array(
                'bold' => true,
                'color' => array('rgb' => '030303'),
                'size' => 12,
                'name' => 'Verdana'
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'f5f5f5')
            )
        );

        $objPHPExcel->getActiveSheet()->getStyle("C3:G3")->applyFromArray($style);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('C3:G3');
        // Renombramos la hoja de trabajo
        $objPHPExcel->getActiveSheet()->setTitle('Lista de tareas');


        // configuramos el documento para que la hoja
        // de trabajo número 0 sera la primera en mostrarse
        // al abrir el documento
        $objPHPExcel->setActiveSheetIndex(0);


        // redireccionamos la salida al navegador del cliente (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="tareas.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
    }

    function lista($idmsj = 0, $filtro = 0) {
        validaSession("formulario");
        // Trayendo modelos
        $this->load->model('categoriamodel');
        $this->load->model('tareamodel');

        $mensaje = $this->session->userdata('mensaje');
        if ($mensaje != "") {
            $mensaje = getHtmlMensaje(1, $mensaje);
        }

        $where = "t.estado_registro = 1";
        if (intval($this->session->userdata('param_idusuario')) != 0) {
            $idusuario_param = $this->session->userdata('param_idusuario');
            $idestado_param = $this->session->userdata('param_idestado');
            if ($idestado_param == 3 || $idestado_param == 4) {
                $where .= " and t.idusuario_rol_creador = " . $idusuario_param . " and t.ultimoestado = " . $idestado_param;
            } else {
                $where .= " and t.idusuario_rol_ejecutor = " . $idusuario_param . " and t.ultimoestado = " . $idestado_param;
            }
        }

        // Trayendo datos
        //$aCategorias = $this->categoriamodel->getdata(array("idcategoria", "nombre"), "estado_registro = 1");
        $aCateUsuario = $this->categoriamodel->getaCateUsuario();
        $auxListaTarea = array();
        if ($this->_rol == 3) {
            $auxListaTarea = $this->tareamodel->getListaTareas($where);
        } else {
            $auxListaTarea = $this->tareamodel->getListaTareas("t.estado_registro = 1 and idusuario_rol_ejecutor = " . $this->_idusuario_actual . " or idusuario_rol_creador = " . $this->_idusuario_actual);
        }

        $aListaTareas = array();
        $aEstados = getEstados();
        if ($auxListaTarea != false) {
            foreach ($auxListaTarea as $itemTarea) {
                // Saber si se envió al administrados o al gestor
                $responsable = $aCateUsuario[$itemTarea->idcategoria]["nomresponsable"];
                if (intval($itemTarea->paraadmin) == 1) {
                    $responsable = "Administrador";
                }

                //*******************************
                array_push($aListaTareas, array("idtarea" => $itemTarea->idtarea,
                    "codigo" => $itemTarea->codigo,
                    "proveedor" => $itemTarea->proveedor,
                    "presupuesto" => $itemTarea->presupuesto,
                    "idusuario_creador" => $itemTarea->idusuario_rol_creador,
                    "idusuario_ejecutor" => $itemTarea->idusuario_rol_ejecutor,
                    "nombre" => $itemTarea->nombre,
                    "prioridad" => $itemTarea->prioridad,
                    "nomlocal" => $itemTarea->nomlocal,
                    "nomestado" => $aEstados[$itemTarea->ultimoestado]["nombre"],
                    "nomcategoria" => $aCateUsuario[$itemTarea->idcategoria]["nomcategoria"],
                    "nomresponsable" => $responsable,
                    "fregistro" => getFechaEs($itemTarea->fecha_registro),
                    "factualizacion" => getFechaEs($itemTarea->fecha_actualizacion),
                    "tipo_transcurrido" => getTiempoTranscurrido($itemTarea->fecha_actualizacion, date("Y-m-d H:i:s")),
                    "imagen" => $aEstados[$itemTarea->ultimoestado]["imagen"],
                    "enlace" => getEnlace($itemTarea->ultimoestado, $itemTarea->idtarea, $itemTarea->paraadmin, $itemTarea->idusuario_rol_creador)
                ));
            }
        }

        // Recibiendo parametros
        //$codMenu = $this->input->post('codmenu');

        $this->session->set_userdata('param_idusuario', "");
        $this->session->set_userdata('param_idestado', "");
        $data['titulo'] = "Lista de tareas";
        $data['divmensaje'] = $mensaje;
        $this->load->model('usuariomodel');
        $oUsuario = $this->usuariomodel->getUsuarios();
        $data['oUsuario'] = $oUsuario;
        $data['aListaTareas'] = $aListaTareas;

        $botones = "";
        if ($this->_rol == 3) {
            $botones.='<a href="' . base_url() . 'panel/resumengestores" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-indent-left"></span> Resúmenes</a> ';
        }
        if ($this->_rol != 1) {
            $botones.='<a href="' . base_url() . 'tarea/listaprogramadas" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-calendar"></span> Programación de tareas</a>';
            $botones.='  <a href="#" id="btnexportar" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-download-alt"></span> Exportar</a>';
        }
        $botones.='  <a href="' . base_url() . 'tarea/crear" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-file"></span> Nuevo</a>';
        $botones.='  <a href="#" id="btnbuscaropcion" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-search"></span> Buscar</a>';


        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Lista de tareas", TRUE);
        $this->template->write_view('content', 'tarea/listar', $data, TRUE);
        $this->template->render();
    }

    public function get_div_busqueda() {
        $this->load->model('localmodel');
        $aLocales = $this->localmodel->getdata(array("idlocal", "nombre"), "estado_registro = 1");
        $data["aLocales"] = $aLocales;
        $this->load->view("tarea/busqueda", $data);
    }

    function crear($idmsj = 0) {
// Trayendo modelos
        validaSession("formulario");
        $this->load->model('categoriamodel');
        $this->load->model('tareamodel');
        $this->load->model('localmodel');
        $this->load->model('archivomodel');

// Trayendo datos
        $rol = $this->session->userdata('rol');
        $aCategorias = $this->categoriamodel->getdata(array("idcategoria", "nombre"), "estado_registro = 1");
        $aLocales = array();
        if ($rol != 1) {
            $aLocales = $this->localmodel->getdata(array("idlocal", "nombre"), "estado_registro = 1");
        }
        $data['titulo'] = "Nueva tarea";
        $data['aCategorias'] = $aCategorias;
        $data['aLocales'] = $aLocales;
        $data['divmensaje'] = getHtmlMensaje($idmsj);
        $this->template->write('botones', '<a href = "' . base_url() . 'tarea/lista" class = "btn btn-sm btn-default" >Atras</a>', TRUE);
        $this->template->write('titleheader', "Nueva tarea", TRUE);
        $this->template->write_view('content', 'tarea/crear', $data, TRUE);
        $this->template->render();
    }

    function poratender($idtarea = 0) {
        validaSession("formulario");
        // Trayendo modelos
        $this->load->model('categoriamodel');
        $this->load->model('tareamodel');
        $this->load->model('tareaestadomodel');
        $this->load->model('localmodel');
        $this->load->model('archivomodel');

        // Trayendo datos
        $auxObjTarea = $this->tareamodel->getListaTareas("t.estado_registro = 1 and t.idtarea = " . $idtarea, 1);
        $divmensaje = "";
        if ($auxObjTarea->ultimoestado != 1) {
            $divmensaje = "Ya se registró esta actividad";
        }
        $aEstados = getEstados();
        $aCateUsuario = $this->categoriamodel->getaCateUsuario();

        // ACTUALIZANDO LA DATA TAREAESTADOS A ENVIAR
        $aTareaEstados = array();
        $this->load->model('usuariomodel');
        $objUsuario = $this->usuariomodel->getUsuarios();
        $auxTareaEstados = $this->tareaestadomodel->getTareaEstados($idtarea);

        if ($auxTareaEstados != false) {

            $ultimaFecha = $auxObjTarea->fecha_registro;
            foreach ($auxTareaEstados as $itemTareaEstado) {
                array_push($aTareaEstados, array("nomestado" => $aEstados[$itemTareaEstado->idestado]["gerundio"],
                    "comentario" => $itemTareaEstado->comentario,
                    "presupuesto" => $itemTareaEstado->presupuesto,
                    "nomusuario" => $objUsuario[$itemTareaEstado->idusuario]["nombre"],
                    "fregistro" => getFechaEs($itemTareaEstado->fecha_registro),
                    "transcurrido" => getTiempoTranscurrido($ultimaFecha, $itemTareaEstado->fecha_registro),
                    "imagen" => $objUsuario[$itemTareaEstado->idusuario]["foto"],
                    "archivos" => $itemTareaEstado->archivos
                ));

                $ultimaFecha = $itemTareaEstado->fecha_registro;
            }
        }

        if ($auxObjTarea != false) {
// Saber si se envió al administrados o al gestor
            $responsable = $aCateUsuario[$auxObjTarea->idcategoria]["nomresponsable"];
            if (intval($auxObjTarea->paraadmin) == 1) {
                $responsable = "Administrador";
            }

            $datetime1 = date_create($auxObjTarea->fecha_registro);
            $datetime2 = date_create(date("Y-m-d H:i:s"));
            $fecha = date_diff($datetime1, $datetime2);
            $tiempoTranscurrido = "";
            if ($fecha->y > 0) {
                $tiempoTranscurrido = $fecha->y . " A ";
            }
            if ($fecha->m > 0) {
                $tiempoTranscurrido = $fecha->m . " M ";
            }
            if ($fecha->d > 0) {
                $tiempoTranscurrido = $fecha->d . " d ";
            }
            if ($fecha->h > 0) {
                $tiempoTranscurrido = $fecha->h . "h ";
            }
            if ($fecha->i > 0) {
                $tiempoTranscurrido = $fecha->i . "m ";
            }
//*******************************
            $objTarea["idtarea"] = $auxObjTarea->idtarea;
            $objTarea["codigo"] = $auxObjTarea->codigo;
            $objTarea["nombre"] = $auxObjTarea->nombre;
            $objTarea["presupuesto"] = $auxObjTarea->presupuesto;
            $objTarea["proveedor"] = $auxObjTarea->proveedor;
            $objTarea["nomlocal"] = $auxObjTarea->nomlocal;
            $objTarea["descripcion"] = $auxObjTarea->descripcion;
            $objTarea["nomestado"] = $aEstados[$auxObjTarea->ultimoestado]["nombre"];
            $objTarea["nomcategoria"] = $aCateUsuario[$auxObjTarea->idcategoria]["nomcategoria"];
            $objTarea["nomresponsable"] = $responsable;
            $objTarea["fregistro"] = getFechaEs($auxObjTarea->fecha_registro);
            $objTarea["factualizacion"] = getFechaEs($auxObjTarea->fecha_actualizacion);
            $objTarea["tipo_transcurrido"] = $tiempoTranscurrido;
            $objTarea["imagen"] = $aEstados[$auxObjTarea->ultimoestado]["imagen"];
            $objTarea["enlace"] = getEnlace($auxObjTarea->ultimoestado, $auxObjTarea->idtarea, $auxObjTarea->paraadmin, $auxObjTarea->idusuario_rol_creador);
        }

// Trayendo los archivos de la tarea
        $aArchivosTarea = array();
        $aArchivosTarea = $this->archivomodel->getArchivosTarea($idtarea);

        $data['titulo'] = "Por atender";
        $data['divmensaje'] = $divmensaje;
        $data['objTarea'] = $objTarea;
        $data['aTareaEstados'] = $aTareaEstados;
        $data['aArchivosTarea'] = $aArchivosTarea;
        $this->template->write('botones', '<a href = "' . base_url() . 'tarea/lista" class = "btn btn-sm btn-default" >Atras</a>', TRUE);
        $this->template->write('titleheader', "Por atender", TRUE);
        $this->template->write_view('content', 'tarea/poratender', $data, TRUE);
        $this->template->render();
    }

    function ejecutar($idtarea = 0) {
        validaSession("formulario");
        // Trayendo modelos
        $this->load->model('categoriamodel');
        $this->load->model('tareamodel');
        $this->load->model('tareaestadomodel');
        $this->load->model('localmodel');
        $this->load->model('archivomodel');
        $objTarea = array();
        $aTareaEstados = array();
        $aArchivosTarea = array();
        $divmensaje = "";
        // Trayendo datos
        $auxObjTarea = $this->tareamodel->getListaTareas("t.estado_registro = 1 and t.idtarea = " . $idtarea, 1);

        if ($auxObjTarea != FALSE) {
            if ($auxObjTarea->ultimoestado == 2) {
                $aEstados = getEstados();
                $aCateUsuario = $this->categoriamodel->getaCateUsuario();

                // ACTUALIZANDO LA DATA TAREAESTADOS A ENVIAR
                $this->load->model('usuariomodel');
                $objUsuario = $this->usuariomodel->getUsuarios();

                $auxTareaEstados = $this->tareaestadomodel->getTareaEstados($idtarea);
                if ($auxTareaEstados != false) {
                    $ultimaFecha = $auxObjTarea->fecha_registro;
                    foreach ($auxTareaEstados as $itemTareaEstado) {
                        array_push($aTareaEstados, array("nomestado" => $aEstados[$itemTareaEstado->idestado]["gerundio"],
                            "comentario" => $itemTareaEstado->comentario,
                            "presupuesto" => $itemTareaEstado->presupuesto,
                            "nomusuario" => $objUsuario[$itemTareaEstado->idusuario]["nombre"],
                            "fregistro" => getFechaEs($itemTareaEstado->fecha_registro),
                            "transcurrido" => getTiempoTranscurrido($ultimaFecha, $itemTareaEstado->fecha_registro),
                            "imagen" => $objUsuario[$itemTareaEstado->idusuario]["foto"],
                            "archivos" => $itemTareaEstado->archivos
                        ));

                        $ultimaFecha = $itemTareaEstado->fecha_registro;
                    }
                }

                if ($auxObjTarea != false) {
// Saber si se envió al administrados o al gestor
                    $responsable = $aCateUsuario[$auxObjTarea->idcategoria]["nomresponsable"];
                    if (intval($auxObjTarea->paraadmin) == 1) {
                        $responsable = "Administrador";
                    }

                    $datetime1 = date_create($auxObjTarea->fecha_registro);
                    $datetime2 = date_create(date("Y-m-d H:i:s"));
                    $fecha = date_diff($datetime1, $datetime2);
                    $tiempoTranscurrido = "";
                    if ($fecha->y > 0) {
                        $tiempoTranscurrido = $fecha->y . " A ";
                    }
                    if ($fecha->m > 0) {
                        $tiempoTranscurrido = $fecha->m . " M ";
                    }
                    if ($fecha->d > 0) {
                        $tiempoTranscurrido = $fecha->d . " d ";
                    }
                    if ($fecha->h > 0) {
                        $tiempoTranscurrido = $fecha->h . "h ";
                    }
                    if ($fecha->i > 0) {
                        $tiempoTranscurrido = $fecha->i . "m ";
                    }
//*******************************
                    $objTarea["idtarea"] = $auxObjTarea->idtarea;
                    $objTarea["codigo"] = $auxObjTarea->codigo;
                    $objTarea["nombre"] = $auxObjTarea->nombre;
                    $objTarea["presupuesto"] = $auxObjTarea->presupuesto;
                    $objTarea["proveedor"] = $auxObjTarea->proveedor;
                    $objTarea["nomlocal"] = $auxObjTarea->nomlocal;
                    $objTarea["descripcion"] = $auxObjTarea->descripcion;
                    $objTarea["nomestado"] = $aEstados[$auxObjTarea->ultimoestado]["nombre"];
                    $objTarea["nomcategoria"] = $aCateUsuario[$auxObjTarea->idcategoria]["nomcategoria"];
                    $objTarea["nomresponsable"] = $responsable;
                    $objTarea["fregistro"] = getFechaEs($auxObjTarea->fecha_registro);
                    $objTarea["factualizacion"] = getFechaEs($auxObjTarea->fecha_actualizacion);
                    $objTarea["tipo_transcurrido"] = $tiempoTranscurrido;
                    $objTarea["imagen"] = $aEstados[$auxObjTarea->ultimoestado]["imagen"];
                    $objTarea["enlace"] = getEnlace($auxObjTarea->ultimoestado, $auxObjTarea->idtarea, $auxObjTarea->paraadmin, $auxObjTarea->idusuario_rol_creador);
                }

// Trayendo los archivos de la tarea
                $aArchivosTarea = array();
                $aArchivosTarea = $this->archivomodel->getArchivosTarea($idtarea);
            } else {
                $divmensaje.="Ya se realizó esta actividad";
            }
        } else {
            $divmensaje.="No existe esta tarea";
        }

        $data['titulo'] = "Por ejecutar";
        $data['divmensaje'] = $divmensaje;
        $data['objTarea'] = $objTarea;
        $data['aTareaEstados'] = $aTareaEstados;
        $data['aArchivosTarea'] = $aArchivosTarea;
        $this->template->write('botones', '<a href = "' . base_url() . 'tarea/lista" class = "btn btn-sm btn-default" >Atras</a>', TRUE);
        $this->template->write('titleheader', "Por ejecutar", TRUE);
        $this->template->write_view('content', 'tarea/ejecutar', $data, TRUE);
        $this->template->render();
    }

    function corregir_ejecucion($idtarea = 0) {
        validaSession("formulario");
        // Trayendo modelos
        $this->load->model('categoriamodel');
        $this->load->model('tareamodel');
        $this->load->model('tareaestadomodel');
        $this->load->model('localmodel');
        $this->load->model('archivomodel');
        $objTarea = array();
        $aTareaEstados = array();
        $aArchivosTarea = array();
        $divmensaje = "";
        // Trayendo datos
        $auxObjTarea = $this->tareamodel->getListaTareas("t.estado_registro = 1 and t.idtarea = " . $idtarea, 1);

        if ($auxObjTarea != FALSE) {
            if ($auxObjTarea->ultimoestado == 6) {
                $aEstados = getEstados();
                $aCateUsuario = $this->categoriamodel->getaCateUsuario();

                // ACTUALIZANDO LA DATA TAREAESTADOS A ENVIAR
                $this->load->model('usuariomodel');
                $objUsuario = $this->usuariomodel->getUsuarios();

                $auxTareaEstados = $this->tareaestadomodel->getTareaEstados($idtarea);
                if ($auxTareaEstados != false) {
                    $ultimaFecha = $auxObjTarea->fecha_registro;
                    foreach ($auxTareaEstados as $itemTareaEstado) {
                        array_push($aTareaEstados, array("nomestado" => $aEstados[$itemTareaEstado->idestado]["gerundio"],
                            "comentario" => $itemTareaEstado->comentario,
                            "presupuesto" => $itemTareaEstado->presupuesto,
                            "nomusuario" => $objUsuario[$itemTareaEstado->idusuario]["nombre"],
                            "fregistro" => getFechaEs($itemTareaEstado->fecha_registro),
                            "transcurrido" => getTiempoTranscurrido($ultimaFecha, $itemTareaEstado->fecha_registro),
                            "imagen" => $objUsuario[$itemTareaEstado->idusuario]["foto"],
                            "archivos" => $itemTareaEstado->archivos
                        ));

                        $ultimaFecha = $itemTareaEstado->fecha_registro;
                    }
                }

                if ($auxObjTarea != false) {
// Saber si se envió al administrados o al gestor
                    $responsable = $aCateUsuario[$auxObjTarea->idcategoria]["nomresponsable"];
                    if (intval($auxObjTarea->paraadmin) == 1) {
                        $responsable = "Administrador";
                    }

                    $datetime1 = date_create($auxObjTarea->fecha_registro);
                    $datetime2 = date_create(date("Y-m-d H:i:s"));
                    $fecha = date_diff($datetime1, $datetime2);
                    $tiempoTranscurrido = "";
                    if ($fecha->y > 0) {
                        $tiempoTranscurrido = $fecha->y . " A ";
                    }
                    if ($fecha->m > 0) {
                        $tiempoTranscurrido = $fecha->m . " M ";
                    }
                    if ($fecha->d > 0) {
                        $tiempoTranscurrido = $fecha->d . " d ";
                    }
                    if ($fecha->h > 0) {
                        $tiempoTranscurrido = $fecha->h . "h ";
                    }
                    if ($fecha->i > 0) {
                        $tiempoTranscurrido = $fecha->i . "m ";
                    }
//*******************************
                    $objTarea["idtarea"] = $auxObjTarea->idtarea;
                    $objTarea["codigo"] = $auxObjTarea->codigo;
                    $objTarea["nombre"] = $auxObjTarea->nombre;
                    $objTarea["presupuesto"] = $auxObjTarea->presupuesto;
                    $objTarea["proveedor"] = $auxObjTarea->proveedor;
                    $objTarea["nomlocal"] = $auxObjTarea->nomlocal;
                    $objTarea["descripcion"] = $auxObjTarea->descripcion;
                    $objTarea["nomestado"] = $aEstados[$auxObjTarea->ultimoestado]["nombre"];
                    $objTarea["nomcategoria"] = $aCateUsuario[$auxObjTarea->idcategoria]["nomcategoria"];
                    $objTarea["nomresponsable"] = $responsable;
                    $objTarea["fregistro"] = getFechaEs($auxObjTarea->fecha_registro);
                    $objTarea["factualizacion"] = getFechaEs($auxObjTarea->fecha_actualizacion);
                    $objTarea["tipo_transcurrido"] = $tiempoTranscurrido;
                    $objTarea["imagen"] = $aEstados[$auxObjTarea->ultimoestado]["imagen"];
                    $objTarea["enlace"] = getEnlace($auxObjTarea->ultimoestado, $auxObjTarea->idtarea, $auxObjTarea->paraadmin, $auxObjTarea->idusuario_rol_creador);
                }

// Trayendo los archivos de la tarea
                $aArchivosTarea = array();
                $aArchivosTarea = $this->archivomodel->getArchivosTarea($idtarea);
            } else {
                $divmensaje.="Ya se realizó esta actividad";
            }
        } else {
            $divmensaje.="No existe esta tarea";
        }

        $data['titulo'] = "Por ejecutar";
        $data['divmensaje'] = $divmensaje;
        $data['objTarea'] = $objTarea;
        $data['aTareaEstados'] = $aTareaEstados;
        $data['aArchivosTarea'] = $aArchivosTarea;
        $this->template->write('botones', '<a href = "' . base_url() . 'tarea/lista" class = "btn btn-sm btn-default" >Atras</a>', TRUE);
        $this->template->write('titleheader', "Corregir ejecución", TRUE);
        $this->template->write_view('content', 'tarea/corregirejecucion', $data, TRUE);
        $this->template->render();
    }

    function editar($idtarea = 0) {
        validaSession("formulario");
// Trayendo modelos
        $this->load->model('categoriamodel');
        $this->load->model('tareamodel');
        $this->load->model('tareaestadomodel');
        $this->load->model('localmodel');
        $this->load->model('archivomodel');
        $msg_error = "";
        $aCategorias = $this->categoriamodel->getdata(array("idcategoria", "nombre"), "estado_registro = 1");
        $aLocales = array();
        if ($this->_rol != 1) {
            $aLocales = $this->localmodel->getdata(array("idlocal", "nombre"), "estado_registro = 1");
        }
// Trayendo datos
        $auxObjTarea = array();
        $aArchivosTarea = array();
        $objTarea = array();
        $auxObjTarea = $this->tareamodel->getListaTareas("t.estado_registro = 1 and t.idtarea = " . intval($idtarea), 1);
        if ($auxObjTarea != false) {
            if ($this->_rol == 1) {
                if ($auxObjTarea->ultimoestado == 1 || $auxObjTarea->ultimoestado == 3) {
                    $objTarea["idtarea"] = $auxObjTarea->idtarea;
                    $objTarea["codigo"] = $auxObjTarea->codigo;
                    $objTarea["nombre"] = $auxObjTarea->nombre;
                    $objTarea["prioridad"] = $auxObjTarea->prioridad;
                    $objTarea["presupuesto"] = $auxObjTarea->presupuesto;
                    $objTarea["proveedor"] = $auxObjTarea->proveedor;
                    $objTarea["idlocal"] = $auxObjTarea->idlocal;
                    $objTarea["descripcion"] = $auxObjTarea->descripcion;
                    $objTarea["idcategoria"] = $auxObjTarea->idcategoria;
                    $objTarea["paraadmin"] = $auxObjTarea->paraadmin;
                    $aArchivosTarea = $this->archivomodel->getArchivosTarea($idtarea);
                } else {
                    $msg_error.="No puede editar, tarea ya atendida";
                }
            } else {
                $objTarea["paraadmin"] = $auxObjTarea->paraadmin;
                $objTarea["idtarea"] = $auxObjTarea->idtarea;
                $objTarea["codigo"] = $auxObjTarea->codigo;
                $objTarea["nombre"] = $auxObjTarea->nombre;
                $objTarea["prioridad"] = $auxObjTarea->prioridad;
                $objTarea["presupuesto"] = $auxObjTarea->presupuesto;
                $objTarea["proveedor"] = $auxObjTarea->proveedor;
                $objTarea["idlocal"] = $auxObjTarea->idlocal;
                $objTarea["descripcion"] = $auxObjTarea->descripcion;
                $objTarea["idcategoria"] = $auxObjTarea->idcategoria;
                $aArchivosTarea = $this->archivomodel->getArchivosTarea($idtarea);
            }
        } else {
            $msg_error.="No existe el id de la tarea";
        }

        $aTareaEstados = array();
        if ($auxObjTarea->ultimoestado != 1) {
            // ACTUALIZANDO LA DATA TAREAESTADOS A ENVIAR
            $this->load->model('usuariomodel');
            $objUsuario = $this->usuariomodel->getUsuarios();
            $aEstados = getEstados();

            $auxTareaEstados = $this->tareaestadomodel->getTareaEstados($idtarea);
            if ($auxTareaEstados != false) {
                $ultimaFecha = $auxObjTarea->fecha_registro;
                foreach ($auxTareaEstados as $itemTareaEstado) {
                    array_push($aTareaEstados, array("nomestado" => $aEstados[$itemTareaEstado->idestado]["gerundio"],
                        "comentario" => $itemTareaEstado->comentario,
                        "presupuesto" => $itemTareaEstado->presupuesto,
                        "nomusuario" => $objUsuario[$itemTareaEstado->idusuario]["nombre"],
                        "fregistro" => getFechaEs($itemTareaEstado->fecha_registro),
                        "transcurrido" => getTiempoTranscurrido($ultimaFecha, $itemTareaEstado->fecha_registro),
                        "imagen" => $objUsuario[$itemTareaEstado->idusuario]["foto"],
                        "archivos" => $itemTareaEstado->archivos
                    ));

                    $ultimaFecha = $itemTareaEstado->fecha_registro;
                }
            }
        }



        // Trayendo los archivos de la tarea
        $htmlMensaje = "";
        if ($msg_error != "") {
            $htmlMensaje = getHtmlMensaje(2, $msg_error);
        }
        $data['titulo'] = "Editando tarea";
        $data['divmensaje'] = $htmlMensaje;
        $data['objTarea'] = $objTarea;
        $data['aArchivosTarea'] = $aArchivosTarea;
        $data['aCategorias'] = $aCategorias;
        $data['aLocales'] = $aLocales;
        $data['aTareaEstados'] = $aTareaEstados;
        $this->template->write('botones', '<a href = "' . base_url() . 'tarea/lista" class = "btn btn-sm btn-default">Atras</a>', TRUE);
        $this->template->write('titleheader', "Editar tarea", TRUE);
        $this->template->write_view('content', 'tarea/editar', $data, TRUE);
        $this->template->render();
    }

    function corregirtarea($idtarea = 0) {
        validaSession("formulario");
// Trayendo modelos
        $this->load->model('categoriamodel');
        $this->load->model('tareamodel');
        $this->load->model('tareaestadomodel');
        $this->load->model('localmodel');
        $this->load->model('archivomodel');
        $msg_error = "";
        $aCategorias = $this->categoriamodel->getdata(array("idcategoria", "nombre"), "estado_registro = 1");
        $aLocales = array();
        if ($this->_rol != 1) {
            $aLocales = $this->localmodel->getdata(array("idlocal", "nombre"), "estado_registro = 1");
        }
// Trayendo datos
        $auxObjTarea = array();
        $aArchivosTarea = array();
        $objTarea = array();
        $auxObjTarea = $this->tareamodel->getListaTareas("t.estado_registro = 1 and t.idtarea = " . intval($idtarea), 1);
        if ($auxObjTarea != false) {
            if ($this->_rol == 1) {
                if ($auxObjTarea->ultimoestado == 1 || $auxObjTarea->ultimoestado == 3) {
                    $objTarea["idtarea"] = $auxObjTarea->idtarea;
                    $objTarea["codigo"] = $auxObjTarea->codigo;
                    $objTarea["nombre"] = $auxObjTarea->nombre;
                    $objTarea["prioridad"] = $auxObjTarea->prioridad;
                    $objTarea["presupuesto"] = $auxObjTarea->presupuesto;
                    $objTarea["proveedor"] = $auxObjTarea->proveedor;
                    $objTarea["idlocal"] = $auxObjTarea->idlocal;
                    $objTarea["descripcion"] = $auxObjTarea->descripcion;
                    $objTarea["idcategoria"] = $auxObjTarea->idcategoria;
                    $aArchivosTarea = $this->archivomodel->getArchivosTarea($idtarea);
                } else {
                    $msg_error.="No puede editar, tarea ya atendida";
                }
            } else {
                $objTarea["idtarea"] = $auxObjTarea->idtarea;
                $objTarea["codigo"] = $auxObjTarea->codigo;
                $objTarea["nombre"] = $auxObjTarea->nombre;
                $objTarea["prioridad"] = $auxObjTarea->prioridad;
                $objTarea["presupuesto"] = $auxObjTarea->presupuesto;
                $objTarea["proveedor"] = $auxObjTarea->proveedor;
                $objTarea["idlocal"] = $auxObjTarea->idlocal;
                $objTarea["descripcion"] = $auxObjTarea->descripcion;
                $objTarea["idcategoria"] = $auxObjTarea->idcategoria;
                $aArchivosTarea = $this->archivomodel->getArchivosTarea($idtarea);
            }
        } else {
            $msg_error.="No existe el id de la tarea";
        }

        $aTareaEstados = array();
        if ($auxObjTarea->ultimoestado != 1) {
            // ACTUALIZANDO LA DATA TAREAESTADOS A ENVIAR
            $this->load->model('usuariomodel');
            $objUsuario = $this->usuariomodel->getUsuarios();
            $aEstados = getEstados();

            $auxTareaEstados = $this->tareaestadomodel->getTareaEstados($idtarea);
            if ($auxTareaEstados != false) {
                $ultimaFecha = $auxObjTarea->fecha_registro;
                foreach ($auxTareaEstados as $itemTareaEstado) {
                    array_push($aTareaEstados, array("nomestado" => $aEstados[$itemTareaEstado->idestado]["gerundio"],
                        "comentario" => $itemTareaEstado->comentario,
                        "presupuesto" => $itemTareaEstado->presupuesto,
                        "nomusuario" => $objUsuario[$itemTareaEstado->idusuario]["nombre"],
                        "fregistro" => getFechaEs($itemTareaEstado->fecha_registro),
                        "transcurrido" => getTiempoTranscurrido($ultimaFecha, $itemTareaEstado->fecha_registro),
                        "imagen" => $objUsuario[$itemTareaEstado->idusuario]["foto"],
                        "archivos" => $itemTareaEstado->archivos
                    ));

                    $ultimaFecha = $itemTareaEstado->fecha_registro;
                }
            }
        }



        // Trayendo los archivos de la tarea
        $htmlMensaje = "";
        if ($msg_error != "") {
            $htmlMensaje = getHtmlMensaje(2, $msg_error);
        }
        $data['titulo'] = "Editando tarea";
        $data['divmensaje'] = $htmlMensaje;
        $data['objTarea'] = $objTarea;
        $data['aArchivosTarea'] = $aArchivosTarea;
        $data['aCategorias'] = $aCategorias;
        $data['aLocales'] = $aLocales;
        $data['aTareaEstados'] = $aTareaEstados;
        $this->template->write('botones', '<a href = "' . base_url() . 'tarea/lista" class = "btn btn-sm btn-default">Atras</a>', TRUE);
        $this->template->write('titleheader', "Editar tarea", TRUE);
        $this->template->write_view('content', 'tarea/corregirtarea', $data, TRUE);
        $this->template->render();
    }

    function listaprogramadas() {
        validaSession("formulario");
        // Trayendo modelos
        $this->load->model('usuariomodel');
        $this->load->model('categoriamodel');
        $this->load->model('tareamodel');
        $this->load->model('programacionmodel');
        $this->load->model('localmodel');
        $this->load->model('archivomodel');
        $msg_error = "";
        $auxListaTareaProg = $this->programacionmodel->getProgramacion();
        $aUsuarios = $this->usuariomodel->getUsuarios();
        $aLocales = $this->localmodel->getLocales();
        $aCateUsuario = $this->categoriamodel->getaCateUsuario();

        $aListaTareas = array();
        if ($auxListaTareaProg != false) {
            foreach ($auxListaTareaProg as $itemTarea) {
                // Saber si se envió al administrados o al gestor
                $responsable = $aUsuarios[$itemTarea->idusuario_rol_creador]["nombre"];
                $datetime1 = date_create($itemTarea->fecha_ultima_creada);
                $datetime2 = date_create(date("Y-m-d H:i:s"));
                $fecha = date_diff($datetime1, $datetime2);
                $tiempoTranscurrido = "";
                if ($fecha->y > 0) {
                    $tiempoTranscurrido = $fecha->y . " A ";
                }
                if ($fecha->m > 0) {
                    $tiempoTranscurrido = $fecha->m . " M ";
                }
                if ($fecha->d > 0) {
                    $tiempoTranscurrido = $fecha->d . " d ";
                }
                if ($fecha->h > 0) {
                    $tiempoTranscurrido = $fecha->h . "h ";
                }
                if ($fecha->i > 0) {
                    $tiempoTranscurrido = $fecha->i . "m ";
                }
                $fechaUltimaCreada = "No ejecutada";
                if ($itemTarea->fecha_ultima_creada != "") {
                    $fechaUltimaCreada = getFechaEs($itemTarea->fecha_ultima_creada);
                }
                //*******************************

                array_push($aListaTareas, array("idprogramacion" => $itemTarea->idprogramacion,
                    "codigo" => $itemTarea->codigo,
                    "repeticion" => $itemTarea->repeticion,
                    "fecha_siguiente" => getFechaEs2($itemTarea->fecha_siguiente),
                    "lunes" => $itemTarea->lunes,
                    "martes" => $itemTarea->martes,
                    "estado_registro" => $itemTarea->estado_registro,
                    "miercoles" => $itemTarea->miercoles,
                    "jueves" => $itemTarea->jueves,
                    "viernes" => $itemTarea->viernes,
                    "sabado" => $itemTarea->sabado,
                    "domingo" => $itemTarea->domingo,
                    "proveedor" => $itemTarea->proveedor,
                    "presupuesto" => $itemTarea->presupuesto,
                    "nombre" => $itemTarea->nombre,
                    "nomlocal" => $aLocales[$itemTarea->idlocal]["nombre"],
                    "nomcategoria" => $aCateUsuario[$itemTarea->idcategoria]["nomcategoria"],
                    "nomresponsable" => $responsable,
                    "finicio" => getFechaEs2($itemTarea->fecha_inicio),
                    "ffin" => getFechaEs2($itemTarea->fecha_fin),
                    "fultimaejecucion" => $fechaUltimaCreada,
                    "tipo_transcurrido" => $tiempoTranscurrido,
                    "enlace" => ""
                ));
            }
        }
        $data['titulo'] = "Programación de tareas (Preventivas)";
        $data['aProgramacion'] = $aListaTareas;
        $botones = ' <a href = "' . base_url() . 'tarea/lista" class = "btn btn-sm btn-default">Atras</a>';
        $botones .= ' <a href = "' . base_url() . 'tarea/programar" class = "btn btn-sm btn-default">Nuevo</a>';
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Programación de tareas", TRUE);
        $this->template->write_view('content', 'tarea/listaprogramadas', $data, TRUE);
        $this->template->render();
    }

    function editarprog($idprogramacion = 0) {
        validaSession("formulario");
        // Trayendo modelos
        $this->load->model('categoriamodel');
        $this->load->model('programacionmodel');
        $this->load->model('tareaprogmodel');
        $this->load->model('archivomodel');
        $this->load->model('localmodel');
        $msg_error = "";
        $aCategorias = $this->categoriamodel->getdata(array("idcategoria", "nombre"), "estado_registro = 1");
        $aLocales = $this->localmodel->getdata(array("idlocal", "nombre"), "estado_registro = 1");
        $aProgramacion = $this->programacionmodel->getdata(array("*"), "idprogramacion = " . $idprogramacion, 1);
        $aTareaProg = $this->tareaprogmodel->getdata(array("*"), "idtareaprog = " . $aProgramacion->idtareaprog, 1);
        $aArchivos = $this->archivomodel->getdata(array("*"), "idprogramacion = " . $aProgramacion->idprogramacion);

        $data['titulo'] = "Editar programacion";
        $data['aProgramacion'] = $aProgramacion;
        $data['aTareaProg'] = $aTareaProg;
        $data['aCategorias'] = $aCategorias;
        $data['aArchivos'] = $aArchivos;
        $data['aLocales'] = $aLocales;
        $this->template->write('botones', '<a href = "' . base_url() . 'tarea/listaprogramadas" class = "btn btn-sm btn-default">Atras</a>', TRUE);
        $this->template->write('titleheader', "Editar programacion", TRUE);
        $this->template->write_view('content', 'tarea/editarprog', $data, TRUE);
        $this->template->render();
    }

    function programar() {
        validaSession("formulario");
        // Trayendo modelos
        $this->load->model('categoriamodel');
        $this->load->model('localmodel');
        $msg_error = "";
        $aCategorias = $this->categoriamodel->getdata(array("idcategoria", "nombre"), "estado_registro = 1");
        $aLocales = $this->localmodel->getdata(array("idlocal", "nombre"), "estado_registro = 1");
        $data['titulo'] = "Programar tarea";
        $data['aCategorias'] = $aCategorias;
        $data['aLocales'] = $aLocales;
        $this->template->write('botones', '<a href = "' . base_url() . 'tarea/listaprogramadas" class = "btn btn-sm btn-default">Atras</a>', TRUE);
        $this->template->write('titleheader', "Programar tarea", TRUE);
        $this->template->write_view('content', 'tarea/programar', $data, TRUE);
        $this->template->render();
    }

    function ver($idtarea = 0) {
        validaSession("formulario");
// Trayendo modelos
        $this->load->model('categoriamodel');
        $this->load->model('tareamodel');
        $this->load->model('tareaestadomodel');
        $this->load->model('localmodel');
        $this->load->model('archivomodel');

        // Trayendo datos
        $auxObjTarea = $this->tareamodel->getListaTareas("t.estado_registro = 1 and t.idtarea = " . $idtarea . "", 1);
        $aEstados = getEstados();
        $aCateUsuario = $this->categoriamodel->getaCateUsuario();

        // ACTUALIZANDO LA DATA TAREAESTADOS A ENVIAR
        $this->load->model('usuariomodel');
        $objUsuario = $this->usuariomodel->getUsuarios();

        $aTareaEstados = array();
        $auxTareaEstados = $this->tareaestadomodel->getTareaEstados($idtarea);
        if ($auxTareaEstados != false) {
            $ultimaFecha = $auxObjTarea->fecha_registro;
            foreach ($auxTareaEstados as $itemTareaEstado) {

                array_push($aTareaEstados, array("nomestado" => $aEstados[$itemTareaEstado->idestado]["gerundio"],
                    "comentario" => $itemTareaEstado->comentario,
                    "nomusuario" => $objUsuario[$itemTareaEstado->idusuario]["nombre"],
                    "presupuesto" => $itemTareaEstado->presupuesto,
                    "fregistro" => getFechaEs($itemTareaEstado->fecha_registro),
                    "transcurrido" => getTiempoTranscurrido($ultimaFecha, $itemTareaEstado->fecha_registro),
                    "imagen" => $objUsuario[$itemTareaEstado->idusuario]["foto"],
                    "archivos" => $itemTareaEstado->archivos
                ));
                $ultimaFecha = $itemTareaEstado->fecha_registro;
            }
        }

        if ($auxObjTarea != false) {
            // Saber si se envió al administrados o al gestor
            $responsable = $aCateUsuario[$auxObjTarea->idcategoria]["nomresponsable"];
            if (intval($auxObjTarea->paraadmin) == 1) {
                $responsable = "Administrador";
            }

            //*******************************
            $objTarea["idtarea"] = $auxObjTarea->idtarea;
            $objTarea["codigo"] = $auxObjTarea->codigo;
            $objTarea["nombre"] = $auxObjTarea->nombre;
            $objTarea["nomlocal"] = $auxObjTarea->nomlocal;
            $objTarea["presupuesto"] = $auxObjTarea->presupuesto;
            $objTarea["proveedor"] = $auxObjTarea->proveedor;
            $objTarea["descripcion"] = $auxObjTarea->descripcion;
            $objTarea["nomestado"] = $aEstados[$auxObjTarea->ultimoestado]["nombre"];
            $objTarea["nomcategoria"] = $aCateUsuario[$auxObjTarea->idcategoria]["nomcategoria"];
            $objTarea["nomresponsable"] = $responsable;
            $objTarea["fregistro"] = getFechaEs($auxObjTarea->fecha_registro);
            $objTarea["factualizacion"] = getFechaEs($auxObjTarea->fecha_actualizacion);
            $objTarea["tipo_transcurrido"] = getTiempoTranscurrido($auxObjTarea->fecha_registro, date("Y-m-d H:i:s"));
            $objTarea["imagen"] = $aEstados[$auxObjTarea->ultimoestado]["imagen"];
            $objTarea["enlace"] = getEnlace($auxObjTarea->ultimoestado, $auxObjTarea->idtarea, $auxObjTarea->paraadmin, $auxObjTarea->idusuario_rol_creador);
        }

        // Trayendo los archivos de la tarea
        $aArchivosTarea = array();
        $aArchivosTarea = $this->archivomodel->getArchivosTarea($idtarea);

        $data['titulo'] = "Actividades de la tarea";
        $data['divmensaje'] = "";
        $data['objTarea'] = $objTarea;
        $data['aArchivosTarea'] = $aArchivosTarea;
        $data['aTareaEstados'] = $aTareaEstados;
        $this->template->write('botones', '<a href = "' . base_url() . 'tarea/lista" class = "btn btn-sm btn-default" >Atras</a>', TRUE);
        $this->template->write('titleheader', "Actividades de la tarea", TRUE);
        $this->template->write_view('content', 'tarea/ver', $data, TRUE);
        $this->template->render();
    }

    function confirmar($idtarea = 0) {
        validaSession("formulario");
        // Trayendo modelos
        $this->load->model('categoriamodel');
        $this->load->model('tareamodel');
        $this->load->model('tareaestadomodel');
        $this->load->model('localmodel');
        $this->load->model('archivomodel');
        $aArchivosTarea = array();
        $aTareaEstados = array();
        $objTarea = array();
        $divmensaje = "";
        // Trayendo datos
        $auxObjTarea = $this->tareamodel->getListaTareas("t.estado_registro = 1 and t.idtarea = " . $idtarea, 1);
        if ($auxObjTarea != FALSE) {
            if ($auxObjTarea->ultimoestado == 4) {
                $aEstados = getEstados();
                $aCateUsuario = $this->categoriamodel->getaCateUsuario();

                // ACTUALIZANDO LA DATA TAREAESTADOS A ENVIAR
                $this->load->model('usuariomodel');
                $objUsuario = $this->usuariomodel->getUsuarios();
                $aTareaEstados = array();
                $auxTareaEstados = $this->tareaestadomodel->getTareaEstados($idtarea);
                if ($auxTareaEstados != false) {
                    $ultimaFecha = $auxObjTarea->fecha_registro;
                    foreach ($auxTareaEstados as $itemTareaEstado) {
                        array_push($aTareaEstados, array("nomestado" => $aEstados[$itemTareaEstado->idestado]["gerundio"],
                            "comentario" => $itemTareaEstado->comentario,
                            "presupuesto" => $itemTareaEstado->presupuesto,
                            "nomusuario" => $objUsuario[$itemTareaEstado->idusuario]["nombre"],
                            "fregistro" => getFechaEs($itemTareaEstado->fecha_registro),
                            "transcurrido" => getTiempoTranscurrido($ultimaFecha, $itemTareaEstado->fecha_registro),
                            "imagen" => $objUsuario[$itemTareaEstado->idusuario]["foto"],
                            "archivos" => $itemTareaEstado->archivos
                        ));
                        $ultimaFecha = $itemTareaEstado->fecha_registro;
                    }
                }

                if ($auxObjTarea != false) {
// Saber si se envió al administrados o al gestor
                    $responsable = $aCateUsuario[$auxObjTarea->idcategoria]["nomresponsable"];
                    if (intval($auxObjTarea->paraadmin) == 1) {
                        $responsable = "Administrador";
                    }

                    $datetime1 = date_create($auxObjTarea->fecha_registro);
                    $datetime2 = date_create(date("Y-m-d H:i:s"));
                    $fecha = date_diff($datetime1, $datetime2);
                    $tiempoTranscurrido = "";
                    if ($fecha->y > 0) {
                        $tiempoTranscurrido = $fecha->y . " A ";
                    }
                    if ($fecha->m > 0) {
                        $tiempoTranscurrido = $fecha->m . " M ";
                    }
                    if ($fecha->d > 0) {
                        $tiempoTranscurrido = $fecha->d . " d ";
                    }
                    if ($fecha->h > 0) {
                        $tiempoTranscurrido = $fecha->h . "h ";
                    }
                    if ($fecha->i > 0) {
                        $tiempoTranscurrido = $fecha->i . "m ";
                    }
//*******************************
                    $objTarea["idtarea"] = $auxObjTarea->idtarea;
                    $objTarea["codigo"] = $auxObjTarea->codigo;
                    $objTarea["nombre"] = $auxObjTarea->nombre;
                    $objTarea["presupuesto"] = $auxObjTarea->presupuesto;
                    $objTarea["proveedor"] = $auxObjTarea->proveedor;
                    $objTarea["nomlocal"] = $auxObjTarea->nomlocal;
                    $objTarea["descripcion"] = $auxObjTarea->descripcion;
                    $objTarea["nomestado"] = $aEstados[$auxObjTarea->ultimoestado]["nombre"];
                    $objTarea["nomcategoria"] = $aCateUsuario[$auxObjTarea->idcategoria]["nomcategoria"];
                    $objTarea["nomresponsable"] = $responsable;
                    $objTarea["fregistro"] = getFechaEs($auxObjTarea->fecha_registro);
                    $objTarea["factualizacion"] = getFechaEs($auxObjTarea->fecha_actualizacion);
                    $objTarea["tipo_transcurrido"] = $tiempoTranscurrido;
                    $objTarea["imagen"] = $aEstados[$auxObjTarea->ultimoestado]["imagen"];
                    $objTarea["enlace"] = getEnlace($auxObjTarea->ultimoestado, $auxObjTarea->idtarea, $auxObjTarea->paraadmin, $auxObjTarea->idusuario_rol_creador);
                }

// Trayendo los archivos de la tarea
                $aArchivosTarea = array();
                $aArchivosTarea = $this->archivomodel->getArchivosTarea($idtarea);
            } else {
                $divmensaje.="Ya se realizó esta actividad";
            }
        } else {
            $divmensaje.="No existe la tarea";
        }


        $data['titulo'] = "Por confirmar";
        $data['divmensaje'] = $divmensaje;
        $data['objTarea'] = $objTarea;
        $data['aTareaEstados'] = $aTareaEstados;
        $data['aArchivosTarea'] = $aArchivosTarea;
        $this->template->write('botones', '<a href = "' . base_url() . 'tarea/lista" class = "btn btn-sm btn-default" >Atras</a>', TRUE);
        $this->template->write('titleheader', "Por confirmar tarea", TRUE);
        $this->template->write_view('content', 'tarea/confirmar', $data, TRUE);
        $this->template->render();
    }

    public function json_buscar() {
        $oLogin = validaSession("json");
        if ($oLogin["msj"] != "") {
            echo json_encode($oLogin);
            exit;
        }
        $this->load->model('tareamodel');
        $this->load->model('categoriamodel');

        $idlocal = $this->input->post("idlocal");
        $finicio = $this->input->post("finicio");
        $ffin = $this->input->post("ffin");
        $montoinicial = $this->input->post("montoinicial");
        $montofinal = $this->input->post("montofinal");
        $montoigual = $this->input->post("montoigual");

        $where = "t.estado_registro = 1";
        if ($idlocal != "") {
            $where.=" AND t.idlocal = " . $idlocal;
        }
        if ($montoigual != "") {
            $where.=" AND t.presupuesto = " . doubleval($montoigual);
        }
        if ($montoinicial != "" && $montofinal != "") {
            $where.=" AND t.presupuesto BETWEEN " . doubleval($montoinicial) . " AND " . doubleval($montofinal) . " ";
        }
        if ($finicio != "" && $ffin != "") {
            $where.=" AND date(t.fecha_registro) BETWEEN '" . getFechaEn2($finicio) . "' and '" . getFechaEn2($ffin) . "' ";
        }

// Trayendo datos
//$aCategorias = $this->categoriamodel->getdata(array("idcategoria", "nombre"), "estado_registro = 1");
        $aCateUsuario = $this->categoriamodel->getaCateUsuario();
        $auxListaTarea = array();
        if ($this->_rol == 3) {
            $auxListaTarea = $this->tareamodel->getListaTareas($where);
        } else {
            $where.=" and (t.idusuario_rol_ejecutor = " . $this->_idusuario_actual . " or t.idusuario_rol_creador = " . $this->_idusuario_actual . ") ";
            $auxListaTarea = $this->tareamodel->getListaTareas($where);
        }

        $aListaTareas = array();
        $aEstados = getEstados();
        if ($auxListaTarea != false) {
            foreach ($auxListaTarea as $itemTarea) {
// Saber si se envió al administrados o al gestor
                $responsable = $aCateUsuario[$itemTarea->idcategoria]["nomresponsable"];
                if (intval($itemTarea->paraadmin) == 1) {
                    $responsable = "Administrador";
                }

                $datetime1 = date_create($itemTarea->fecha_registro);
                $datetime2 = date_create(date("Y-m-d H:i:s"));
                $fecha = date_diff($datetime1, $datetime2);
                $tiempoTranscurrido = "";
                if ($fecha->y > 0) {
                    $tiempoTranscurrido = $fecha->y . " A ";
                }
                if ($fecha->m > 0) {
                    $tiempoTranscurrido = $fecha->m . " M ";
                }
                if ($fecha->d > 0) {
                    $tiempoTranscurrido = $fecha->d . " d ";
                }
                if ($fecha->h > 0) {
                    $tiempoTranscurrido = $fecha->h . "h ";
                }
                if ($fecha->i > 0) {
                    $tiempoTranscurrido = $fecha->i . "m ";
                }
//*******************************
                array_push($aListaTareas, array("idtarea" => $itemTarea->idtarea,
                    "codigo" => $itemTarea->codigo,
                    "proveedor" => $itemTarea->proveedor,
                    "presupuesto" => $itemTarea->presupuesto,
                    "nombre" => $itemTarea->nombre,
                    "nomlocal" => $itemTarea->nomlocal,
                    "nomestado" => $aEstados[$itemTarea->ultimoestado]["nombre"],
                    "nomcategoria" => $aCateUsuario[$itemTarea->idcategoria]["nomcategoria"],
                    "nomresponsable" => $responsable,
                    "fregistro" => getFechaEs($itemTarea->fecha_registro),
                    "factualizacion" => getFechaEs($itemTarea->fecha_actualizacion),
                    "tipo_transcurrido" => $tiempoTranscurrido,
                    "imagen" => $aEstados[$itemTarea->ultimoestado]["imagen"],
                    "enlace" => getEnlace($itemTarea->ultimoestado, $itemTarea->idtarea, $itemTarea->paraadmin, $itemTarea->idusuario_rol_creador)
                ));
            }
        }
        $data['titulo'] = "Lista de tareas";
        $data['aListaTareas'] = $aListaTareas;
        $this->load->view("tarea/listabusqueda", $data);
    }

    public function json_corregir_tarea() {
        $oLogin = validaSession("json");
        if ($oLogin["msj"] != "") {
            echo json_encode($oLogin);
            exit;
        }
        $this->load->model('tareamodel');
        $this->load->model('tareaestadomodel');
        $this->load->model('categoriamodel');
        $this->load->model('archivomodel');
        $this->load->model('localmodel');

        $idtarea = $this->input->post("idtarea");
        $nombre = $this->input->post("nombre");
        $prioridad = $this->input->post("prioridad");
        $presupuesto = $this->input->post("presupuesto");
        $proveedor = $this->input->post("proveedor");
        $descripcion = $this->input->post("descripcion");
        $idcategoria = $this->input->post("idcategoria");
        $paraadmin = $this->input->post("admin");
        $idlocal = $this->input->post("idlocal");
        if ($idlocal == "") {
            $idlocal = $this->_idlocal;
        }
        $msg_error = "";
        // Validar si existe el id de tarea
        $auxObjTarea = $this->tareamodel->getdata(array("*"), "estado_registro = 1 and idtarea = " . $idtarea, 1);
        if ($auxObjTarea != FALSE) {
            //Procedencia de la edición
            $ejecuta = 0;
            if ($this->_rol == 1) {
                if ($auxObjTarea->ultimoestado == 3) {
                    $ejecuta = 1;
                }
            } else {
                $ejecuta = 1;
            }

            if ($ejecuta == 1) {
                $idestado = $auxObjTarea->ultimoestado;
                if ($idestado == 1 || $idestado == 3) {
                    $idestado = 1;
                }
                $aCateUsuario = $this->categoriamodel->getaCateUsuario();
                $idusuario_ejecutor = $aCateUsuario[$idcategoria] ["idusuario"];
                $idusuario_creador = $this->_idusuario_actual;
                if (intval($paraadmin) == 1) {
                    if ($this->_rol != 1) {
                        $objLocal = $this->localmodel->getdata(array("idusuario_responsable"), "idlocal = " . $idlocal, 1);
                        $idusuario_ejecutor = $objLocal->idusuario_responsable;
                        $idusuario_creador = $this->_idusuario_actual;
                    } else {
                        $idusuario_ejecutor = $aCateUsuario[$idcategoria]["idusuario"];
                        $idusuario_creador = $this->_idusuario_actual;
                    }
                } else {
                    if ($this->_rol != 1) {
                        $objLocal = $this->localmodel->getdata(array("idusuario_responsable"), "idlocal = " . $idlocal, 1);
                        $idusuario_creador = $objLocal->idusuario_responsable;
                    }
                }
                $archivos = array();
                if (!empty($_FILES)) {
                    $archivos = subirArchivos($_FILES);
                }
                //****** CAMBIOS DE EDICIÓN ***************
                $cambio = "";
                if ($nombre != $auxObjTarea->nombre) {
                    $cambio.="Se cambió el nombre de ".$auxObjTarea->nombre." a " . $nombre . ". <br>";
                }
                if ($prioridad != $auxObjTarea->prioridad) {
                    $cambio.="Se cambió la prioridad de ".getNombrePrioridad($auxObjTarea->prioridad)." a " . getNombrePrioridad($prioridad).". <br>";
                }
                if ($presupuesto != $auxObjTarea->presupuesto) {
                    $cambio.="Se cambió el presupuesto de ".$auxObjTarea->presupuesto." a " . $presupuesto . ". <br>";
                }
                if ($proveedor != $auxObjTarea->proveedor) {
                    $cambio.="Se cambió el proveedor de ".$auxObjTarea->proveedor." a " . $proveedor . ". <br>";
                }
                if ($descripcion != $auxObjTarea->descripcion) {
                    $cambio.="Se cambió la descripción de ".$auxObjTarea->descripcion." a " . $descripcion . ".<br>";
                }
                if ($idcategoria != $auxObjTarea->idcategoria) {
                    $cambio.="Se cambió de categoría de ".$aCateUsuario[$auxObjTarea->idcategoria]["nomcategoria"]." a " . $aCateUsuario[$idcategoria]["nomcategoria"] . ". <br>";
                }
                if ($paraadmin != $auxObjTarea->paraadmin) {
                    $nomParaAdminin = "para el administrador";
                    if ($paraadmin == 0) {
                        $nomParaAdminin = "para el gestor";
                    }
                    $cambio.="Se cambió el destino a \"" . $nomParaAdminin . "\"" . "<br>";
                }
                if ($idlocal != $auxObjTarea->idlocal) {
                    $cambio.="Se cambió el local a " . $idlocal . "\n";
                }


                //*****************************************
                // Creando la tarea
                $objTarea = array(
                    "nombre" => $nombre,
                    "descripcion" => $descripcion,
                    "idlocal" => $idlocal,
                    "presupuesto" => doubleval($presupuesto),
                    "proveedor" => $proveedor,
                    "idcategoria" => $idcategoria,
                    "prioridad" => $prioridad,
                    "paraadmin" => $paraadmin,
                    "idusuario_rol_creador" => $idusuario_creador,
                    "idusuario_rol_ejecutor" => $idusuario_ejecutor,
                    "ultimoestado" => $idestado,
                    "fecha_actualizacion" => $this->_fecha_actual,
                    "estado_registro" => 1
                );

                $this->tareamodel->editar($idtarea, $objTarea);
                if ($idtarea > 0) {
// Guardando su actividad
                    $objTareaEstado = array("idusuario" => $this->_idusuario_actual,
                        "idtarea" => $idtarea,
                        "presupuesto" => doubleval($presupuesto),
                        "idestado" => 1,
                        "comentario" => $cambio
                    );
                    $idTareaEstado = $this->tareaestadomodel->guardar($objTareaEstado);

                    if ($idTareaEstado > 0) {
                        foreach ($archivos as $archi) {
                            $objArchivo = array("idtarea_estado" => $idTareaEstado,
                                "url" => $archi["ruta"],
                                "nombre" => $archi["nombre"]
                            );
                            $numFilasArchivos = $this->archivomodel->guardar($objArchivo);
                            if ($numFilasArchivos < 1) {
                                $msg_error.= "Error al registrar archivo" . $idTareaEstado;
                            }
                        }
                    } else {
                        $msg_error.="No se registró la bitacora tarea-estado";
                    }
                } else {
                    $msg_error.="No se registró la tarea";
                }
            } else {
                if ($this->_rol == 1) {
                    $msg_error.="Ya se puede atendió la tarea";
                } else {
                    $msg_error.="No se puede editar";
                }
            }
        } else {
            $msg_error.="No existe la tarea";
        }

        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "La tarea fué editada");
        }
        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

	public function json_corregir_ejecutar() {
        $oLogin = validaSession("json");
        if ($oLogin[
                "msj"] != "") {
            echo json_encode($oLogin);
            exit;
        }
        $this->load->model('tareamodel');
        $this->load->model('tareaestadomodel');
        $this->load->model('categoriamodel');
        $this->load->model('archivomodel');

        $accion = $this->input->post("accion");
        $descripcion = $this->input->post("descripcion");
        $idtarea = $this->input->post("idtarea");
        $proveedor = $this->input->post("proveedor");
        $presupuesto = $this->input->post("presupuesto");
        $msg_error = "";


// Valido que exista el idtarea enviado
        $auxObjTarea = $this->tareamodel->getdata(array("idcategoria", "nombre", "ultimoestado", "codigo", "idusuario_rol_creador", "paraadmin", "idlocal"), "idtarea = " . $idtarea, 1);
        if ($auxObjTarea != false) {
            if ($auxObjTarea->ultimoestado == 6) {
                // Jugar con los estados
                // Ver que acción realizará (1= Al estado 2 y 0= Al estado 3)
                $estado = 4;

                if ($accion == 0) {
                    $estado = 3;
                }

// Creando una nueva actividad realizada
                $objTareaEstado = array("idusuario" => $this->_idusuario_actual,
                    "idtarea" => $idtarea,
                    "idestado" => $estado,
                    "presupuesto" => doubleval($presupuesto),
                    "comentario" => $descripcion
                );
                $idTareaEstado = $this->tareaestadomodel->guardar($objTareaEstado);
                if ($idTareaEstado > 0) {
// Actualizando el ultimo estado para esta tarea
                    $objTarea = array(
                        "ultimoestado" => $estado,
                        "presupuesto" => doubleval($presupuesto),
                        "fecha_actualizacion" => date("Y-m-d H:i:s"),
                        "proveedor" => $proveedor,
                    );
                    $this->tareamodel->editar($idtarea, $objTarea);

// Guardando los archivo de la actividad
                    $archivos = array();
                    if (!empty($_FILES)) {
                        $archivos = subirArchivos($_FILES);
                    }
                    foreach ($archivos as $archi) {
                        $objArchivo = array("idtarea_estado" => $idTareaEstado,
                            "url" => $archi["ruta"],
                            "nombre" => $archi["nombre"]
                        );
                        $numFilasArchivos = $this->archivomodel->guardar($objArchivo);
                        if ($numFilasArchivos < 1) {
                            $msg_error.= "Error al registrar archivo" . $idTareaEstado;
                        }
                    }
                } else {
                    $msg_error.="No se registro actividad";
                }
            } else {
                $msg_error.="Este estado ya se registró";
            }
        } else {
            $msg_error.="No existe esta tarea";
        }

        $this->load->model('usuariomodel');
        $objUsuario = $this->usuariomodel->getUsuarios();
        // $objUsuario[$idusuario_ejecutor]["correo"]
        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "La tarea fué ejecutada exitosamente");
            enviarEmail(array(
                "FROM" => "Hikari- Notificaciones",
                "FROMNAME" => "sistemas@hikari.pe",
                "asunto" => "Actualización de la tarea - " . $auxObjTarea->codigo,
                "mensaje" => 'La tarea "' . $auxObjTarea->nombre . '" con código "' . $auxObjTarea->codigo . '" se acaba de ejecutar',
                "titulo" => "De: " . $objUsuario[$this->_idusuario_actual]["nombre"] . "<br>Para: " . $objUsuario[$auxObjTarea->idusuario_rol_creador]["nombre"],
                "correo" => $objUsuario[$auxObjTarea->idusuario_rol_creador]["nombre"]
            ));
        }

        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    public function json_editar() {
        $oLogin = validaSession("json");
        if ($oLogin["msj"] != "") {
            echo json_encode($oLogin);
            exit;
        }
        $this->load->model('tareamodel');
        $this->load->model('tareaestadomodel');
        $this->load->model('categoriamodel');
        $this->load->model('archivomodel');
        $this->load->model('localmodel');

        $idtarea = $this->input->post("idtarea");
        $nombre = $this->input->post("nombre");
        $prioridad = $this->input->post("prioridad");
        $presupuesto = $this->input->post("presupuesto");
        $proveedor = $this->input->post("proveedor");
        $descripcion = $this->input->post("descripcion");
        $idcategoria = $this->input->post("idcategoria");
        $paraadmin = $this->input->post("admin");
        $idlocal = $this->input->post("idlocal");
        if ($idlocal == "") {
            $idlocal = $this->_idlocal;
        }
        $msg_error = "";
        // Validar si existe el id de tarea
        $auxObjTarea = $this->tareamodel->getdata(array("*"), "estado_registro = 1 and idtarea = " . $idtarea, 1);
        if ($auxObjTarea != FALSE) {
            //Procedencia de la edición
            $ejecuta = 0;
            if ($this->_rol == 1) {
                if ($auxObjTarea->ultimoestado == 1) {
                    $ejecuta = 1;
                }
            } else {
                $ejecuta = 1;
            }

            if ($ejecuta == 1) {
                $idestado = $auxObjTarea->ultimoestado;
                if ($idestado == 1 || $idestado == 3) {
                    $idestado = 1;
                }
                $aCateUsuario = $this->categoriamodel->getaCateUsuario();
                $idusuario_ejecutor = $aCateUsuario[$idcategoria] ["idusuario"];
                $idusuario_creador = $this->_idusuario_actual;
                if (intval($paraadmin) == 1) {
                    if ($this->_rol != 1) {
                        $objLocal = $this->localmodel->getdata(array("idusuario_responsable"), "idlocal = " . $idlocal, 1);
                        $idusuario_ejecutor = $objLocal->idusuario_responsable;
                        $idusuario_creador = $this->_idusuario_actual;
                    } else {
                        $idusuario_ejecutor = $aCateUsuario[$idcategoria]["idusuario"];
                        $idusuario_creador = $this->_idusuario_actual;
                    }
                } else {
                    if ($this->_rol != 1) {
                        $objLocal = $this->localmodel->getdata(array("idusuario_responsable"), "idlocal = " . $idlocal, 1);
                        $idusuario_creador = $objLocal->idusuario_responsable;
                    }
                }
                $archivos = array();
                if (!empty($_FILES)) {
                    $archivos = subirArchivos($_FILES);
                }
                //****** CAMBIOS DE EDICIÓN ***************
                $cambio = "";
                if ($nombre != $auxObjTarea->nombre) {
                    $cambio.="Se cambió el nombre a " . $nombre . "<br>";
                }
                if ($prioridad != $auxObjTarea->prioridad) {
                    $cambio.="Se cambió la prioridad a " . getNombrePrioridad($prioridad);
                }
                if ($presupuesto != $auxObjTarea->presupuesto) {
                    $cambio.="Se cambió el presupuesto a " . $presupuesto . "<br>";
                }
                if ($proveedor != $auxObjTarea->proveedor) {
                    $cambio.="Se cambió el proveedor a " . $proveedor . "<br>";
                }
                if ($descripcion != $auxObjTarea->descripcion) {
                    $cambio.="Se cambió la descripción a " . $descripcion . "<br>";
                }
                if ($idcategoria != $auxObjTarea->idcategoria) {
                    $cambio.="Se cambió de categoría a " . $aCateUsuario[$idcategoria]["nomcategoria"] . "<br>";
                }
                if ($paraadmin != $auxObjTarea->paraadmin) {
                    $nomParaAdminin = "para el administrador";
                    if ($paraadmin == 0) {
                        $nomParaAdminin = "para el gestor";
                    }
                    $cambio.="Se cambió el destino a \"" . $nomParaAdminin . "\"" . "<br>";
                }
                if ($idlocal != $auxObjTarea->idlocal) {
                    $cambio.="Se cambió el local a " . $idlocal . "<br>";
                }


                //*****************************************
                // Creando la tarea
                $objTarea = array(
                    "nombre" => $nombre,
                    "descripcion" => $descripcion,
                    "idlocal" => $idlocal,
                    "presupuesto" => doubleval($presupuesto),
                    "proveedor" => $proveedor,
                    "idcategoria" => $idcategoria,
                    "prioridad" => $prioridad,
                    "paraadmin" => $paraadmin,
                    "idusuario_rol_creador" => $idusuario_creador,
                    "idusuario_rol_ejecutor" => $idusuario_ejecutor,
                    "ultimoestado" => $idestado,
                    "fecha_actualizacion" => $this->_fecha_actual,
                    "estado_registro" => 1
                );

                $this->tareamodel->editar($idtarea, $objTarea);
                if ($idtarea > 0) {
// Guardando su actividad
                    $objTareaEstado = array("idusuario" => $this->_idusuario_actual,
                        "idtarea" => $idtarea,
                        "presupuesto" => doubleval($presupuesto),
                        "idestado" => 1,
                        "comentario" => $cambio
                    );
                    $idTareaEstado = $this->tareaestadomodel->guardar($objTareaEstado);

                    if ($idTareaEstado > 0) {
                        foreach ($archivos as $archi) {
                            $objArchivo = array("idtarea_estado" => $idTareaEstado,
                                "url" => $archi["ruta"],
                                "nombre" => $archi["nombre"]
                            );
                            $numFilasArchivos = $this->archivomodel->guardar($objArchivo);
                            if ($numFilasArchivos < 1) {
                                $msg_error.= "Error al registrar archivo" . $idTareaEstado;
                            }
                        }
                    } else {
                        $msg_error.="No se registró la bitacora tarea-estado";
                    }
                } else {
                    $msg_error.="No se registró la tarea";
                }
            } else {
                if ($this->_rol == 1) {
                    $msg_error.="Ya se puede atendió la tarea";
                } else {
                    $msg_error.="No se puede editar";
                }
            }
        } else {
            $msg_error.="No existe la tarea";
        }

        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "La tarea fué editada");
        }
        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    public function enviar() {
        enviarEmail(array(
            "FROM" => "Hikari- Notificaciones",
            "FROMNAME" => "sistemas@hikari.pe",
            "asunto" => "Se registró una nueva tarea",
            "titulo" => "De Victor para Yess",
            "mensaje" => "jajaja",
            "correo" => "vicrea@beta.pe"
        ));
    }

    public function json_programar() {

        $oLogin = validaSession("json");
        if ($oLogin[
                "msj"] != "") {
            echo json_encode($oLogin);
            exit;
        }
        $this->load->model('tareaprogmodel');
        $this->load->model('tareaestadomodel');
        $this->load->model('categoriamodel');
        $this->load->model('archivomodel');
        $this->load->model('localmodel');
        $this->load->model('programacionmodel');

        // DATOS DE LA PROGRAMACIÓN
        $lunes = $this->input->post("lunes");
        $martes = $this->input->post("martes");
        $miercoles = $this->input->post("miercoles");
        $jueves = $this->input->post("jueves");
        $viernes = $this->input->post("viernes");
        $sabado = $this->input->post("sabado");
        $domingo = $this->input->post("domingo");
        $repeticion = $this->input->post("repeticion");
        $finicio = $this->input->post("finicio");
        $ffin = $this->input->post("ffin");
        $objProgramacion = array(
            "lunes" => $lunes,
            "martes" => $martes,
            "miercoles" => $miercoles,
            "jueves" => $jueves,
            "viernes" => $viernes,
            "sabado" => $sabado,
            "domingo" => $domingo,
            "repeticion" => $repeticion,
            "fecha_inicio" => getFechaEn2($finicio),
            "fecha_fin" => getFechaEn2($ffin),
            "estado_registro" => 1
        );

        // DATOS DE LA TAREA
        $nombre = $this->input->post("nombre");
        $prioridad = $this->input->post("prioridad");
        $descripcion = $this->input->post("descripcion");
        $idcategoria = $this->input->post("idcategoria");
        $paraadmin = $this->input->post("admin");
        $idlocal = $this->input->post("idlocal");
        if ($idlocal == "") {
            $idlocal = $this->_idlocal;
        }
        $msg_error = "";
        $aCateUsuario = $this->categoriamodel->getaCateUsuario();
        $idusuario_ejecutor = $aCateUsuario[$idcategoria]["idusuario"];
        $idusuario_creador = $this->_idusuario_actual;
        if (intval($paraadmin) == 1) {
            if ($this->_rol != 1) {
                $objLocal = $this->localmodel->getdata(array("idusuario_responsable"), "idlocal = " . $idlocal, 1);
                $idusuario_ejecutor = $objLocal->idusuario_responsable;
                $idusuario_creador = $this->_idusuario_actual;
            } else {
                $idusuario_ejecutor = $aCateUsuario[$idcategoria]["idusuario"];
                $idusuario_creador = $this->_idusuario_actual;
            }
        } else {
            if ($this->_rol != 1) {
                $objLocal = $this->localmodel->getdata(array("idusuario_responsable"), "idlocal = " . $idlocal, 1);
                $idusuario_creador = $objLocal->idusuario_responsable;
            }
        }


        $archivos = array();
        if (!empty($_FILES)) {
            $archivos = subirArchivos($_FILES);
        }

// Creando la tarea
        $random = rand(1, 99999);
        $codigo = "TP-" . substr(date("Y"), 2, 2) . date("m") . "-" . str_pad($random, 3, "0", STR_PAD_LEFT);
        $objTarea = array(
            "codigo" => $codigo,
            "nombre" => $nombre,
            "descripcion" => $descripcion,
            "idlocal" => $idlocal,
            "idcategoria" => $idcategoria,
            "prioridad" => $prioridad,
            "paraadmin" => $paraadmin,
            "idusuario_rol_creador" => $idusuario_creador,
            "idusuario_rol_ejecutor" => $idusuario_ejecutor
        );

        $idtarea = $this->tareaprogmodel->guardar($objTarea);
        if ($idtarea > 0) {
            // Guardando su actividad
            $objProgramacion["idtareaprog"] = $idtarea;
            $idprogramacion = $this->programacionmodel->guardar($objProgramacion);

            if ($idprogramacion > 0) {
                foreach ($archivos as $archi) {
                    $objArchivo = array("idtarea_estado" => $idprogramacion,
                        "url" => $archi["ruta"],
                        "nombre" => $archi["nombre"]
                    );
                    $numFilasArchivos = $this->archivomodel->guardar($objArchivo);
                    if ($numFilasArchivos < 1) {
                        $msg_error.= "Error al registrar archivo" . $idprogramacion;
                    }
                }
            } else {
                $msg_error.="No se registró la bitacora tarea-estado";
            }
        } else {
            $msg_error.="No se registró la tarea";
        }
        $objProgramacion["idprogramacion"] = $idprogramacion;
        $this->ejecuta_cron($objProgramacion, $objTarea);
        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "La tarea se creó exitosamente");
        }
        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    // ESTADO = 1
    public function json_registrar() {

        $oLogin = validaSession("json");
        if ($oLogin[
                "msj"] != "") {
            echo json_encode($oLogin);
            exit;
        }
        $this->load->model('tareamodel');
        $this->load->model('tareaestadomodel');
        $this->load->model('categoriamodel');
        $this->load->model('archivomodel');
        $this->load->model('localmodel');

        $nombre = $this->input->post("nombre");
        $prioridad = $this->input->post("prioridad");
        $descripcion = $this->input->post("descripcion");
        $idcategoria = $this->input->post("idcategoria");
        $paraadmin = $this->input->post("admin");
        $idlocal = $this->input->post("idlocal");
        if ($idlocal == "") {
            $idlocal = $this->_idlocal;
        }
        $msg_error = "";
        $aCateUsuario = $this->categoriamodel->getaCateUsuario();
        $idusuario_ejecutor = $aCateUsuario[$idcategoria]["idusuario"];
        $idusuario_creador = $this->_idusuario_actual;
        if (intval($paraadmin) == 1) {
            if ($this->_rol != 1) {
                $objLocal = $this->localmodel->getdata(array("idusuario_responsable"), "idlocal = " . $idlocal, 1);
                $idusuario_ejecutor = $objLocal->idusuario_responsable;
                $idusuario_creador = $this->_idusuario_actual;
            } else {
                $idusuario_ejecutor = $aCateUsuario[$idcategoria]["idusuario"];
                $idusuario_creador = $this->_idusuario_actual;
            }
        } else {
            if ($this->_rol != 1) {
                $objLocal = $this->localmodel->getdata(array("idusuario_responsable"), "idlocal = " . $idlocal, 1);
                $idusuario_creador = $objLocal->idusuario_responsable;
            }
        }


        $archivos = array();
        if (!empty($_FILES)) {
            $archivos = subirArchivos($_FILES);
        }

// Creando la tarea
        $random = rand(1, 99999);

        $objTarea = array(
            "nombre" => $nombre,
            "descripcion" => $descripcion,
            "idlocal" => $idlocal,
            "idcategoria" => $idcategoria,
            "prioridad" => $prioridad,
            "paraadmin" => $paraadmin,
            "idusuario_rol_creador" => $idusuario_creador,
            "idusuario_rol_ejecutor" => $idusuario_ejecutor,
            "ultimoestado" => 1,
            "fecha_registro" => $this->_fecha_actual,
            "fecha_actualizacion" => $this->_fecha_actual,
            "estado_registro" => 1
        );
        $idtarea = $this->tareamodel->guardar($objTarea);
        $codigo = "TC-" . substr(date("Y"), 2, 2) . date("m") . "-" . str_pad($idtarea, 3, "0", STR_PAD_LEFT);
        $objTarea = array();
        $objTarea["codigo"] = $codigo;
        $this->tareamodel->editar($idtarea, $objTarea);
        if ($idtarea > 0) {
// Guardando su actividad
            $objTareaEstado = array(
                "idusuario" => $this->_idusuario_actual,
                "idtarea" => $idtarea,
                "iniciador" => 1,
                "idestado" => 1,
                "comentario" => $descripcion
            );
            $idTareaEstado = $this->tareaestadomodel->guardar($objTareaEstado);

            if ($idTareaEstado > 0) {
                foreach ($archivos as $archi) {
                    $objArchivo = array("idtarea_estado" => $idTareaEstado,
                        "url" => $archi["ruta"],
                        "nombre" => $archi["nombre"]
                    );
                    $numFilasArchivos = $this->archivomodel->guardar($objArchivo);
                    if ($numFilasArchivos < 1) {
                        $msg_error.= "Error al registrar archivo" . $idTareaEstado;
                    }
                }
            } else {
                $msg_error.="No se registró la bitacora tarea-estado";
            }
        } else {
            $msg_error.="No se registró la tarea";
        }

        $this->load->model('usuariomodel');
        $objUsuario = $this->usuariomodel->getUsuarios();
        // $objUsuario[$idusuario_ejecutor]["correo"]
        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "La tarea se creó exitosamente");
            enviarEmail(array(
                "FROM" => "Hikari- Notificaciones",
                "FROMNAME" => "sistemas@hikari.pe",
                "asunto" => "Se registró una nueva tarea",
                "mensaje" => 'Se creó la tarea llamada "' . $nombre . '" con código "' . $codigo . '"',
                "titulo" => "De: " . $objUsuario[$idusuario_creador]["nombre"] . "<br>Para: " . $objUsuario[$idusuario_ejecutor]["nombre"],
                "correo" => $objUsuario[$idusuario_ejecutor]["correo"]
            ));
        }

        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    // ESTADO = 2
    public function json_por_atender() {
        $oLogin = validaSession("json");
        if ($oLogin[
                "msj"] != "") {
            echo json_encode($oLogin);
            exit;
        }
        $this->load->model('tareamodel');
        $this->load->model('tareaestadomodel');
        $this->load->model('categoriamodel');
        $this->load->model('archivomodel');

        $accion = $this->input->post("accion");
        $descripcion = $this->input->post("descripcion");
        $idtarea = $this->input->post("idtarea");
        $proveedor = $this->input->post("proveedor");
        $presupuesto = $this->input->post("presupuesto");
        $msg_error = "";


// Valido que exista el idtarea enviado
        $auxObjTarea = $this->tareamodel->getdata(array("idusuario_rol_creador", "ultimoestado", "nombre", "codigo", "idcategoria", "paraadmin", "idlocal"), "idtarea = " . $idtarea, 1);

        if ($auxObjTarea != false) {
// Jugar con los estados
// Ver que acción realizará (1= Al estado 2 y 0= Al estado 3)
            if ($auxObjTarea->ultimoestado == 1) {
                $estado = 2;
                $nomAcion = "atendió";
                if ($accion == 0) {
                    $nomAcion = "rechazó";
                    $estado = 3;
                }

// Creando una nueva actividad realizada
                $objTareaEstado = array("idusuario" => $this->_idusuario_actual,
                    "idtarea" => $idtarea,
                    "idestado" => $estado,
                    "presupuesto" => doubleval($presupuesto),
                    "comentario" => $descripcion
                );
                $idTareaEstado = $this->tareaestadomodel->guardar($objTareaEstado);
                if ($idTareaEstado > 0) {
// Actualizando el ultimo estado para esta tarea
                    $objTarea = array(
                        "presupuesto" => doubleval($presupuesto),
                        "fecha_actualizacion" => date("Y-m-d H:i:s"),
                        "proveedor" => $proveedor,
                        "ultimoestado" => $estado
                    );
                    $this->tareamodel->editar($idtarea, $objTarea);

// Guardando los archivo de la actividad
                    $archivos = array();
                    if (!empty($_FILES)) {
                        $archivos = subirArchivos($_FILES);
                    }
                    foreach ($archivos as $archi) {
                        $objArchivo = array("idtarea_estado" => $idTareaEstado,
                            "url" => $archi["ruta"],
                            "nombre" => $archi["nombre"]
                        );
                        $numFilasArchivos = $this->archivomodel->guardar($objArchivo);
                        if ($numFilasArchivos < 1) {
                            $msg_error.= "Error al registrar archivo" . $idTareaEstado;
                        }
                    }
                } else {
                    $msg_error.="No se registro actividad";
                }
            } else {
                $msg_error.="Ya se realizó este estado";
            }
        } else {
            $msg_error.="No existe esta tarea";
        }

        $this->load->model('usuariomodel');
        $objUsuario = $this->usuariomodel->getUsuarios();
        // $objUsuario[$idusuario_ejecutor]["correo"]
        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "La tarea se " . $nomAcion);
            enviarEmail(array(
                "FROM" => "Hikari- Notificaciones",
                "FROMNAME" => "sistemas@hikari.pe",
                "asunto" => "Actualización de la tarea - " . $auxObjTarea->codigo,
                "mensaje" => 'La tarea "' . $auxObjTarea->nombre . '" con código "' . $auxObjTarea->codigo . '" se ' . $nomAcion,
                "titulo" => "De: " . $objUsuario[$this->_idusuario_actual]["nombre"] . "<br>Para: " . $objUsuario[$auxObjTarea->idusuario_rol_creador]["nombre"],
                "correo" => $objUsuario[$auxObjTarea->idusuario_rol_creador]["nombre"]
            ));
        }

        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    public function json_ejecutar() {
        $oLogin = validaSession("json");
        if ($oLogin[
                "msj"] != "") {
            echo json_encode($oLogin);
            exit;
        }
        $this->load->model('tareamodel');
        $this->load->model('tareaestadomodel');
        $this->load->model('categoriamodel');
        $this->load->model('archivomodel');

        $accion = $this->input->post("accion");
        $descripcion = $this->input->post("descripcion");
        $idtarea = $this->input->post("idtarea");
        $proveedor = $this->input->post("proveedor");
        $presupuesto = $this->input->post("presupuesto");
        $msg_error = "";


// Valido que exista el idtarea enviado
        $auxObjTarea = $this->tareamodel->getdata(array("idcategoria", "nombre", "ultimoestado", "codigo", "idusuario_rol_creador", "paraadmin", "idlocal"), "idtarea = " . $idtarea, 1);
        if ($auxObjTarea != false) {
            if ($auxObjTarea->ultimoestado == 2) {
                // Jugar con los estados
                // Ver que acción realizará (1= Al estado 2 y 0= Al estado 3)
                $estado = 4;

                if ($accion == 0) {
                    $estado = 3;
                }

// Creando una nueva actividad realizada
                $objTareaEstado = array("idusuario" => $this->_idusuario_actual,
                    "idtarea" => $idtarea,
                    "idestado" => $estado,
                    "presupuesto" => doubleval($presupuesto),
                    "comentario" => $descripcion
                );
                $idTareaEstado = $this->tareaestadomodel->guardar($objTareaEstado);
                if ($idTareaEstado > 0) {
// Actualizando el ultimo estado para esta tarea
                    $objTarea = array(
                        "ultimoestado" => $estado,
                        "presupuesto" => doubleval($presupuesto),
                        "fecha_actualizacion" => date("Y-m-d H:i:s"),
                        "proveedor" => $proveedor,
                    );
                    $this->tareamodel->editar($idtarea, $objTarea);

// Guardando los archivo de la actividad
                    $archivos = array();
                    if (!empty($_FILES)) {
                        $archivos = subirArchivos($_FILES);
                    }
                    foreach ($archivos as $archi) {
                        $objArchivo = array("idtarea_estado" => $idTareaEstado,
                            "url" => $archi["ruta"],
                            "nombre" => $archi["nombre"]
                        );
                        $numFilasArchivos = $this->archivomodel->guardar($objArchivo);
                        if ($numFilasArchivos < 1) {
                            $msg_error.= "Error al registrar archivo" . $idTareaEstado;
                        }
                    }
                } else {
                    $msg_error.="No se registro actividad";
                }
            } else {
                $msg_error.="Este estado ya se registró";
            }
        } else {
            $msg_error.="No existe esta tarea";
        }

        $this->load->model('usuariomodel');
        $objUsuario = $this->usuariomodel->getUsuarios();
        // $objUsuario[$idusuario_ejecutor]["correo"]
        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "La tarea fué ejecutada exitosamente");
            enviarEmail(array(
                "FROM" => "Hikari- Notificaciones",
                "FROMNAME" => "sistemas@hikari.pe",
                "asunto" => "Actualización de la tarea - " . $auxObjTarea->codigo,
                "mensaje" => 'La tarea "' . $auxObjTarea->nombre . '" con código "' . $auxObjTarea->codigo . '" se acaba de ejecutar',
                "titulo" => "De: " . $objUsuario[$this->_idusuario_actual]["nombre"] . "<br>Para: " . $objUsuario[$auxObjTarea->idusuario_rol_creador]["nombre"],
                "correo" => $objUsuario[$auxObjTarea->idusuario_rol_creador]["nombre"]
            ));
        }

        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    public function json_confirmar() {
        $oLogin = validaSession("json");
        if ($oLogin[
                "msj"] != "") {
            echo json_encode($oLogin);
            exit;
        }
        $this->load->model('tareamodel');
        $this->load->model('tareaestadomodel');
        $this->load->model('categoriamodel');
        $this->load->model('archivomodel');

        $accion = $this->input->post("accion");
        $descripcion = $this->input->post("descripcion");
        $idtarea = $this->input->post("idtarea");
        $msg_error = "";


        // Valido que exista el idtarea enviado
        $auxObjTarea = $this->tareamodel->getdata(array("idcategoria", "ultimoestado", "presupuesto", "nombre", "codigo", "idusuario_rol_creador", "idusuario_rol_ejecutor", "paraadmin", "idlocal"), "idtarea = " . $idtarea, 1);
        if ($auxObjTarea != false) {
            if ($auxObjTarea->ultimoestado == 4) {
                // Jugar con los estados
                // Ver que acción realizará (1= Al estado 2 y 0= Al estado 3)
                $estado = 5;
                $nomAccion = "está terminada";
                if ($accion == 0) {
                    $nomAccion = "no está conforme";
                    $estado = 6;
                }

                // Creando una nueva actividad realizada
                $objTareaEstado = array("idusuario" => $this->_idusuario_actual,
                    "idtarea" => $idtarea,
                    "idestado" => $estado,
                    "presupuesto" => doubleval($auxObjTarea->presupuesto),
                    "comentario" => $descripcion
                );
                $idTareaEstado = $this->tareaestadomodel->guardar($objTareaEstado);
                if ($idTareaEstado > 0) {
                    // Actualizando el ultimo estado para esta tarea
                    $objTarea = array(
                        "ultimoestado" => $estado,
                        "fecha_actualizacion" => date("Y-m-d H:i:s")
                    );
                    $this->tareamodel->editar($idtarea, $objTarea);

                    // Guardando los archivo de la actividad
                    $archivos = array();
                    if (!empty($_FILES)) {
                        $archivos = subirArchivos($_FILES);
                    }
                    foreach ($archivos as $archi) {
                        $objArchivo = array("idtarea_estado" => $idTareaEstado,
                            "url" => $archi["ruta"],
                            "nombre" => $archi["nombre"]
                        );
                        $numFilasArchivos = $this->archivomodel->guardar($objArchivo);
                        if ($numFilasArchivos < 1) {
                            $msg_error.= "Error al registrar archivo" . $idTareaEstado;
                        }
                    }
                } else {
                    $msg_error.="No se registro actividad";
                }
            } else {
                $msg_error.="Ya se realizó esta actividad";
            }
        } else {
            $msg_error.="No existe esta tarea";
        }

        $this->load->model('usuariomodel');
        $objUsuario = $this->usuariomodel->getUsuarios();
        // $objUsuario[$idusuario_ejecutor]["correo"]
        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "La tarea " . $nomAccion . "");
            enviarEmail(array(
                "FROM" => "Hikari- Notificaciones",
                "FROMNAME" => "sistemas@hikari.pe",
                "asunto" => "Actualización de la tarea - " . $auxObjTarea->codigo,
                "mensaje" => 'La tarea "' . $auxObjTarea->nombre . '" con código "' . $auxObjTarea->codigo . '" ' . $nomAccion,
                "titulo" => "De: " . $objUsuario[$this->_idusuario_actual]["nombre"] . "<br>Para: " . $objUsuario[$auxObjTarea->idusuario_rol_ejecutor]["nombre"],
                "correo" => $objUsuario[$auxObjTarea->idusuario_rol_ejecutor]["nombre"]
            ));
        }

        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

}
