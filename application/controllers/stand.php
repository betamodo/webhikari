<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Stand extends MY_Controller {

    private $_fecha_actual;
    private $_idusuario_actual;
    private $_idlocal;
    private $_rol;

    function __construct() {
        parent::__construct();
        $this->_fecha_actual = date("Y-m-d H:i:s");
        $this->load->model('standmodel');
        $this->_idusuario_actual = $this->session->userdata('idusuario');
        $this->_rol = $this->session->userdata('rol');
        $this->_idlocal = $this->session->userdata('idlocal');
        $this->template->set_template('default');
        $this->load->helper(array('url', 'funciones'));
    }

    function index() {
        $this->login();
    }

    public function lista() {
        // Trayendo modelos
        $mensaje = $this->session->userdata('mensaje');
        if ($mensaje != "") {
            $mensaje = getHtmlMensaje(1, $mensaje);
        }
        $this->session->set_userdata('mensaje', "");

        $aListaStand = $this->standmodel->getdata(array("idstand","codigo","descripcion","estado_registro"));
        $data['titulo'] = "Lista de stands";
        $data['mensaje'] = $mensaje;
        $data['aListaStand'] = $aListaStand;
        $botones = "";
        $botones .= '<a href = "' . base_url() . 'panel" class = "btn btn-sm btn-default" >Atras</a> ';
        $botones .= '<a href = "' . base_url() . 'stand/nuevo" class = "btn btn-sm btn-default" >Nuevo</a> ';
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Stands.", TRUE);
        $this->template->write_view('content', 'mantenimiento/stand/lista', $data, TRUE);
        $this->template->render();
    }

    public function json_crear_stand() {

        $codigo = $this->input->post("codigo");        
        $estado = $this->input->post("estado");
		$descripcion = $this->input->post("descripcion");
        $msg_error = "";
        // Validar si existe el id de tarea
        $auxStand = array();
        $oStand = array();
        $auxStand = $this->standmodel->getExiste("codigo", $codigo, 0, 1);

        if ($auxStand->cantidad == 0) {
            $oStand = array(
                "codigo" => $codigo,
				"descripcion" => $descripcion,
                "estado_registro" => $estado
            );
            $idstand = $this->standmodel->guardar($oStand);
            // Subimos los archivos si el usuario fue creado exitosamente
            if ($idstand < 1) {
                $msg_error.="No se pudo registrar";
            }
        } else {
            $msg_error.="El codigo ingresado ya existe";
        }
        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "El stand se cre� exitosamente");
        }
        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    public function json_editar_stand() {

        $idstand = $this->input->post("idstand");
        $codigo = $this->input->post("codigo");
		$descripcion = $this->input->post("descripcion");
        $estado = $this->input->post("estado");

        $msg_error = "";
        $auxStand = array();
        $oStand = array();
        $auxStand = $this->standmodel->getExiste("codigo", $codigo, $idstand, 1);
        if ($auxStand->cantidad == 0) {
            $oStand = array(
                "codigo" => $codigo,                
				"descripcion" => $descripcion,                
                "estado_registro" => $estado
            );
            $this->standmodel->editar($idstand, $oStand);
        } else {
            $msg_error.="El codigo ingresado ya existe";
        }
        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "El stand se edit� exitosamente");
        }
        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    public function nuevo() {
        $data['titulo'] = "Nuevo stand";
        $botones = "";
        $botones .= '<a href = "' . base_url() . 'stand/lista" class = "btn btn-sm btn-default" >Atras</a> ';
		$this->template->add_js('media/js/stand.js');
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Nuevo Stand", TRUE);
        $this->template->write_view('content', 'mantenimiento/stand/crear', $data, TRUE);
        $this->template->render();
    }

    public function editar($idstand) {
        // Trayendo modelos
        $oStand = $this->standmodel->getdata(array("*"), "idstand = " . $idstand, 1);
        if ($oStand == FALSE) {
            $this->session->set_userdata('mensaje', "No existe el stand");
        }

        $data['titulo'] = "Editar stand";
        $data['oStand'] = $oStand;
        $botones = "";
        $botones .= '<a href = "' . base_url() . 'stand/lista" class = "btn btn-sm btn-default" >Atras</a> ';
        $botones .= '<a href = "' . base_url() . 'stand/nuevo" class = "btn btn-sm btn-default" >Nuevo</a> ';
		$this->template->add_js('media/js/stand.js');
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Editar stand", TRUE);
        $this->template->write_view('content', 'mantenimiento/stand/editar', $data, TRUE);
        $this->template->render();
    }

}
