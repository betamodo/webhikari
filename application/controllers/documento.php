<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}


class Documento extends MY_Controller {

    private $_fecha_actual;
    private $_idusuario_actual;
    private $_idlocal;
    private $_rol;

    function __construct() {
        parent::__construct();
		$this->_fecha_actual = date("Y-m-d H:i:s");
        $this->load->model('documentomodel');
        $this->_idusuario_actual = $this->session->userdata('idusuario');
        $this->_rol = $this->session->userdata('rol');
        $this->_idlocal = $this->session->userdata('idlocal');
        $this->template->set_template('default');
        $this->load->helper(array('url', 'funciones', 'mysql_to_excel_helper'));
    }

    function index() {
        redirect(base_url() . "documento/lista");
    }

    function comingsoon() {
        $this->load->view('comingsoon', array());
    }

    public function json_buscar() {
        $oLogin = validaSession("json");
        if ($oLogin["msj"] != "") {
            echo "session";
            exit;
        }
        $this->load->model('filemodel');
        $this->load->model('standmodel');
        $this->load->model('categoriamodel');
		
		// CAMBIOS
		$idalmacen = $this->input->post("idalmacen");
        $file = $this->input->post("file");
		$stand = $this->input->post("stand");
        $box = $this->input->post("box");
		$estado = $this->input->post("estado");
		$remitente = $this->input->post("remitente");
		$idtipo = $this->input->post("idtipo");
		$codigo = $this->input->post("codigo");
		$codbox = $this->input->post("codbox");
		$asunto = $this->input->post("asunto");
		$ffin = $this->input->post("ffin");
        $finicio = $this->input->post("finicio");
		$idcategoria = $this->input->post("idcategoria");
		
		// session de filtros
		$this->session->set_userdata('filtro_idalmacen', $idalmacen);
        $this->session->set_userdata('filtro_file', $file);
		$this->session->set_userdata('filtro_stand', $stand);
        $this->session->set_userdata('filtro_box', $box);
		$this->session->set_userdata('filtro_estado', $estado);
		$this->session->set_userdata('filtro_idtipo', $idtipo);
		$this->session->set_userdata('filtro_codigo', $codigo);
		$this->session->set_userdata('filtro_codbox', $codbox);
		$this->session->set_userdata('filtro_asunto', $asunto);
		$this->session->set_userdata('filtro_remitente', $remitente);
		$this->session->set_userdata('filtro_ffin', $ffin);
        $this->session->set_userdata('filtro_finicio', $finicio);
		$this->session->set_userdata('filtro_categoria', $idcategoria);
		
        $aStand = array();
        $aFiles = array();
        $aFiles = $this->filemodel->getFiles();
        $aStand = $this->standmodel->getStand();
       
		$where = "d.estado_registro = 1";
        if ($idalmacen != "") {
            $where.=" AND d.idalmacen = " . $idalmacen;
        }
        if ($file != "") {
            $where.=" AND d.idfile = ".$file;
        }
		if ($stand != "") {
            $where.=" AND d.idstand = ".$stand;
        }
        if ($estado != "") {
            $where.=" AND d.estado = ".$estado;
        }
        if ($idtipo != "") {
            $where.=" AND d.idtipodoc = " . intval($idtipo);
        }
        if ($codigo != "") {
            $where.=" AND d.codigo like '%" . $codigo . "%'";
        }
		if ($remitente != "") {
            $where.=" AND d.remitente like '%" . $remitente . "%'";
        }
		if ($idcategoria != "") {
            $where.=" AND d.idcategoria = " . intval($idcategoria);
        }
        if ($asunto != "") {
            $where.=" AND d.asunto like '%" . $asunto . "%'";
        }
        
        if ($finicio != "" && $ffin != "") {
            $where.=" AND date(d.fecha_recepcion) BETWEEN '" . getFechaEn2($finicio) . "' and '" . getFechaEn2($ffin) . "' ";
        }
		
        $aCategoria = $this->categoriamodel->getaCateUsuario();
        $aListaDocumentos = $this->documentomodel->getListaDocumentos($where);
        $data['aFiles'] = $aFiles;
        $data['aStand'] = $aStand;
        $data['aCategoria'] = $aCategoria;
        $data['aListaDocumentos'] = $aListaDocumentos;
        $data['origen'] = "buscar";

        $this->load->view("documentos/listabusqueda", $data);
    }

    public function get_div_valor($tipo = "texto") {
        $this->load->model('tipodocmodel');
        $this->load->model('categoriamodel');
        $oTipoDoc = array();
        switch ($tipo) {
            case "tipodoc":
                $this->load->model('tipodocmodel');
                $oTipoDoc = $this->tipodocmodel->getdata(array("idtipodoc", "nombre"));
                break;

            default:
                break;
        }
        $data["oTipoDoc"] = $oTipoDoc;
        $data["tipo"] = $tipo;
        $this->load->view("documentos/valor", $data);
    }

    public function get_div_derivar($iddoc = 0) {
        $this->load->model('tipodocmodel');
        $this->load->model('categoriamodel');

        $oDocumento = array();
        $aCategorias = array();
        $oDocumento = $this->documentomodel->getdata(array("iddocumento", "asunto", "codigo", "remitente", "idcategoria"), "iddocumento = " . $iddoc, 1);
        if ($oDocumento != false) {
            $aCategorias = $this->categoriamodel->getdata(array("idcategoria", "nombre"));
        }

        $data["oDocumento"] = $oDocumento;
        $data["aCategorias"] = $aCategorias;
        $this->load->view("documentos/divderivar", $data);
    }
	 public function get_div_box() {
		 header('Content-Type: text/html; charset=UTF-8'); 
        $this->load->model('almacenmodel');
        $aAlmacen = array();
        $aAlmacen = $this->almacenmodel->getdata(array("idalmacen", "nombre"),"estado_registro = 1");
        
        $data["aAlmacen"] = $aAlmacen;
        $this->load->view("documentos/divbox", $data);
    }

    public function lista() {
		validaSession("formulario");
        
        // Trayendo modelos
        $mensaje = $this->session->userdata('mensaje');
        if ($mensaje != "") {
            $mensaje = getHtmlMensaje(1, $mensaje);
        }
        $this->session->set_userdata('mensaje', "");
        $aCampos = array(
            "identificador" => "Nro identificador del doc.",
            "codigo" => "Código generado",
            "remitente" => "Remitente",
            "asunto" => "Asunto del doc.",
            "idtipodoc" => "Tipo de doc.",
            "fecha_recepcion" => "Fecha recepción",
            "fecha_registro" => "Fecha registro",
            "estado" => "Estado del doc.",
            "idalmacen" => "Almacén",
            "idfile" => "COD. File",
            "idstand" => "COD. Stand",
            "codbox" => "COD. Box"
        );
        $data["aCampos"] = $aCampos;
		$this->load->model('tipodocmodel');
        $this->load->model('almacenmodel');
		

        $aTipodoc = array();
		$aCategorias = array();
        $aTipodoc = $this->tipodocmodel->getdata(array("idtipodoc", "nombre"));
        $aAlmacen = $this->almacenmodel->getdata(array("idalmacen", "nombre"));
		$this->load->model('filemodel');
		$this->load->model('categoriamodel');
		$this->load->model('standmodel');
		$aStand = array();
        $aFiles = array(); 
		$aCategorias = $this->categoriamodel->getdata(array("idcategoria", "nombre"));
        $aFiles = $this->filemodel->getdata(array("idfile","codigo"));
        $aStand = $this->standmodel->getdata(array("idstand","codigo"));
		$data['aCategorias'] = $aCategorias;
		$data['aFiles'] = $aFiles;
        $data['aStand'] = $aStand;
        $data["aTipodoc"] = $aTipodoc;
        $data["aAlmacen"] = $aAlmacen;
        $data["titulo"] = "Lista de documentos";
        $botones = "";
        //$botones .= '<a href = "' . base_url() . 'tarea/lista" class = "btn btn-sm btn-default" >Atras</a> ';
        $botones .= '<a href = "' . base_url() . 'documento/nuevo" class = "btn btn-sm btn-default" >Nuevo</a> <a href = "javascript:void(0);" class = "btn btn-sm btn-default btnaddbox" >Adjuntar a box</a>';
        $this->template->add_js('media/js/buscador.js');
        $this->template->add_js('media/js/documento.js');
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Lista de documentos", TRUE);
        $this->template->write_view('content', 'documentos/listar', $data, TRUE);
        $this->template->render();
    }

    public function json_buscar_tag() {
        $this->load->model('tagsmodel');
        $aEtiquetas = array();
        $aEtiquetas = $this->tagsmodel->getdata(array("idtags as value", "nombre as label"));
        echo json_encode($aEtiquetas);
    }

    public function json_crear_documento() {

        $this->load->model('archivodocmodel');
		if(intval($this->_idusuario_actual)==0){
			echo "<script type='text/javascript'>alert('".utf8_decode("Su sessión expiró")."');window.location.href='http://www.intranet.hikari.com.pe';</script>";
			exit;
		}
		/*------------- VALIDANDO LOS CAMPOS ------------------ */
		$msjCampos="";
		if($this->input->post("cbofile")==""){
			$msjCampos.="Debe seleccionar el file \n";
		}
		if($this->input->post("cboLugar")==""){
			$msjCampos.="Debe seleccionar el almacén del documento \n";
		}
		if($this->input->post("txtorigen")==""){
			$msjCampos.="Debe escribir el remitente del documento \n";
		}
		if($this->input->post("cbocategoria")==""){
			$msjCampos.="Debe seleccionar a quién va dirigido el documento \n";
		}
		if($this->input->post("txtasunto")==""){ 
			$msjCampos.="Debe escribir el asunto del documento \n";
		}
		
		if($this->input->post("cbotipodoc")==""){
			$msjCampos.="Debe seleccionar el tipo de documento \n";
		}
		
		if($this->input->post("frecepcion")==""){
			$msjCampos.="Debe sLugar Almacenajeeleccionar la fecha de recepción \n";
		}
		
		if($msjCampos!=""){
			$this->session->set_userdata('mensaje', $msjCampos);
			redirect(base_url() . "documento/nuevo", 'refresh');
			exit;
		}
		
		/*echo "<pre>";
		var_dump($_FILES);
		echo "</pre>";
		exit; */
		/*---------- BORRAR DESPUÉS DE MIGRA -------------------*/
		
		$idfile = $this->input->post("cbofile");
		$idstand = $this->input->post("cbostand");
		$idalmacen = $this->input->post("cboLugar");
		$codbox = $this->input->post("txtbox");
		
		/*---------- ./ BORRAR DESPUÉS DE MIGRA -------------------*/
        $identificador = $this->input->post("txtidentificador");
        $remitente = $this->input->post("txtorigen");
        $idcategoria = $this->input->post("cbocategoria");
        $asunto = $this->input->post("txtasunto");
        $idtipodoc = $this->input->post("cbotipodoc");
        $frecepcion = $this->input->post("frecepcion");
        $frecepcion = getFechaEn2($frecepcion);
        $descripcion = $this->input->post("txtdescripcion");
        $estado_registro = 1;
		
		$msg_error = "";
        // Validar si existe el id de tarea
        $auxDocumento = array();
        $objDocumento = array();
        $auxDocumento = $this->documentomodel->getExiste("identificador", $identificador, 0, 1);
		$objDocumento = array(
				"idfile" => $idfile,
				"idstand" => $idstand,
				"idalmacen" => $idalmacen,
				"codbox" => $codbox,
                "identificador" => $identificador,
                "remitente" => $remitente,
                "idcategoria" => $idcategoria,
                "asunto" => $asunto,
                "idtipodoc" => $idtipodoc,
                "fecha_recepcion" => $frecepcion,
                "estado" => 3,
                "idusuario_creador" => $this->_idusuario_actual,
                "comentario" => $descripcion,
                "fecha_registro" => $this->_fecha_actual,
                "fecha_modificacion" => $this->_fecha_actual,
                "estado_registro" => $estado_registro
            );
			
            $iddocumento = $this->documentomodel->guardar($objDocumento);
            $objDocumento = array(
                "codigo" => "DOC-" . date("Y") . date("m") . "-" . str_pad($iddocumento, 5, "0", STR_PAD_LEFT)
            );
            $this->documentomodel->editar($iddocumento, $objDocumento);
            // Subimos los archivos si el usuario fue creado exitosamente
            if ($iddocumento < 1) {
                $msg_error.="No se pudo registrar";
            } else {
                $archivos = array();
                if (!empty($_FILES)) {
                    $archivos = subirArchivos2($_FILES);
                }
                foreach ($archivos as $archi) {
                    $objArchivo = array("iddocumento" => $iddocumento,
                        "url" => $archi["ruta"],
                        "idtipoarchivo" => 1
                    );
                    $numFilasArchivos = $this->archivodocmodel->guardar($objArchivo);
                    if ($numFilasArchivos < 1) {
                        $msg_error.= "Error al registrar archivo" . $iddocumento;
                    }
                }
            }
			$this->guardarMovimiento($iddocumento, "Se creó el documento", 3);
        
        if ($msg_error != "") {
			$this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "El documento se creó exitosamente");
        }
        redirect(base_url() . "documento/lista", 'refresh');
    }
 
	  public function json_mover_documento() {
        $this->load->model('archivodocmodel');
		$this->load->model('almacenmodel');
        $ids = $this->input->post("ids");
		$idlugar = $this->input->post("idlugar");
        $codbox = $this->input->post("box"); 
		$oAlmacen=$this->almacenmodel->getdata(array("nombre"),"idalmacen = ".$idlugar,1);
		$nomAlmacen=$oAlmacen->nombre;
        $msg_error = "";
		$aIds = explode(",", trim($ids,",")); 
		//echo var_dump($aIds);exit;
		$contador=0;
		foreach ($aIds as $itemId) {
			$objDocumento = array();
			$objDocumento = array(
				"idalmacen" => $idlugar,
				"estado" => 5,
				"codbox" => $codbox,
				"fecha_modificacion" => $this->_fecha_actual            
			);
			$this->documentomodel->editar($itemId, $objDocumento);
			//$this->guardarMovimiento($itemId, 'Se movió el documento al almacén "'.$nomAlmacen.'" en la caja '.$codbox, 5);			
        }
        echo $contador;exit;
       
    }
 
    public function json_editar_documento() {
        $this->load->model('transportemodel');
        $this->load->model('archivodocmodel');

        // Datos del documento
        $iddoc = $this->input->post("iddoc");
        $vDoc = $this->documentomodel->getdata(array("estado"), "iddocumento = " . $iddoc, 1);
        $identificador = $this->input->post("identificador");
        $objDocumento["identificador"] = $identificador;

        $frecepcion = $this->input->post("frecepcion");
        $frecepcion = getFechaEn2($frecepcion);
        $objDocumento["fecha_recepcion"] = $frecepcion;

        $remitente = $this->input->post("remitente");
        $objDocumento["remitente"] = $remitente;

        $idcategoria = $this->input->post("idcategoria");
        $objDocumento["idcategoria"] = $idcategoria;

        $idtipodoc = $this->input->post("idtipodoc");
        $objDocumento["idtipodoc"] = $idtipodoc;

        $asunto = $this->input->post("asunto");
        $objDocumento["asunto"] = $asunto;

        $descripcion = $this->input->post("descripcion");
        $objDocumento["comentario"] = $descripcion;

        if ($vDoc->estado == 2) {
            $fenvio = $this->input->post("fenvio");
            $fenvio = getFechaEn2($fenvio);
            $idlugar = $this->input->post("idlugar");
            $modalidad = $this->input->post("modalidad");
            $responsable = $this->input->post("responsable");
            $comentario_traslado = $this->input->post("comentario_traslado");
            $oTrans = $this->transportemodel->getdata(array("idtransporte"), "iddocumento = " . $iddoc, 1);
            $oTransporte = array(
                "fecha_envio" => $fenvio,
                "modalidad" => $modalidad,
                "responsable" => $responsable,
                "comentario" => $comentario_traslado
            );
            $this->transportemodel->editar($oTrans->idtransporte, $oTransporte);
        }
        if ($vDoc->estado == 3 || $vDoc->estado == 4) {
            $fecha_almacen = $this->input->post("fecha_almacen");
            $fecha_almacen = getFechaEn2($fecha_almacen);
            $objDocumento["fecha_almacenamiento"] = $fecha_almacen;

            $idfile = $this->input->post("idfile");
            $objDocumento["idfile"] = $idfile;

            $idstand = $this->input->post("idstand");
            $objDocumento["idstand"] = $idstand;

            $codbox = $this->input->post("codbox");
            $objDocumento["codbox"] = $codbox;

            $comentario_almacen = $this->input->post("comentario_almacen");
            $objDocumento["comentario_almacen"] = $comentario_almacen;
        }

        // Datos del traslado
        // Datos de almacén



        $estado_registro = 1;
        $msg_error = "";
        // Validar si existe el id de tarea
        $auxDocumento = array();

        $auxDocumento = $this->documentomodel->getExiste("identificador", $identificador, $iddoc, 1);
        if ($auxDocumento->cantidad == 0) {

            $this->documentomodel->editar($iddoc, $objDocumento);
            // Subimos los archivos si el usuario fue creado exitosamente
            if ($iddoc < 1) {
                $msg_error.="No se pudo registrar";
            } else {
                $archivos = array();
                if (!empty($_FILES)) {
                    $archivos = subirArchivos($_FILES);
                }
                foreach ($archivos as $archi) {
                    $objArchivo = array("iddocumento" => $iddoc,
                        "url" => $archi["ruta"],
                        "idtipoarchivo" => 1
                    );
                    $numFilasArchivos = $this->archivodocmodel->guardar($objArchivo);
                    if ($numFilasArchivos < 1) {
                        $msg_error.= "Error al registrar archivo" . $iddoc;
                    }
                }
            }
        } else {
            $msg_error.="El documento ya fué registrado";
        }
        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "El documento se editó exitosamente");
        }
        $this->guardarMovimiento($iddoc, "Se editó el documento", $vDoc->estado);
        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    public function nuevo() {

        // Trayendo modelos
        $this->load->model('usuariomodel');
        $this->load->model('almacenmodel');
        $this->load->model('archivodocmodel');
        $this->load->model('tipodocmodel');
        $this->load->model('categoriamodel');
		$this->load->model('filemodel');
		$this->load->model('standmodel');
        $aCategoria = $this->categoriamodel->getdata(array("idcategoria", "nombre"));
        $data["aCategoria"] = $aCategoria;
        $aTipoDoc = $this->tipodocmodel->getdata(array("*"), "estado_registro = 1");
        $aAlmacenes = $this->almacenmodel->getdata(array("*"), "estado_registro = 1");
		$aFile = $this->filemodel->getdata(array("*"), "estado_registro = 1");
		$aStand = $this->standmodel->getdata(array("*"), "estado_registro = 1");
        //$aArchivosDoc = $this->archivodocmodel->getdata(array("*"), "iddocumento = 1");
        $data['titulo'] = "Nuevo documento";
        $data['aAlmacenes'] = $aAlmacenes;
        $data['aTipoDoc'] = $aTipoDoc;
		$data['aFile'] = $aFile;
		$data['aStand'] = $aStand;
        //$data['aArchivosDoc'] = $aArchivosDoc;
        $botones = "";
        $botones .= '<a href = "' . base_url() . 'documento/lista" class = "btn btn-sm btn-default" >Atras</a> ';
        $this->template->add_css('media/tag/jquery.tagit.css');
        //$this->template->add_css('media/tag/Bootstrap.css');
        //$this->template->add_js('media/tag/tag-it.js');
        //$this->template->add_js('media/tag/configuracion.js');
        $this->template->add_js('media/js/documento.js');
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Nuevo documento", TRUE);
        $this->template->write_view('content', 'documentos/crear', $data, TRUE);
        $this->template->render();
    }

    public function editar($iddocumento) {
        // Trayendo modelos
        $this->load->model('usuariomodel');
        $this->load->model('almacenmodel');
        $this->load->model('tagsmodel');
        $this->load->model('archivodocmodel');
        $this->load->model('filemodel');
        $this->load->model('standmodel');
        $this->load->model('tipodocmodel');
        $this->load->model('categoriamodel');
        $this->load->model('transportemodel');

        $aArchivosDoc = array();
        $aArchivosDocTransporte = array();
        $aArchivosDocAlmacen = array();
        $aAlmacenes = array();
        $aTags = array();
        $aTipoDoc = array();
        $objDocumento = array();
        $aCategoria = array();
        $objDocumento = $this->documentomodel->getdata(array("*"), "iddocumento = " . $iddocumento, 1);
        if ($objDocumento != FALSE) {
            $this->session->set_userdata('mensaje', "");
            $aArchivosDoc = $this->archivodocmodel->getdata(array("*"), "iddocumento = " . $iddocumento . " and idtipoarchivo = 1");
            $aArchivosDocTransporte = $this->archivodocmodel->getdata(array("*"), "iddocumento = " . $iddocumento . " and idtipoarchivo = 2");
            $aArchivosDocAlmacen = $this->archivodocmodel->getdata(array("*"), "iddocumento = " . $iddocumento . " and idtipoarchivo = 3");
            $aTipoDoc = $this->tipodocmodel->getdata(array("*"), "estado_registro = 1");
            $aFile = $this->filemodel->getdata(array("*"), "estado_registro = 1");
            $aStand = $this->standmodel->getdata(array("*"), "estado_registro = 1");
            $aAlmacenes = $this->almacenmodel->getdata(array("*"), "estado_registro = 1");
            $aCategoria = $this->categoriamodel->getdata(array("idcategoria", "nombre"));
        }

        $oTransporte = array();
        $oTransporte = $this->transportemodel->getdata(array("*"), "iddocumento = " . $iddocumento, 1);


        $data['oTransporte'] = $oTransporte;
        $data['titulo'] = "Editar documento";
        $data["aCategoria"] = $aCategoria;
        $data['aFile'] = $aFile;
        $data['aStand'] = $aStand;
        $data['objDocumento'] = $objDocumento;
        $data['aArchivos'] = $aArchivosDoc;
        $data['aArchivosDocTransporte'] = $aArchivosDocTransporte;
        $data['aArchivosDocAlmacen'] = $aArchivosDocAlmacen;
        $data['aAlmacenes'] = $aAlmacenes;
        $data['aTipoDoc'] = $aTipoDoc;

        $botones = "";
        $botones .= '<a href = "' . base_url() . 'documento/lista" class = "btn btn-sm btn-default" >Atras</a> ';
        $botones .= '<a href = "' . base_url() . 'documento/nuevo" class = "btn btn-sm btn-default" >Nuevo</a> ';
        $this->template->add_css('media/tag/jquery.tagit.css');
        //$this->template->add_css('media/tag/Bootstrap.css');
        //$this->template->add_js('media/tag/tag-it.js');
        //$this->template->add_js('media/tag/configuracion2.js');
        $this->template->add_js('media/js/documento.js');
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Editar documento", TRUE);
        $this->template->write_view('content', 'documentos/editar', $data, TRUE);
        $this->template->render();
    }

    public function json_transportar_documento() {
        $this->load->model('archivodocmodel');
        $this->load->model('transportemodel');
        $this->load->model('almacenmodel');
        $idlugar = $this->input->post("idlugar");
        $iddoc = $this->input->post("iddocumento");
        $modalidad = $this->input->post("modalidad");
        $responsable = $this->input->post("responsable");
        $fecha_envio = $this->input->post("fecha_envio");
        $descripcion = $this->input->post("descripcion");
        $msg_error = "";
        // Validar si existe el id de tarea
        $oTransporte = array();
        $oTransporte = array(
            "iddocumento" => $iddoc,
            "fecha_envio" => getFechaEn2($fecha_envio),
            "modalidad" => $modalidad,
            "responsable" => $responsable,
            "comentario" => $descripcion,
            "fecha_registro" => $this->_fecha_actual
        );

        $this->transportemodel->guardar($oTransporte);
        $oDocumento = array();
        $oDocumento = array(
            "estado" => 2,
            "idalmacen" => $idlugar
        );
        $this->documentomodel->editar($iddoc, $oDocumento);
        // Subimos los archivos si el usuario fue creado exitosamente
        if ($iddoc < 1) {
            $msg_error.="No se pudo registrar";
        } else {
            $archivos = array();
            if (!empty($_FILES)) {
                $archivos = subirArchivos($_FILES);
            }
            foreach ($archivos as $archi) {
                $objArchivo = array("iddocumento" => $iddoc,
                    "url" => $archi["ruta"],
                    "idtipoarchivo" => 2
                );
                $numFilasArchivos = $this->archivodocmodel->guardar($objArchivo);
                if ($numFilasArchivos < 1) {
                    $msg_error.= "Error al registrar archivo" . $iddoc;
                }
            }
        }

        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "Los datos del transporte fueron guardados");
        }
        $this->guardarMovimiento($iddoc, "Se realizó el transporte del documento", 2);
        $oUsuarioAlmacen = array();
        $oUsuarioAlmacen = $this->almacenmodel->getUsuarioAlmacen("idalmacen = " . $idlugar, 1);
        enviarEmail(array(
            "FROM" => "Hikari- Notificaciones",
            "FROMNAME" => "sistemas@hikari.pe",
            "asunto" => "Se te está enviando un documento",
            "titulo" => "Hola " . $oUsuarioAlmacen->nombre,
            "mensaje" => "Se te está enviando un documento a su almacén",
            "correo" => $oUsuarioAlmacen->correo
        ));
        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    public function json_almacenar_documento() {
        $this->load->model('archivodocmodel');
        $this->load->model('categoriamodel');
        $iddoc = $this->input->post("iddoc");
        $accion = $this->input->post("accion");
        $fecha_recepcion = $this->input->post("fecha_recepcion");

        $idfile = $this->input->post("idfile");
        $idstand = $this->input->post("idstand");
        $codbox = $this->input->post("codbox");
        $descripcion = $this->input->post("descripcion");
        $msg_error = "";
        // Validar si existe el id de tarea
        $auxDocumento = array();
        $auxDocumento = $this->documentomodel->getdata(array("idcategoria"), "iddocumento = " . $iddoc, 1);
        $oDocumento = array();
        $oDocumento = array(
            "fecha_almacenamiento" => getFechaEn($fecha_recepcion),
            "idfile" => $idfile,
            "codbox" => $codbox,
            "idstand" => $idstand,
            "comentario_almacen" => $descripcion,
            "fecha_modificacion" => $this->_fecha_actual,
            "estado" => 3
        );
        $this->documentomodel->editar($iddoc, $oDocumento);
        // Subimos los archivos si el usuario fue creado exitosamente
        if ($iddoc < 1) {
            $msg_error.="No se pudo registrar";
        } else {
            $archivos = array();
            if (!empty($_FILES)) {
                $archivos = subirArchivos($_FILES);
            }
            foreach ($archivos as $archi) {
                $objArchivo = array("iddocumento" => $iddoc,
                    "url" => $archi["ruta"],
                    "idtipoarchivo" => 3
                );
                $numFilasArchivos = $this->archivodocmodel->guardar($objArchivo);
                if ($numFilasArchivos < 1) {
                    $msg_error.= "Error al registrar archivo" . $iddoc;
                }
            }
        }

        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "El documento se creó exitosamente");
        }
        if ($accion == 2) {
            $aCateUsuario = $this->categoriamodel->getaCateUsuario();
            $oDocumento = array();
            $oDocumento["estado"] = 4;
            $this->documentomodel->editar($iddoc, $oDocumento);
            enviarEmail(array(
                "FROM" => "Hikari- Notificaciones",
                "FROMNAME" => "sistemas@hikari.pe",
                "asunto" => "Tienes un nuevo documento",
                "titulo" => "Hola " . $aCateUsuario[$auxDocumento->idcategoria]["nomresponsable"],
                "mensaje" => "Te acaba de llegar un nuevo documento",
                "correo" => $aCateUsuario[$auxDocumento->idcategoria]["correo"]
            ));
            $this->session->set_userdata('mensaje', "El documento se creó exitosamente");
            $this->guardarMovimiento($iddoc, "Se almacenó y derivó el documento al destinatario", 4);
        } else {
            $this->guardarMovimiento($iddoc, "Se almacenó el documento", 3);
        }

        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    public function json_derivar_documento() {
        $this->load->model('archivodocmodel');
        $this->load->model('categoriamodel');
        $iddoc = $this->input->post("iddoc");
        $descripcion = $this->input->post("descripcion");
        $idcategoria = $this->input->post("idcategoria");
        $accion = $this->input->post("accion");
        $estado_registro = 4;

        $msg_error = "";
        $oAuxDocumento = $this->documentomodel->getdata(array("remitente", "asunto", "codigo"), "iddocumento = " . $iddoc);
        // Validar si existe el id de tarea
        $oDocumento = array();
        $oDocumento = array(
            "fecha_modificacion" => $this->_fecha_actual,
            "estado" => $estado_registro
        );

        $this->documentomodel->editar($iddoc, $oDocumento);
        // Subimos los archivos si el usuario fue creado exitosamente
        if ($iddoc < 1) {
            $msg_error.="No se pudo registrar";
        } else {
            $archivos = array();
            if (!empty($_FILES)) {
                $archivos = subirArchivos($_FILES);
            }
            foreach ($archivos as $archi) {
                $objArchivo = array("iddocumento" => $iddoc,
                    "url" => $archi["ruta"],
                    "idtipoarchivo" => 4
                );
                $numFilasArchivos = $this->archivodocmodel->guardar($objArchivo);
                if ($numFilasArchivos < 1) {
                    $msg_error.= "Error al registrar archivo" . $iddoc;
                }
            }
        }

        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $idusuario_ejecutor = 0;
            $aCateUsuario = $this->categoriamodel->getaCateUsuario();
            $idusuario_ejecutor = $aCateUsuario[$idcategoria]["idusuario"];
            $idusuario_creador = $this->_idusuario_actual;
            $this->guardarMovimiento($iddoc, "Se derivó el documento al destinatario (" . $aCateUsuario[$idcategoria]["nomresponsable"] . ") ", 4);
            enviarEmail(array(
                "FROM" => "Hikari- Notificaciones",
                "FROMNAME" => "sistemas@hikari.pe",
                "asunto" => "Tienes un nuevo documento",
                "titulo" => "Hola " . $aCateUsuario[$idcategoria]["nomresponsable"],
                "mensaje" => "Te acaba de llegar un nuevo documento",
                "correo" => $aCateUsuario[$idcategoria]["correo"]
            ));
            $this->session->set_userdata('mensaje', "El documento se creó exitosamente");
        }

        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    public function transportar($iddocumento) {
        // Trayendo modelos
        $this->load->model('usuariomodel');
        $this->load->model('almacenmodel');
        $this->load->model('tagsmodel');
        $this->load->model('archivodocmodel');
        $this->load->model('standmodel');
        $this->load->model('filemodel');
        $this->load->model('tipodocmodel');
        $this->load->model('categoriamodel');
        $this->load->model('movimientomodel');
        $aArchivosDoc = array();
        $aArchivosDocTransporte = array();
        $aArchivosDocAlmacen = array();
        $aTags = array();
        $aDocs = array();
        $objDocumento = $this->documentomodel->getListaDocumentos("iddocumento = " . $iddocumento, 1);

        if ($objDocumento != FALSE) {
            $oCategoria = $this->categoriamodel->getdata(array("nombre"), "idcategoria = " . $objDocumento->idcategoria, 1);
            $destinatario = $oCategoria->nombre;

            $oTipoDoc = $this->tipodocmodel->getdata(array("nombre"), "idtipodoc = " . $objDocumento->idtipodoc, 1);
            $tipodoc = $oTipoDoc->nombre;

            $oUsuario = $this->usuariomodel->getdata(array("nombre"), "idusuario = " . $objDocumento->idusuario_creador, 1);
            $nomusuario = $oUsuario->nombre;
            if (intval($objDocumento->idfile) > 0) {
                $oFile = $this->filemodel->getdata(array("codigo"), "idfile = 1" . $objDocumento->idfile, 1);
                $nomfile = $oFile->codigo;
            } else {
                $nomfile = "";
            }

            if (intval($objDocumento->idstand) > 0) {
                $oStand = $this->standmodel->getdata(array("codigo"), "idstand = " . $objDocumento->idstand, 1);
                $nomStand = $oStand->codigo;
            } else {
                $nomStand = "";
            }


            $nomAlmacenaje = "";
            if ($objDocumento->codbox == "") {
                $nomAlmacenaje = "Stand";
            } elseif ($objDocumento->idalmacen == 1) {
                $nomAlmacenaje = "Box";
            }
            $aDocs = array(
                "iddocumento" => $objDocumento->iddocumento,
                "codigo" => $objDocumento->codigo,
                "remitente" => $objDocumento->remitente,
                "destinatario" => $destinatario,
                "asunto" => $objDocumento->asunto,
                "fecha_recepcion" => getFechaEs2($objDocumento->fecha_recepcion),
                "tipodoc" => $tipodoc,
                "nomalmacen" => $objDocumento->nomalmacen,
                "file" => $nomfile,
                "stand" => $nomStand,
                "box" => $objDocumento->codbox,
                "comentario" => $objDocumento->comentario,
                "nomcreador" => $nomusuario,
                "nomalmacenaje" => $nomAlmacenaje,
                "nomestado" => getnomEstadoDoc($objDocumento->estado)
            );

            $this->session->set_userdata('mensaje', "");
            $aMovimiento = array();
            $aMovimiento = $this->movimientomodel->getdata(array("*"), "iddocumento = " . $iddocumento);
            $data['aMovimiento'] = $aMovimiento;
            $aAlmacenes = array();
            $aAlmacenes = $this->almacenmodel->getdata(array("*"), "estado_registro = 1");
            $data['aAlmacenes'] = $aAlmacenes;
            $aArchivosDoc = $this->archivodocmodel->getdata(array("*"), "iddocumento = " . $iddocumento . " and idtipoarchivo = 1");
            $aArchivosDocTransporte = $this->archivodocmodel->getdata(array("*"), "iddocumento = " . $iddocumento . " and idtipoarchivo = 2");
            $aArchivosDocAlmacen = $this->archivodocmodel->getdata(array("*"), "iddocumento = " . $iddocumento . " and idtipoarchivo = 3");
        }

        $data['titulo'] = "Transporte del documento";
        $oUsuario = $this->usuariomodel->getUsuarios();
        $data['oUsuario'] = $oUsuario;
        $data['objDocumento'] = $aDocs;
        $data['aArchivos'] = $aArchivosDoc;
        $data['aArchivosDocTransporte'] = $aArchivosDocTransporte;
        $data['aArchivosDocAlmacen'] = $aArchivosDocAlmacen;

        $botones = "";
        $botones .= '<a href = "' . base_url() . 'documento/lista" class = "btn btn-sm btn-default" >Atras</a> ';
        $botones .= '<a href = "' . base_url() . 'documento/nuevo" class = "btn btn-sm btn-default" >Nuevo</a> ';
        //$this->template->add_css('media/tag/jquery.tagit.css');
        //$this->template->add_css('media/tag/Bootstrap.css');
        //$this->template->add_js('media/tag/tag-it.js');
        //$this->template->add_js('media/tag/configuracion2.js');
        $this->template->add_js('media/js/documento.js');
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Transporte del documento", TRUE);
        $this->template->write_view('content', 'documentos/transportar', $data, TRUE);
        $this->template->render();
    }

    public function ver($iddocumento) {
        // Trayendo modelos
        $this->load->model('usuariomodel');
        $this->load->model('almacenmodel');
        $this->load->model('transportemodel');
        $this->load->model('archivodocmodel');
        $this->load->model('standmodel');
        $this->load->model('filemodel');
        $this->load->model('tipodocmodel');
        $this->load->model('categoriamodel');
        $this->load->model('movimientomodel');
        $aArchivosDoc = array();
        $aArchivosDocTransporte = array();
        $aArchivosDocAlmacen = array();
        $aTags = array();
        $aDocs = array();
        $objDocumento = $this->documentomodel->getListaDocumentos("iddocumento = " . $iddocumento, 1);

        if ($objDocumento != FALSE) {
            $oCategoria = $this->categoriamodel->getdata(array("nombre"), "idcategoria = " . $objDocumento->idcategoria, 1);
            $destinatario = $oCategoria->nombre;

            $oTipoDoc = $this->tipodocmodel->getdata(array("nombre"), "idtipodoc = " . $objDocumento->idtipodoc, 1);
            $tipodoc = $oTipoDoc->nombre;

            $oUsuario = $this->usuariomodel->getdata(array("nombre"), "idusuario = " . $objDocumento->idusuario_creador, 1);
            $nomusuario = $oUsuario->nombre;
            if (intval($objDocumento->idfile) > 0) {
                $oFile = $this->filemodel->getdata(array("codigo"), "idfile = " . $objDocumento->idfile, 1);
                $nomfile = $oFile->codigo;
            } else {
                $nomfile = "";
            }

            if (intval($objDocumento->idstand) > 0) {
                $oStand = $this->standmodel->getdata(array("codigo"), "idstand = " . $objDocumento->idstand, 1);
                $nomStand = $oStand->codigo;
            } else {
                $nomStand = "";
            }


            $nomAlmacenaje = "";
            if ($objDocumento->codbox == "") {
                $nomAlmacenaje = "Almacén central/ Stand";
            } elseif ($objDocumento->idalmacen == 1) {
                $nomAlmacenaje = "Almacén externo/ Box";
            }
            $aDocs = array(
                "codigo" => $objDocumento->codigo,
                "origen" => $objDocumento->remitente,
                "destinatario" => $destinatario,
                "asunto" => $objDocumento->asunto,
                "fecha_recepcion" => getFechaEs2($objDocumento->fecha_recepcion),
                "tipodoc" => $tipodoc,
                "nomalmacen" => $objDocumento->nomalmacen,
                "file" => $nomfile,
                "stand" => $nomStand,
                "codbox" => $objDocumento->codbox,
                "comentario" => $objDocumento->comentario,
                "nomcreador" => $nomusuario,
                "nomalmacenaje" => $nomAlmacenaje,
                "nomestado" => getnomEstadoDoc($objDocumento->estado)
            );
            $oTransporte = array();
            $oTransporte = $this->transportemodel->getdata(array("*"), "iddocumento = " . $iddocumento, 1);
            $aMovimiento = array();
            $aMovimiento = $this->movimientomodel->getdata(array("*"), "iddocumento = " . $iddocumento);
            $data['aMovimiento'] = $aMovimiento;
            $this->session->set_userdata('mensaje', "");
            $aArchivosDoc = $this->archivodocmodel->getdata(array("*"), "iddocumento = " . $iddocumento . " and idtipoarchivo = 1");
            $aArchivosDocTransporte = $this->archivodocmodel->getdata(array("*"), "iddocumento = " . $iddocumento . " and idtipoarchivo = 2");
            $aArchivosDocAlmacen = $this->archivodocmodel->getdata(array("*"), "iddocumento = " . $iddocumento . " and idtipoarchivo = 3");
        }
        $oUsuario = $this->usuariomodel->getUsuarios();
        $data['oUsuario'] = $oUsuario;
        $data['titulo'] = "Vista documento";
        $data['oTransporte'] = $oTransporte;
        $data['objDocumento'] = $aDocs;
        $data['aArchivos'] = $aArchivosDoc;
        $data['aArchivosDocTransporte'] = $aArchivosDocTransporte;
        $data['aArchivosDocAlmacen'] = $aArchivosDocAlmacen;

        $botones = "";
        $botones .= '<a href = "' . base_url() . 'documento/lista" class = "btn btn-sm btn-default" >Atras</a> ';
        $botones .= '<a href = "' . base_url() . 'documento/nuevo" class = "btn btn-sm btn-default" >Nuevo</a> ';
        $this->template->add_css('media/tag/jquery.tagit.css');
        //$this->template->add_css('media/tag/Bootstrap.css');
        //$this->template->add_js('media/tag/tag-it.js');
        //$this->template->add_js('media/tag/configuracion2.js');
        $this->template->add_js('media/js/documento.js');
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Vista documento", TRUE);
        $this->template->write_view('content', 'documentos/ver', $data, TRUE);
        $this->template->render();
    }

    public function almacenar($iddocumento) {
        // Trayendo modelos
        $this->load->model('usuariomodel');
        $this->load->model('almacenmodel');
        $this->load->model('transportemodel');
        $this->load->model('archivodocmodel');
        $this->load->model('standmodel');
        $this->load->model('filemodel');
        $this->load->model('tipodocmodel');
        $this->load->model('categoriamodel');
        $this->load->model('movimientomodel');
        $aArchivosDoc = array();
        $aArchivosDocTransporte = array();
        $aArchivosDocAlmacen = array();
        $aFile = array();
        $aDocs = array();
        $oTransporte = array();
        $objDocumento = $this->documentomodel->getListaDocumentos("iddocumento = " . $iddocumento, 1);

        if ($objDocumento != FALSE) {
            $oCategoria = $this->categoriamodel->getdata(array("nombre"), "idcategoria = " . $objDocumento->idcategoria, 1);
            $destinatario = $oCategoria->nombre;

            $oTipoDoc = $this->tipodocmodel->getdata(array("nombre"), "idtipodoc = " . $objDocumento->idtipodoc, 1);
            $tipodoc = $oTipoDoc->nombre;

            $oAlmacen = $this->almacenmodel->getdata(array("nombre"), "idalmacen = " . $objDocumento->idalmacen, 1);
            $nomalmacen = $oAlmacen->nombre;

            $oUsuario = $this->usuariomodel->getdata(array("nombre"), "idusuario = " . $objDocumento->idusuario_creador, 1);
            $nomusuario = $oUsuario->nombre;
            if (intval($objDocumento->idfile) > 0) {
                $oFile = $this->filemodel->getdata(array("codigo"), "idfile = " . $objDocumento->idfile, 1);
                $nomfile = $oFile->codigo;
            } else {
                $nomfile = "";
            }

            if (intval($objDocumento->idstand) > 0) {
                $oStand = $this->standmodel->getdata(array("codigo"), "idstand = " . $objDocumento->idstand, 1);
                $nomStand = $oStand->codigo;
            } else {
                $nomStand = "";
            }


            $nomAlmacenaje = "";
            if ($objDocumento->codbox == "") {
                $nomAlmacenaje = "Stand";
            } elseif ($objDocumento->idalmacen == 1) {
                $nomAlmacenaje = "Box";
            }
            $aDocs = array(
                "iddocumento" => $objDocumento->iddocumento,
                "codigo" => $objDocumento->codigo,
                "remitente" => $objDocumento->remitente,
                "nomalmacen" => $nomalmacen,
                "destinatario" => $destinatario,
                "asunto" => $objDocumento->asunto,
                "fecha_recepcion" => getFechaEs2($objDocumento->fecha_recepcion),
                "tipodoc" => $tipodoc,
                "nomalmacen" => $objDocumento->nomalmacen,
                "file" => $nomfile,
                "stand" => $nomStand,
                "box_estante" => $objDocumento->codbox,
                "comentario" => $objDocumento->comentario,
                "nomcreador" => $nomusuario,
                "nomalmacenaje" => $nomAlmacenaje,
                "nomestado" => getnomEstadoDoc($objDocumento->estado)
            );
            $aMovimiento = array();
            $aMovimiento = $this->movimientomodel->getdata(array("*"), "iddocumento = " . $iddocumento);
            $data['aMovimiento'] = $aMovimiento;
            $this->session->set_userdata('mensaje', "");
            $aFile = $this->filemodel->getdata(array("*"), "estado_registro = 1");
            $aStand = $this->standmodel->getdata(array("*"), "estado_registro = 1");
            $oTransporte = $this->transportemodel->getdata(array("*"), "iddocumento = " . $iddocumento, 1);
            $aArchivosDoc = $this->archivodocmodel->getdata(array("*"), "iddocumento = " . $iddocumento . " and idtipoarchivo = 1");
            $aArchivosDocTransporte = $this->archivodocmodel->getdata(array("*"), "iddocumento = " . $iddocumento . " and idtipoarchivo = 2");
            $aArchivosDocAlmacen = $this->archivodocmodel->getdata(array("*"), "iddocumento = " . $iddocumento . " and idtipoarchivo = 3");
        }
        $oUsuario = $this->usuariomodel->getUsuarios();
        $data['oUsuario'] = $oUsuario;
        $data['titulo'] = "Transporte del documento";
        $data['aFile'] = $aFile;
        $data['aStand'] = $aStand;
        $data['oTransporte'] = $oTransporte;
        $data['objDocumento'] = $aDocs;
        $data['aArchivos'] = $aArchivosDoc;
        $data['aArchivosDocTransporte'] = $aArchivosDocTransporte;
        $data['aArchivosDocAlmacen'] = $aArchivosDocAlmacen;

        $botones = "";
        $botones .= '<a href = "' . base_url() . 'documento/lista" class = "btn btn-sm btn-default" >Atras</a> ';
        $botones .= '<a href = "' . base_url() . 'documento/nuevo" class = "btn btn-sm btn-default" >Nuevo</a> ';
        //$this->template->add_css('media/tag/jquery.tagit.css');
        //$this->template->add_css('media/tag/Bootstrap.css');
        //$this->template->add_js('media/tag/tag-it.js');
        //$this->template->add_js('media/tag/configuracion2.js');
        $this->template->add_js('media/js/documento.js');
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Almacenar del documento", TRUE);
        $this->template->write_view('content', 'documentos/almacenar', $data, TRUE);
        $this->template->render();
    }

    public function guardarMovimiento($idmovimiento = 0, $mensaje = "", $estado = 0) {
        $this->load->model('movimientomodel');
        $oMovimiento = array();
        $oMovimiento = array(
            "iddocumento" => $idmovimiento,
            "estado" => $estado,
            "idusuario_creador" => $this->_idusuario_actual,
            "fecha_creacion" => $this->_fecha_actual,
            "comentario" => $mensaje
        );
        $this->movimientomodel->guardar($oMovimiento);
    }

}
