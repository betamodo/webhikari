<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Usuario extends MY_Controller {

    private $_fecha_actual;
    private $_idusuario_actual;
    private $_idlocal;
    private $_rol;

    function __construct() {
        parent::__construct();
        $this->_fecha_actual = date("Y-m-d H:i:s");
        $this->load->model('usuariomodel');
        $this->_idusuario_actual = $this->session->userdata('idusuario');
        $this->_rol = $this->session->userdata('rol');
        $this->_idlocal = $this->session->userdata('idlocal');
        $this->template->set_template('default');
        $this->load->helper(array('url', 'funciones', 'mysql_to_excel_helper'));
    }

    function index() {
        $this->login();
    }

    // [VISTA] Validar usuario.
    function login() {
        //$this->session->sess_destroy();
        if (intval($this->session->userdata('idusuario')) == 0) {
            $datos['mensaje'] = "Ingrese al sistema";
            $this->load->view('login', $datos);
        } else {
            redirect(base_url() . "tarea/lista", 'refresh');
        }
    }

    // [JSON] Método de validar usuario.
    function entrar() {
        $this->load->model('configuracionmodel');
        $this->load->model('localmodel');
        $this->load->model('categoriamodel');
        $usuario = $this->input->post('nomusuario');
        $clave = $this->input->post('clave');

        $existe_usuario = false;
        $objUsuario = $this->usuariomodel->login($usuario, $clave);

        $retorno = 0;
        if ($objUsuario == false) {
            $retorno = 0;
        } else {
            // Trayendo todas las configuraciones
            $aConfiguraciones = $this->configuracionmodel->getdata(array("*"));
            foreach ($aConfiguraciones as $itemConfig) {
                $this->session->set_userdata($itemConfig->variable, $itemConfig->valor);
            }
            // Buscando si tiene otra cuenta
            // Si tiene cuenta como local
            $htmlCuentasLocales = "<li><span>Locales</span></li>";
            $htmlCuentasGestores = "<li><span>Gestores</span></li>";
            $htmlCuentaActual = "";

            $aCuentaLocales = array();
            $aCuentaGestores = array();

            $htmlCuentaActual = "<li><a style='background-color: #F5F5F5;' href='" . base_url() . "usuario/entrarcuenta/" . $objUsuario->rol . "/" . intval($objUsuario->idlocal) . "'>Cuenta Logueada</a></li>";
            $aCuentaLocales = $this->localmodel->getdata(array("*"), "idusuario_responsable = " . $objUsuario->idusuario);
            $aCuentaGestores = $this->categoriamodel->getdata(array("*"), "idusuario_responsable = " . $objUsuario->idusuario);

            if ($aCuentaLocales == FALSE) {
                $htmlCuentasLocales = "";
            }

            if ($aCuentaGestores == FALSE) {
                $htmlCuentasGestores = "";
            }
            if (count($aCuentaLocales) > 1) {
                foreach ($aCuentaLocales as $itemCuentaLocal) {
                    $htmlCuentasLocales.="<li><a href='" . base_url() . "usuario/entrarcuenta/1/" . $itemCuentaLocal->idlocal . "'>" . $itemCuentaLocal->nombre . "</a></li>";
                }
            } else {
                $htmlCuentasLocales = "";
            }


            if (count($aCuentaGestores) > 1) {
                foreach ($aCuentaGestores as $itemCuentaGestor) {
                    $htmlCuentasGestores.="<li><a href='" . base_url() . "usuario/entrarcuenta/2/0'></i>" . $itemCuentaGestor->nombre . "</a></li>";
                }
            } else {
                $htmlCuentasGestores = "";
            }
            
            // Guardando datos de sessión del Usuario
            $this->session->set_userdata('htmlcuentaslocales', $htmlCuentasLocales);
            $this->session->set_userdata('htmlcuentasgestores', $htmlCuentasGestores);
            $this->session->set_userdata('htmlcuentasactual', $htmlCuentaActual);
            $this->session->set_userdata('idusuario', $objUsuario->idusuario);
            $this->session->set_userdata('nombre', $objUsuario->nombre);
            $this->session->set_userdata('correo', $objUsuario->correo);
            $this->session->set_userdata('rol', $objUsuario->rol);
            $this->session->set_userdata('idlocal', $objUsuario->idlocal);
            $this->session->set_userdata('nomlocal', $objUsuario->nomlocal);
            if ($objUsuario->rol == 5) {
                $retorno = 2;
            } elseif($objUsuario->rol==6) {
                $retorno = 3;
            }else{
				$retorno = 1;
			} 
        }
        echo $retorno;
        exit;
    }

    //[VISTA] Cerrar session
    function salir() {
        $this->session->sess_destroy();
        redirect(base_url(), 'refresh');
    }

    function entrarcuenta($rol = "", $idbandera = 0) {
        $this->session->set_userdata("rol", $rol);
        $this->session->set_userdata('idlocal', $idbandera);
        redirect(base_url());
        exit;
    }

    public function lista() {
        validaSession("formulario");
        // Trayendo modelos
        $mensaje = $this->session->userdata('mensaje');
        if ($mensaje != "") {
            $mensaje = getHtmlMensaje(1, $mensaje);
        }
        $aListaUsuarios = $this->usuariomodel->getListaUsuarios("u.idusuario > 0");
        $this->session->set_userdata('mensaje', "");
        $data['titulo'] = "Lista de usuarios";
        $data['mensaje'] = $mensaje;
        $data['aListaUsuarios'] = $aListaUsuarios;
        $botones = "";
        $botones .= '<a href = "' . base_url() . 'panel" class = "btn btn-sm btn-default" >Atras</a> ';
        $botones .= '<a href = "' . base_url() . 'usuario/nuevo" class = "btn btn-sm btn-default" >Nuevo</a> ';
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Usuarios", TRUE);
        $this->template->write_view('content', 'mantenimiento/usuarios/lista', $data, TRUE);
        $this->template->render();
    }

    public function json_crear_usuario() {

        $this->load->model('usuariomodel');
        $this->load->model('archivomodel');

        $nombre = $this->input->post("nombre");
        $username = $this->input->post("username");
        $correo = $this->input->post("correo");
        $clave = $this->input->post("clave");
        $rol = $this->input->post("rol");
        $estado = $this->input->post("estado");
        $msg_error = "";
        // Validar si existe el id de tarea
        $auxUsuario = array();
        $objUsuario = array();
        $auxUsuario = $this->usuariomodel->getExiste("correo", $correo, 0, 1);

        if ($auxUsuario->cantidad == 0) {
            $auxUsuario2 = $this->usuariomodel->getExiste("nomusuario", $username, 0, 1);
            if ($auxUsuario2->cantidad == 0) {
                $archivos = array();
                $foto = "";
                if (!empty($_FILES)) {
                    $archivos = subirArchivos($_FILES, "avatars");
                }
                foreach ($archivos as $archi) {
                    $foto = $archi["ruta"];
                }
                $objUsuario = array(
                    "nombre" => $nombre,
                    "nomusuario" => $username,
                    "clave" => $clave,
                    "rol" => $rol,
                    "foto" => $foto,
                    "estado_registro" => $estado,
                    "correo" => $correo
                );
                $idusuario = $this->usuariomodel->guardar($objUsuario);
                // Subimos los archivos si el usuario fue creado exitosamente
                if ($idusuario < 1) {
                    $msg_error.="No se pudo registrar";
                }
            } else {
                $msg_error.="El username ingresado ya existe";
            }
        } else {
            $msg_error.="El correo ingresado ya existe";
        }
        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "El usuario se creó exitosamente");
        }
        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    public function json_editar_usuario() {

        $this->load->model('usuariomodel');
        $this->load->model('archivomodel');

        $idusuario = $this->input->post("idusuario");
        $nombre = $this->input->post("nombre");
        $username = $this->input->post("username");
        $correo = $this->input->post("correo");
        $clave = $this->input->post("clave");
        $rol = $this->input->post("rol");
        $estado = $this->input->post("estado");
        $msg_error = "";
        // Validar si existe el id de tarea
        $auxUsuario = array();
        $objUsuario = array();
        $auxUsuario = $this->usuariomodel->getExiste("correo", $correo, $idusuario, 1);

        if ($auxUsuario->cantidad == 0) {
            $auxUsuario2 = $this->usuariomodel->getExiste("nomusuario", $username, $idusuario, 1);
            if ($auxUsuario2->cantidad == 0) {
                $archivos = array();
                $foto = "";
                if (!empty($_FILES)) {
                    $archivos = subirArchivos($_FILES, "avatars");
                }
                foreach ($archivos as $archi) {
                    $foto = $archi["ruta"];
                }
                $objUsuario = array(
                    "nombre" => $nombre,
                    "nomusuario" => $username,
                    "clave" => $clave,
                    "rol" => $rol,
                    "estado_registro" => $estado,
                    "correo" => $correo
                );

                if ($foto != "") {
                    $objUsuario["foto"] = $foto;
                }
                $this->usuariomodel->editar($idusuario, $objUsuario);
            } else {
                $msg_error.="El username ingresado ya existe";
            }
        } else {
            $msg_error.="El correo ingresado ya existe";
        }
        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "El usuario se editó exitosamente");
        }
        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    public function nuevo() {

        // Trayendo modelos
        $this->load->model('usuariomodel');
        $this->load->model('categoriamodel');

        $data['titulo'] = "Lista de usuarios";
        $botones = "";
        $botones .= '<a href = "' . base_url() . 'usuario/lista" class = "btn btn-sm btn-default" >Atras</a> ';
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Usuarios", TRUE);
        $this->template->write_view('content', 'mantenimiento/usuarios/crear', $data, TRUE);
        $this->template->render();
    }

    public function editar($idusuario) {
        // Trayendo modelos
        $this->load->model('usuariomodel');
        $this->load->model('categoriamodel');
        $objUsuario = $this->usuariomodel->getdata(array("*"), "idusuario = " . $idusuario, 1);
        if ($objUsuario == FALSE) {
            $this->session->set_userdata('mensaje', "");
        }

        $data['titulo'] = "Editar usuario";
        $data['objUsuario'] = $objUsuario;
        $botones = "";
        $botones .= '<a href = "' . base_url() . 'usuario/lista" class = "btn btn-sm btn-default" >Atras</a> ';
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Usuarios", TRUE);
        $this->template->write_view('content', 'mantenimiento/usuarios/editar', $data, TRUE);
        $this->template->render();
    }

}
