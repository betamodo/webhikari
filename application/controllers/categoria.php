<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Categoria extends MY_Controller {

    private $_fecha_actual;
    private $_idusuario_actual;
    private $_idlocal;
    private $_rol;

    function __construct() {
        parent::__construct();
        $this->_fecha_actual = date("Y-m-d H:i:s");
        $this->load->model('categoriamodel');
        $this->_idusuario_actual = $this->session->userdata('idusuario');
        $this->_rol = $this->session->userdata('rol');
        $this->_idlocal = $this->session->userdata('idlocal');
        $this->template->set_template('default');
        $this->load->helper(array('url', 'funciones', 'mysql_to_excel_helper'));
    }

    function index() {
        $this->login();
    }

    public function lista() {
        // Trayendo modelos
        $mensaje = $this->session->userdata('mensaje');
        if ($mensaje != "") {
            $mensaje = getHtmlMensaje(1, $mensaje);
        }
        $this->session->set_userdata('mensaje', "");

        $aListaCategoria = $this->categoriamodel->getListaCategorias("c.idcategoria > 0");
        $data['titulo'] = "Lista de categorias";
        $data['mensaje'] = $mensaje;
        $data['aListaCategoria'] = $aListaCategoria;
        $botones = "";
        $botones .= '<a href = "' . base_url() . 'panel" class = "btn btn-sm btn-default" >Atras</a> ';
        $botones .= '<a href = "' . base_url() . 'categoria/nuevo" class = "btn btn-sm btn-default" >Nuevo</a> ';
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Categorias", TRUE);
        $this->template->write_view('content', 'mantenimiento/categoria/lista', $data, TRUE);
        $this->template->render();
    }

    public function json_crear_categoria() {

        $nombre = $this->input->post("nombre");
        $idusuario = $this->input->post("idusuario");
        $estado = $this->input->post("estado");
        $msg_error = "";
        // Validar si existe el id de tarea
        $auxCategoria = array();
        $objCategoria = array();
        $auxCategoria = $this->categoriamodel->getExiste("nombre", $nombre, 0, 1);

        if ($auxCategoria->cantidad == 0) {
            $objCategoria = array(
                "nombre" => $nombre,
                "idusuario_responsable" => $idusuario,
                "estado_registro" => $estado
            );
            $idCategoria = $this->categoriamodel->guardar($objCategoria);
            // Subimos los archivos si el usuario fue creado exitosamente
            if ($idCategoria < 1) {
                $msg_error.="No se pudo registrar";
            }
        } else {
            $msg_error.="El nombre ingresado ya existe";
        }
        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "La categoría se creó exitosamente");
        }
        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    public function json_editar_categoria() {

        $idCategoria = $this->input->post("idcategoria");
        $nombre = $this->input->post("nombre");
        $idusuario = $this->input->post("idusuario");
        $estado = $this->input->post("estado");

        $msg_error = "";
        // Validar si existe el id de tarea
        $auxCategoria = array();
        $objCategoria = array();
        $auxCategoria = $this->categoriamodel->getExiste("nombre", $nombre, $idCategoria, 1);
        if ($auxCategoria->cantidad == 0) {
            $objCategoria = array(
                "nombre" => $nombre,
                "idusuario_responsable" => $idusuario,
                "estado_registro" => $estado
            );
            $this->categoriamodel->editar($idCategoria, $objCategoria);
        } else {
            $msg_error.="El nombre ingresado ya existe";
        }
        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "La categoría se editó exitosamente");
        }
        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    public function nuevo() {

        // Trayendo modelos
        $this->load->model('usuariomodel');
        $aUsuarios = $this->usuariomodel->getdata(array("idusuario", "nombre"), "rol = 2");
        $data['titulo'] = "Nueva Categoría";
        $data['aUsuarios'] = $aUsuarios;
        $botones = "";
        $botones .= '<a href = "' . base_url() . 'categoria/lista" class = "btn btn-sm btn-default" >Atras</a> ';
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Nuevo local", TRUE);
        $this->template->write_view('content', 'mantenimiento/categoria/crear', $data, TRUE);
        $this->template->render();
    }

    public function editar($idCategoria) {
        // Trayendo modelos
        $this->load->model('usuariomodel');
        $aUsuarios = $this->usuariomodel->getdata(array("idusuario", "nombre"), "rol = 2");

        $objCategoria = $this->categoriamodel->getdata(array("*"), "idcategoria = " . $idCategoria, 1);
        if ($objCategoria == FALSE) {
            $this->session->set_userdata('mensaje', "");
        }

        $data['titulo'] = "Editar categoria";
        $data['objCategoria'] = $objCategoria;
        $data['aUsuarios'] = $aUsuarios;
        $botones = "";
        $botones .= '<a href = "' . base_url() . 'categoria/lista" class = "btn btn-sm btn-default" >Atras</a> ';
        $botones .= '<a href = "' . base_url() . 'categoria/nuevo" class = "btn btn-sm btn-default" >Nuevo</a> ';
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Editar categoria", TRUE);
        $this->template->write_view('content', 'mantenimiento/categoria/editar', $data, TRUE);
        $this->template->render();
    }

}
