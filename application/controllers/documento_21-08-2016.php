<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Documento extends MY_Controller {

    private $_fecha_actual;
    private $_idusuario_actual;
    private $_idlocal;
    private $_rol;

    function __construct() {
        parent::__construct();
        $this->_fecha_actual = date("Y-m-d H:i:s");
        $this->load->model('documentomodel');
        $this->_idusuario_actual = $this->session->userdata('idusuario');
        $this->_rol = $this->session->userdata('rol');
        $this->_idlocal = $this->session->userdata('idlocal');
        $this->template->set_template('default');
        $this->load->helper(array('url', 'funciones', 'mysql_to_excel_helper'));
    }

    function index() {
        redirect(base_url() . "documento/lista");
    }

    function comingsoon() {
        $this->load->view('comingsoon', array());
    }

    public function json_buscar() {
        $oLogin = validaSession("json");
        if ($oLogin["msj"] != "") {
            echo json_encode($oLogin);
            exit;
        }

        $idalmacen = $this->input->post("idalmacen");
        $file = $this->input->post("file");
        $box = $this->input->post("box");
        $idtipo = $this->input->post("idtipo");
        $codigo = $this->input->post("codigo");
        $asunto = $this->input->post("asunto");
        $descripcion = $this->input->post("descripcion");
        $ffin = $this->input->post("ffin");
        $finicio = $this->input->post("finicio");

        $where = "d.estado_registro = 1";
        if ($idalmacen != "") {
            $where.=" AND d.idalmacen = " . $idalmacen;
        }
        if ($file != "") {
            $where.=" AND d.file like '%" . $file . "%'";
        }
        if ($box != "") {
            $where.=" AND d.file like '%" . $box . "%'";
        }
        if ($idtipo != "") {
            $where.=" AND d.idtipodoc = " . intval($idtipo);
        }
        if ($codigo != "") {
            $where.=" AND d.codigo like '%" . $codigo . "%'";
        }
        if ($asunto != "") {
            $where.=" AND d.asunto like '%" . $asunto . "%'";
        }
        if ($descripcion != "") {
            $where.=" AND d.comentario like '%" . $descripcion . "%'";
        }
        if ($finicio != "" && $ffin != "") {
            $where.=" AND date(d.fecha_recepcion) BETWEEN '" . getFechaEn2($finicio) . "' and '" . getFechaEn2($ffin) . "' ";
        }

        $aListaDocumentos = $this->documentomodel->getListaDocumentos($where);
        $data['aListaDocumentos'] = $aListaDocumentos;
        $data['origen'] = "buscar";

        $this->load->view("documentos/listabusqueda", $data);
    }

    public function get_div_busqueda() {
        $this->load->model('tipodocmodel');
        $this->load->model('almacenmodel');
        $aTipodoc = array();
        $aTipodoc = $this->tipodocmodel->getdata(array("idtipo_doc", "nombre"));
        $aAlmacen = $this->almacenmodel->getdata(array("idalmacen", "nombre"));

        $data["aTipodoc"] = $aTipodoc;
        $data["aAlmacen"] = $aAlmacen;
        $this->load->view("documentos/busqueda", $data);
    }

    public function lista() {
        // Trayendo modelos
        $mensaje = $this->session->userdata('mensaje');
        if ($mensaje != "") {
            $mensaje = getHtmlMensaje(1, $mensaje);
        }
        $this->session->set_userdata('mensaje', "");

        $this->load->model('tipodocmodel');
        $this->load->model('almacenmodel');
        $aTipodoc = array();
        $aTipodoc = $this->tipodocmodel->getdata(array("idtipo_doc", "nombre"));
        $aAlmacen = $this->almacenmodel->getdata(array("idalmacen", "nombre"));

        $data["aTipodoc"] = $aTipodoc;
        $data["aAlmacen"] = $aAlmacen;
        $data['titulo'] = "Lista de documentos";
        $data['mensaje'] = $mensaje;

        $botones = "";
        //$botones .= '<a href = "' . base_url() . 'tarea/lista" class = "btn btn-sm btn-default" >Atras</a> ';
        $botones .= '<a href = "' . base_url() . 'documento/nuevo" class = "btn btn-sm btn-default" >Nuevo</a> ';
        $this->template->add_js('media/js/documento.js');
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Lista de documentos", TRUE);
        $this->template->write_view('content', 'documentos/listar', $data, TRUE);
        $this->template->render();
    }

    public function json_buscar_tag() {
        $this->load->model('tagsmodel');
        $aEtiquetas = array();
        $aEtiquetas = $this->tagsmodel->getdata(array("idtags as value", "nombre as label"));
        echo json_encode($aEtiquetas);
    }

    public function json_crear_documento() {
        $this->load->model('archivodocmodel');
        $asunto = $this->input->post("asunto");
        $origen = $this->input->post("origen");
        $idtipodoc = $this->input->post("idtipodoc");
        $idlugar = $this->input->post("idlugar");
        $identificador = $this->input->post("identificador");
        $codfile = $this->input->post("codfile");
        $idtipo = $this->input->post("idtipo");
        $codbox = $this->input->post("codbox");
        $descripcion = $this->input->post("descripcion");
        $tags = $this->input->post("tags");
        $estado_registro = 1;

        $msg_error = "";
        // Validar si existe el id de tarea
        $auxDocumento = array();
        $objDocumento = array();
        $auxDocumento = $this->documentomodel->getExiste("identificador", $identificador, 0, 1);
        $aTags = array();
        $aTags = explode(",", $tags);
        $aTagsBD = array();
        $idstags = "";
        foreach ($aTags as $itemTags) {

            if (intval($itemTags) == 0) {
                $this->load->model('tagsmodel');
                $objTag = array();
                $objTag = array(
                    "nombre" => $itemTags
                );
                $idTagNuevo = $this->tagsmodel->guardar($objTag);
                $idstags.=$idTagNuevo . ",";
            } else {
                $idstags.=$itemTags . ",";
            }
        }

        if ($auxDocumento->cantidad == 0) {
            $objDocumento = array(
                "asunto" => $asunto,
                "tags" => trim($idstags, ","),
                "identificador" => $identificador,
                "origen" => $origen,
                "idtipodoc" => $idtipodoc,
                "idalmacen" => $idlugar,
                "file" => $codfile,
                "idtipo_almacen" => $idtipo,
                "box_estante" => $codbox,
                "comentario" => $descripcion,
                "fecha_registro" => $this->_fecha_actual,
                "fecha_modificacion" => $this->_fecha_actual,
                "fecha_recepcion" => $this->_fecha_actual,
                "estado_registro" => $estado_registro
            );
            $iddocumento = $this->documentomodel->guardar($objDocumento);
            $objDocumento = array(
                "codigo" => "DOC-" . date("Y") . date("m") . "-" . str_pad($iddocumento, 5, "0", STR_PAD_LEFT)
            );
            $this->documentomodel->editar($iddocumento, $objDocumento);
            // Subimos los archivos si el usuario fue creado exitosamente
            if ($iddocumento < 1) {
                $msg_error.="No se pudo registrar";
            } else {
                $archivos = array();
                if (!empty($_FILES)) {
                    $archivos = subirArchivos($_FILES);
                }
                foreach ($archivos as $archi) {
                    $objArchivo = array("iddocumento" => $iddocumento,
                        "url" => $archi["ruta"],
                        "nombre" => $archi["nombre"]
                    );
                    $numFilasArchivos = $this->archivodocmodel->guardar($objArchivo);
                    if ($numFilasArchivos < 1) {
                        $msg_error.= "Error al registrar archivo" . $iddocumento;
                    }
                }
            }
        } else {
            $msg_error.="El documento ya fué registrado";
        }
        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "El documento se creó exitosamente");
        }

        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    public function json_mover_documento() {
        $this->load->model('archivodocmodel');
        $iddoc = $this->input->post("iddoc");
        $idlugar = $this->input->post("idlugar");
        $codfile = $this->input->post("codfile");
        $codbox = $this->input->post("codbox");
        $estado_registro = 2;

        $msg_error = "";
        $objDocumento = array();
        $objDocumento = array(
            "idalmacen" => $idlugar,
            "file" => $codfile,
            "box_estante" => $codbox,
            "fecha_modificacion" => $this->_fecha_actual,
            "estado_registro" => $estado_registro
        );

        $this->documentomodel->editar($iddoc, $objDocumento);
        // Subimos los archivos si el usuario fue creado exitosamente

        $archivos = array();
        if (!empty($_FILES)) {
            $archivos = subirArchivos($_FILES);
        }
        foreach ($archivos as $archi) {
            $objArchivo = array("iddocumento" => $iddoc,
                "url" => $archi["ruta"],
                "nombre" => $archi["nombre"]
            );
            $numFilasArchivos = $this->archivodocmodel->guardar($objArchivo);
            if ($numFilasArchivos < 1) {
                $msg_error.= "Error al registrar archivo" . $iddoc;
            }
        }
        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "El documento se movió exitosamente");
        }

        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    public function json_editar_documento() {
        $this->load->model('archivodocmodel');
        $iddoc = $this->input->post("iddoc");
        $asunto = $this->input->post("asunto");
        $origen = $this->input->post("origen");
        $idtipodoc = $this->input->post("idtipodoc");
        $idlugar = $this->input->post("idlugar");
        $identificador = $this->input->post("identificador");
        $codfile = $this->input->post("codfile");
        $idtipo = $this->input->post("idtipo");
        $codbox = $this->input->post("codbox");
        $descripcion = $this->input->post("descripcion");
        $tags = $this->input->post("tags");
        $estado_registro = 1;

        $msg_error = "";
        // Validar si existe el id de tarea
        $auxDocumento = array();
        $objDocumento = array();
        $auxDocumento = $this->documentomodel->getExiste("identificador", $identificador, $iddoc, 1);
        $aTags = array();
        $aTags = explode(",", $tags);
        $aTagsBD = array();
        $idstags = "";
        foreach ($aTags as $itemTags) {
            if (intval($itemTags) == 0) {
                $this->load->model('tagsmodel');
                $objTag = array();
                $objTag = array(
                    "nombre" => $itemTags
                );
                $idTagNuevo = $this->tagsmodel->guardar($objTag);
                $idstags.=$idTagNuevo . ",";
            } else {
                $idstags.=$itemTags . ",";
            }
        }

        if ($auxDocumento->cantidad == 0) {
            $objDocumento = array(
                "asunto" => $asunto,
                "tags" => trim($idstags, ","),
                "identificador" => $identificador,
                "origen" => $origen,
                "idtipodoc" => $idtipodoc,
                "idalmacen" => $idlugar,
                "file" => $codfile,
                "idtipo_almacen" => $idtipo,
                "box_estante" => $codbox,
                "comentario" => $descripcion,
                "fecha_modificacion" => $this->_fecha_actual,
                "fecha_recepcion" => $this->_fecha_actual,
                "estado_registro" => $estado_registro
            );

            $this->documentomodel->editar($iddoc, $objDocumento);
            // Subimos los archivos si el usuario fue creado exitosamente
            if ($iddoc < 1) {
                $msg_error.="No se pudo registrar";
            } else {
                $archivos = array();
                if (!empty($_FILES)) {
                    $archivos = subirArchivos($_FILES);
                }
                foreach ($archivos as $archi) {
                    $objArchivo = array("iddocumento" => $iddoc,
                        "url" => $archi["ruta"],
                        "nombre" => $archi["nombre"]
                    );
                    $numFilasArchivos = $this->archivodocmodel->guardar($objArchivo);
                    if ($numFilasArchivos < 1) {
                        $msg_error.= "Error al registrar archivo" . $iddoc;
                    }
                }
            }
        } else {
            $msg_error.="El documento ya fué registrado";
        }
        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "El documento se creó exitosamente");
        }

        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    public function nuevo() {

        // Trayendo modelos
        $this->load->model('usuariomodel');
        $this->load->model('almacenmodel');
        $this->load->model('archivodocmodel');
        $this->load->model('tipodocmodel');

        $aTipoDoc = $this->tipodocmodel->getdata(array("*"), "estado_registro = 1");
        $aAlmacenes = $this->almacenmodel->getdata(array("*"), "estado_registro = 1");
        //$aArchivosDoc = $this->archivodocmodel->getdata(array("*"), "iddocumento = 1");
        $data['titulo'] = "Nuevo documento";
        $data['aAlmacenes'] = $aAlmacenes;
        $data['aTipoDoc'] = $aTipoDoc;
        //$data['aArchivosDoc'] = $aArchivosDoc;
        $botones = "";
        $botones .= '<a href = "' . base_url() . 'documento/lista" class = "btn btn-sm btn-default" >Atras</a> ';
        $this->template->add_css('media/tag/jquery.tagit.css');
        //$this->template->add_css('media/tag/Bootstrap.css');
        $this->template->add_js('media/tag/tag-it.js');
        $this->template->add_js('media/tag/configuracion.js');
        $this->template->add_js('media/js/documento.js');
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Nuevo documento", TRUE);
        $this->template->write_view('content', 'documentos/crear', $data, TRUE);
        $this->template->render();
    }

    public function ver($iddocumento) {
        // Trayendo modelos
        $this->load->model('usuariomodel');
        $this->load->model('almacenmodel');
        $this->load->model('tagsmodel');
        $this->load->model('archivodocmodel');
        $this->load->model('tipodocmodel');
        $aArchivosDoc = array();
        $aAlmacenes = array();
        $aTags = array();
        $aTipoDoc = array();
        $objDocumento = array();
        $objDocumento = $this->documentomodel->getdata(array("*"), "iddocumento = " . $iddocumento, 1);
        if ($objDocumento != FALSE) {
            $this->session->set_userdata('mensaje', "");
            $aArchivosDoc = $this->archivodocmodel->getdata(array("*"), "iddocumento = " . $iddocumento);
            $aTipoDoc = $this->tipodocmodel->getdata(array("*"), "estado_registro = 1");
            $aAlmacenes = $this->almacenmodel->getdata(array("*"), "estado_registro = 1");
            $aTags = $this->tagsmodel->getdata(array("*"), "idtags in (" . $objDocumento->tags . ")");
        }

        $data['titulo'] = "Vista documento";
        $data['aTags'] = $aTags;
        $data['objDocumento'] = $objDocumento;
        $data['aArchivos'] = $aArchivosDoc;
        $data['aAlmacenes'] = $aAlmacenes;
        $data['aTipoDoc'] = $aTipoDoc;

        $botones = "";
        $botones .= '<a href = "' . base_url() . 'documento/lista" class = "btn btn-sm btn-default" >Atras</a> ';
        $botones .= '<a href = "' . base_url() . 'documento/nuevo" class = "btn btn-sm btn-default" >Nuevo</a> ';
        $this->template->add_css('media/tag/jquery.tagit.css');
        //$this->template->add_css('media/tag/Bootstrap.css');
        $this->template->add_js('media/tag/tag-it.js');
        $this->template->add_js('media/tag/configuracion2.js');
        $this->template->add_js('media/js/documento.js');
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Vista categoria", TRUE);
        $this->template->write_view('content', 'documentos/ver', $data, TRUE);
        $this->template->render();
    }

    public function editar($iddocumento) {
        // Trayendo modelos
        $this->load->model('usuariomodel');
        $this->load->model('almacenmodel');
        $this->load->model('tagsmodel');
        $this->load->model('archivodocmodel');
        $this->load->model('tipodocmodel');
        $aArchivosDoc = array();
        $aAlmacenes = array();
        $aTags = array();
        $aTipoDoc = array();
        $objDocumento = array();
        $objDocumento = $this->documentomodel->getdata(array("*"), "iddocumento = " . $iddocumento, 1);
        if ($objDocumento != FALSE) {
            $this->session->set_userdata('mensaje', "");
            $aArchivosDoc = $this->archivodocmodel->getdata(array("*"), "iddocumento = " . $iddocumento);
            $aTipoDoc = $this->tipodocmodel->getdata(array("*"), "estado_registro = 1");
            $aAlmacenes = $this->almacenmodel->getdata(array("*"), "estado_registro = 1");
            $aTags = $this->tagsmodel->getdata(array("*"), "idtags in (" . $objDocumento->tags . ")");
        }

        $data['titulo'] = "Editar documento";
        $data['aTags'] = $aTags;
        $data['objDocumento'] = $objDocumento;
        $data['aArchivos'] = $aArchivosDoc;
        $data['aAlmacenes'] = $aAlmacenes;
        $data['aTipoDoc'] = $aTipoDoc;

        $botones = "";
        $botones .= '<a href = "' . base_url() . 'documento/lista" class = "btn btn-sm btn-default" >Atras</a> ';
        $botones .= '<a href = "' . base_url() . 'documento/nuevo" class = "btn btn-sm btn-default" >Nuevo</a> ';
        $this->template->add_css('media/tag/jquery.tagit.css');
        //$this->template->add_css('media/tag/Bootstrap.css');
        $this->template->add_js('media/tag/tag-it.js');
        $this->template->add_js('media/tag/configuracion2.js');
        $this->template->add_js('media/js/documento.js');
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Editar documento", TRUE);
        $this->template->write_view('content', 'documentos/editar', $data, TRUE);
        $this->template->render();
    }

    public function mover($iddocumento) {
        // Trayendo modelos
        $this->load->model('usuariomodel');
        $this->load->model('almacenmodel');
        $this->load->model('tagsmodel');
        $this->load->model('archivodocmodel');
        $this->load->model('tipodocmodel');
        $aArchivosDoc = array();
        $aAlmacenes = array();
        $aTags = array();
        $aTipoDoc = array();
        $objDocumento = array();
        $objDocumento = $this->documentomodel->getListaDocumentos("iddocumento = " . $iddocumento, 1);
        if ($objDocumento != FALSE) {
            $this->session->set_userdata('mensaje', "");
            $aArchivosDoc = $this->archivodocmodel->getdata(array("*"), "iddocumento = " . $iddocumento);
            $aTipoDoc = $this->tipodocmodel->getdata(array("*"), "estado_registro = 1");
            $aAlmacenes = $this->almacenmodel->getdata(array("*"), "estado_registro = 1");
            $aTags = $this->tagsmodel->getdata(array("*"), "idtags in (" . $objDocumento->tags . ")");
        }

        $data['titulo'] = "Mover documento";
        $data['aTags'] = $aTags;
        $data['objDocumento'] = $objDocumento;
        $data['aArchivos'] = $aArchivosDoc;
        $data['aAlmacenes'] = $aAlmacenes;
        $data['aTipoDoc'] = $aTipoDoc;

        $botones = "";
        $botones .= '<a href = "' . base_url() . 'documento/lista" class = "btn btn-sm btn-default" >Atras</a> ';
        $botones .= '<a href = "' . base_url() . 'documento/nuevo" class = "btn btn-sm btn-default" >Nuevo</a> ';
        $this->template->add_css('media/tag/jquery.tagit.css');
        //$this->template->add_css('media/tag/Bootstrap.css');
        $this->template->add_js('media/tag/tag-it.js');
        $this->template->add_js('media/tag/configuracion2.js');
        $this->template->add_js('media/js/documento.js');
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Mover documento", TRUE);
        $this->template->write_view('content', 'documentos/mover', $data, TRUE);
        $this->template->render();
    }

}
