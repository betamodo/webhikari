<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tarea extends My_Controller {

    private $_fecha_actual;
    private $_idusuario_actual;
    private $_idlocal;
    private $_rol;

    function __construct() {
        parent::__construct();
        date_default_timezone_set("America/Lima");
        $this->_fecha_actual = date("Y-m-d H:i:s");
        $this->_idusuario_actual = $this->session->userdata('idusuario');
        $this->_rol = $this->session->userdata('rol');
        $this->_idlocal = $this->session->userdata('idlocal');
        $this->load->model('respuestasmodel');
        $this->template->set_template('default');
        $this->load->helper(array('url', 'funciones', 'mysql_to_excel_helper'));
        $this->load->library('My_PHPExcel');
    }

    function index() {
        redirect(base_url() . "tarea/lista", 'refresh');
    }

    public function dias_transcurridos($fecha_i, $fecha_f) {
        $dias = (strtotime($fecha_i) - strtotime($fecha_f)) / 86400;
        $dias = abs($dias);
        $dias = floor($dias);
        return $dias;
    }

    public function correr_cron() {
        $this->load->model('tareaprogmodel');
        $this->load->model('tareamodel');
        $this->load->model('usuariomodel');
        $this->load->model('programacionmodel');
        $this->load->model('categoriamodel');
        $this->load->model('localmodel');
        $this->load->model('configuracionmodel');
        $oConfiguracion = $this->configuracionmodel->getdata(array("valor"), "variable = 'dias_aviso'", 1);

        $aProgramacion = $this->programacionmodel->getdata(array("*"), "estado_registro = 1");

        foreach ($aProgramacion as $itemProgramacion) {
            echo $itemProgramacion->fecha_siguiente . "<br>";
            $diasTranscurridos = $this->dias_transcurridos(date("Y-m-d", strtotime($itemProgramacion->fecha_siguiente)), date("Y-m-d", strtotime($this->_fecha_actual)));
            if ($diasTranscurridos <= $oConfiguracion->valor) {
                if ($diasTranscurridos == 0) {

                    $oTareaProg = $this->tareaprogmodel->getdata(array("*"), "idtareaprog = " . $itemProgramacion->idtareaprog, 1);

                    $aCateUsuario = $this->categoriamodel->getaCateUsuario();
                    $idusuario_ejecutor = $aCateUsuario[$oTareaProg->idcategoria]["idusuario"];
                    $idusuario_creador = $oTareaProg->idusuario_rol_creador;
                    if (intval($oTareaProg->paraadmin) == 1) {
                        $objLocal = $this->localmodel->getdata(array("idusuario_responsable"), "idlocal = " . $oTareaProg->idlocal, 1);
                        $idusuario_ejecutor = $objLocal->idusuario_responsable;
                        $idusuario_creador = $oTareaProg->idusuario_rol_ejecutor;
                    } else {
                        if ($oTareaProg->idlocal == 17) {
                            $idusuario_creador = $oTareaProg->idusuario_rol_creador;
                        } else {
                            $objLocal = $this->localmodel->getdata(array("idusuario_responsable"), "idlocal = " . $oTareaProg->idlocal, 1);
                            $idusuario_creador = $objLocal->idusuario_responsable;
                        }
                    }

                    $objTarea = array(
                        "nombre" => $oTareaProg->nombre,
                        "descripcion" => $oTareaProg->descripcion,
                        "idlocal" => $oTareaProg->idlocal,
                        "idcategoria" => $oTareaProg->idcategoria,
                        "prioridad" => $oTareaProg->prioridad,
                        "paraadmin" => $oTareaProg->paraadmin,
                        "idusuario_rol_creador" => $idusuario_creador,
                        "idusuario_rol_ejecutor" => $idusuario_ejecutor,
                        "idusuario_creador" => $oTareaProg->idusuario_rol_creador,
                        "ultimoestado" => 1,
                        "fecha_registro" => $this->_fecha_actual,
                        "fecha_actualizacion" => $this->_fecha_actual,
                        "estado_registro" => 1
                    );
                    $idtareaNueva = $this->tareamodel->guardar($objTarea);
                    $codigo = "TP-" . substr(date("Y"), 2, 2) . date("m") . "-" . str_pad($idtareaNueva, 3, "0", STR_PAD_LEFT);
                    $objTarea["codigo"] = $codigo;
                    $this->tareamodel->editar($idtareaNueva, $objTarea);
                    if ($idtareaNueva > 0) {
                        $objUsuario = $this->usuariomodel->getUsuarios();
                        //*************************************************
                        $objProg = array();
                        foreach ($itemProgramacion as $key => $value) {
                            $objProg[$key] = $value;
                        }

                        $objTarea = array();
                        foreach ($oTareaProg as $keyTarea => $valueTarea) {
                            $objTarea[$keyTarea] = $valueTarea;
                        }

                        //*************************************************
                        $this->ejecuta_cron($objProg, $objTarea, $origen = 2);
                        enviarEmail(array(
                            "FROM" => "Hikari- Notificaciones",
                            "FROMNAME" => "sistemas@hikari.pe",
                            "asunto" => "Se registró una nueva tarea",
                            "mensaje" => 'Se creó la tarea llamada "' . $oTareaProg->nombre . '" con código "' . $codigo . '" que es una tarea programada',
                            "titulo" => "De: " . $objUsuario[$idusuario_creador]["nombre"] . "<br>Para: " . $objUsuario[$idusuario_ejecutor]["nombre"],
                            "correo" => $objUsuario[$idusuario_ejecutor]["correo"]
                        ));
                    }
                } elseif ($diasTranscurridos > 0) {

                    $objUsuario = $this->usuariomodel->getUsuarios();
                    $oTareaProg = $this->tareaprogmodel->getdata(array("*"), "idtareaprog = " . $itemProgramacion->idtareaprog, 1);
//                    echo "*****************<br>";
//                    echo "NOTIFICACION<br>";
//                    echo 'Faltan ' . $diasTranscurridos . ' dias para la realizacion de la tarea "' . $oTareaProg->nombre . '" <br>';
//                    echo '*****************<br><br>';
                    enviarEmail(array(
                        "FROM" => "Hikari- Notificaciones",
                        "FROMNAME" => "sistemas@hikari.pe",
                        "asunto" => "Aviso de creación de tarea preventiva",
                        "mensaje" => 'Faltan ' . $diasTranscurridos . ' dias para la realización de la tarea "' . $oTareaProg->nombre . '" <br>',
                        "titulo" => "Hola " . $objUsuario[$oTareaProg->idusuario_rol_creador]["nombre"],
                        "correo" => $objUsuario[$oTareaProg->idusuario_rol_creador]["correo"]
                    ));
                    enviarEmail(array(
                        "FROM" => "Hikari- Notificaciones",
                        "FROMNAME" => "sistemas@hikari.pe",
                        "asunto" => "Aviso de creación de tarea preventiva",
                        "mensaje" => 'Faltan ' . $diasTranscurridos . ' dias para la realización de la tarea "' . $oTareaProg->nombre . '" <br>',
                        "titulo" => "Hola " . $objUsuario[$oTareaProg->idusuario_rol_ejecutor]["nombre"],
                        "correo" => $objUsuario[$oTareaProg->idusuario_rol_ejecutor]["correo"]
                    ));
                }
            }
            //echo $diasTranscurridos . "<br>";
        }
    }

    public function ejecuta_cron($aProgramacion = array(), $objTareaProg = array(), $origen = 0) {
        $this->load->model('tareaprogmodel');
        $this->load->model('programacionmodel');
        $this->load->model('tareamodel');
        $auxoProgramacion = array();
        $idtareaRegistrada = 0;
        $ejecuta = 0;
        $repeticion = $aProgramacion["repeticion"];
        $fechaActual = date("Y-m-d");
        $fechaInicio = $aProgramacion["fecha_inicio"];

        $aDias = array(
            "lunes" => $aProgramacion["lunes"],
            "martes" => $aProgramacion["martes"],
            "miercoles" => $aProgramacion["miercoles"],
            "jueves" => $aProgramacion["jueves"],
            "viernes" => $aProgramacion["viernes"],
            "sabado" => $aProgramacion["sabado"],
            "domingo" => $aProgramacion["domingo"]
        );
        if ($repeticion == 1) {
            $idDiaActual = date("w", strtotime($fechaActual));

            $fechaProxima = getProximoDia($aDias, $fechaInicio, $idDiaActual);
//            echo "Fecha Actual: ".$fechaActual."<br>";
//            echo "Fecha Inicio: ".$fechaInicio."<br>";
//            echo "Fecha Proxima: ".$fechaProxima."<br>";
//            exit;
            if (strcmp($fechaActual, $fechaProxima) == 0) {
                //echo "Igual";
                $auxoProgramacion["fecha_actualizacion"] = $fechaActual;
                $fechaSiguiente = date('Y-m-d', strtotime('+1 day', strtotime($fechaActual)));

                $idDiaActualSiguiente = date("w", strtotime($fechaSiguiente));
                $fechaProxima = getProximoDia($aDias, $fechaSiguiente, $idDiaActualSiguiente, 1);

                $auxoProgramacion["fecha_siguiente"] = $fechaProxima;
                // Registra tarea
                $ejecuta = 1;
            } else {

                $auxoProgramacion["fecha_actualizacion"] = $fechaInicio;
                $auxoProgramacion["fecha_siguiente"] = $fechaProxima;
                $ejecuta = 0;
            }
        } elseif ($repeticion == 2) {
            if (strcmp($fechaActual, $fechaInicio) == 0) {
                $auxoProgramacion["fecha_actualizacion"] = $fechaActual;
                $fechaProxima = getProximoSemana($fechaInicio);
                $auxoProgramacion["fecha_siguiente"] = $fechaProxima;
                $ejecuta = 1;
            } else {
                $fechaProxima = getProximoSemana($fechaInicio);
                $auxoProgramacion["fecha_siguiente"] = $fechaProxima;
                $ejecuta = 0;
            }
        } elseif ($repeticion == 3) {
            if (strcmp($fechaActual, $fechaInicio) == 0) {
                $auxoProgramacion["fecha_actualizacion"] = $fechaActual;
                $fechaProxima = getProximoMes($fechaInicio);
                $auxoProgramacion["fecha_siguiente"] = $fechaProxima;
                $ejecuta = 1;
            } elseif (strtotime($fechaInicio) > strtotime($fechaActual)) {
                $auxoProgramacion["fecha_siguiente"] = $fechaInicio;
                $ejecuta = 0;
            } else {
                $fechaProxima = getProximoMes($fechaInicio);
                $auxoProgramacion["fecha_siguiente"] = $fechaProxima;
                $ejecuta = 0;
            }
        } elseif ($repeticion == 5) {
            if (strcmp($fechaActual, $fechaInicio) == 0) {
                $auxoProgramacion["fecha_actualizacion"] = $fechaActual;
                $fechaProxima = getProximoMesProgramado($fechaInicio, 2);
                $auxoProgramacion["fecha_siguiente"] = $fechaProxima;
                $ejecuta = 1;
            } elseif (strtotime($fechaInicio) > strtotime($fechaActual)) {
                $auxoProgramacion["fecha_siguiente"] = $fechaInicio;
                $ejecuta = 0;
            } else {
                $fechaProxima = getProximoMesProgramado($fechaInicio, 2);
                $auxoProgramacion["fecha_siguiente"] = $fechaProxima;
                $ejecuta = 0;
            }
        } elseif ($repeticion == 6) {
            if (strcmp($fechaActual, $fechaInicio) == 0) {
                $auxoProgramacion["fecha_actualizacion"] = $fechaActual;
                $fechaProxima = getProximoMesProgramado($fechaInicio, 3);
                $auxoProgramacion["fecha_siguiente"] = $fechaProxima;
                $ejecuta = 1;
            } elseif (strtotime($fechaInicio) > strtotime($fechaActual)) {
                $auxoProgramacion["fecha_siguiente"] = $fechaInicio;
                $ejecuta = 0;
            } else {
                $fechaProxima = getProximoMesProgramado($fechaInicio, 3);
                $auxoProgramacion["fecha_siguiente"] = $fechaProxima;
                $ejecuta = 0;
            }
        } elseif ($repeticion == 7) {
            if (strcmp($fechaActual, $fechaInicio) == 0) {
                $auxoProgramacion["fecha_actualizacion"] = $fechaActual;
                $fechaProxima = getProximoMesProgramado($fechaInicio, 4);
                $auxoProgramacion["fecha_siguiente"] = $fechaProxima;
                $ejecuta = 1;
            } elseif (strtotime($fechaInicio) > strtotime($fechaActual)) {
                $auxoProgramacion["fecha_siguiente"] = $fechaInicio;
                $ejecuta = 0;
            } else {
                $fechaProxima = getProximoMesProgramado($fechaInicio, 4);
                $auxoProgramacion["fecha_siguiente"] = $fechaProxima;
                $ejecuta = 0;
            }
        } elseif ($repeticion == 8) {
            if (strcmp($fechaActual, $fechaInicio) == 0) {
                $auxoProgramacion["fecha_actualizacion"] = $fechaActual;
                $fechaProxima = getProximoMesProgramado($fechaInicio, 5);
                $auxoProgramacion["fecha_siguiente"] = $fechaProxima;
                $ejecuta = 1;
            } elseif (strtotime($fechaInicio) > strtotime($fechaActual)) {
                $auxoProgramacion["fecha_siguiente"] = $fechaInicio;
                $ejecuta = 0;
            } else {
                $fechaProxima = getProximoMesProgramado($fechaInicio, 5);
                $auxoProgramacion["fecha_siguiente"] = $fechaProxima;
                $ejecuta = 0;
            }
        } elseif ($repeticion == 9) {
            if (strcmp($fechaActual, $fechaInicio) == 0) {
                $auxoProgramacion["fecha_actualizacion"] = $fechaActual;
                $fechaProxima = getProximoMesProgramado($fechaInicio, 6);
                $auxoProgramacion["fecha_siguiente"] = $fechaProxima;
                $ejecuta = 1;
            } elseif (strtotime($fechaInicio) > strtotime($fechaActual)) {
                $auxoProgramacion["fecha_siguiente"] = $fechaInicio;
                $ejecuta = 0;
            } else {
                $fechaProxima = getProximoMesProgramado($fechaInicio, 6);
                $auxoProgramacion["fecha_siguiente"] = $fechaProxima;
                $ejecuta = 0;
            }
        } elseif ($repeticion == 10) {
            if (strcmp($fechaActual, $fechaInicio) == 0) {
                $auxoProgramacion["fecha_actualizacion"] = $fechaActual;
                $fechaProxima = getProximoMesProgramado($fechaInicio, 7);
                $auxoProgramacion["fecha_siguiente"] = $fechaProxima;
                $ejecuta = 1;
            } elseif (strtotime($fechaInicio) > strtotime($fechaActual)) {
                $auxoProgramacion["fecha_siguiente"] = $fechaInicio;
                $ejecuta = 0;
            } else {
                $fechaProxima = getProximoMesProgramado($fechaInicio, 7);
                $auxoProgramacion["fecha_siguiente"] = $fechaProxima;
                $ejecuta = 0;
            }
        } elseif ($repeticion == 11) {
            if (strcmp($fechaActual, $fechaInicio) == 0) {
                $auxoProgramacion["fecha_actualizacion"] = $fechaActual;
                $fechaProxima = getProximoMesProgramado($fechaInicio, 8);
                $auxoProgramacion["fecha_siguiente"] = $fechaProxima;
                $ejecuta = 1;
            } elseif (strtotime($fechaInicio) > strtotime($fechaActual)) {
                $auxoProgramacion["fecha_siguiente"] = $fechaInicio;
                $ejecuta = 0;
            } else {
                $fechaProxima = getProximoMesProgramado($fechaInicio, 8);
                $auxoProgramacion["fecha_siguiente"] = $fechaProxima;
                $ejecuta = 0;
            }
        } elseif ($repeticion == 12) {
            if (strcmp($fechaActual, $fechaInicio) == 0) {
                $auxoProgramacion["fecha_actualizacion"] = $fechaActual;
                $fechaProxima = getProximoMesProgramado($fechaInicio, 9);
                $auxoProgramacion["fecha_siguiente"] = $fechaProxima;
                $ejecuta = 1;
            } elseif (strtotime($fechaInicio) > strtotime($fechaActual)) {
                $auxoProgramacion["fecha_siguiente"] = $fechaInicio;
                $ejecuta = 0;
            } else {
                $fechaProxima = getProximoMesProgramado($fechaInicio, 9);
                $auxoProgramacion["fecha_siguiente"] = $fechaProxima;
                $ejecuta = 0;
            }
        } elseif ($repeticion == 13) {
            if (strcmp($fechaActual, $fechaInicio) == 0) {
                $auxoProgramacion["fecha_actualizacion"] = $fechaActual;
                $fechaProxima = getProximoMesProgramado($fechaInicio, 10);
                $auxoProgramacion["fecha_siguiente"] = $fechaProxima;
                $ejecuta = 1;
            } elseif (strtotime($fechaInicio) > strtotime($fechaActual)) {
                $auxoProgramacion["fecha_siguiente"] = $fechaInicio;
                $ejecuta = 0;
            } else {
                $fechaProxima = getProximoMesProgramado($fechaInicio, 10);
                $auxoProgramacion["fecha_siguiente"] = $fechaProxima;
                $ejecuta = 0;
            }
        } elseif ($repeticion == 14) {
            if (strcmp($fechaActual, $fechaInicio) == 0) {
                $auxoProgramacion["fecha_actualizacion"] = $fechaActual;
                $fechaProxima = getProximoMesProgramado($fechaInicio, 11);
                $auxoProgramacion["fecha_siguiente"] = $fechaProxima;
                $ejecuta = 1;
            } elseif (strtotime($fechaInicio) > strtotime($fechaActual)) {
                $auxoProgramacion["fecha_siguiente"] = $fechaInicio;
                $ejecuta = 0;
            } else {
                $fechaProxima = getProximoMesProgramado($fechaInicio, 11);
                $auxoProgramacion["fecha_siguiente"] = $fechaProxima;
                $ejecuta = 0;
            }
        } elseif ($repeticion == 4) {
            if (strcmp($fechaActual, $fechaInicio) == 0) {
                $auxoProgramacion["fecha_actualizacion"] = $fechaActual;
                $fechaProxima = getProximoAnio($fechaInicio);
                $auxoProgramacion["fecha_siguiente"] = $fechaProxima;
                $ejecuta = 1;
            } else {
                $fechaProxima = getProximoAnio($fechaInicio);
                $auxoProgramacion["fecha_siguiente"] = $fechaProxima;
                $ejecuta = 0;
            }
        }
        if ($ejecuta == 1) {
            $objTarea = array();
            $objTarea["codigo"] = $objTareaProg["codigo"];
            $objTarea["nombre"] = $objTareaProg["nombre"];
            $objTarea["descripcion"] = $objTareaProg["descripcion"];
            $objTarea["presupuesto"] = "";
            $objTarea["proveedor"] = "";
            $objTarea["ultimoestado"] = 1;
            $objTarea["idlocal"] = $objTareaProg["idlocal"];
            $objTarea["idcategoria"] = $objTareaProg["idcategoria"];
            $objTarea["prioridad"] = $objTareaProg["prioridad"];
            $objTarea["paraadmin"] = $objTareaProg["paraadmin"];
            $objTarea["idusuario_rol_creador"] = $objTareaProg["idusuario_rol_creador"];
            $objTarea["idusuario_rol_ejecutor"] = $objTareaProg["idusuario_rol_ejecutor"];
            $objTarea["fecha_actualizacion"] = $this->_fecha_actual;
            $objTarea["fecha_registro"] = $this->_fecha_actual;
            $objTarea["estado_registro"] = 1;
            if (strtotime($auxoProgramacion["fecha_siguiente"]) > strtotime($aProgramacion["fecha_fin"])) {
                $auxoProgramacion["fecha_siguiente"] = $aProgramacion["fecha_fin"];
            }
            $this->programacionmodel->editar($aProgramacion["idprogramacion"], $auxoProgramacion);
            if ($origen == 0) {
                $idtareaRegistrada = $this->tareamodel->guardar($objTarea);
            } else {
                $idtareaRegistrada = $origen;
            }
        } else {
            if ($origen == 0) {
                if (strtotime($auxoProgramacion["fecha_siguiente"]) > strtotime($aProgramacion["fecha_fin"])) {
                    $auxoProgramacion["fecha_siguiente"] = $aProgramacion["fecha_fin"];
                }
                $this->programacionmodel->editar($aProgramacion["idprogramacion"], $auxoProgramacion);
            }
        }
//        echo "<pre>";
//        var_dump($objTarea);
//        echo "<pre>";
        return $idtareaRegistrada;
    }

    function json_exportarPresupuestos($idlocal, $finicio, $ffin, $montoinicial, $montofinal, $montoigual) {
        // configuramos las propiedades del documento
        $this->load->model('tareamodel');
        $this->load->model('categoriamodel');

        $where = "t.estado_registro = 1";
        if ($idlocal != "-1") {
            $where.=" AND t.idlocal = " . $idlocal;
        }
        if ($montoigual != "-1") {
            $where.=" AND t.presupuesto = " . doubleval($montoigual);
        }
        if ($montoinicial != "-1" && $montofinal != "-1") {
            $where.=" AND t.presupuesto BETWEEN " . doubleval($montoinicial) . " AND " . doubleval($montofinal) . " ";
        }
        if ($finicio != "-1" && $ffin != "-1") {
            $where.=" AND date(t.fecha_registro) BETWEEN '" . getFechaEn($finicio) . "' and '" . getFechaEn($ffin) . "' ";
        }

        // Trayendo datos
        //$aCategorias = $this->categoriamodel->getdata(array("idcategoria", "nombre"), "estado_registro = 1");
        $aCateUsuario = $this->categoriamodel->getaCateUsuario();
        $auxListaTarea = array();
        if ($this->_rol == 3) {
            $auxListaTarea = $this->tareamodel->getListaTareas($where);
        } else {
            $where.=" and (t.idusuario_rol_ejecutor = " . $this->_idusuario_actual . " or t.idusuario_rol_creador = " . $this->_idusuario_actual . ") ";
            $auxListaTarea = $this->tareamodel->getListaTareas($where);
        }

        $aListaTareas = array();
        $aEstados = getEstados();
        if ($auxListaTarea != false) {
            foreach ($auxListaTarea as $itemTarea) {
                // Saber si se envió al administrados o al gestor
                $responsable = $aCateUsuario[$itemTarea->idcategoria]["nomresponsable"];
                if (intval($itemTarea->paraadmin) == 1) {
                    $responsable = "Administrador";
                }

                $datetime1 = date_create($itemTarea->fecha_registro);
                $datetime2 = date_create(date("Y-m-d H:i:s"));
                $fecha = date_diff($datetime1, $datetime2);
                $tiempoTranscurrido = "";
                if ($fecha->y > 0) {
                    $tiempoTranscurrido = $fecha->y . " A ";
                }
                if ($fecha->m > 0) {
                    $tiempoTranscurrido = $fecha->m . " M ";
                }
                if ($fecha->d > 0) {
                    $tiempoTranscurrido = $fecha->d . " d ";
                }
                if ($fecha->h > 0) {
                    $tiempoTranscurrido = $fecha->h . "h ";
                }
                if ($fecha->i > 0) {
                    $tiempoTranscurrido = $fecha->i . "m ";
                }
                //*******************************
                array_push($aListaTareas, array(
                    "idtarea" => $itemTarea->idtarea,
                    "codigo" => $itemTarea->codigo,
                    "proveedor" => $itemTarea->proveedor,
                    "presupuesto" => $itemTarea->presupuesto,
                    "nombre" => $itemTarea->nombre,
                    "nomlocal" => $itemTarea->nomlocal,
                    "nomestado" => $aEstados[$itemTarea->ultimoestado][0]["nombre"],
                    "nomcategoria" => $aCateUsuario[$itemTarea->idcategoria]["nomcategoria"],
                    "nomresponsable" => $responsable,
                    "fregistro" => getFechaEs($itemTarea->fecha_registro),
                    "factualizacion" => getFechaEs($itemTarea->fecha_actualizacion),
                    "tipo_transcurrido" => $tiempoTranscurrido,
                    "imagen" => $aEstados[$itemTarea->ultimoestado][0]["imagen"],
                    "enlace" => getEnlace($itemTarea->ultimoestado, $itemTarea->idtarea, $itemTarea->paraadmin, $itemTarea->idusuario_rol_creador)
                ));
            }
        }

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Arkos Noem Arenom")
                ->setLastModifiedBy("Arkos Noem Arenom")
                ->setTitle("Office 2007 XLSX Test Document")
                ->setSubject("Office 2007 XLSX Test Document")
                ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                ->setKeywords("office 2007 openxml php")
                ->setCategory("Test result file");


        $iHeader = 6;
        // agregamos información a las celdas
        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $iHeader, 'Código')
                ->setCellValue('B' . $iHeader, 'Local')
                ->setCellValue('C' . $iHeader, 'Tarea')
                ->setCellValue('D' . $iHeader, 'Categoría')
                ->setCellValue('E' . $iHeader, 'Responsable')
                ->setCellValue('F' . $iHeader, 'Estado')
                ->setCellValue('G' . $iHeader, 'F. Registro')
                ->setCellValue('H' . $iHeader, 'F. Modificación')
                ->setCellValue('I' . $iHeader, 'Proveedor')
                ->setCellValue('J' . $iHeader, 'Presupuesto');

        $styleArray = array(
            'font' => array(
                'bold' => true,
                'color' => array('rgb' => '030303'),
                'size' => 10,
                'name' => 'Verdana'
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'f5f5f5')
        ));

        $objPHPExcel->getActiveSheet()->getStyle('A' . $iHeader)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('B' . $iHeader)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('C' . $iHeader)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('D' . $iHeader)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('E' . $iHeader)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('F' . $iHeader)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('G' . $iHeader)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('H' . $iHeader)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('I' . $iHeader)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('J' . $iHeader)->applyFromArray($styleArray);

        //Añadir una imagen 
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('ZealImg');
        $objDrawing->setDescription('Image inserted by Zeal');
        $objDrawing->setPath(getcwd() . "/media/img/logo_marca.png");
        $objDrawing->setHeight(65);
        $objDrawing->setCoordinates('B2');
        $objDrawing->setOffsetX(10);
        $objDrawing->getShadow()->setVisible(true);
        $objDrawing->getShadow()->setDirection(36);
        $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
        // Llenando el excel
        $i = 7;
        foreach ($aListaTareas as $itemTarea) {
            // La librería puede manejar la codificación de caracteres UTF-8
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $i, $itemTarea ["codigo"])
                    ->setCellValue('B' . $i, $itemTarea ["nomlocal"])
                    ->setCellValue('C' . $i, $itemTarea ["nombre"])
                    ->setCellValue('D' . $i, $itemTarea ["nomcategoria"])
                    ->setCellValue('E' . $i, $itemTarea ["nomresponsable"])
                    ->setCellValue('F' . $i, $itemTarea ["nomestado"])
                    ->setCellValue('G' . $i, $itemTarea ["fregistro"])
                    ->setCellValue('H' . $i, $itemTarea ["factualizacion"])
                    ->setCellValue('I' . $i, $itemTarea ["proveedor"])
                    ->setCellValue('J' . $i, $itemTarea ["presupuesto"]);
            $i++;
        }

        foreach (range('A', 'Z') as $columnID) {

            $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }
        $objPHPExcel->getActiveSheet()->getCell('C3')->setValue('LISTA DE TAREAS');
        $style = array(
            'font' => array(
                'bold' => true,
                'color' => array('rgb' => '030303'),
                'size' => 12,
                'name' => 'Verdana'
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'f5f5f5')
            )
        );

        $objPHPExcel->getActiveSheet()->getStyle("C3:G3")->applyFromArray($style);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('C3:G3');
        // Renombramos la hoja de trabajo
        $objPHPExcel->getActiveSheet()->setTitle('Lista de tareas');


        // configuramos el documento para que la hoja
        // de trabajo número 0 sera la primera en mostrarse
        // al abrir el documento
        $objPHPExcel->setActiveSheetIndex(0);


        // redireccionamos la salida al navegador del cliente (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="tareas.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
    }

    function lista($idmsj = 0, $filtro = 0) {
        validaSession("formulario");
        // Trayendo modelos
        $this->load->model('categoriamodel');
        $this->load->model('tareamodel');

        $mensaje = $this->session->userdata('mensaje');
        if ($mensaje != "") {
            $mensaje = getHtmlMensaje(1, $mensaje);
        }

        $idlocal = $this->session->userdata('filtro_local');
        $finicio = $this->session->userdata('filtro_fecha_inicio');
        $idestado = $this->session->userdata('filtro_estados');
        $ffin = $this->session->userdata('filtro_fecha_fin');
        $montoinicial = $this->session->userdata('filtro_monto_inicial');
        $montofinal = $this->session->userdata('filtro_monto_final');
        $montoigual = $this->session->userdata('filtro_monto_igual');
        $idusuario = $this->session->userdata('filtro_monto_ejecutor');

        $where = "t.estado_registro = 1";
        if ($idusuario != "") {
            if ($this->_rol == 3) {
                $where.=" AND (t.idusuario_rol_ejecutor = " . $idusuario . " OR t.idusuario_rol_creador = " . $idusuario . " OR t.idusuario_creador = " . $idusuario . ")";
            } else {
                $where.=" AND t.idusuario_rol_ejecutor = " . $idusuario;
            }
        }
        if ($this->_rol != 1) {
            if ($idlocal != "") {
                $where.=" AND t.idlocal = " . $idlocal;
            }
        }
        if ($this->_rol == 1 && $idlocal == "") {
            $where.=" AND t.idlocal = " . $this->_idlocal;
        }

        if ($idestado != "") {
            $where.=" AND t.ultimoestado = " . $idestado;
        }
        if ($montoigual != "") {
            $where.=" AND t.presupuesto = " . doubleval($montoigual);
        }
        if ($montoinicial != "" && $montofinal != "") {
            $where.=" AND t.presupuesto BETWEEN " . doubleval($montoinicial) . " AND " . doubleval($montofinal) . " ";
        }
        if ($finicio != "" && $ffin != "") {
            $where.=" AND date(t.fecha_registro) BETWEEN '" . getFechaEn2($finicio) . "' AND '" . getFechaEn2($ffin) . "' ";
        }
        /*
          if (intval($this->session->userdata('param_idusuario')) != 0) {
          $idusuario_param = $this->session->userdata('param_idusuario');
          $idestado_param = $this->session->userdata('param_idestado');
          if ($idestado_param == 3 || $idestado_param == 4) {
          $where .= " AND t.idusuario_rol_creador = " . $idusuario_param . " AND t.ultimoestado = " . $idestado_param;
          } else {
          $where .= " AND t.idusuario_rol_ejecutor = " . $idusuario_param . " AND t.ultimoestado = " . $idestado_param;
          }
          }
          if (intval($this->session->userdata('param_idlocal')) != 0) {
          $idlocal_param = $this->session->userdata('param_idlocal');
          $idestadolocal_param = $this->session->userdata('param_idestadolocal');
          if ($idestadolocal_param == 3 || $idestadolocal_param == 4) {
          $where .= " AND t.idlocal = " . $idlocal_param . " AND t.ultimoestado = " . $idestadolocal_param;
          } else {
          $where .= " AND t.idlocal = " . $idlocal_param . " AND t.ultimoestado = " . $idestadolocal_param;
          }
          $this->session->set_userdata('param_idlocal', "");
          $this->session->set_userdata('param_idestadolocal', "");
          } */

        // Trayendo datos
        //$aCategorias = $this->categoriamodel->getdata(array("idcategoria", "nombre"), "estado_registro = 1");
        $aCateUsuario = $this->categoriamodel->getaCateUsuario();
        $auxListaTarea = array();
        if ($this->_rol == 3) {
            $auxListaTarea = $this->tareamodel->getListaTareas($where);
        } elseif ($this->_rol == 1) {
            $auxListaTarea = $this->tareamodel->getListaTareas($where);
            //$auxListaTarea = $this->tareamodel->getListaTareas($where);
        } else {
            $auxListaTarea = $this->tareamodel->getListaTareas($where);
            //$auxListaTarea = $this->tareamodel->getListaTareas(" AND idusuario_rol_ejecutor = " . $this->_idusuario_actual . " or idusuario_rol_creador = " . $this->_idusuario_actual);
        }
        $aparecePpto = 1;
        $aListaTareas = array();
        $aEstados = getEstados();
        if ($auxListaTarea != false) {
            foreach ($auxListaTarea as $itemTarea) {
                // Saber si se envió al administrados o al gestor
                $responsable = $aCateUsuario[$itemTarea->idcategoria]["nomresponsable"];
                if (intval($itemTarea->paraadmin) == 1) {
                    $responsable = "Administrador";
                }

                //*******************************
                array_push($aListaTareas, array("idtarea" => $itemTarea->idtarea,
                    "codigo" => $itemTarea->codigo,
                    "proveedor" => $itemTarea->proveedor,
                    "presupuesto" => $itemTarea->presupuesto,
                    "idusuario_creador" => $itemTarea->idusuario_rol_creador,
                    "idusuario_ejecutor" => $itemTarea->idusuario_rol_ejecutor,
                    "idusuario" => $itemTarea->idusuario_creador,
                    "nombre" => $itemTarea->nombre,
                    "paraadmin" => $itemTarea->paraadmin,
                    "ultimoestado" => $itemTarea->ultimoestado,
                    "idtarea" => $itemTarea->idtarea,
                    "prioridad" => $itemTarea->prioridad,
                    "nomlocal" => $itemTarea->nomlocal,
                    "nomestado" => $aEstados[$itemTarea->ultimoestado][0]["nombre"],
                    "nomcategoria" => $aCateUsuario[$itemTarea->idcategoria]["nomcategoria"],
                    "nomresponsable" => $responsable,
                    "fregistro" => getFechaEs($itemTarea->fecha_registro),
                    "factualizacion" => getFechaEs($itemTarea->fecha_actualizacion),
                    "tipo_transcurrido" => getTiempoTranscurrido($itemTarea->fecha_actualizacion, date("Y-m-d H:i:s")),
                    "imagen" => $aEstados[$itemTarea->ultimoestado][0]["imagen"],
                    "enlace" => getEnlace($itemTarea->ultimoestado, $itemTarea->idtarea, $itemTarea->paraadmin, $itemTarea->idusuario_rol_creador)
                ));
            }
        }

        // Recibiendo parametros
        //$codMenu = $this->input->post('codmenu');

        $this->session->set_userdata('param_idusuario', "");
        $this->session->set_userdata('param_idestado', "");
        $data['titulo'] = "Lista de tareas";
        $data['divmensaje'] = $mensaje;
        $this->load->model('usuariomodel');
        $oUsuario = $this->usuariomodel->getUsuarios();
        $data['oUsuario'] = $oUsuario;
        $data['aListaTareas'] = $aListaTareas;

        $botones = "";
        if ($this->_rol == 3) {
            $botones.='<a href="' . base_url() . 'panel/resumengestores" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-indent-left"></span> Resúmenes</a> ';
            $botones.='<a href="' . base_url() . 'tarea/listapresu" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-indent-left"></span> Lista de Ppto</a> ';
        }
        if ($this->_rol != 1) {
            $botones.='<a href="' . base_url() . 'tarea/listaprogramadas" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-calendar"></span> Programación de tareas</a>';
            $botones.='  <a href="#" id="btnexportar" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-download-alt"></span> Exportar</a>';
        }
        if ($this->_rol != 3) {
            $botones.='  <a href="javascript:void(0)" id="btnEnviappto" data-toggle="modal" data-target=".bs-example-modal-lg" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-send"></span> Crear bloque Ppto</a>';
        }

        $botones.='  <a href="' . base_url() . 'tarea/crear" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-file"></span> Nuevo</a>';
        $botones.='  <a href="#" id="btnbuscaropcion" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-search"></span> Buscar</a>';

        $data['origen'] = "listar";
        $data['idgrupo'] = 0;
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Lista de tareas", TRUE);
        $this->template->write_view('content', 'tarea/listar', $data, TRUE);
        $this->template->render();
    }

    function evaluargrupo($idgrupo = 0) {
        validaSession("formulario");
        // Trayendo modelos
        $this->load->model('categoriamodel');
        $this->load->model('tareamodel');
        $this->load->model('grupomodel');
        $mensaje = $this->session->userdata('mensaje');
        if ($mensaje != "") {
            $mensaje = getHtmlMensaje(1, $mensaje);
        }

        $where = "t.estado_registro = 1 and t.idgrupo = " . $idgrupo;

        $oGrupo = $this->grupomodel->getdata(array("*"), "idgrupo = " . $idgrupo, 1);

        // Trayendo datos
        //$aCategorias = $this->categoriamodel->getdata(array("idcategoria", "nombre"), "estado_registro = 1");
        $aCateUsuario = $this->categoriamodel->getaCateUsuario();
        $auxListaTarea = array();
        $auxListaTarea = $this->tareamodel->getListaTareas($where);

        $aListaTareas = array();
        $aEstados = getEstados();
        if ($auxListaTarea != false) {
            foreach ($auxListaTarea as $itemTarea) {
                $this->cambiarLectura($itemTarea->idtarea);
                // Saber si se envió al administrados o al gestor
                $responsable = $aCateUsuario[$itemTarea->idcategoria]["nomresponsable"];
                if (intval($itemTarea->paraadmin) == 1) {
                    $responsable = "Administrador";
                }

                //*******************************
                array_push($aListaTareas, array("idtarea" => $itemTarea->idtarea,
                    "codigo" => $itemTarea->codigo,
                    "proveedor" => $itemTarea->proveedor,
                    "numrechazos" => $itemTarea->numrechazos,
                    "ppto_aprobado" => $itemTarea->ppto_aprobado,
                    "presupuesto" => $itemTarea->presupuesto,
                    "idusuario_creador" => $itemTarea->idusuario_rol_creador,
                    "idusuario_ejecutor" => $itemTarea->idusuario_rol_ejecutor,
                    "nombre" => $itemTarea->nombre,
                    "paraadmin" => $itemTarea->paraadmin,
                    "ultimoestado" => $itemTarea->ultimoestado,
                    "idtarea" => $itemTarea->idtarea,
                    "prioridad" => $itemTarea->prioridad,
                    "nomlocal" => $itemTarea->nomlocal,
                    "nomestado" => $aEstados[$itemTarea->ultimoestado][0]["nombre"],
                    "nomcategoria" => $aCateUsuario[$itemTarea->idcategoria]["nomcategoria"],
                    "nomresponsable" => $responsable,
                    "fregistro" => getFechaEs($itemTarea->fecha_registro),
                    "factualizacion" => getFechaEs($itemTarea->fecha_actualizacion),
                    "tipo_transcurrido" => getTiempoTranscurrido($itemTarea->fecha_actualizacion, date("Y-m-d H:i:s")),
                    "imagen" => $aEstados[$itemTarea->ultimoestado][0]["imagen"],
                    "enlace" => getEnlace($itemTarea->ultimoestado, $itemTarea->idtarea, $itemTarea->paraadmin, $itemTarea->idusuario_rol_creador)
                ));
            }
        }

        // Recibiendo parametros
        //$codMenu = $this->input->post('codmenu');

        $this->session->set_userdata('param_idusuario', "");
        $this->session->set_userdata('param_idestado', "");
        $data['titulo'] = "Evaluar grupo de tareas";
        $data['divmensaje'] = $mensaje;
        $data['montoevaluado'] = $oGrupo->estado_registro;
        $data['monto'] = $oGrupo->monto_aprobado;
        $this->load->model('usuariomodel');
        $oUsuario = $this->usuariomodel->getUsuarios();
        $data['oUsuario'] = $oUsuario;
        $data['origen'] = "evaluar";
        $data['aListaTareas'] = $aListaTareas;
        $data['idgrupo'] = $idgrupo;
        $botones = "";
        $botones.='  <a href="' . base_url() . 'tarea/listapresu" class="btn btn-sm btn-default">Atras</a>';
        $botones.='  <a href="#" id="btnEvaluarAprob" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-saved"></span> Aprobar</a>';


        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Presupuestos por aprobar", TRUE);
        $this->template->write_view('content', 'tarea/evaluargrupo', $data, TRUE);
        $this->template->render();
    }

    function listapresu() {
        validaSession("formulario");
        // Trayendo modelos
        $this->load->model('categoriamodel');
        $this->load->model('tareamodel');

        $mensaje = $this->session->userdata('mensaje');
        if ($mensaje != "") {
            $mensaje = getHtmlMensaje(1, $mensaje);
        }

        $where = "t.estado_registro = 1";
        if (intval($this->session->userdata('param_idusuario')) != 0) {
            $idusuario_param = $this->session->userdata('param_idusuario');
            $idestado_param = $this->session->userdata('param_idestado');
            if ($idestado_param == 3 || $idestado_param == 4) {
                $where .= " and t.idusuario_rol_creador = " . $idusuario_param . " and t.ultimoestado = " . $idestado_param;
            } else {
                $where .= " and t.idusuario_rol_ejecutor = " . $idusuario_param . " and t.ultimoestado = " . $idestado_param;
            }
        }

        // Trayendo datos
        $aGrupos = array();
        $aGrupos = $this->tareamodel->getListaGrupos("idgrupo > 0");
        $botones = "";
        $botones.='<a href = "' . base_url() . 'tarea/lista" class = "btn btn-sm btn-default" >Atras</a>';
        $botones.='  <a href="#" id="btnbuscaropcion" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-search"></span> Buscar</a>';
        $data['botones'] = $botones;
        $data['titulo'] = "Lista de presupuestos";
        $data['aGrupos'] = $aGrupos;

        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Lista de presupuestos", TRUE);
        $this->template->write_view('content', 'tarea/listapresu', $data, TRUE);
        $this->template->render();
    }

    public function get_div_busqueda() {
        $this->load->model('localmodel');
        $this->load->model('usuariomodel');
        $aLocales = array();
        if ($this->_rol == 1) {
            $aLocales = $this->localmodel->getdata(array("idlocal", "nombre"), "estado_registro = 1 and idlocal = " . $this->_idlocal);
            $aGestores = $this->usuariomodel->getdata(array("idusuario", "nombre"));
        } else {
            $aGestores = $this->usuariomodel->getdata(array("idusuario", "nombre"));
            $aLocales = $this->localmodel->getdata(array("idlocal", "nombre"), "estado_registro = 1");
        }

        $data["aLocales"] = $aLocales;
        $data["aGestores"] = $aGestores;
        $this->load->view("tarea/busqueda", $data);
    }

    function crear($idmsj = 0) {
// Trayendo modelos
        validaSession("formulario");
        $this->load->model('categoriamodel');
        $this->load->model('tareamodel');
        $this->load->model('localmodel');
        $this->load->model('archivomodel');

// Trayendo datos
        $rol = $this->session->userdata('rol');
        $aCategorias = $this->categoriamodel->getdata(array("idcategoria", "nombre"), "estado_registro = 1");
        $aLocales = array();
        if ($rol != 1) {
            $aLocales = $this->localmodel->getdata(array("idlocal", "nombre"), "estado_registro = 1");
        }
        $data['titulo'] = "Nueva tarea";
        $data['aCategorias'] = $aCategorias;
        $data['aLocales'] = $aLocales;
        $data['divmensaje'] = getHtmlMensaje($idmsj);
        $this->template->write('botones', '<a href = "' . base_url() . 'tarea/lista" class = "btn btn-sm btn-default" >Atras</a>', TRUE);
        $this->template->write('titleheader', "Nueva tarea", TRUE);
        $this->template->write_view('content', 'tarea/crear', $data, TRUE);
        $this->template->render();
    }

    function poratender($idtarea = 0) {
        validaSession("formulario");
        // Trayendo modelos
        $this->load->model('categoriamodel');
        $this->load->model('tareamodel');
        $this->load->model('tareaestadomodel');
        $this->load->model('localmodel');
        $this->load->model('archivomodel');
        $this->cambiarLectura($idtarea);
        // Trayendo datos
        $auxObjTarea = $this->tareamodel->getListaTareas("t.estado_registro = 1 and t.idtarea = " . $idtarea, 1);
        $divmensaje = "";
        if ($auxObjTarea->ultimoestado != 1) {
            $divmensaje = "Ya se registró esta actividad";
        }
        $aEstados = getEstados();
        $aCateUsuario = $this->categoriamodel->getaCateUsuario();

        // ACTUALIZANDO LA DATA TAREAESTADOS A ENVIAR
        $aTareaEstados = array();
        $this->load->model('usuariomodel');
        $objUsuario = $this->usuariomodel->getUsuarios();
        $auxTareaEstados = $this->tareaestadomodel->getTareaEstados($idtarea);

        if ($auxTareaEstados != false) {

            $ultimaFecha = $auxObjTarea->fecha_registro;
            foreach ($auxTareaEstados as $itemTareaEstado) {
                array_push($aTareaEstados, array("nomestado" => $aEstados[$itemTareaEstado->idestado][$itemTareaEstado->evaluacion]["gerundio"],
                    "comentario" => $itemTareaEstado->comentario,
                    "presupuesto" => $itemTareaEstado->presupuesto,
                    "idtarea_estado" => $itemTareaEstado->idtarea_estado,
                    "aRespuestas" => $this->respuestasmodel->getdata(array("*"), "idtarea_estado = " . $itemTareaEstado->idtarea_estado),
                    "nomusuario" => $objUsuario[$itemTareaEstado->idusuario]["nombre"],
                    "fregistro" => getFechaEs($itemTareaEstado->fecha_registro),
                    "transcurrido" => getTiempoTranscurrido($ultimaFecha, $itemTareaEstado->fecha_registro),
                    "imagen" => $objUsuario[$itemTareaEstado->idusuario]["foto"],
                    "archivos" => $itemTareaEstado->archivos
                ));

                $ultimaFecha = $itemTareaEstado->fecha_registro;
            }
        }

        if ($auxObjTarea != false) {
// Saber si se envió al administrados o al gestor
            $responsable = $aCateUsuario[$auxObjTarea->idcategoria]["nomresponsable"];
            if (intval($auxObjTarea->paraadmin) == 1) {
                $responsable = "Administrador";
            }

            $datetime1 = date_create($auxObjTarea->fecha_registro);
            $datetime2 = date_create(date("Y-m-d H:i:s"));
            $fecha = date_diff($datetime1, $datetime2);
            $tiempoTranscurrido = "";
            if ($fecha->y > 0) {
                $tiempoTranscurrido = $fecha->y . " A ";
            }
            if ($fecha->m > 0) {
                $tiempoTranscurrido = $fecha->m . " M ";
            }
            if ($fecha->d > 0) {
                $tiempoTranscurrido = $fecha->d . " d ";
            }
            if ($fecha->h > 0) {
                $tiempoTranscurrido = $fecha->h . "h ";
            }
            if ($fecha->i > 0) {
                $tiempoTranscurrido = $fecha->i . "m ";
            }
//*******************************
            $objTarea["idtarea"] = $auxObjTarea->idtarea;
            $objTarea["codigo"] = $auxObjTarea->codigo;
            $objTarea["nombre"] = $auxObjTarea->nombre;
            $objTarea["presupuesto"] = $auxObjTarea->presupuesto;
            $objTarea["proveedor"] = $auxObjTarea->proveedor;
            $objTarea["paraadmin"] = $auxObjTarea->paraadmin;
            $objTarea["nomlocal"] = $auxObjTarea->nomlocal;
            $objTarea["descripcion"] = $auxObjTarea->descripcion;
            $objTarea["nomestado"] = $aEstados[$auxObjTarea->ultimoestado][0]["nombre"];
            $objTarea["nomcategoria"] = $aCateUsuario[$auxObjTarea->idcategoria]["nomcategoria"];
            $objTarea["nomresponsable"] = $responsable;
            $objTarea["fregistro"] = getFechaEs($auxObjTarea->fecha_registro);
            $objTarea["factualizacion"] = getFechaEs($auxObjTarea->fecha_actualizacion);
            $objTarea["tipo_transcurrido"] = $tiempoTranscurrido;
            $objTarea["imagen"] = $aEstados[$auxObjTarea->ultimoestado][0]["imagen"];
            $objTarea["enlace"] = getEnlace($auxObjTarea->ultimoestado, $auxObjTarea->idtarea, $auxObjTarea->paraadmin, $auxObjTarea->idusuario_rol_creador);
        }

// Trayendo los archivos de la tarea
        $aArchivosTarea = array();
        $aArchivosTarea = $this->archivomodel->getArchivosTarea($idtarea);
        $this->load->model('usuariomodel');
        $objUsuario = $this->usuariomodel->getUsuarios();
        $data['oUsuario'] = $objUsuario;
        $data['titulo'] = "Por atender";
        $data['divmensaje'] = $divmensaje;
        $data['objTarea'] = $objTarea;
        $data['aTareaEstados'] = $aTareaEstados;
        $data['aArchivosTarea'] = $aArchivosTarea;
        $this->template->write('botones', '<a href = "' . base_url() . 'tarea/lista" class = "btn btn-sm btn-default" >Atras</a>', TRUE);
        $this->template->write('titleheader', "Por atender", TRUE);
        $this->template->write_view('content', 'tarea/poratender', $data, TRUE);
        $this->template->render();
    }

    function corregirppto($idtarea = 0) {
        validaSession("formulario");
        // Trayendo modelos
        $this->cambiarLectura($idtarea);
        $this->load->model('categoriamodel');
        $this->load->model('tareamodel');
        $this->load->model('tareaestadomodel');
        $this->load->model('localmodel');
        $this->load->model('archivomodel');

        // Trayendo datos
        $auxObjTarea = $this->tareamodel->getListaTareas("t.estado_registro = 1 and t.idtarea = " . $idtarea, 1);
        $divmensaje = "";
        if ($auxObjTarea->ultimoestado != 8) {
            $divmensaje = "Ya se registró esta actividad";
        }
        $aEstados = getEstados();
        $aCateUsuario = $this->categoriamodel->getaCateUsuario();

        // ACTUALIZANDO LA DATA TAREAESTADOS A ENVIAR
        $aTareaEstados = array();
        $this->load->model('usuariomodel');
        $objUsuario = $this->usuariomodel->getUsuarios();
        $auxTareaEstados = $this->tareaestadomodel->getTareaEstados($idtarea);

        if ($auxTareaEstados != false) {

            $ultimaFecha = $auxObjTarea->fecha_registro;
            foreach ($auxTareaEstados as $itemTareaEstado) {
                array_push($aTareaEstados, array("nomestado" => $aEstados[$itemTareaEstado->idestado][$itemTareaEstado->evaluacion]["gerundio"],
                    "comentario" => $itemTareaEstado->comentario,
                    "idtarea_estado" => $itemTareaEstado->idtarea_estado,
                    "aRespuestas" => $this->respuestasmodel->getdata(array("*"), "idtarea_estado = " . $itemTareaEstado->idtarea_estado),
                    "presupuesto" => $itemTareaEstado->presupuesto,
                    "nomusuario" => $objUsuario[$itemTareaEstado->idusuario]["nombre"],
                    "fregistro" => getFechaEs($itemTareaEstado->fecha_registro),
                    "transcurrido" => getTiempoTranscurrido($ultimaFecha, $itemTareaEstado->fecha_registro),
                    "imagen" => $objUsuario[$itemTareaEstado->idusuario]["foto"],
                    "archivos" => $itemTareaEstado->archivos
                ));

                $ultimaFecha = $itemTareaEstado->fecha_registro;
            }
        }

        if ($auxObjTarea != false) {
// Saber si se envió al administrados o al gestor
            $responsable = $aCateUsuario[$auxObjTarea->idcategoria]["nomresponsable"];
            if (intval($auxObjTarea->paraadmin) == 1) {
                $responsable = "Administrador";
            }

            $datetime1 = date_create($auxObjTarea->fecha_registro);
            $datetime2 = date_create(date("Y-m-d H:i:s"));
            $fecha = date_diff($datetime1, $datetime2);
            $tiempoTranscurrido = "";
            if ($fecha->y > 0) {
                $tiempoTranscurrido = $fecha->y . " A ";
            }
            if ($fecha->m > 0) {
                $tiempoTranscurrido = $fecha->m . " M ";
            }
            if ($fecha->d > 0) {
                $tiempoTranscurrido = $fecha->d . " d ";
            }
            if ($fecha->h > 0) {
                $tiempoTranscurrido = $fecha->h . "h ";
            }
            if ($fecha->i > 0) {
                $tiempoTranscurrido = $fecha->i . "m ";
            }
//*******************************
            $objTarea["idtarea"] = $auxObjTarea->idtarea;
            $objTarea["codigo"] = $auxObjTarea->codigo;
            $objTarea["nombre"] = $auxObjTarea->nombre;
            $objTarea["presupuesto"] = $auxObjTarea->presupuesto;
            $objTarea["proveedor"] = $auxObjTarea->proveedor;
            $objTarea["nomlocal"] = $auxObjTarea->nomlocal;
            $objTarea["descripcion"] = $auxObjTarea->descripcion;
            $objTarea["nomestado"] = $aEstados[$auxObjTarea->ultimoestado][0]["nombre"];
            $objTarea["nomcategoria"] = $aCateUsuario[$auxObjTarea->idcategoria]["nomcategoria"];
            $objTarea["nomresponsable"] = $responsable;
            $objTarea["fregistro"] = getFechaEs($auxObjTarea->fecha_registro);
            $objTarea["factualizacion"] = getFechaEs($auxObjTarea->fecha_actualizacion);
            $objTarea["tipo_transcurrido"] = $tiempoTranscurrido;
            $objTarea["imagen"] = $aEstados[$auxObjTarea->ultimoestado][0]["imagen"];
            $objTarea["enlace"] = getEnlace($auxObjTarea->ultimoestado, $auxObjTarea->idtarea, $auxObjTarea->paraadmin, $auxObjTarea->idusuario_rol_creador);
        }

// Trayendo los archivos de la tarea
        $aArchivosTarea = array();
        $aArchivosTarea = $this->archivomodel->getArchivosTarea($idtarea);

        $data['titulo'] = "Por corregir presupuesto";
        $data['divmensaje'] = $divmensaje;
        $data['objTarea'] = $objTarea;
        $data['aTareaEstados'] = $aTareaEstados;
        $data['aArchivosTarea'] = $aArchivosTarea;
        $this->template->write('botones', '<a href = "' . base_url() . 'tarea/lista" class = "btn btn-sm btn-default" >Atras</a>', TRUE);
        $this->template->write('titleheader', "Por corregir presupuesto", TRUE);
        $this->template->write_view('content', 'tarea/corregirppto', $data, TRUE);
        $this->template->render();
    }

    function ejecutar($idtarea = 0) {
        validaSession("formulario");
        $this->cambiarLectura($idtarea);
        // Trayendo modelos
        $this->load->model('categoriamodel');
        $this->load->model('tareamodel');
        $this->load->model('tareaestadomodel');
        $this->load->model('localmodel');
        $this->load->model('archivomodel');
        $objTarea = array();
        $aTareaEstados = array();
        $aArchivosTarea = array();
        $divmensaje = "";
        // Trayendo datos
        $auxObjTarea = $this->tareamodel->getListaTareas("t.estado_registro = 1 and t.idtarea = " . $idtarea, 1);

        if ($auxObjTarea != FALSE) {
            if ($auxObjTarea->ultimoestado == 2) {
                $aEstados = getEstados();
                $aCateUsuario = $this->categoriamodel->getaCateUsuario();

                // ACTUALIZANDO LA DATA TAREAESTADOS A ENVIAR
                $this->load->model('usuariomodel');
                $objUsuario = $this->usuariomodel->getUsuarios();

                $auxTareaEstados = $this->tareaestadomodel->getTareaEstados($idtarea);
                if ($auxTareaEstados != false) {
                    $ultimaFecha = $auxObjTarea->fecha_registro;
                    foreach ($auxTareaEstados as $itemTareaEstado) {
                        array_push($aTareaEstados, array("nomestado" => $aEstados[$itemTareaEstado->idestado][$itemTareaEstado->evaluacion]["gerundio"],
                            "comentario" => $itemTareaEstado->comentario,
                            "presupuesto" => $itemTareaEstado->presupuesto,
                            "idtarea_estado" => $itemTareaEstado->idtarea_estado,
                            "aRespuestas" => $this->respuestasmodel->getdata(array("*"), "idtarea_estado = " . $itemTareaEstado->idtarea_estado),
                            "nomusuario" => $objUsuario[$itemTareaEstado->idusuario]["nombre"],
                            "fregistro" => getFechaEs($itemTareaEstado->fecha_registro),
                            "transcurrido" => getTiempoTranscurrido($ultimaFecha, $itemTareaEstado->fecha_registro),
                            "imagen" => $objUsuario[$itemTareaEstado->idusuario]["foto"],
                            "archivos" => $itemTareaEstado->archivos
                        ));

                        $ultimaFecha = $itemTareaEstado->fecha_registro;
                    }
                }

                if ($auxObjTarea != false) {
// Saber si se envió al administrados o al gestor
                    $responsable = $aCateUsuario[$auxObjTarea->idcategoria]["nomresponsable"];
                    if (intval($auxObjTarea->paraadmin) == 1) {
                        $responsable = "Administrador";
                    }

                    $datetime1 = date_create($auxObjTarea->fecha_registro);
                    $datetime2 = date_create(date("Y-m-d H:i:s"));
                    $fecha = date_diff($datetime1, $datetime2);
                    $tiempoTranscurrido = "";
                    if ($fecha->y > 0) {
                        $tiempoTranscurrido = $fecha->y . " A ";
                    }
                    if ($fecha->m > 0) {
                        $tiempoTranscurrido = $fecha->m . " M ";
                    }
                    if ($fecha->d > 0) {
                        $tiempoTranscurrido = $fecha->d . " d ";
                    }
                    if ($fecha->h > 0) {
                        $tiempoTranscurrido = $fecha->h . "h ";
                    }
                    if ($fecha->i > 0) {
                        $tiempoTranscurrido = $fecha->i . "m ";
                    }
//*******************************
                    $objTarea["idtarea"] = $auxObjTarea->idtarea;
                    $objTarea["codigo"] = $auxObjTarea->codigo;
                    $objTarea["nombre"] = $auxObjTarea->nombre;
                    $objTarea["paraadmin"] = $auxObjTarea->paraadmin;
                    $objTarea["presupuesto"] = $auxObjTarea->presupuesto;
                    $objTarea["proveedor"] = $auxObjTarea->proveedor;
                    $objTarea["nomlocal"] = $auxObjTarea->nomlocal;
                    $objTarea["descripcion"] = $auxObjTarea->descripcion;
                    $objTarea["nomestado"] = $aEstados[$auxObjTarea->ultimoestado][0]["nombre"];
                    $objTarea["nomcategoria"] = $aCateUsuario[$auxObjTarea->idcategoria]["nomcategoria"];
                    $objTarea["nomresponsable"] = $responsable;
                    $objTarea["fregistro"] = getFechaEs($auxObjTarea->fecha_registro);
                    $objTarea["factualizacion"] = getFechaEs($auxObjTarea->fecha_actualizacion);
                    $objTarea["tipo_transcurrido"] = $tiempoTranscurrido;
                    $objTarea["imagen"] = $aEstados[$auxObjTarea->ultimoestado][0]["imagen"];
                    $objTarea["enlace"] = getEnlace($auxObjTarea->ultimoestado, $auxObjTarea->idtarea, $auxObjTarea->paraadmin, $auxObjTarea->idusuario_rol_creador);
                }

// Trayendo los archivos de la tarea
                $aArchivosTarea = array();
                $aArchivosTarea = $this->archivomodel->getArchivosTarea($idtarea);
            } else {
                $divmensaje.="Ya se realizó esta actividad";
            }
        } else {
            $divmensaje.="No existe esta tarea";
        }
        $this->load->model('usuariomodel');
        $objUsuario = $this->usuariomodel->getUsuarios();
        $data['oUsuario'] = $objUsuario;
        $data['titulo'] = "Por ejecutar";
        $data['divmensaje'] = $divmensaje;
        $data['objTarea'] = $objTarea;
        $data['aTareaEstados'] = $aTareaEstados;
        $data['aArchivosTarea'] = $aArchivosTarea;
        $this->template->write('botones', '<a href = "' . base_url() . 'tarea/lista" class = "btn btn-sm btn-default" >Atras</a>', TRUE);
        $this->template->write('titleheader', "Por ejecutar", TRUE);
        $this->template->write_view('content', 'tarea/ejecutar', $data, TRUE);
        $this->template->render();
    }

    //***********  EVALUACIÓN DE PRESUPUESTO ************************
    function evaluarppto($idtarea = 0) {
        validaSession("formulario");
        $this->cambiarLectura($idtarea);
        // Trayendo modelos
        $this->load->model('categoriamodel');
        $this->load->model('tareamodel');
        $this->load->model('tareaestadomodel');
        $this->load->model('localmodel');
        $this->load->model('archivomodel');
        $objTarea = array();
        $aTareaEstados = array();
        $aArchivosTarea = array();
        $divmensaje = "";
        // Trayendo datos
        $auxObjTarea = $this->tareamodel->getListaTareas("t.estado_registro = 1 and t.idtarea = " . $idtarea, 1);

        if ($auxObjTarea != FALSE) {
            if ($auxObjTarea->ultimoestado == 11 || $auxObjTarea->ultimoestado == 10) {
                $aEstados = getEstados();
                $aCateUsuario = $this->categoriamodel->getaCateUsuario();

                // ACTUALIZANDO LA DATA TAREAESTADOS A ENVIAR
                $this->load->model('usuariomodel');
                $objUsuario = $this->usuariomodel->getUsuarios();

                $auxTareaEstados = $this->tareaestadomodel->getTareaEstados($idtarea);
                if ($auxTareaEstados != false) {
                    $ultimaFecha = $auxObjTarea->fecha_registro;
                    foreach ($auxTareaEstados as $itemTareaEstado) {
                        array_push($aTareaEstados, array("nomestado" => $aEstados[$itemTareaEstado->idestado][$itemTareaEstado->evaluacion]["gerundio"],
                            "comentario" => $itemTareaEstado->comentario,
                            "presupuesto" => $itemTareaEstado->presupuesto,
                            "idtarea_estado" => $itemTareaEstado->idtarea_estado,
                            "aRespuestas" => $this->respuestasmodel->getdata(array("*"), "idtarea_estado = " . $itemTareaEstado->idtarea_estado),
                            "nomusuario" => $objUsuario[$itemTareaEstado->idusuario]["nombre"],
                            "fregistro" => getFechaEs($itemTareaEstado->fecha_registro),
                            "transcurrido" => getTiempoTranscurrido($ultimaFecha, $itemTareaEstado->fecha_registro),
                            "imagen" => $objUsuario[$itemTareaEstado->idusuario]["foto"],
                            "archivos" => $itemTareaEstado->archivos
                        ));

                        $ultimaFecha = $itemTareaEstado->fecha_registro;
                    }
                }

                if ($auxObjTarea != false) {
// Saber si se envió al administrados o al gestor
                    $responsable = $aCateUsuario[$auxObjTarea->idcategoria]["nomresponsable"];
                    if (intval($auxObjTarea->paraadmin) == 1) {
                        $responsable = "Administrador";
                    }

                    $datetime1 = date_create($auxObjTarea->fecha_registro);
                    $datetime2 = date_create(date("Y-m-d H:i:s"));
                    $fecha = date_diff($datetime1, $datetime2);
                    $tiempoTranscurrido = "";
                    if ($fecha->y > 0) {
                        $tiempoTranscurrido = $fecha->y . " A ";
                    }
                    if ($fecha->m > 0) {
                        $tiempoTranscurrido = $fecha->m . " M ";
                    }
                    if ($fecha->d > 0) {
                        $tiempoTranscurrido = $fecha->d . " d ";
                    }
                    if ($fecha->h > 0) {
                        $tiempoTranscurrido = $fecha->h . "h ";
                    }
                    if ($fecha->i > 0) {
                        $tiempoTranscurrido = $fecha->i . "m ";
                    }
//*******************************
                    $objTarea["idtarea"] = $auxObjTarea->idtarea;
                    $objTarea["codigo"] = $auxObjTarea->codigo;
                    $objTarea["nombre"] = $auxObjTarea->nombre;
                    $objTarea["presupuesto"] = $auxObjTarea->presupuesto;
                    $objTarea["proveedor"] = $auxObjTarea->proveedor;
                    $objTarea["nomlocal"] = $auxObjTarea->nomlocal;
                    $objTarea["descripcion"] = $auxObjTarea->descripcion;
                    $objTarea["nomestado"] = $aEstados[$auxObjTarea->ultimoestado][0]["nombre"];
                    $objTarea["nomcategoria"] = $aCateUsuario[$auxObjTarea->idcategoria]["nomcategoria"];
                    $objTarea["nomresponsable"] = $responsable;
                    $objTarea["fregistro"] = getFechaEs($auxObjTarea->fecha_registro);
                    $objTarea["factualizacion"] = getFechaEs($auxObjTarea->fecha_actualizacion);
                    $objTarea["tipo_transcurrido"] = $tiempoTranscurrido;
                    $objTarea["imagen"] = $aEstados[$auxObjTarea->ultimoestado][0]["imagen"];
                    $objTarea["enlace"] = getEnlace($auxObjTarea->ultimoestado, $auxObjTarea->idtarea, $auxObjTarea->paraadmin, $auxObjTarea->idusuario_rol_creador);
                }

// Trayendo los archivos de la tarea
                $aArchivosTarea = array();
                $aArchivosTarea = $this->archivomodel->getArchivosTarea($idtarea);
            } else {
                $divmensaje.="Ya se realizó esta actividad";
            }
        } else {
            $divmensaje.="No existe esta tarea";
        }

        $data['titulo'] = "Evaluar presupuesto";
        $data['divmensaje'] = $divmensaje;
        $data['objTarea'] = $objTarea;
        $data['aTareaEstados'] = $aTareaEstados;
        $data['aArchivosTarea'] = $aArchivosTarea;
        $this->template->write('botones', '<a href = "' . base_url() . 'tarea/lista" class = "btn btn-sm btn-default" >Atras</a>', TRUE);
        $this->template->write('titleheader', "Evaluar presupuesto", TRUE);
        $this->template->write_view('content', 'tarea/evaluarppto', $data, TRUE);
        $this->template->render();
    }

    public function json_rechazar() {
        $oLogin = validaSession("json");
        if ($oLogin[
                "msj"] != "") {
            echo json_encode($oLogin);
            exit;
        }
        $this->load->model('tareamodel');
        $this->load->model('usuariomodel');
        $this->load->model('tareaestadomodel');
        $this->load->model('categoriamodel');
        $this->load->model('archivomodel');
        $this->load->model('grupomodel');

        $idtarea = $this->input->post("idtarea");

        $objauxTarea = $this->tareamodel->getdata(array("*"), "idtarea = " . $idtarea, 1);
        $numRechazos = $objauxTarea->numrechazos;
        $numRechazos++;
        $objTareaEstado = array("idusuario" => $this->_idusuario_actual,
            "idtarea" => $idtarea,
            "idestado" => 1,
            "evaluacion" => 1,
            "comentario" => "La tarea fue rechazada por el director"
        );
        $idTareaEstado = $this->tareaestadomodel->guardar($objTareaEstado);
        if ($idTareaEstado > 0) {
            // Actualizando el ultimo estado para esta tarea
            $objTarea = array(
                "ultimoestado" => 1,
                "ppto_aprobado" => 2,
                "numrechazos" => $numRechazos,
                "fecha_actualizacion" => date("Y-m-d H:i:s"),
            );
            $this->tareamodel->editar($idtarea, $objTarea);
            $oTareasPresupuestar = $this->tareamodel->getdata(array("count(idtarea) as cantidad"), "ultimoestado = 10 and idgrupo = " . $objauxTarea->idgrupo, 1);
            // Traemos datos del grupo
            $oauxGrupo = $this->grupomodel->getdata(array("cantidadtareas", "montototal", "cant_rechazados", "monto_rechazados"), "idgrupo = " . $objauxTarea->idgrupo, 1);
            // Encontramos la cantidad de rechazados y el monto de lo rechazado
            $cantTareaGrupo = $oauxGrupo->cant_rechazados;
            $montoTareaGrupo = $oauxGrupo->monto_rechazados;
            // Sumamos las cantidades y montos rechazados
            $cantTareaGrupo++;
            $montoTareaGrupo = $montoTareaGrupo + $objauxTarea->presupuesto;
            $objGrupoAx = array("cant_rechazados" => $cantTareaGrupo, "monto_rechazados" => $montoTareaGrupo);
            $this->grupomodel->editar($objauxTarea->idgrupo, $objGrupoAx);

            if ($oTareasPresupuestar->cantidad == 0) {
                $objGrupo = array();
                $objGrupo = array(
                    "monto_aprobado" => 0,
                    "cantidad_aprobados" => 0,
                    "estado_registro" => 3
                );
                $this->grupomodel->editar($objauxTarea->idgrupo, $objGrupo);
            }
            $objUsuario = $this->usuariomodel->getUsuarios();
            $idnotificacion = $objauxTarea->idusuario_rol_ejecutor;
            enviarEmail(array(
                "FROM" => "Hikari- Notificaciones",
                "FROMNAME" => "sistemas@hikari.pe",
                "asunto" => "Actualización de la tarea - " . $objauxTarea->codigo,
                "mensaje" => 'La tarea "' . $objauxTarea->nombre . '" con código "' . $objauxTarea->codigo . '" fué rechazada',
                "titulo" => "De: " . $objUsuario[$this->_idusuario_actual]["nombre"] . "<br>Para: " . $objUsuario[$idnotificacion]["nombre"],
                "correo" => $objUsuario[$idnotificacion]["correo"]
            ));
            $this->registrarLectura($idTareaEstado, $idnotificacion, 'La tarea "' . $objauxTarea->nombre . '" con código "' . $objauxTarea->codigo . '" fué rechazada por ' . $objUsuario[$this->_idusuario_actual]["nombre"]);
            
            // NOTOTIFICACIONES DE RESPUESTAS
            $this->NotiRespuestas($objUsuario, $objauxTarea, $idTareaEstado,'La tarea "' . $objauxTarea->nombre . '" con código "' . $objauxTarea->codigo . '" fué rechazada');
        }
        echo $idtarea;
    }

    public function json_evaluar() {
        $oLogin = validaSession("json");
        if ($oLogin[
                "msj"] != "") {
            echo json_encode($oLogin);
            exit;
        }
        $this->load->model('tareamodel');
        $this->load->model('tareaestadomodel');
        $this->load->model('categoriamodel');
        $this->load->model('archivomodel');
        $this->load->model('grupomodel');

        $idgrupo = $this->input->post("idgrupo");
        $monto = $this->input->post("monto");

        $ids = $this->input->post("ids");
        $msg_error = "";

        $aIds = explode(",", $ids);
        $objIds = array();
        $cantAprob = 0;
        $this->load->model('usuariomodel');
        $objUsuario = $this->usuariomodel->getUsuarios();
        $aGrupos = array();
        $aGrupos = $this->tareamodel->getdata(array("idtarea"), "idgrupo = " . $idgrupo);
        foreach ($aGrupos as $itemGrupo) {
            $estado = 8;
            $pptAprob = 2;
            $descripcion = "rechazada";
            foreach ($aIds as $itemId) {
                if ($itemGrupo->idtarea == $itemId) {
                    $estado = 2;
                    $cantAprob++;
                    $pptAprob = 1;
                    $descripcion = "aprobada";
                    break;
                }
            }
            $objTareaEstado = array("idusuario" => $this->_idusuario_actual,
                "idtarea" => $itemGrupo->idtarea,
                "idestado" => $estado,
                "evaluacion" => 1,
                "comentario" => "La tarea fue " . $descripcion
            );
            $idTareaEstado = $this->tareaestadomodel->guardar($objTareaEstado);
            if ($idTareaEstado > 0) {
                // Actualizando el ultimo estado para esta tarea

                $objTarea = array(
                    "ultimoestado" => $estado,
                    "ppto_aprobado" => $pptAprob,
                    "fecha_actualizacion" => date("Y-m-d H:i:s"),
                );
                $objauxTarea = $this->tareamodel->getdata(array("*"), "idtarea = " . intval($itemGrupo->idtarea), 1);
                if ($pptAprob == 2) {
                    $numRechazos = 0;
                    if ($numRechazos != false) {
                        $numRechazos = $objauxTarea->numrechazos;
                        $numRechazos++;
                        $objTarea["numrechazos"] = $numRechazos;
                    }
                }
                $this->tareamodel->editar($itemGrupo->idtarea, $objTarea);
            }
            $auxObjTarea = $this->tareamodel->getListaTareas("t.estado_registro = 1 and t.idtarea = " . $itemGrupo->idtarea, 1);
            if ($estado == 8) {
                // A corregir
                $idnotificacion = $auxObjTarea->idusuario_rol_ejecutor;
            } else {
                // A ejecutar
                $idnotificacion = $auxObjTarea->idusuario_rol_ejecutor;
            }
            enviarEmail(array(
                "FROM" => "Hikari- Notificaciones",
                "FROMNAME" => "sistemas@hikari.pe",
                "asunto" => "Actualización de la tarea - " . $objauxTarea->codigo,
                "mensaje" => 'La tarea "' . $objauxTarea->nombre . '" con código "' . $objauxTarea->codigo . '" fué ' . $descripcion,
                "titulo" => "De: " . $objUsuario[$this->_idusuario_actual]["nombre"] . "<br>Para: " . $objUsuario[$idnotificacion]["nombre"],
                "correo" => $objUsuario[$idnotificacion]["correo"]
            ));

            $this->registrarLectura($idTareaEstado, $idnotificacion, 'La tarea "' . $objauxTarea->nombre . '" con código "' . $objauxTarea->codigo . '" fué ' . $descripcion);
        }
        $objauxGrupo = array();
        $objauxGrupo = $this->grupomodel->getdata(array("cantidadtareas", "montototal"), "idgrupo = " . $idgrupo, 1);
        //***** CONFIGURANDO CANTIDADES **************
        //$cantRechazadosActual=intval($objauxGrupo->cant_rechazados);
        //$montoRechazadosActual=intval($objauxGrupo->monto_rechazados);

        $objGrupo = array();
        $objGrupo = array(
            "monto_aprobado" => doubleval($monto),
            "cantidad_aprobados" => intval($cantAprob),
            "monto_rechazados" => (doubleval($objauxGrupo->montototal) - doubleval($monto)),
            "cant_rechazados" => intval($objauxGrupo->cantidadtareas) - intval($cantAprob),
            "estado_registro" => 3
        );
        $this->grupomodel->editar($idgrupo, $objGrupo);
        
        $this->NotiRespuestas($objUsuario, $objauxTarea, $idTareaEstado, 'La tarea "' . $objauxTarea->nombre . '" con código "' . $objauxTarea->codigo . '" fué ' . $descripcion);
        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    //***************************************************************
    function corregir_ejecucion($idtarea = 0) {
        validaSession("formulario");
        // Trayendo modelos
        $this->cambiarLectura($idtarea);
        $this->load->model('categoriamodel');
        $this->load->model('tareamodel');
        $this->load->model('tareaestadomodel');
        $this->load->model('localmodel');
        $this->load->model('archivomodel');
        $objTarea = array();
        $aTareaEstados = array();
        $aArchivosTarea = array();
        $divmensaje = "";
        // Trayendo datos
        $auxObjTarea = $this->tareamodel->getListaTareas("t.estado_registro = 1 and t.idtarea = " . $idtarea, 1);

        if ($auxObjTarea != FALSE) {
            if ($auxObjTarea->ultimoestado == 6) {
                $aEstados = getEstados();
                $aCateUsuario = $this->categoriamodel->getaCateUsuario();

                // ACTUALIZANDO LA DATA TAREAESTADOS A ENVIAR
                $this->load->model('usuariomodel');
                $objUsuario = $this->usuariomodel->getUsuarios();

                $auxTareaEstados = $this->tareaestadomodel->getTareaEstados($idtarea);
                if ($auxTareaEstados != false) {
                    $ultimaFecha = $auxObjTarea->fecha_registro;
                    foreach ($auxTareaEstados as $itemTareaEstado) {
                        array_push($aTareaEstados, array("nomestado" => $aEstados[$itemTareaEstado->idestado][$itemTareaEstado->evaluacion]["gerundio"],
                            "comentario" => $itemTareaEstado->comentario,
                            "presupuesto" => $itemTareaEstado->presupuesto,
                            "idtarea_estado" => $itemTareaEstado->idtarea_estado,
                            "aRespuestas" => $this->respuestasmodel->getdata(array("*"), "idtarea_estado = " . $itemTareaEstado->idtarea_estado),
                            "nomusuario" => $objUsuario[$itemTareaEstado->idusuario]["nombre"],
                            "fregistro" => getFechaEs($itemTareaEstado->fecha_registro),
                            "transcurrido" => getTiempoTranscurrido($ultimaFecha, $itemTareaEstado->fecha_registro),
                            "imagen" => $objUsuario[$itemTareaEstado->idusuario]["foto"],
                            "archivos" => $itemTareaEstado->archivos
                        ));

                        $ultimaFecha = $itemTareaEstado->fecha_registro;
                    }
                }

                if ($auxObjTarea != false) {
// Saber si se envió al administrados o al gestor
                    $responsable = $aCateUsuario[$auxObjTarea->idcategoria]["nomresponsable"];
                    if (intval($auxObjTarea->paraadmin) == 1) {
                        $responsable = "Administrador";
                    }

                    $datetime1 = date_create($auxObjTarea->fecha_registro);
                    $datetime2 = date_create(date("Y-m-d H:i:s"));
                    $fecha = date_diff($datetime1, $datetime2);
                    $tiempoTranscurrido = "";
                    if ($fecha->y > 0) {
                        $tiempoTranscurrido = $fecha->y . " A ";
                    }
                    if ($fecha->m > 0) {
                        $tiempoTranscurrido = $fecha->m . " M ";
                    }
                    if ($fecha->d > 0) {
                        $tiempoTranscurrido = $fecha->d . " d ";
                    }
                    if ($fecha->h > 0) {
                        $tiempoTranscurrido = $fecha->h . "h ";
                    }
                    if ($fecha->i > 0) {
                        $tiempoTranscurrido = $fecha->i . "m ";
                    }
//*******************************
                    $objTarea["idtarea"] = $auxObjTarea->idtarea;
                    $objTarea["codigo"] = $auxObjTarea->codigo;
                    $objTarea["nombre"] = $auxObjTarea->nombre;
                    $objTarea["presupuesto"] = $auxObjTarea->presupuesto;
                    $objTarea["proveedor"] = $auxObjTarea->proveedor;
                    $objTarea["nomlocal"] = $auxObjTarea->nomlocal;
                    $objTarea["descripcion"] = $auxObjTarea->descripcion;
                    $objTarea["nomestado"] = $aEstados[$auxObjTarea->ultimoestado][0]["nombre"];
                    $objTarea["nomcategoria"] = $aCateUsuario[$auxObjTarea->idcategoria]["nomcategoria"];
                    $objTarea["nomresponsable"] = $responsable;
                    $objTarea["fregistro"] = getFechaEs($auxObjTarea->fecha_registro);
                    $objTarea["factualizacion"] = getFechaEs($auxObjTarea->fecha_actualizacion);
                    $objTarea["tipo_transcurrido"] = $tiempoTranscurrido;
                    $objTarea["imagen"] = $aEstados[$auxObjTarea->ultimoestado][0]["imagen"];
                    $objTarea["enlace"] = getEnlace($auxObjTarea->ultimoestado, $auxObjTarea->idtarea, $auxObjTarea->paraadmin, $auxObjTarea->idusuario_rol_creador);
                }

// Trayendo los archivos de la tarea
                $aArchivosTarea = array();
                $aArchivosTarea = $this->archivomodel->getArchivosTarea($idtarea);
            } else {
                $divmensaje.="Ya se realizó esta actividad";
            }
        } else {
            $divmensaje.="No existe esta tarea";
        }
        $this->load->model('usuariomodel');
        $objUsuario = $this->usuariomodel->getUsuarios();
        $data['oUsuario'] = $objUsuario;
        $data['titulo'] = "Por ejecutar";
        $data['divmensaje'] = $divmensaje;
        $data['objTarea'] = $objTarea;
        $data['aTareaEstados'] = $aTareaEstados;
        $data['aArchivosTarea'] = $aArchivosTarea;
        $this->template->write('botones', '<a href = "' . base_url() . 'tarea/lista" class = "btn btn-sm btn-default" >Atras</a>', TRUE);
        $this->template->write('titleheader', "Corregir ejecución", TRUE);
        $this->template->write_view('content', 'tarea/corregirejecucion', $data, TRUE);
        $this->template->render();
    }

    function editar($idtarea = 0) {
        validaSession("formulario");
        // Trayendo modelos
        $this->cambiarLectura($idtarea);
        $this->load->model('categoriamodel');
        $this->load->model('tareamodel');
        $this->load->model('tareaestadomodel');
        $this->load->model('localmodel');
        $this->load->model('archivomodel');
        $msg_error = "";
        $aCategorias = $this->categoriamodel->getdata(array("idcategoria", "nombre"), "estado_registro = 1");
        $aLocales = array();
        if ($this->_rol != 1) {
            $aLocales = $this->localmodel->getdata(array("idlocal", "nombre"), "estado_registro = 1");
        }
        // Trayendo datos
        $auxObjTarea = array();
        $aArchivosTarea = array();
        $objTarea = array();
        $auxObjTarea = $this->tareamodel->getListaTareas("t.estado_registro = 1 and t.idtarea = " . intval($idtarea), 1);
        if ($auxObjTarea != false) {
            if ($this->_rol == 1) {
                if ($auxObjTarea->ultimoestado == 1 || $auxObjTarea->ultimoestado == 3) {
                    $objTarea["idtarea"] = $auxObjTarea->idtarea;
                    $objTarea["codigo"] = $auxObjTarea->codigo;
                    $objTarea["nombre"] = $auxObjTarea->nombre;
                    $objTarea["prioridad"] = $auxObjTarea->prioridad;
                    $objTarea["presupuesto"] = $auxObjTarea->presupuesto;
                    $objTarea["proveedor"] = $auxObjTarea->proveedor;
                    $objTarea["idlocal"] = $auxObjTarea->idlocal;
                    $objTarea["descripcion"] = $auxObjTarea->descripcion;
                    $objTarea["idcategoria"] = $auxObjTarea->idcategoria;
                    $objTarea["paraadmin"] = $auxObjTarea->paraadmin;
                    $aArchivosTarea = $this->archivomodel->getArchivosTarea($idtarea);
                } else {
                    $msg_error.="No puede editar, tarea ya atendida";
                }
            } else {
                $objTarea["paraadmin"] = $auxObjTarea->paraadmin;
                $objTarea["idtarea"] = $auxObjTarea->idtarea;
                $objTarea["codigo"] = $auxObjTarea->codigo;
                $objTarea["nombre"] = $auxObjTarea->nombre;
                $objTarea["prioridad"] = $auxObjTarea->prioridad;
                $objTarea["presupuesto"] = $auxObjTarea->presupuesto;
                $objTarea["proveedor"] = $auxObjTarea->proveedor;
                $objTarea["idlocal"] = $auxObjTarea->idlocal;
                $objTarea["descripcion"] = $auxObjTarea->descripcion;
                $objTarea["idcategoria"] = $auxObjTarea->idcategoria;
                $aArchivosTarea = $this->archivomodel->getArchivosTarea($idtarea);
            }
        } else {
            $msg_error.="No existe el id de la tarea";
        }

        $aTareaEstados = array();
        if ($auxObjTarea->ultimoestado != 1) {
            // ACTUALIZANDO LA DATA TAREAESTADOS A ENVIAR
            $this->load->model('usuariomodel');
            $objUsuario = $this->usuariomodel->getUsuarios();
            $aEstados = getEstados();

            $auxTareaEstados = $this->tareaestadomodel->getTareaEstados($idtarea);
            if ($auxTareaEstados != false) {
                $ultimaFecha = $auxObjTarea->fecha_registro;
                foreach ($auxTareaEstados as $itemTareaEstado) {
                    array_push($aTareaEstados, array("nomestado" => $aEstados[$itemTareaEstado->idestado][$itemTareaEstado->evaluacion]["gerundio"],
                        "comentario" => $itemTareaEstado->comentario,
                        "presupuesto" => $itemTareaEstado->presupuesto,
                        "idtarea_estado" => $itemTareaEstado->idtarea_estado,
                        "aRespuestas" => $this->respuestasmodel->getdata(array("*"), "idtarea_estado = " . $itemTareaEstado->idtarea_estado),
                        "nomusuario" => $objUsuario[$itemTareaEstado->idusuario]["nombre"],
                        "fregistro" => getFechaEs($itemTareaEstado->fecha_registro),
                        "transcurrido" => getTiempoTranscurrido($ultimaFecha, $itemTareaEstado->fecha_registro),
                        "imagen" => $objUsuario[$itemTareaEstado->idusuario]["foto"],
                        "archivos" => $itemTareaEstado->archivos
                    ));

                    $ultimaFecha = $itemTareaEstado->fecha_registro;
                }
            }
        }



        // Trayendo los archivos de la tarea
        $htmlMensaje = "";
        if ($msg_error != "") {
            $htmlMensaje = getHtmlMensaje(2, $msg_error);
        }
        $this->load->model('usuariomodel');
        $objUsuario = $this->usuariomodel->getUsuarios();
        $data['oUsuario'] = $objUsuario;
        $data['titulo'] = "Editando tarea";
        $data['divmensaje'] = $htmlMensaje;
        $data['objTarea'] = $objTarea;
        $data['aArchivosTarea'] = $aArchivosTarea;
        $data['aCategorias'] = $aCategorias;
        $data['aLocales'] = $aLocales;
        $data['aTareaEstados'] = $aTareaEstados;
        $this->template->write('botones', '<a href = "' . base_url() . 'tarea/lista" class = "btn btn-sm btn-default">Atras</a>', TRUE);
        $this->template->write('titleheader', "Editar tarea", TRUE);
        $this->template->write_view('content', 'tarea/editar', $data, TRUE);
        $this->template->render();
    }

    function corregirtarea($idtarea = 0) {
        validaSession("formulario");
        $this->cambiarLectura($idtarea);
// Trayendo modelos
        $this->load->model('categoriamodel');
        $this->load->model('tareamodel');
        $this->load->model('tareaestadomodel');
        $this->load->model('localmodel');
        $this->load->model('archivomodel');
        $msg_error = "";
        $aCategorias = $this->categoriamodel->getdata(array("idcategoria", "nombre"), "estado_registro = 1");
        $aLocales = array();
        if ($this->_rol != 1) {
            $aLocales = $this->localmodel->getdata(array("idlocal", "nombre"), "estado_registro = 1");
        }
// Trayendo datos
        $auxObjTarea = array();
        $aArchivosTarea = array();
        $objTarea = array();
        $auxObjTarea = $this->tareamodel->getListaTareas("t.estado_registro = 1 and t.idtarea = " . intval($idtarea), 1);
        if ($auxObjTarea != false) {
            if ($this->_rol == 1) {
                if ($auxObjTarea->ultimoestado == 1 || $auxObjTarea->ultimoestado == 3) {
                    $objTarea["idtarea"] = $auxObjTarea->idtarea;
                    $objTarea["codigo"] = $auxObjTarea->codigo;
                    $objTarea["paraadmin"] = $auxObjTarea->paraadmin;
                    $objTarea["nombre"] = $auxObjTarea->nombre;
                    $objTarea["prioridad"] = $auxObjTarea->prioridad;
                    $objTarea["presupuesto"] = $auxObjTarea->presupuesto;
                    $objTarea["proveedor"] = $auxObjTarea->proveedor;
                    $objTarea["idlocal"] = $auxObjTarea->idlocal;
                    $objTarea["descripcion"] = $auxObjTarea->descripcion;
                    $objTarea["idcategoria"] = $auxObjTarea->idcategoria;
                    $aArchivosTarea = $this->archivomodel->getArchivosTarea($idtarea);
                } else {
                    $msg_error.="No puede editar, tarea ya atendida";
                }
            } else {
                $objTarea["idtarea"] = $auxObjTarea->idtarea;
                $objTarea["codigo"] = $auxObjTarea->codigo;
                $objTarea["paraadmin"] = $auxObjTarea->paraadmin;
                $objTarea["nombre"] = $auxObjTarea->nombre;
                $objTarea["prioridad"] = $auxObjTarea->prioridad;
                $objTarea["presupuesto"] = $auxObjTarea->presupuesto;
                $objTarea["proveedor"] = $auxObjTarea->proveedor;
                $objTarea["idlocal"] = $auxObjTarea->idlocal;
                $objTarea["descripcion"] = $auxObjTarea->descripcion;
                $objTarea["idcategoria"] = $auxObjTarea->idcategoria;
                $aArchivosTarea = $this->archivomodel->getArchivosTarea($idtarea);
            }
        } else {
            $msg_error.="No existe el id de la tarea";
        }

        $aTareaEstados = array();
        if ($auxObjTarea->ultimoestado != 1) {
            // ACTUALIZANDO LA DATA TAREAESTADOS A ENVIAR
            $this->load->model('usuariomodel');
            $objUsuario = $this->usuariomodel->getUsuarios();
            $aEstados = getEstados();

            $auxTareaEstados = $this->tareaestadomodel->getTareaEstados($idtarea);
            if ($auxTareaEstados != false) {
                $ultimaFecha = $auxObjTarea->fecha_registro;
                foreach ($auxTareaEstados as $itemTareaEstado) {
                    array_push($aTareaEstados, array("nomestado" => $aEstados[$itemTareaEstado->idestado][$itemTareaEstado->evaluacion]["gerundio"],
                        "comentario" => $itemTareaEstado->comentario,
                        "presupuesto" => $itemTareaEstado->presupuesto,
                        "idtarea_estado" => $itemTareaEstado->idtarea_estado,
                        "aRespuestas" => $this->respuestasmodel->getdata(array("*"), "idtarea_estado = " . $itemTareaEstado->idtarea_estado),
                        "nomusuario" => $objUsuario[$itemTareaEstado->idusuario]["nombre"],
                        "fregistro" => getFechaEs($itemTareaEstado->fecha_registro),
                        "transcurrido" => getTiempoTranscurrido($ultimaFecha, $itemTareaEstado->fecha_registro),
                        "imagen" => $objUsuario[$itemTareaEstado->idusuario]["foto"],
                        "archivos" => $itemTareaEstado->archivos
                    ));

                    $ultimaFecha = $itemTareaEstado->fecha_registro;
                }
            }
        }



        // Trayendo los archivos de la tarea
        $htmlMensaje = "";
        if ($msg_error != "") {
            $htmlMensaje = getHtmlMensaje(2, $msg_error);
        }
        $this->load->model('usuariomodel');
        $objUsuario = $this->usuariomodel->getUsuarios();
        $data['oUsuario'] = $objUsuario;
        $data['titulo'] = "Editando tarea";
        $data['divmensaje'] = $htmlMensaje;
        $data['objTarea'] = $objTarea;
        $data['aArchivosTarea'] = $aArchivosTarea;
        $data['aCategorias'] = $aCategorias;
        $data['aLocales'] = $aLocales;
        $data['aTareaEstados'] = $aTareaEstados;
        $this->template->write('botones', '<a href = "' . base_url() . 'tarea/lista" class = "btn btn-sm btn-default">Atras</a>', TRUE);
        $this->template->write('titleheader', "Editar tarea", TRUE);
        $this->template->write_view('content', 'tarea/corregirtarea', $data, TRUE);
        $this->template->render();
    }

    function listaprogramadas() {
        validaSession("formulario");
        // Trayendo modelos
        $this->load->model('usuariomodel');
        $this->load->model('categoriamodel');
        $this->load->model('tareamodel');
        $this->load->model('programacionmodel');
        $this->load->model('localmodel');
        $this->load->model('archivomodel');
        $msg_error = "";
        $auxListaTareaProg = $this->programacionmodel->getProgramacion();
        $aUsuarios = $this->usuariomodel->getUsuarios();
        $aLocales = $this->localmodel->getLocales();
        $aCateUsuario = $this->categoriamodel->getaCateUsuario();

        $aListaTareas = array();
        if ($auxListaTareaProg != false) {
            foreach ($auxListaTareaProg as $itemTarea) {
                // Saber si se envió al administrados o al gestor
                $responsable = $aUsuarios[$itemTarea->idusuario_rol_creador]["nombre"];
                $datetime1 = date_create($itemTarea->fecha_ultima_creada);
                $datetime2 = date_create(date("Y-m-d H:i:s"));
                $fecha = date_diff($datetime1, $datetime2);
                $tiempoTranscurrido = "";
                if ($fecha->y > 0) {
                    $tiempoTranscurrido = $fecha->y . " A ";
                }
                if ($fecha->m > 0) {
                    $tiempoTranscurrido = $fecha->m . " M ";
                }
                if ($fecha->d > 0) {
                    $tiempoTranscurrido = $fecha->d . " d ";
                }
                if ($fecha->h > 0) {
                    $tiempoTranscurrido = $fecha->h . "h ";
                }
                if ($fecha->i > 0) {
                    $tiempoTranscurrido = $fecha->i . "m ";
                }
                $fechaUltimaCreada = "No ejecutada";
                if ($itemTarea->fecha_ultima_creada != "") {
                    $fechaUltimaCreada = getFechaEs2($itemTarea->fecha_ultima_creada);
                }
                //*******************************

                array_push($aListaTareas, array("idprogramacion" => $itemTarea->idprogramacion,
                    "codigo" => $itemTarea->codigo,
                    "repeticion" => $itemTarea->repeticion,
                    "fecha_siguiente" => getFechaEs2($itemTarea->fecha_siguiente),
                    "lunes" => $itemTarea->lunes,
                    "martes" => $itemTarea->martes,
                    "estado_registro" => $itemTarea->estado_registro,
                    "miercoles" => $itemTarea->miercoles,
                    "jueves" => $itemTarea->jueves,
                    "viernes" => $itemTarea->viernes,
                    "sabado" => $itemTarea->sabado,
                    "domingo" => $itemTarea->domingo,
                    "proveedor" => $itemTarea->proveedor,
                    "presupuesto" => $itemTarea->presupuesto,
                    "nombre" => $itemTarea->nombre,
                    "nomlocal" => $aLocales[$itemTarea->idlocal]["nombre"],
                    "nomcategoria" => $aCateUsuario[$itemTarea->idcategoria]["nomcategoria"],
                    "nomresponsable" => $responsable,
                    "finicio" => getFechaEs2($itemTarea->fecha_inicio),
                    "ffin" => getFechaEs2($itemTarea->fecha_fin),
                    "fultimaejecucion" => $fechaUltimaCreada,
                    "tipo_transcurrido" => $tiempoTranscurrido,
                    "enlace" => "<a href='" . base_url() . "tarea/editarprog/" . $itemTarea->idprogramacion . "'>Editar</a>"
                ));
            }
        }
        $data['titulo'] = utf8_decode("Programación de tareas (Preventivas)");
        $data['aProgramacion'] = $aListaTareas;
        $botones = ' <a href = "' . base_url() . 'tarea/lista" class = "btn btn-sm btn-default">Atras</a>';
        $botones .= ' <a href = "' . base_url() . 'tarea/programar" class = "btn btn-sm btn-default">Nuevo</a>';
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Programación de tareas", TRUE);
        $this->template->write_view('content', 'tarea/listaprogramadas', $data, TRUE);
        $this->template->render();
    }

    function editarprog($idprogramacion = 0) {
        validaSession("formulario");
        // Trayendo modelos
        $this->load->model('categoriamodel');
        $this->load->model('programacionmodel');
        $this->load->model('tareaprogmodel');
        $this->load->model('archivomodel');
        $this->load->model('localmodel');
        $msg_error = "";
        $aCategorias = $this->categoriamodel->getdata(array("idcategoria", "nombre"), "estado_registro = 1");
        $aLocales = $this->localmodel->getdata(array("idlocal", "nombre"), "estado_registro = 1");
        $aProgramacion = $this->programacionmodel->getdata(array("*"), "idprogramacion = " . $idprogramacion, 1);
        $aTareaProg = $this->tareaprogmodel->getdata(array("*"), "idtareaprog = " . $aProgramacion->idtareaprog, 1);
        $aArchivos = $this->archivomodel->getdata(array("*"), "idprogramacion = " . $aProgramacion->idprogramacion);

        $data['titulo'] = "Editar programacion";
        $data['aProgramacion'] = $aProgramacion;
        $data['aTareaProg'] = $aTareaProg;
        $data['aCategorias'] = $aCategorias;
        $data['aArchivos'] = $aArchivos;
        $data['aLocales'] = $aLocales;
        $this->template->write('botones', '<a href = "' . base_url() . 'tarea/listaprogramadas" class = "btn btn-sm btn-default">Atras</a>', TRUE);
        $this->template->write('titleheader', "Editar programacion", TRUE);
        $this->template->write_view('content', 'tarea/editarprog', $data, TRUE);
        $this->template->render();
    }

    function programar() {
        validaSession("formulario");
        // Trayendo modelos
        $this->load->model('categoriamodel');
        $this->load->model('localmodel');
        $msg_error = "";
        $aCategorias = $this->categoriamodel->getdata(array("idcategoria", "nombre"), "estado_registro = 1");
        $aLocales = $this->localmodel->getdata(array("idlocal", "nombre"), "estado_registro = 1");
        $data['titulo'] = "Programar tarea";
        $data['aCategorias'] = $aCategorias;
        $data['aLocales'] = $aLocales;
        $this->template->write('botones', '<a href = "' . base_url() . 'tarea/listaprogramadas" class = "btn btn-sm btn-default">Atras</a>', TRUE);
        $this->template->write('titleheader', "Programar tarea", TRUE);
        $this->template->write_view('content', 'tarea/programar', $data, TRUE);
        $this->template->render();
    }

    function ver($idtarea = 0) {
        validaSession("formulario");
        $this->cambiarLectura($idtarea);
        // Trayendo modelos
        $this->load->model('categoriamodel');
        $this->load->model('tareamodel');
        $this->load->model('tareaestadomodel');
        $this->load->model('localmodel');
        $this->load->model('archivomodel');
        $this->load->model('respuestasmodel');

        // Trayendo datos

        $auxObjTarea = $this->tareamodel->getListaTareas("t.estado_registro = 1 and t.idtarea = " . $idtarea . "", 1);
        //var_dump($auxObjTarea);exit;
        $aEstados = getEstados();
        $aCateUsuario = $this->categoriamodel->getaCateUsuario();

        // ACTUALIZANDO LA DATA TAREAESTADOS A ENVIAR
        $this->load->model('usuariomodel');
        $objUsuario = $this->usuariomodel->getUsuarios();

        $aTareaEstados = array();
        $auxTareaEstados = $this->tareaestadomodel->getTareaEstados($idtarea);
        //var_dump($auxTareaEstados);exit;
        if ($auxTareaEstados != false) {
            $ultimaFecha = $auxObjTarea->fecha_registro;
            foreach ($auxTareaEstados as $itemTareaEstado) {
                array_push($aTareaEstados, array("nomestado" => $aEstados[$itemTareaEstado->idestado][$itemTareaEstado->evaluacion]["gerundio"],
                    "comentario" => $itemTareaEstado->comentario,
                    "nomusuario" => $objUsuario[$itemTareaEstado->idusuario]["nombre"],
                    "presupuesto" => $itemTareaEstado->presupuesto,
                    "idtarea_estado" => $itemTareaEstado->idtarea_estado,
                    "aRespuestas" => $this->respuestasmodel->getdata(array("*"), "idtarea_estado = " . $itemTareaEstado->idtarea_estado),
                    "fregistro" => getFechaEs($itemTareaEstado->fecha_registro),
                    "transcurrido" => getTiempoTranscurrido($ultimaFecha, $itemTareaEstado->fecha_registro),
                    "imagen" => $objUsuario[$itemTareaEstado->idusuario]["foto"],
                    "archivos" => $itemTareaEstado->archivos
                ));
                $ultimaFecha = $itemTareaEstado->fecha_registro;
            }
        }

        if ($auxObjTarea != false) {
            // Saber si se envió al administrados o al gestor
            $responsable = $aCateUsuario[$auxObjTarea->idcategoria]["nomresponsable"];
            if (intval($auxObjTarea->paraadmin) == 1) {
                $responsable = "Administrador";
            }

            //*******************************
            $objTarea["idtarea"] = $auxObjTarea->idtarea;
            $objTarea["codigo"] = $auxObjTarea->codigo;
            $objTarea["nombre"] = $auxObjTarea->nombre;
            $objTarea["nomlocal"] = $auxObjTarea->nomlocal;
            $objTarea["presupuesto"] = $auxObjTarea->presupuesto;
            $objTarea["proveedor"] = $auxObjTarea->proveedor;
            $objTarea["descripcion"] = $auxObjTarea->descripcion;
            $objTarea["nomestado"] = $aEstados[$auxObjTarea->ultimoestado][0]["nombre"];
            $objTarea["nomcategoria"] = $aCateUsuario[$auxObjTarea->idcategoria]["nomcategoria"];
            $objTarea["nomresponsable"] = $responsable;
            $objTarea["fregistro"] = getFechaEs($auxObjTarea->fecha_registro);
            $objTarea["factualizacion"] = getFechaEs($auxObjTarea->fecha_actualizacion);
            $objTarea["tipo_transcurrido"] = getTiempoTranscurrido($auxObjTarea->fecha_registro, date("Y-m-d H:i:s"));
            $objTarea["imagen"] = $aEstados[$auxObjTarea->ultimoestado][0]["imagen"];
            $objTarea["enlace"] = getEnlace($auxObjTarea->ultimoestado, $auxObjTarea->idtarea, $auxObjTarea->paraadmin, $auxObjTarea->idusuario_rol_creador);
        }

        // Trayendo los archivos de la tarea
        $aArchivosTarea = array();
        $aArchivosTarea = $this->archivomodel->getArchivosTarea($idtarea);
        $oUsuario = $this->usuariomodel->getUsuarios();
        $data['oUsuario'] = $oUsuario;
        $data['titulo'] = "Actividades de la tarea";
        $data['divmensaje'] = "";
        $data['objTarea'] = $objTarea;
        $data['aArchivosTarea'] = $aArchivosTarea;
        $data['aTareaEstados'] = $aTareaEstados;
        $this->template->write('botones', '<a href = "' . base_url() . 'tarea/lista" class = "btn btn-sm btn-default" >Atras</a>', TRUE);
        $this->template->write('titleheader', "Actividades de la tarea", TRUE);
        $this->template->write_view('content', 'tarea/ver', $data, TRUE);
        $this->template->render();
    }

    function json_responder() {
        $this->load->model('usuariomodel');
        $this->load->model('tareamodel');
        $this->load->model('tareaestadomodel');
        $this->load->model('respuestasmodel');
        $oUsuario = $this->usuariomodel->getUsuarios();
        $idtarea_estado = $this->input->post("id");

        $objTareaEstado = $this->tareaestadomodel->getdata(array("idtarea", "idusuario"), "idtarea_estado = " . $idtarea_estado, 1);
        $auxObjTarea = $this->tareamodel->getdata(array("*"), "idtarea = " . $objTareaEstado->idtarea, 1);

        $descripcion = $this->input->post("descripcion");
        $auxUsuario = array();
        $auxUsuario = array(
            "idtarea_estado" => $idtarea_estado,
            "idusuario_creador" => $this->_idusuario_actual,
            "texto" => $descripcion
        );
        $idrespuesta = 0;
        $idrespuesta = $this->respuestasmodel->guardar($auxUsuario);
        if ($idrespuesta > 0) {
            $this->load->model('usuariomodel');
            $objUsuario = $this->usuariomodel->getUsuarios();
            $idusuarioEnviar = $objTareaEstado->idusuario;
            if ($objTareaEstado->idusuario == $this->_idusuario_actual) {
                $idusuarioEnviar = $objTareaEstado->idusuario;
            }
            if ($this->_rol == 3) {
                enviarEmail(array(
                    "FROM" => "Hikari- Notificaciones",
                    "FROMNAME" => "sistemas@hikari.pe",
                    "asunto" => "Actualización de la tarea - " . $auxObjTarea->codigo,
                    "mensaje" => 'Se acaba de escribir un comentario en la tarea "' . $auxObjTarea->nombre . '" con código "' . $auxObjTarea->codigo . '"',
                    "titulo" => "De: " . $objUsuario[$this->_idusuario_actual]["nombre"] . "<br>Para: " . $objUsuario[$auxObjTarea->idusuario_rol_ejecutor]["nombre"],
                    "correo" => $objUsuario[$auxObjTarea->idusuario_rol_ejecutor]["correo"]
                ));
                $this->registrarLectura($idtarea_estado, $auxObjTarea->idusuario_rol_ejecutor, 'La tarea "' . $auxObjTarea->nombre . '" con código "' . $auxObjTarea->codigo . '" acaba de recibir un comentario');
                enviarEmail(array(
                    "FROM" => "Hikari- Notificaciones",
                    "FROMNAME" => "sistemas@hikari.pe",
                    "asunto" => "Actualización de la tarea - " . $auxObjTarea->codigo,
                    "mensaje" => 'Se acaba de escribir un comentario en la tarea "' . $auxObjTarea->nombre . '" con código "' . $auxObjTarea->codigo . '"',
                    "titulo" => "De: " . $objUsuario[$this->_idusuario_actual]["nombre"] . "<br>Para: " . $objUsuario[$auxObjTarea->idusuario_rol_creador]["nombre"],
                    "correo" => $objUsuario[$auxObjTarea->idusuario_rol_creador]["correo"]
                ));
                $this->registrarLectura($idtarea_estado, $auxObjTarea->idusuario_rol_creador, 'La tarea "' . $auxObjTarea->nombre . '" con código "' . $auxObjTarea->codigo . '" acaba de recibir un comentario');
            } else {
                enviarEmail(array(
                    "FROM" => "Hikari- Notificaciones",
                    "FROMNAME" => "sistemas@hikari.pe",
                    "asunto" => "Actualización de la tarea - " . $auxObjTarea->codigo,
                    "mensaje" => 'La tarea "' . $auxObjTarea->nombre . '" con código "' . $auxObjTarea->codigo . '" se acaba de editar',
                    "titulo" => "De: " . $objUsuario[$this->_idusuario_actual]["nombre"] . "<br>Para: " . $objUsuario[$auxObjTarea->idusuario_rol_ejecutor]["nombre"],
                    "correo" => $objUsuario[$objTareaEstado->idusuario]["correo"]
                ));
                $this->registrarLectura($idtarea_estado, $auxObjTarea->idusuario, 'La tarea "' . $auxObjTarea->nombre . '" con código "' . $auxObjTarea->codigo . '" acaba de recibir un comentario');
            }
            //$this->registrarLectura($idTareaEstado,$objTareaEstado->idusuario);
             $this->NotiRespuestas($objUsuario, $auxObjTarea, $idtarea_estado, 'La tarea "' . $auxObjTarea->nombre . '" con código "' . $auxObjTarea->codigo . '" se acaba de editar');
            echo '<div class="responder clearifx">
                                        <span class="spn-avatar pull-left media-object" href="#">
                                            <i class="glyphicon glyphicon-user"></i>
                                        </span>
                                        <div class="media-body">
                                            <b>' . $oUsuario[$this->_idusuario_actual]["nombre"] . '</b>
                                            <p>' . $descripcion . '</p>
                                        </div>    
                 </div>';
        } else {
            echo "ERROR CONEXIÓN";
        }
    }

    function movertarea($idtarea = 0) {
        $this->load->model('tareamodel');
        $data = array();
        $auxObjTarea = $this->tareamodel->getdata(array("*"), "idtarea = " . $idtarea, 1);
        $data['titulo'] = "Cambiar estado";
        $data['idtarea'] = $idtarea;
        $data['objTarea'] = $auxObjTarea;
        $data['idestado'] = $auxObjTarea->ultimoestado;
        $this->template->write('botones', '<a href = "' . base_url() . 'tarea/lista" class = "btn btn-sm btn-default" >Atras</a>', TRUE);
        $this->template->write('titleheader', "Cambiar de estado", TRUE);
        $this->template->write_view('content', 'tarea/movertarea', $data, TRUE);
        $this->template->render();
    }

    function json_cambiaestado() {
        $this->load->model('tareamodel');
        $this->load->model('tareaestadomodel');
        $this->load->model('usuariomodel');

        $idtarea = $this->input->post("idtarea");
        $idestado = $this->input->post("idestado");

        $auxObjTarea = $this->tareamodel->getdata(array("*"), "idtarea = " . $idtarea, 1);

        // Cambiar de estado la tarea
        $objTarea = array();
        $objTarea = array(
            "ultimoestado" => $idestado
        );
        $this->tareamodel->editar($idtarea, $objTarea);

        // Guardar bitacora
        $oEstados = getEstados();
        $mensaje = 'El director cambió de estado la tarea "' . $auxObjTarea->nombre . '" de "' . $oEstados[$auxObjTarea->ultimoestado][0]["nombre"] . '" a "' . $oEstados[$idestado][0]["nombre"] . '"';

        $objTareaEstado = array();
        $objTareaEstado = array("idusuario" => $this->_idusuario_actual,
            "idtarea" => $idtarea,
            "idestado" => $idestado,
            "comentario" => $mensaje
        );
        $idTareaEstado = $this->tareaestadomodel->guardar($objTareaEstado);
        if ($idTareaEstado > 0) {
            $this->session->set_userdata('mensaje', "La tarea se cambió de estado");
            $objUsuario = $this->usuariomodel->getUsuarios();
            enviarEmail(array(
                "FROM" => "Hikari- Notificaciones",
                "FROMNAME" => "sistemas@hikari.pe",
                "asunto" => "Actualización de la tarea - " . $auxObjTarea->codigo,
                "mensaje" => $mensaje,
                "titulo" => "De: " . $objUsuario[$this->_idusuario_actual]["nombre"] . "<br>Para: " . $objUsuario[$auxObjTarea->idusuario_rol_creador]["nombre"],
                "correo" => $objUsuario[$auxObjTarea->idusuario_rol_creador]["correo"]
            ));
            $this->registrarLectura($idTareaEstado, $auxObjTarea->idusuario_rol_creador, $mensaje);
            enviarEmail(array(
                "FROM" => "Hikari- Notificaciones",
                "FROMNAME" => "sistemas@hikari.pe",
                "asunto" => "Actualización de la tarea - " . $auxObjTarea->codigo,
                "mensaje" => $mensaje,
                "titulo" => "De: " . $objUsuario[$this->_idusuario_actual]["nombre"] . "<br>Para: " . $objUsuario[$auxObjTarea->idusuario_rol_ejecutor]["nombre"],
                "correo" => $objUsuario[$auxObjTarea->idusuario_rol_ejecutor]["correo"]
            ));
            $this->registrarLectura($idTareaEstado, $auxObjTarea->idusuario_rol_ejecutor, $mensaje);
        }
        
        $this->NotiRespuestas($objUsuario, $auxObjTarea, $idTareaEstado, $mensaje);
        echo json_encode(array("msj" => "", "data" => array()));
    }

    function confirmar($idtarea = 0) {
        validaSession("formulario");
        $this->cambiarLectura($idtarea);
        // Trayendo modelos
        $this->load->model('categoriamodel');
        $this->load->model('tareamodel');
        $this->load->model('tareaestadomodel');
        $this->load->model('localmodel');
        $this->load->model('archivomodel');
        $aArchivosTarea = array();
        $aTareaEstados = array();
        $objTarea = array();
        $divmensaje = "";
        // Trayendo datos
        $auxObjTarea = $this->tareamodel->getListaTareas("t.estado_registro = 1 and t.idtarea = " . $idtarea, 1);
        if ($auxObjTarea != FALSE) {
            if ($auxObjTarea->ultimoestado == 4) {
                $aEstados = getEstados();
                $aCateUsuario = $this->categoriamodel->getaCateUsuario();

                // ACTUALIZANDO LA DATA TAREAESTADOS A ENVIAR
                $this->load->model('usuariomodel');
                $objUsuario = $this->usuariomodel->getUsuarios();
                $aTareaEstados = array();
                $auxTareaEstados = $this->tareaestadomodel->getTareaEstados($idtarea);
                if ($auxTareaEstados != false) {
                    $ultimaFecha = $auxObjTarea->fecha_registro;
                    foreach ($auxTareaEstados as $itemTareaEstado) {
                        array_push($aTareaEstados, array("nomestado" => $aEstados[$itemTareaEstado->idestado][$itemTareaEstado->evaluacion]["gerundio"],
                            "comentario" => $itemTareaEstado->comentario,
                            "presupuesto" => $itemTareaEstado->presupuesto,
                            "idtarea_estado" => $itemTareaEstado->idtarea_estado,
                            "aRespuestas" => $this->respuestasmodel->getdata(array("*"), "idtarea_estado = " . $itemTareaEstado->idtarea_estado),
                            "nomusuario" => $objUsuario[$itemTareaEstado->idusuario]["nombre"],
                            "fregistro" => getFechaEs($itemTareaEstado->fecha_registro),
                            "transcurrido" => getTiempoTranscurrido($ultimaFecha, $itemTareaEstado->fecha_registro),
                            "imagen" => $objUsuario[$itemTareaEstado->idusuario]["foto"],
                            "archivos" => $itemTareaEstado->archivos
                        ));
                        $ultimaFecha = $itemTareaEstado->fecha_registro;
                    }
                }

                if ($auxObjTarea != false) {
// Saber si se envió al administrados o al gestor
                    $responsable = $aCateUsuario[$auxObjTarea->idcategoria]["nomresponsable"];
                    if (intval($auxObjTarea->paraadmin) == 1) {
                        $responsable = "Administrador";
                    }

                    $datetime1 = date_create($auxObjTarea->fecha_registro);
                    $datetime2 = date_create(date("Y-m-d H:i:s"));
                    $fecha = date_diff($datetime1, $datetime2);
                    $tiempoTranscurrido = "";
                    if ($fecha->y > 0) {
                        $tiempoTranscurrido = $fecha->y . " A ";
                    }
                    if ($fecha->m > 0) {
                        $tiempoTranscurrido = $fecha->m . " M ";
                    }
                    if ($fecha->d > 0) {
                        $tiempoTranscurrido = $fecha->d . " d ";
                    }
                    if ($fecha->h > 0) {
                        $tiempoTranscurrido = $fecha->h . "h ";
                    }
                    if ($fecha->i > 0) {
                        $tiempoTranscurrido = $fecha->i . "m ";
                    }
//*******************************
                    $objTarea["idtarea"] = $auxObjTarea->idtarea;
                    $objTarea["codigo"] = $auxObjTarea->codigo;
                    $objTarea["nombre"] = $auxObjTarea->nombre;
                    $objTarea["presupuesto"] = $auxObjTarea->presupuesto;
                    $objTarea["proveedor"] = $auxObjTarea->proveedor;
                    $objTarea["nomlocal"] = $auxObjTarea->nomlocal;
                    $objTarea["descripcion"] = $auxObjTarea->descripcion;
                    $objTarea["nomestado"] = $aEstados[$auxObjTarea->ultimoestado][0]["nombre"];
                    $objTarea["nomcategoria"] = $aCateUsuario[$auxObjTarea->idcategoria]["nomcategoria"];
                    $objTarea["nomresponsable"] = $responsable;
                    $objTarea["fregistro"] = getFechaEs($auxObjTarea->fecha_registro);
                    $objTarea["factualizacion"] = getFechaEs($auxObjTarea->fecha_actualizacion);
                    $objTarea["tipo_transcurrido"] = $tiempoTranscurrido;
                    $objTarea["imagen"] = $aEstados[$auxObjTarea->ultimoestado][0]["imagen"];
                    $objTarea["enlace"] = getEnlace($auxObjTarea->ultimoestado, $auxObjTarea->idtarea, $auxObjTarea->paraadmin, $auxObjTarea->idusuario_rol_creador);
                }

// Trayendo los archivos de la tarea
                $aArchivosTarea = array();
                $aArchivosTarea = $this->archivomodel->getArchivosTarea($idtarea);
            } else {
                $divmensaje.="Ya se realizó esta actividad";
            }
        } else {
            $divmensaje.="No existe la tarea";
        }

        $this->load->model('usuariomodel');
        $objUsuario = $this->usuariomodel->getUsuarios();
        $data['oUsuario'] = $objUsuario;
        $data['titulo'] = "Por confirmar";
        $data['divmensaje'] = $divmensaje;
        $data['objTarea'] = $objTarea;
        $data['aTareaEstados'] = $aTareaEstados;
        $data['aArchivosTarea'] = $aArchivosTarea;
        $this->template->write('botones', '<a href = "' . base_url() . 'tarea/lista" class = "btn btn-sm btn-default" >Atras</a>', TRUE);
        $this->template->write('titleheader', "Por confirmar tarea", TRUE);
        $this->template->write_view('content', 'tarea/confirmar', $data, TRUE);
        $this->template->render();
    }

    public function json_buscar() {
        $oLogin = validaSession("json");
        if ($oLogin["msj"] != "") {
            echo json_encode($oLogin);
            exit;
        }
        $this->load->model('tareamodel');
        $this->load->model('categoriamodel');
        $this->load->model('usuariomodel');

        $idlocal = $this->input->post("idlocal");
        $finicio = $this->input->post("finicio");
        $idestado = $this->input->post("idestado");
        $ffin = $this->input->post("ffin");
        $montoinicial = $this->input->post("montoinicial");
        $montofinal = $this->input->post("montofinal");
        $montoigual = $this->input->post("montoigual");
        $idusuario = $this->input->post("idusuario");

        $this->session->set_userdata('filtro_local', $idlocal);
        $this->session->set_userdata('filtro_fecha_inicio', $finicio);
        $this->session->set_userdata('filtro_fecha_fin', $ffin);
        $this->session->set_userdata('filtro_estados', $idestado);
        $this->session->set_userdata('filtro_monto_inicial', $montoinicial);
        $this->session->set_userdata('filtro_monto_final', $montofinal);
        $this->session->set_userdata('filtro_monto_igual', $montoigual);
        $this->session->set_userdata('filtro_monto_ejecutor', $idusuario);

        $where = "t.estado_registro = 1";
        if ($idusuario != "") {
            if ($this->_rol == 3) {
                $where.=" AND (t.idusuario_rol_ejecutor = " . $idusuario . " OR t.idusuario_rol_creador = " . $idusuario . " OR t.idusuario_creador = " . $idusuario . ")";
            } else {
                $where.=" AND t.idusuario_rol_ejecutor = " . $idusuario;
            }
        }
        if ($this->_rol != 1) {
            if ($idlocal != "") {
                $where.=" AND t.idlocal = " . $idlocal;
            }
        }

        if ($idestado != "") {
            $where.=" AND t.ultimoestado = " . $idestado;
        }
        if ($montoigual != "") {
            $where.=" AND t.presupuesto = " . doubleval($montoigual);
        }
        if ($montoinicial != "" && $montofinal != "") {
            $where.=" AND t.presupuesto BETWEEN " . doubleval($montoinicial) . " AND " . doubleval($montofinal) . " ";
        }
        if ($finicio != "" && $ffin != "") {
            $where.=" AND date(t.fecha_registro) BETWEEN '" . getFechaEn2($finicio) . "' AND '" . getFechaEn2($ffin) . "' ";
        }

        // Trayendo datos
        //$aCategorias = $this->categoriamodel->getdata(array("idcategoria", "nombre"), "estado_registro = 1");
        $aCateUsuario = $this->categoriamodel->getaCateUsuario();
        $auxListaTarea = array();
        //echo $this->_rol ;exit;
        if ($this->_rol == 3) {
            $auxListaTarea = $this->tareamodel->getListaTareas($where);
        } elseif ($this->_rol == 1) {
            //$where.=" AND t.idlocal = " . $this->_idlocal;
            $auxListaTarea = $this->tareamodel->getListaTareas("t.estado_registro = 1 AND t.idlocal = " . $this->_idlocal . "");
        } else {
            //$where.=" AND (t.idusuario_rol_ejecutor = " . $this->_idusuario_actual . " or t.idusuario_rol_creador = " . $this->_idusuario_actual . ") ";
            $auxListaTarea = $this->tareamodel->getListaTareas($where);
        }

        $aListaTareas = array();
        $aEstados = getEstados();
        if ($auxListaTarea != false) {
            foreach ($auxListaTarea as $itemTarea) {
// Saber si se envió al administrados o al gestor
                $responsable = $aCateUsuario[$itemTarea->idcategoria]["nomresponsable"];
                if (intval($itemTarea->paraadmin) == 1) {
                    $responsable = "Administrador";
                }

                $datetime1 = date_create($itemTarea->fecha_registro);
                $datetime2 = date_create(date("Y-m-d H:i:s"));
                $fecha = date_diff($datetime1, $datetime2);
                $tiempoTranscurrido = "";
                if ($fecha->y > 0) {
                    $tiempoTranscurrido = $fecha->y . " A ";
                }
                if ($fecha->m > 0) {
                    $tiempoTranscurrido = $fecha->m . " M ";
                }
                if ($fecha->d > 0) {
                    $tiempoTranscurrido = $fecha->d . " d ";
                }
                if ($fecha->h > 0) {
                    $tiempoTranscurrido = $fecha->h . "h ";
                }
                if ($fecha->i > 0) {
                    $tiempoTranscurrido = $fecha->i . "m ";
                }
                //*******************************
                array_push($aListaTareas, array("idtarea" => $itemTarea->idtarea,
                    "codigo" => $itemTarea->codigo,
                    "proveedor" => $itemTarea->proveedor,
                    "presupuesto" => $itemTarea->presupuesto,
                    "idusuario_creador" => $itemTarea->idusuario_rol_creador,
                    "idusuario_ejecutor" => $itemTarea->idusuario_rol_ejecutor,
                    "nombre" => $itemTarea->nombre,
                    "idusuario" => $itemTarea->idusuario_creador,
                    "paraadmin" => $itemTarea->paraadmin,
                    "ultimoestado" => $itemTarea->ultimoestado,
                    "idtarea" => $itemTarea->idtarea,
                    "prioridad" => $itemTarea->prioridad,
                    "nomlocal" => $itemTarea->nomlocal,
                    "nomestado" => $aEstados[$itemTarea->ultimoestado][0]["nombre"],
                    "nomcategoria" => $aCateUsuario[$itemTarea->idcategoria]["nomcategoria"],
                    "nomresponsable" => $responsable,
                    "fregistro" => getFechaEs($itemTarea->fecha_registro),
                    "factualizacion" => getFechaEs($itemTarea->fecha_actualizacion),
                    "tipo_transcurrido" => getTiempoTranscurrido($itemTarea->fecha_actualizacion, date("Y-m-d H:i:s")),
                    "imagen" => $aEstados[$itemTarea->ultimoestado][0]["imagen"],
                    "enlace" => getEnlace($itemTarea->ultimoestado, $itemTarea->idtarea, $itemTarea->paraadmin, $itemTarea->idusuario_rol_creador)
                ));
            }
        }
        $oUsuario = $this->usuariomodel->getUsuarios();
        $data['oUsuario'] = $oUsuario;
        $data['titulo'] = "Lista de tareas";
        $data['aListaTareas'] = $aListaTareas;
        $data['origen'] = "buscar";

        $this->load->view("tarea/listabusqueda", $data);
    }

    public function json_corregir_tarea() {
        $oLogin = validaSession("json");
        if ($oLogin["msj"] != "") {
            echo json_encode($oLogin);
            exit;
        }
        $this->load->model('tareamodel');
        $this->load->model('tareaestadomodel');
        $this->load->model('categoriamodel');
        $this->load->model('archivomodel');
        $this->load->model('localmodel');

        $idtarea = $this->input->post("idtarea");
        $nombre = $this->input->post("nombre");
        $prioridad = $this->input->post("prioridad");
        $presupuesto = $this->input->post("presupuesto");
        $proveedor = $this->input->post("proveedor");
        $descripcion = $this->input->post("descripcion");
        $idcategoria = $this->input->post("idcategoria");
        $paraadmin = $this->input->post("admin");
        $idlocal = $this->input->post("idlocal");
        if ($idlocal == "") {
            $idlocal = $this->_idlocal;
        }
        $msg_error = "";
        // Validar si existe el id de tarea
        $auxObjTarea = $this->tareamodel->getdata(array("*"), "estado_registro = 1 and idtarea = " . $idtarea, 1);
        if ($auxObjTarea != FALSE) {
            //Procedencia de la edición
            $ejecuta = 0;
            if ($this->_rol == 1) {
                if ($auxObjTarea->ultimoestado == 3) {
                    $ejecuta = 1;
                }
            } else {
                $ejecuta = 1;
            }

            if ($ejecuta == 1) {
                $idestado = $auxObjTarea->ultimoestado;
                if ($idestado == 1 || $idestado == 3) {
                    $idestado = 1;
                }
                $aCateUsuario = $this->categoriamodel->getaCateUsuario();
                $idusuario_ejecutor = $aCateUsuario[$idcategoria] ["idusuario"];
                $idusuario_creador = $this->_idusuario_actual;
                if (intval($paraadmin) == 1) {
                    if ($this->_rol != 1) {
                        $objLocal = $this->localmodel->getdata(array("idusuario_responsable"), "idlocal = " . $idlocal, 1);
                        $idusuario_ejecutor = $objLocal->idusuario_responsable;
                        $idusuario_creador = $this->_idusuario_actual;
                    } else {
                        $idusuario_ejecutor = $aCateUsuario[$idcategoria]["idusuario"];
                        $idusuario_creador = $this->_idusuario_actual;
                    }
                } else {
                    if ($this->_rol != 1) {
                        $objLocal = $this->localmodel->getdata(array("idusuario_responsable"), "idlocal = " . $idlocal, 1);
                        $idusuario_creador = $objLocal->idusuario_responsable;
                    }
                }
                $archivos = array();
                if (!empty($_FILES)) {
                    $archivos = subirArchivos($_FILES);
                }
                //****** CAMBIOS DE EDICIÓN ***************
                $cambio = "";
                if ($nombre != $auxObjTarea->nombre) {
                    $cambio.="Se cambió el nombre de " . $auxObjTarea->nombre . " a " . $nombre . ". <br>";
                }
                if ($prioridad != $auxObjTarea->prioridad) {
                    $cambio.="Se cambió la prioridad de " . getNombrePrioridad($auxObjTarea->prioridad) . " a " . getNombrePrioridad($prioridad) . ". <br>";
                }
                if ($presupuesto != $auxObjTarea->presupuesto) {
                    $cambio.="Se cambió el presupuesto de " . $auxObjTarea->presupuesto . " a " . $presupuesto . ". <br>";
                }
                if ($proveedor != $auxObjTarea->proveedor) {
                    $cambio.="Se cambió el proveedor de " . $auxObjTarea->proveedor . " a " . $proveedor . ". <br>";
                }
                if ($descripcion != $auxObjTarea->descripcion) {
                    $cambio.="Se cambió la descripción de " . $auxObjTarea->descripcion . " a " . $descripcion . ".<br>";
                }
                if ($idcategoria != $auxObjTarea->idcategoria) {
                    $cambio.="Se cambió de categoría de " . $aCateUsuario[$auxObjTarea->idcategoria]["nomcategoria"] . " a " . $aCateUsuario[$idcategoria]["nomcategoria"] . ". <br>";
                }
                if ($paraadmin != $auxObjTarea->paraadmin) {
                    $nomParaAdminin = "para el administrador";
                    if ($paraadmin == 0) {
                        $nomParaAdminin = "para el gestor";
                    }
                    $cambio.="Se cambió el destino a \"" . $nomParaAdminin . "\"" . "<br>";
                }
                if ($idlocal != $auxObjTarea->idlocal) {
                    $cambio.="Se cambió el local a " . $idlocal . "\n";
                }


                //*****************************************
                // Creando la tarea
                $objTarea = array(
                    "nombre" => $nombre,
                    "descripcion" => $descripcion,
                    "idlocal" => $idlocal,
                    "presupuesto" => doubleval($presupuesto),
                    "proveedor" => $proveedor,
                    "idcategoria" => $idcategoria,
                    "prioridad" => $prioridad,
                    "paraadmin" => $paraadmin,
                    "idusuario_rol_creador" => $idusuario_creador,
                    "idusuario_rol_ejecutor" => $idusuario_ejecutor,
                    "ultimoestado" => $idestado,
                    "fecha_actualizacion" => $this->_fecha_actual,
                    "estado_registro" => 1
                );

                $this->tareamodel->editar($idtarea, $objTarea);
                if ($idtarea > 0) {
// Guardando su actividad
                    $objTareaEstado = array("idusuario" => $this->_idusuario_actual,
                        "idtarea" => $idtarea,
                        "presupuesto" => doubleval($presupuesto),
                        "idestado" => 1,
                        "comentario" => $cambio
                    );
                    $idTareaEstado = $this->tareaestadomodel->guardar($objTareaEstado);

                    if ($idTareaEstado > 0) {
                        foreach ($archivos as $archi) {
                            $objArchivo = array("idtarea_estado" => $idTareaEstado,
                                "url" => $archi["ruta"],
                                "nombre" => $archi["nombre"]
                            );
                            $numFilasArchivos = $this->archivomodel->guardar($objArchivo);
                            if ($numFilasArchivos < 1) {
                                $msg_error.= "Error al registrar archivo" . $idTareaEstado;
                            }
                        }
                    } else {
                        $msg_error.="No se registró la bitacora tarea-estado";
                    }
                } else {
                    $msg_error.="No se registró la tarea";
                }
            } else {
                if ($this->_rol == 1) {
                    $msg_error.="Ya se puede atendió la tarea";
                } else {
                    $msg_error.="No se puede editar";
                }
            }
        } else {
            $msg_error.="No existe la tarea";
        }

        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "La tarea fué editada");
            $this->load->model('usuariomodel');
            $objUsuario = $this->usuariomodel->getUsuarios();
            enviarEmail(array(
                "FROM" => "Hikari- Notificaciones",
                "FROMNAME" => "sistemas@hikari.pe",
                "asunto" => "Actualización de la tarea - " . $auxObjTarea->codigo,
                "mensaje" => 'La tarea "' . $auxObjTarea->nombre . '" con código "' . $auxObjTarea->codigo . '" se acaba de editar',
                "titulo" => "De: " . $objUsuario[$this->_idusuario_actual]["nombre"] . "<br>Para: " . $objUsuario[$auxObjTarea->idusuario_rol_ejecutor]["nombre"],
                "correo" => $objUsuario[$auxObjTarea->idusuario_rol_ejecutor]["correo"]
            ));
            $this->registrarLectura($idTareaEstado, $auxObjTarea->idusuario_rol_ejecutor, 'La tarea "' . $auxObjTarea->nombre . '" con código "' . $auxObjTarea->codigo . '" se acaba de editar');
        }
        
        $this->NotiRespuestas($objUsuario, $auxObjTarea, $idTareaEstado, 'La tarea "' . $auxObjTarea->nombre . '" con código "' . $auxObjTarea->codigo . '" se acaba de editar');
        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    public function json_corregir_ejecutar() {
        $oLogin = validaSession("json");
        if ($oLogin[
                "msj"] != "") {
            echo json_encode($oLogin);
            exit;
        }
        $this->load->model('tareamodel');
        $this->load->model('tareaestadomodel');
        $this->load->model('categoriamodel');
        $this->load->model('archivomodel');

        $accion = $this->input->post("accion");
        $descripcion = $this->input->post("descripcion");
        $idtarea = $this->input->post("idtarea");
        $proveedor = $this->input->post("proveedor");
        $presupuesto = $this->input->post("presupuesto");
        $msg_error = "";


// Valido que exista el idtarea enviado
        $auxObjTarea = $this->tareamodel->getdata(array("*"), "idtarea = " . $idtarea, 1);
        if ($auxObjTarea != false) {
            if ($auxObjTarea->ultimoestado == 6) {
                // Jugar con los estados
                // Ver que acción realizará (1= Al estado 2 y 0= Al estado 3)
                $estado = 4;

                if ($accion == 0) {
                    $estado = 3;
                }

// Creando una nueva actividad realizada
                $objTareaEstado = array("idusuario" => $this->_idusuario_actual,
                    "idtarea" => $idtarea,
                    "idestado" => $estado,
                    "presupuesto" => doubleval($presupuesto),
                    "comentario" => $descripcion
                );
                $idTareaEstado = $this->tareaestadomodel->guardar($objTareaEstado);
                if ($idTareaEstado > 0) {
// Actualizando el ultimo estado para esta tarea
                    $objTarea = array(
                        "ultimoestado" => $estado,
                        "presupuesto" => doubleval($presupuesto),
                        "fecha_actualizacion" => date("Y-m-d H:i:s"),
                        "proveedor" => $proveedor,
                    );
                    $this->tareamodel->editar($idtarea, $objTarea);

// Guardando los archivo de la actividad
                    $archivos = array();
                    if (!empty($_FILES)) {
                        $archivos = subirArchivos($_FILES);
                    }
                    foreach ($archivos as $archi) {
                        $objArchivo = array("idtarea_estado" => $idTareaEstado,
                            "url" => $archi["ruta"],
                            "nombre" => $archi["nombre"]
                        );
                        $numFilasArchivos = $this->archivomodel->guardar($objArchivo);
                        if ($numFilasArchivos < 1) {
                            $msg_error.= "Error al registrar archivo" . $idTareaEstado;
                        }
                    }
                } else {
                    $msg_error.="No se registro actividad";
                }
            } else {
                $msg_error.="Este estado ya se registró";
            }
        } else {
            $msg_error.="No existe esta tarea";
        }

        $this->load->model('usuariomodel');
        $objUsuario = $this->usuariomodel->getUsuarios();
        // $objUsuario[$idusuario_ejecutor]["correo"]
        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "La tarea fué ejecutada exitosamente");
            enviarEmail(array(
                "FROM" => "Hikari- Notificaciones",
                "FROMNAME" => "sistemas@hikari.pe",
                "asunto" => "Actualización de la tarea - " . $auxObjTarea->codigo,
                "mensaje" => 'La tarea "' . $auxObjTarea->nombre . '" con código "' . $auxObjTarea->codigo . '" se acaba de ejecutar',
                "titulo" => "De: " . $objUsuario[$this->_idusuario_actual]["nombre"] . "<br>Para: " . $objUsuario[$auxObjTarea->idusuario_rol_creador]["nombre"],
                "correo" => $objUsuario[$auxObjTarea->idusuario_rol_creador]["correo"]
            ));
            $this->registrarLectura($idTareaEstado, $auxObjTarea->idusuario_rol_creador, 'La tarea "' . $auxObjTarea->nombre . '" con código "' . $auxObjTarea->codigo . '" se acaba de ejecutar');
        }
        $this->NotiRespuestas($objUsuario, $auxObjTarea, $idTareaEstado, 'La tarea "' . $auxObjTarea->nombre . '" con código "' . $auxObjTarea->codigo . '" se acaba de ejecutar');
        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    public function json_editar() {
        $oLogin = validaSession("json");
        if ($oLogin["msj"] != "") {
            echo json_encode($oLogin);
            exit;
        }
        $this->load->model('tareamodel');
        $this->load->model('tareaestadomodel');
        $this->load->model('categoriamodel');
        $this->load->model('archivomodel');
        $this->load->model('localmodel');

        $idtarea = $this->input->post("idtarea");
        $nombre = $this->input->post("nombre");
        $prioridad = $this->input->post("prioridad");
        $presupuesto = $this->input->post("presupuesto");
        $proveedor = $this->input->post("proveedor");
        $descripcion = $this->input->post("descripcion");
        $idcategoria = $this->input->post("idcategoria");
        $paraadmin = $this->input->post("admin");
        $idlocal = $this->input->post("idlocal");
        if ($idlocal == "") {
            $idlocal = $this->_idlocal;
        }
        $msg_error = "";
        // Validar si existe el id de tarea
        $auxObjTarea = $this->tareamodel->getdata(array("*"), "estado_registro = 1 and idtarea = " . $idtarea, 1);
        if ($auxObjTarea != FALSE) {
            //Procedencia de la edición
            $ejecuta = 0;
            if ($this->_rol == 1) {
                if ($auxObjTarea->ultimoestado == 1) {
                    $ejecuta = 1;
                }
            } else {
                $ejecuta = 1;
            }

            if ($ejecuta == 1) {
                $idestado = $auxObjTarea->ultimoestado;
                if ($idestado == 1 || $idestado == 3) {
                    $idestado = 1;
                }
                $aCateUsuario = $this->categoriamodel->getaCateUsuario();
                $idusuario_ejecutor = $aCateUsuario[$idcategoria] ["idusuario"];
                $idusuario_creador = $this->_idusuario_actual;
                if (intval($paraadmin) == 1) {
                    if ($this->_rol != 1) {
                        $objLocal = $this->localmodel->getdata(array("idusuario_responsable"), "idlocal = " . $idlocal, 1);
                        $idusuario_ejecutor = $objLocal->idusuario_responsable;
                        $idusuario_creador = $this->_idusuario_actual;
                    } else {
                        $idusuario_ejecutor = $aCateUsuario[$idcategoria]["idusuario"];
                        $idusuario_creador = $this->_idusuario_actual;
                    }
                } else {
                    if ($this->_rol != 1) {
                        $objLocal = $this->localmodel->getdata(array("idusuario_responsable"), "idlocal = " . $idlocal, 1);
                        $idusuario_creador = $objLocal->idusuario_responsable;
                    }
                }
                $archivos = array();
                if (!empty($_FILES)) {
                    $archivos = subirArchivos($_FILES);
                }
                //****** CAMBIOS DE EDICIÓN ***************
                $cambio = "";
                if ($nombre != $auxObjTarea->nombre) {
                    $cambio.="Se cambió el nombre a " . $nombre . "<br>";
                }
                if ($prioridad != $auxObjTarea->prioridad) {
                    $cambio.="Se cambió la prioridad a " . getNombrePrioridad($prioridad);
                }
                if ($presupuesto != $auxObjTarea->presupuesto) {
                    $cambio.="Se cambió el presupuesto a " . $presupuesto . "<br>";
                }
                if ($proveedor != $auxObjTarea->proveedor) {
                    $cambio.="Se cambió el proveedor a " . $proveedor . "<br>";
                }
                if ($descripcion != $auxObjTarea->descripcion) {
                    $cambio.="Se cambió la descripción a " . $descripcion . "<br>";
                }
                if ($idcategoria != $auxObjTarea->idcategoria) {
                    $cambio.="Se cambió de categoría a " . $aCateUsuario[$idcategoria]["nomcategoria"] . "<br>";
                }
                if ($paraadmin != $auxObjTarea->paraadmin) {
                    $nomParaAdminin = "para el administrador";
                    if ($paraadmin == 0) {
                        $nomParaAdminin = "para el gestor";
                    }
                    $cambio.="Se cambió el destino a \"" . $nomParaAdminin . "\"" . "<br>";
                }
                if ($idlocal != $auxObjTarea->idlocal) {
                    $cambio.="Se cambió el local a " . $idlocal . "<br>";
                }


                //*****************************************
                // Creando la tarea
                $objTarea = array(
                    "nombre" => $nombre,
                    "descripcion" => $descripcion,
                    "idlocal" => $idlocal,
                    "presupuesto" => doubleval($presupuesto),
                    "proveedor" => $proveedor,
                    "idcategoria" => $idcategoria,
                    "prioridad" => $prioridad,
                    "paraadmin" => $paraadmin,
                    "idusuario_rol_creador" => $idusuario_creador,
                    "idusuario_rol_ejecutor" => $idusuario_ejecutor,
                    "ultimoestado" => $idestado,
                    "fecha_actualizacion" => $this->_fecha_actual,
                    "estado_registro" => 1
                );

                $this->tareamodel->editar($idtarea, $objTarea);
                if ($idtarea > 0) {
                    // Guardando su actividad
                    $objTareaEstado = array("idusuario" => $this->_idusuario_actual,
                        "idtarea" => $idtarea,
                        "presupuesto" => doubleval($presupuesto),
                        "idestado" => 1,
                        "comentario" => $cambio
                    );
                    $idTareaEstado = $this->tareaestadomodel->guardar($objTareaEstado);

                    if ($idTareaEstado > 0) {
                        foreach ($archivos as $archi) {
                            $objArchivo = array("idtarea_estado" => $idTareaEstado,
                                "url" => $archi["ruta"],
                                "nombre" => $archi["nombre"]
                            );
                            $numFilasArchivos = $this->archivomodel->guardar($objArchivo);
                            if ($numFilasArchivos < 1) {
                                $msg_error.= "Error al registrar archivo" . $idTareaEstado;
                            }
                        }
                    } else {
                        $msg_error.="No se registró la bitacora tarea-estado";
                    }
                } else {
                    $msg_error.="No se registró la tarea";
                }
            } else {
                if ($this->_rol == 1) {
                    $msg_error.="Ya se puede atendió la tarea";
                } else {
                    $msg_error.="No se puede editar";
                }
            }
        } else {
            $msg_error.="No existe la tarea";
        }

        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
            $this->load->model('usuariomodel');
            $objUsuario = $this->usuariomodel->getUsuarios();
            enviarEmail(array(
                "FROM" => "Hikari- Notificaciones",
                "FROMNAME" => "sistemas@hikari.pe",
                "asunto" => "Actualización de la tarea - " . $auxObjTarea->codigo,
                "mensaje" => 'La tarea "' . $auxObjTarea->nombre . '" con código "' . $auxObjTarea->codigo . '" se acaba de editar',
                "titulo" => "De: " . $objUsuario[$this->_idusuario_actual]["nombre"] . "<br>Para: " . $objUsuario[$auxObjTarea->idusuario_rol_ejecutor]["nombre"],
                "correo" => $objUsuario[$auxObjTarea->idusuario_rol_ejecutor]["correo"]
            ));
            $this->registrarLectura($idTareaEstado, $auxObjTarea->idusuario_rol_ejecutor, 'La tarea "' . $auxObjTarea->nombre . '" con código "' . $auxObjTarea->codigo . '" se acaba de editar');
        } else {
            $this->session->set_userdata('mensaje', "La tarea fué editada");
        }
        
        $this->NotiRespuestas($objUsuario, $auxObjTarea, $idTareaEstado, 'La tarea "' . $auxObjTarea->nombre . '" con código "' . $auxObjTarea->codigo . '" se acaba de editar');
        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    public function enviar() {
        enviarEmail(array(
            "FROM" => "Hikari- Notificaciones",
            "FROMNAME" => "sistemas@hikari.pe",
            "asunto" => "Se registró una nueva tarea",
            "titulo" => "De Victor para Yess",
            "mensaje" => "jajaja",
            "correo" => "vicrea@beta.pe"
        ));
    }

    public function json_programar() {

        $oLogin = validaSession("json");
        if ($oLogin[
                "msj"] != "") {
            echo json_encode($oLogin);
            exit;
        }
        $this->load->model('tareaprogmodel');
        $this->load->model('tareaestadomodel');
        $this->load->model('categoriamodel');
        $this->load->model('archivomodel');
        $this->load->model('localmodel');
        $this->load->model('programacionmodel');

        // DATOS DE LA PROGRAMACIÓN
        $lunes = $this->input->post("lunes");
        $martes = $this->input->post("martes");
        $miercoles = $this->input->post("miercoles");
        $jueves = $this->input->post("jueves");
        $viernes = $this->input->post("viernes");
        $sabado = $this->input->post("sabado");
        $domingo = $this->input->post("domingo");
        $repeticion = $this->input->post("repeticion");
        $finicio = $this->input->post("finicio");
        $ffin = $this->input->post("ffin");
        $proveedor = $this->input->post("proveedor");
        $presupuesto = $this->input->post("presupuesto");

        $objProgramacion = array(
            "lunes" => $lunes,
            "martes" => $martes,
            "miercoles" => $miercoles,
            "jueves" => $jueves,
            "viernes" => $viernes,
            "sabado" => $sabado,
            "domingo" => $domingo,
            "repeticion" => $repeticion,
            "fecha_inicio" => getFechaEn2($finicio),
            "fecha_fin" => getFechaEn2($ffin),
            "estado_registro" => 1
        );

        // DATOS DE LA TAREA
        $nombre = $this->input->post("nombre");
        $prioridad = $this->input->post("prioridad");
        $descripcion = $this->input->post("descripcion");
        $idcategoria = $this->input->post("idcategoria");
        $paraadmin = $this->input->post("admin");
        $idlocal = $this->input->post("idlocal");
        if ($idlocal == "") {
            $idlocal = $this->_idlocal;
        }
        $msg_error = "";
        $aCateUsuario = $this->categoriamodel->getaCateUsuario();
        $idusuario_ejecutor = $aCateUsuario[$idcategoria]["idusuario"];
        $idusuario_creador = $this->_idusuario_actual;
        if (intval($paraadmin) == 1) {
            if ($this->_rol != 1) {
                $objLocal = $this->localmodel->getdata(array("idusuario_responsable"), "idlocal = " . $idlocal, 1);
                $idusuario_ejecutor = $objLocal->idusuario_responsable;
                $idusuario_creador = $this->_idusuario_actual;
            } else {
                $idusuario_ejecutor = $aCateUsuario[$idcategoria]["idusuario"];
                $idusuario_creador = $this->_idusuario_actual;
            }
        } else {
            if ($this->_rol != 1) {
                $objLocal = $this->localmodel->getdata(array("idusuario_responsable"), "idlocal = " . $idlocal, 1);
                $idusuario_creador = $objLocal->idusuario_responsable;
            }
        }


        $archivos = array();
        if (!empty($_FILES)) {
            $archivos = subirArchivos($_FILES);
        }

        // Creando la tarea
        $random = rand(1, 99999);
        $codigo = "TP-" . substr(date("Y"), 2, 2) . date("m") . "-" . str_pad($random, 3, "0", STR_PAD_LEFT);
        $objTarea = array(
            "codigo" => $codigo,
            "nombre" => $nombre,
            "descripcion" => $descripcion,
            "proveedor" => $proveedor,
            "presupuesto" => $presupuesto,
            "idlocal" => $idlocal,
            "idcategoria" => $idcategoria,
            "prioridad" => $prioridad,
            "paraadmin" => $paraadmin,
            "idusuario_rol_creador" => $idusuario_creador,
            "idusuario_rol_ejecutor" => $idusuario_ejecutor
        );
        //var_dump($objTarea);exit;
        $idtarea = $this->tareaprogmodel->guardar($objTarea);
        if ($idtarea > 0) {
            // Guardando su actividad
            $objProgramacion["idtareaprog"] = $idtarea;
            $idprogramacion = $this->programacionmodel->guardar($objProgramacion);

            if ($idprogramacion > 0) {
                foreach ($archivos as $archi) {
                    $objArchivo = array("idtarea_estado" => $idprogramacion,
                        "url" => $archi["ruta"],
                        "nombre" => $archi["nombre"]
                    );
                    $numFilasArchivos = $this->archivomodel->guardar($objArchivo);
                    if ($numFilasArchivos < 1) {
                        $msg_error.= "Error al registrar archivo" . $idprogramacion;
                    }
                }
            } else {
                $msg_error.="No se registró la bitacora tarea-estado";
            }
        } else {
            $msg_error.="No se registró la tarea";
        }
        $objProgramacion["idprogramacion"] = $idprogramacion;
        $idFueEjecutada = 0;
        $idFueEjecutada = $this->ejecuta_cron($objProgramacion, $objTarea);
        if ($idFueEjecutada > 0) {
            $objTarea = array();
            $objTarea = array(
                "fecha_ultima_creada" => $this->_fecha_actual
            );
            $this->tareaprogmodel->editar($idtarea, $objTarea);
        }
        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "La tarea se creó exitosamente");
        }
        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    // ESTADO = 1
    public function json_registrar() {

        $oLogin = validaSession("json");
        if ($oLogin[
                "msj"] != "") {
            echo json_encode($oLogin);
            exit;
        }
        $this->load->model('tareamodel');
        $this->load->model('tareaestadomodel');
        $this->load->model('categoriamodel');
        $this->load->model('archivomodel');
        $this->load->model('localmodel');

        $nombre = $this->input->post("nombre");
        $prioridad = $this->input->post("prioridad");
        $descripcion = $this->input->post("descripcion");
        $idcategoria = $this->input->post("idcategoria");
        $paraadmin = $this->input->post("admin");
        $idlocal = $this->input->post("idlocal");
        if ($idlocal == "") {
            $idlocal = $this->_idlocal;
        }
        $msg_error = "";
        $aCateUsuario = $this->categoriamodel->getaCateUsuario();
        $idusuario_ejecutor = $aCateUsuario[$idcategoria]["idusuario"];
        $idusuario_creador = $this->_idusuario_actual;
        if (intval($paraadmin) == 1) {
            if ($this->_rol != 1) {
                $objLocal = $this->localmodel->getdata(array("idusuario_responsable"), "idlocal = " . $idlocal, 1);
                $idusuario_ejecutor = $objLocal->idusuario_responsable;
                $idusuario_creador = $this->_idusuario_actual;
            } else {
                $idusuario_ejecutor = $aCateUsuario[$idcategoria]["idusuario"];
                $idusuario_creador = $this->_idusuario_actual;
            }
        } else {
            if ($idlocal == 17 && $this->_rol == 2) {
                $idusuario_creador = $this->_idusuario_actual;
            } else {
                if ($idlocal == 17 && $this->_rol == 3) {
                    $idusuario_creador = $this->_idusuario_actual;
                } else {
                    if ($this->_rol != 1) {
                        $objLocal = $this->localmodel->getdata(array("idusuario_responsable"), "idlocal = " . $idlocal, 1);
                        $idusuario_creador = $objLocal->idusuario_responsable;
                    }
                }
            }
        }


        $archivos = array();
        if (!empty($_FILES)) {
            $archivos = subirArchivos($_FILES);
        }

// Creando la tarea
        $random = rand(1, 99999);

        $objTarea = array(
            "nombre" => $nombre,
            "descripcion" => $descripcion,
            "idlocal" => $idlocal,
            "idcategoria" => $idcategoria,
            "prioridad" => $prioridad,
            "paraadmin" => $paraadmin,
            "idusuario_rol_creador" => $idusuario_creador,
            "idusuario_rol_ejecutor" => $idusuario_ejecutor,
            "idusuario_creador" => $this->_idusuario_actual,
            "ultimoestado" => 1,
            "fecha_registro" => $this->_fecha_actual,
            "fecha_actualizacion" => $this->_fecha_actual,
            "estado_registro" => 1
        );
        $idtarea = $this->tareamodel->guardar($objTarea);
        $codigo = "TC-" . substr(date("Y"), 2, 2) . date("m") . "-" . str_pad($idtarea, 3, "0", STR_PAD_LEFT);
        $objTarea = array();
        $objTarea["codigo"] = $codigo;
        $this->tareamodel->editar($idtarea, $objTarea);
        if ($idtarea > 0) {
            // Guardando su actividad
            $objTareaEstado = array(
                "idusuario" => $this->_idusuario_actual,
                "idtarea" => $idtarea,
                "iniciador" => 1,
                "idestado" => 1,
                "comentario" => $descripcion
            );
            $idTareaEstado = $this->tareaestadomodel->guardar($objTareaEstado);

            if ($idTareaEstado > 0) {
                foreach ($archivos as $archi) {
                    $objArchivo = array("idtarea_estado" => $idTareaEstado,
                        "url" => $archi["ruta"],
                        "nombre" => $archi["nombre"]
                    );
                    $numFilasArchivos = $this->archivomodel->guardar($objArchivo);
                    if ($numFilasArchivos < 1) {
                        $msg_error.= "Error al registrar archivo" . $idTareaEstado;
                    }
                }
            } else {
                $msg_error.="No se registró la bitacora tarea-estado";
            }
        } else {
            $msg_error.="No se registró la tarea";
        }

        $this->load->model('usuariomodel');

        $objUsuario = $this->usuariomodel->getUsuarios();
        // $objUsuario[$idusuario_ejecutor]["correo"]
        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "La tarea se creó exitosamente");
            enviarEmail(array(
                "FROM" => "Hikari- Notificaciones",
                "FROMNAME" => "sistemas@hikari.pe",
                "asunto" => "Se registró una nueva tarea",
                "mensaje" => 'Se creó la tarea llamada "' . $nombre . '" con código "' . $codigo . '"',
                "titulo" => "De: " . $objUsuario[$idusuario_creador]["nombre"] . "<br>Para: " . $objUsuario[$idusuario_ejecutor]["nombre"],
                "correo" => $objUsuario[$idusuario_ejecutor]["correo"]
            ));
            // marcar como no leido
            $this->registrarLectura($idTareaEstado, $idusuario_ejecutor, 'Se creó la tarea llamada "' . $nombre . '" con código "' . $codigo . '"');
        }
        if ($prioridad == 3) {
            $aGesDirectores = $this->usuariomodel->getdata(array("*"), "rol in (2,3)");
            foreach ($aGesDirectores as $itemGesDirector) {
                enviarEmail(array(
                    "FROM" => "Hikari- Notificaciones",
                    "FROMNAME" => "sistemas@hikari.pe",
                    "asunto" => "Se registró una nueva tarea",
                    "mensaje" => 'Se creó la tarea llamada "' . $nombre . '" con código "' . $codigo . '", esta tarea tiene prioridad alta',
                    "titulo" => "De: " . $objUsuario[$idusuario_creador]["nombre"] . "<br>Para: " . $objUsuario[$idusuario_ejecutor]["nombre"],
                    "correo" => $itemGesDirector->correo
                ));
                $this->registrarLectura($idTareaEstado, $itemGesDirector->idusuario, 'Se creó la tarea llamada "' . $nombre . '" con código "' . $codigo . '", esta tarea tiene prioridad alta');
            }
        }

        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    public function redirecconaTarea($idtarea = 0, $idlectura = 0) {
        $this->load->model('tareamodel');
        $this->load->model('lecturamodel');
        $oTarea = $this->tareamodel->getdata(array("*"), "idtarea = " . $idtarea, 1);
        redireccionaAccion($oTarea->ultimoestado, $oTarea->idtarea, $oTarea->paraadmin, $oTarea->idusuario_rol_creador, $oTarea->idgrupo, $oTarea->idusuario_rol_ejecutor);
    }

    public function registrarLectura($idtareaEstado = 0, $idusuario = 0, $mensaje = "") {
        $this->load->model('lecturamodel');
        // marcar como no leido
        $objLectura = array();
        $objLectura = array(
            "leido" => 0,
            "textoaccion" => $mensaje,
            "idtarea_estado" => $idtareaEstado,
            "idusuario" => $idusuario
        );
        $this->lecturamodel->guardar($objLectura);
    }

    public function json_totalnotificaciones() {
        $this->load->model('lecturamodel');
        $oLectura = $this->lecturamodel->getdata(array("count(idlectura) as total"), "leido = 0 and idusuario = " . $this->_idusuario_actual, 1);
        if (intval($oLectura->total) > 0) {
            echo '<span class="label label-danger" style="font-size: 9px;">(' . $oLectura->total . ')</span>';
        } else {
            echo '';
        }
    }

    public function json_notificaciones() {
        $this->load->model('lecturamodel');
        $oLectura = $this->lecturamodel->listaNotificaciones($this->_idusuario_actual);
        $data["oLectura"] = $oLectura;

        $this->load->view("tarea/divnotificiones", $data);
    }

    // ESTADO = 2
    public function json_por_corregirppto() {
        $oLogin = validaSession("json");
        if ($oLogin[
                "msj"] != "") {
            echo json_encode($oLogin);
            exit;
        }
        $this->load->model('tareamodel');
        $this->load->model('tareaestadomodel');
        $this->load->model('categoriamodel');
        $this->load->model('archivomodel');

        $accion = $this->input->post("accion");
        $descripcion = $this->input->post("descripcion");
        $idtarea = $this->input->post("idtarea");
        $proveedor = $this->input->post("proveedor");
        $presupuesto = $this->input->post("presupuesto");
        $msg_error = "";


// Valido que exista el idtarea enviado
        $auxObjTarea = $this->tareamodel->getdata(array("*"), "idtarea = " . $idtarea, 1);

        if ($auxObjTarea != false) {
// Jugar con los estados
// Ver que acción realizará (1= Al estado 2 y 0= Al estado 3)
            if ($auxObjTarea->ultimoestado == 8) {
                $estado = 10;
                $idnotificacion = $auxObjTarea->idusuario_rol_ejecutor;
                $nomAcion = "atendió";
                if ($accion == 0) {
                    $nomAcion = "rechazó";
                    $estado = 3;
                    $idnotificacion = $auxObjTarea->idusuario_rol_creador;
                } elseif ($accion == 2) {
                    $nomAcion = "envió a evaluar presupuesto";
                    $estado = 10;
                    $idnotificacion = $auxObjTarea->idusuario_rol_ejecutor;
                }

// Creando una nueva actividad realizada
                $objTareaEstado = array("idusuario" => $this->_idusuario_actual,
                    "idtarea" => $idtarea,
                    "idestado" => $estado,
                    "evaluacion" => 1,
                    "presupuesto" => doubleval($presupuesto),
                    "comentario" => $descripcion
                );
                $idTareaEstado = $this->tareaestadomodel->guardar($objTareaEstado);
                if ($idTareaEstado > 0) {
// Actualizando el ultimo estado para esta tarea
                    $objTarea = array(
                        "presupuesto" => doubleval($presupuesto),
                        "fecha_actualizacion" => date("Y-m-d H:i:s"),
                        "proveedor" => $proveedor,
                        "ultimoestado" => $estado
                    );
                    $this->tareamodel->editar($idtarea, $objTarea);

// Guardando los archivo de la actividad
                    $archivos = array();
                    if (!empty($_FILES)) {
                        $archivos = subirArchivos($_FILES);
                    }
                    foreach ($archivos as $archi) {
                        $objArchivo = array("idtarea_estado" => $idTareaEstado,
                            "url" => $archi["ruta"],
                            "nombre" => $archi["nombre"]
                        );
                        $numFilasArchivos = $this->archivomodel->guardar($objArchivo);
                        if ($numFilasArchivos < 1) {
                            $msg_error.= "Error al registrar archivo" . $idTareaEstado;
                        }
                    }
                } else {
                    $msg_error.="No se registro actividad";
                }
            } else {
                $msg_error.="Ya se realizó este estado";
            }
        } else {
            $msg_error.="No existe esta tarea";
        }

        $this->load->model('usuariomodel');
        $objUsuario = $this->usuariomodel->getUsuarios();
        // $objUsuario[$idusuario_ejecutor]["correo"]
        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "La tarea se " . $nomAcion);
            enviarEmail(array(
                "FROM" => "Hikari- Notificaciones",
                "FROMNAME" => "sistemas@hikari.pe",
                "asunto" => "Actualización de la tarea - " . $auxObjTarea->codigo,
                "mensaje" => 'La tarea "' . $auxObjTarea->nombre . '" con código "' . $auxObjTarea->codigo . '" se ' . $nomAcion,
                "titulo" => "De: " . $objUsuario[$this->_idusuario_actual]["nombre"] . "<br>Para: " . $objUsuario[$idnotificacion]["nombre"],
                "correo" => $objUsuario[$idnotificacion]["nombre"]
            ));
            $this->registrarLectura($idTareaEstado, $idnotificacion, 'La tarea "' . $auxObjTarea->nombre . '" con código "' . $auxObjTarea->codigo . '" se ' . $nomAcion);
        }
        
        $this->NotiRespuestas($objUsuario, $auxObjTarea, $idTareaEstado, 'La tarea "' . $auxObjTarea->nombre . '" con código "' . $auxObjTarea->codigo . '" se ' . $nomAcion);
        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    public function json_por_enviarpresu() {
        $oLogin = validaSession("json");
        if ($oLogin[
                "msj"] != "") {
            echo json_encode($oLogin);
            exit;
        }
        $this->load->model('tareamodel');
        $this->load->model('usuariomodel');
        $this->load->model('tareaestadomodel');
        $this->load->model('categoriamodel');
        $this->load->model('archivomodel');
        $this->load->model('grupomodel');
        $objUsuario = $this->usuariomodel->getUsuarios();
        $nombre = $this->input->post("nombre");
        $descripcion = $this->input->post("descripcion");
        $ids = trim($this->input->post("ids"), ",");
        $presus = trim($this->input->post("presus"), ",");
        $aIds = explode(",", $ids);
        $aPresus = explode(",", $presus);
        $montototal = 0;
        $objGrupo = array(
            "nombre" => $nombre,
            "cantidadtareas" => count($aIds),
            "descripcion" => $descripcion,
            "idusuario_creador" => $this->_idusuario_actual,
            "fecha_envio" => $this->_fecha_actual
        );
        $idgrupo = $this->grupomodel->guardar($objGrupo);
        if ($idgrupo > 0) {

            $objPresus = array();
            $objIds = array();
            foreach ($aIds as $keyId => $itemId) {
                $objIds[$keyId] = $itemId;
            }
            foreach ($aPresus as $keyPresu => $idPresus) {
                $objPresus[$keyPresu] = $idPresus;
                $montototal+=$idPresus;
            }
            $estado = 10;
            foreach ($objIds as $key => $item) {

                // Registrando 
                $objTareaEstado = array("idusuario" => $this->_idusuario_actual,
                    "idtarea" => $item,
                    "idestado" => $estado,
                    "evaluacion" => 1,
                    "presupuesto" => doubleval($objPresus[$key]),
                    "comentario" => 'Se envió el presupuesto para ser aprobado dentro del grupo llamado "' . $nombre . '"'
                );
                $idTareaEstado = $this->tareaestadomodel->guardar($objTareaEstado);
                if ($idTareaEstado > 0) {
                    $objTarea = array(
                        "presupuesto" => doubleval($objPresus[$key]),
                        "fecha_actualizacion" => date("Y-m-d H:i:s"),
                        "ultimoestado" => $estado,
                        "idgrupo" => $idgrupo
                    );
                    $this->tareamodel->editar($item, $objTarea);
                    $this->cambiarLectura($item);
                }
            }
            $objGrupo = array();
            $objGrupo = array(
                "montototal" => $montototal
            );
            $this->grupomodel->editar($idgrupo, $objGrupo);
        } else {
            $msg_error .= "No se pudo registrar";
        }
        $msg_error = "";
        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "Se envió presupuesto a enviar");

            $aDirectores = array();
            $aDirectores = $this->usuariomodel->getdata(array("idusuario", "nombre", "correo"), "rol = 3");

            foreach ($aDirectores as $itemDirector) {
                enviarEmail(array(
                    "FROM" => "Hikari- Notificaciones",
                    "FROMNAME" => "sistemas@hikari.pe",
                    "asunto" => 'Creación de bloque de presupuesto - "' . $nombre . '"',
                    "mensaje" => 'Se acaba se crear un nuevo bloque de presupuestos denominado ' . $nombre,
                    "titulo" => "Hola " . $itemDirector->nombre . "",
                    "correo" => $itemDirector->correo
                ));
                $this->registrarLectura($idTareaEstado, $itemDirector->idusuario, 'Se acaba se crear un nuevo bloque de presupuestos denominado ' . $nombre);
                //$this->NotiRespuestas($objUsuario, $objauxTarea, $idTareaEstado, 'Se acaba se crear un nuevo bloque de presupuestos denominado ' . $nombre);
            }
        }
        
        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    public function json_por_atender() {
        $oLogin = validaSession("json");
        if ($oLogin[
                "msj"] != "") {
            echo json_encode($oLogin);
            exit;
        }
        $this->load->model('tareamodel');
        $this->load->model('tareaestadomodel');
        $this->load->model('categoriamodel');
        $this->load->model('archivomodel');

        $accion = $this->input->post("accion");
        $descripcion = $this->input->post("descripcion");
        $idtarea = $this->input->post("idtarea");
        $proveedor = $this->input->post("proveedor");
        $presupuesto = $this->input->post("presupuesto");
        $msg_error = "";


// Valido que exista el idtarea enviado
        $auxObjTarea = $this->tareamodel->getdata(array("*"), "idtarea = " . $idtarea, 1);
        $evaluacion = 0;
        if ($auxObjTarea != false) {
// Jugar con los estados
// Ver que acción realizará (1= Al estado 2 y 0= Al estado 3)
            if ($auxObjTarea->ultimoestado == 1) {
                $estado = 2;
                $idnotificacion = $auxObjTarea->idusuario_rol_ejecutor;
                $nomAcion = "atendió y fue aceptada";
                if ($accion == 0) {
                    $nomAcion = "envió a corregir";
                    $idnotificacion = $auxObjTarea->idusuario_rol_creador;
                    $evaluacion = 2;
                    $estado = 3;
                } elseif ($accion == 2) {
                    $evaluacion = 1;
                    $nomAcion = "envió a preparar presupuesto";
                    $idnotificacion = $auxObjTarea->idusuario_rol_ejecutor;
                    $estado = 11;
                } elseif ($accion == 9) {
                    $nomAcion = "rechazó";
                    $estado = 9;
                    $idnotificacion = $auxObjTarea->idusuario_rol_creador;
                }

// Creando una nueva actividad realizada
                $objTareaEstado = array("idusuario" => $this->_idusuario_actual,
                    "idtarea" => $idtarea,
                    "idestado" => $estado,
                    "evaluacion" => $evaluacion,
                    "presupuesto" => doubleval($presupuesto),
                    "comentario" => $descripcion
                );
                $idTareaEstado = $this->tareaestadomodel->guardar($objTareaEstado);
                if ($idTareaEstado > 0) {
// Actualizando el ultimo estado para esta tarea
                    $objTarea = array(
                        "presupuesto" => doubleval($presupuesto),
                        "fecha_actualizacion" => date("Y-m-d H:i:s"),
                        "proveedor" => $proveedor,
                        "ultimoestado" => $estado
                    );
                    $this->tareamodel->editar($idtarea, $objTarea);

// Guardando los archivo de la actividad
                    $archivos = array();
                    if (!empty($_FILES)) {
                        $archivos = subirArchivos($_FILES);
                    }
                    foreach ($archivos as $archi) {
                        $objArchivo = array("idtarea_estado" => $idTareaEstado,
                            "url" => $archi["ruta"],
                            "nombre" => $archi["nombre"]
                        );
                        $numFilasArchivos = $this->archivomodel->guardar($objArchivo);
                        if ($numFilasArchivos < 1) {
                            $msg_error.= "Error al registrar archivo" . $idTareaEstado;
                        }
                    }
                } else {
                    $msg_error.="No se registro actividad";
                }
            } else {
                $msg_error.="Ya se realizó este estado";
            }
        } else {
            $msg_error.="No existe esta tarea";
        }

        $this->load->model('usuariomodel');
        $objUsuario = $this->usuariomodel->getUsuarios();
        // $objUsuario[$idusuario_ejecutor]["correo"]
        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "La tarea se " . $nomAcion);
            enviarEmail(array(
                "FROM" => "Hikari- Notificaciones",
                "FROMNAME" => "sistemas@hikari.pe",
                "asunto" => "Actualización de la tarea - " . $auxObjTarea->codigo,
                "mensaje" => 'La tarea "' . $auxObjTarea->nombre . '" con código "' . $auxObjTarea->codigo . '" se ' . $nomAcion,
                "titulo" => "De: " . $objUsuario[$this->_idusuario_actual]["nombre"] . "<br>Para: " . $objUsuario[$auxObjTarea->idusuario_rol_creador]["nombre"],
                "correo" => $objUsuario[$idnotificacion]["correo"]
            ));
            $this->registrarLectura($idTareaEstado, $idnotificacion, 'La tarea "' . $auxObjTarea->nombre . '" con código "' . $auxObjTarea->codigo . '" se ' . $nomAcion);
            $this->NotiRespuestas($objUsuario, $auxObjTarea, $idTareaEstado, 'La tarea "' . $auxObjTarea->nombre . '" con código "' . $auxObjTarea->codigo . '" se ' . $nomAcion);
        }

        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    public function json_ejecutar() {
        $oLogin = validaSession("json");
        if ($oLogin[
                "msj"] != "") {
            echo json_encode($oLogin);
            exit;
        }
        $this->load->model('tareamodel');
        $this->load->model('tareaestadomodel');
        $this->load->model('categoriamodel');
        $this->load->model('archivomodel');

        $accion = $this->input->post("accion");
        $descripcion = $this->input->post("descripcion");
        $idtarea = $this->input->post("idtarea");
        $proveedor = $this->input->post("proveedor");
        $presupuesto = $this->input->post("presupuesto");
        $msg_error = "";


// Valido que exista el idtarea enviado
        $auxObjTarea = $this->tareamodel->getdata(array("*"), "idtarea = " . $idtarea, 1);
        if ($auxObjTarea != false) {
            if ($auxObjTarea->ultimoestado == 2) {
                // Jugar con los estados
                // Ver que acción realizará (1= Al estado 2 y 0= Al estado 3)
                if ($this->_rol == 1 && $auxObjTarea->paraadmin) {
                    $estado = 5;
                    $nomAccion = "Terminada";
                    $idnotificacion = $auxObjTarea->idusuario_rol_creador;
                } else {
                    $estado = 4;
                    $nomAccion = "ejecutada exitosamente";
                    $idnotificacion = $auxObjTarea->idusuario_rol_creador;
                }

                if ($accion == 0) {
                    $estado = 9;
                    $idnotificacion = $auxObjTarea->idusuario_rol_creador;
                    $nomAccion = "rechazada";
                }

// Creando una nueva actividad realizada
                $objTareaEstado = array("idusuario" => $this->_idusuario_actual,
                    "idtarea" => $idtarea,
                    "idestado" => $estado,
                    "presupuesto" => doubleval($presupuesto),
                    "comentario" => $descripcion
                );
                $idTareaEstado = $this->tareaestadomodel->guardar($objTareaEstado);
                if ($idTareaEstado > 0) {
// Actualizando el ultimo estado para esta tarea
                    $objTarea = array(
                        "ultimoestado" => $estado,
                        "presupuesto" => doubleval($presupuesto),
                        "fecha_actualizacion" => date("Y-m-d H:i:s"),
                        "proveedor" => $proveedor,
                    );
                    $this->tareamodel->editar($idtarea, $objTarea);

// Guardando los archivo de la actividad
                    $archivos = array();
                    if (!empty($_FILES)) {
                        $archivos = subirArchivos($_FILES);
                    }
                    foreach ($archivos as $archi) {
                        $objArchivo = array("idtarea_estado" => $idTareaEstado,
                            "url" => $archi["ruta"],
                            "nombre" => $archi["nombre"]
                        );
                        $numFilasArchivos = $this->archivomodel->guardar($objArchivo);
                        if ($numFilasArchivos < 1) {
                            $msg_error.= "Error al registrar archivo" . $idTareaEstado;
                        }
                    }
                } else {
                    $msg_error.="No se registro actividad";
                }
            } else {
                $msg_error.="Este estado ya se registró";
            }
        } else {
            $msg_error.="No existe esta tarea";
        }

        $this->load->model('usuariomodel');
        $objUsuario = $this->usuariomodel->getUsuarios();
        // $objUsuario[$idusuario_ejecutor]["correo"]
        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "La tarea fué " . $nomAccion);
            enviarEmail(array(
                "FROM" => "Hikari- Notificaciones",
                "FROMNAME" => "sistemas@hikari.pe",
                "asunto" => "Actualización de la tarea - " . $auxObjTarea->codigo,
                "mensaje" => 'La tarea "' . $auxObjTarea->nombre . '" con código "' . $auxObjTarea->codigo . '" fué ' . $nomAccion,
                "titulo" => "De: " . $objUsuario[$this->_idusuario_actual]["nombre"] . "<br>Para: " . $objUsuario[$idnotificacion]["nombre"],
                "correo" => $objUsuario[$idnotificacion]["correo"]
            ));
            $this->registrarLectura($idTareaEstado, $idnotificacion, 'La tarea "' . $auxObjTarea->nombre . '" con código "' . $auxObjTarea->codigo . '" fué ' . $nomAccion);
            $this->NotiRespuestas($objUsuario, $auxObjTarea, $idTareaEstado, 'La tarea "' . $auxObjTarea->nombre . '" con código "' . $auxObjTarea->codigo . '" fué ' . $nomAccion);
        }

        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    public function json_confirmar() {
        $oLogin = validaSession("json");
        if ($oLogin[
                "msj"] != "") {
            echo json_encode($oLogin);
            exit;
        }
        $this->load->model('tareamodel');
        $this->load->model('tareaestadomodel');
        $this->load->model('categoriamodel');
        $this->load->model('archivomodel');

        $accion = $this->input->post("accion");
        $descripcion = $this->input->post("descripcion");
        $idtarea = $this->input->post("idtarea");
        $msg_error = "";


        // Valido que exista el idtarea enviado
        $auxObjTarea = $this->tareamodel->getdata(array("*"), "idtarea = " . $idtarea, 1);
        if ($auxObjTarea != false) {
            if ($auxObjTarea->ultimoestado == 4) {
                // Jugar con los estados
                // Ver que acción realizará (1= Al estado 2 y 0= Al estado 3)
                $estado = 5;
                $nomAccion = "está terminada";
                $idnotificacion = $auxObjTarea->idusuario_rol_ejecutor;
                if ($accion == 0) {
                    $nomAccion = "no está conforme";
                    $estado = 6;
                }

                // Creando una nueva actividad realizada
                $objTareaEstado = array("idusuario" => $this->_idusuario_actual,
                    "idtarea" => $idtarea,
                    "idestado" => $estado,
                    "presupuesto" => doubleval($auxObjTarea->presupuesto),
                    "comentario" => $descripcion
                );
                $idTareaEstado = $this->tareaestadomodel->guardar($objTareaEstado);
                if ($idTareaEstado > 0) {
                    // Actualizando el ultimo estado para esta tarea
                    $objTarea = array(
                        "ultimoestado" => $estado,
                        "fecha_actualizacion" => date("Y-m-d H:i:s")
                    );
                    $this->tareamodel->editar($idtarea, $objTarea);

                    // Guardando los archivo de la actividad
                    $archivos = array();
                    if (!empty($_FILES)) {
                        $archivos = subirArchivos($_FILES);
                    }
                    foreach ($archivos as $archi) {
                        $objArchivo = array("idtarea_estado" => $idTareaEstado,
                            "url" => $archi["ruta"],
                            "nombre" => $archi["nombre"]
                        );
                        $numFilasArchivos = $this->archivomodel->guardar($objArchivo);
                        if ($numFilasArchivos < 1) {
                            $msg_error.= "Error al registrar archivo" . $idTareaEstado;
                        }
                    }
                } else {
                    $msg_error.="No se registro actividad";
                }
            } else {
                $msg_error.="Ya se realizó esta actividad";
            }
        } else {
            $msg_error.="No existe esta tarea";
        }

        $this->load->model('usuariomodel');
        $objUsuario = $this->usuariomodel->getUsuarios();
        // $objUsuario[$idusuario_ejecutor]["correo"]
        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "La tarea " . $nomAccion . "");
            enviarEmail(array(
                "FROM" => "Hikari- Notificaciones",
                "FROMNAME" => "sistemas@hikari.pe",
                "asunto" => "Actualización de la tarea - " . $auxObjTarea->codigo,
                "mensaje" => 'La tarea "' . $auxObjTarea->nombre . '" con código "' . $auxObjTarea->codigo . '" ' . $nomAccion,
                "titulo" => "De: " . $objUsuario[$this->_idusuario_actual]["nombre"] . "<br>Para: " . $objUsuario[$auxObjTarea->idusuario_rol_ejecutor]["nombre"],
                "correo" => $objUsuario[$auxObjTarea->idusuario_rol_ejecutor]["correo"]
            ));
            $this->registrarLectura($idTareaEstado, $auxObjTarea->idusuario_rol_ejecutor, 'La tarea "' . $auxObjTarea->nombre . '" con código "' . $auxObjTarea->codigo . '" ' . $nomAccion);
            $this->NotiRespuestas($objUsuario, $auxObjTarea, $idTareaEstado, 'La tarea "' . $auxObjTarea->nombre . '" con código "' . $auxObjTarea->codigo . '" ' . $nomAccion);
        }

        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    public function cambiarLectura($idtarea = 0) {
        $this->load->model('tareaestadomodel');
        $this->load->model('tareamodel');
        $this->load->model('lecturamodel');

        $oLecturaTareaEstado = $this->lecturamodel->getLecturasTareaEstado("l.leido = 0 and l.idusuario = " . $this->_idusuario_actual . " and t.idtarea = " . $idtarea);
        if ($oLecturaTareaEstado != false) {
            foreach ($oLecturaTareaEstado as $itemLecturaTareaEstado) {
                $oLectura = array();
                $oLectura = array(
                    "fecha_leido" => $this->_fecha_actual,
                    "leido" => 1
                );
                $this->lecturamodel->editar($itemLecturaTareaEstado->idlectura, $oLectura);
            }
        }
    }

    public function NotiRespuestas($objUsuario = array(), $objauxTarea = array(), $idTareaEstado = 0, $mensaje="") {
        $oUsuariosRespuesta = $this->respuestasmodel->getUsuariosRespuestas("t.idtarea = " . $objauxTarea->idtarea . " and r.idusuario_creador not in (" . $objauxTarea->idusuario_rol_creador . "," . $objauxTarea->idusuario_rol_ejecutor . ", " . $objauxTarea->idusuario_creador . ")");
        if ($oUsuariosRespuesta != FALSE) {
            foreach ($oUsuariosRespuesta as $itemUsuarioRespuesta) {
                $idnotificacion = $itemUsuarioRespuesta->idusuario_creador;
                enviarEmail(array(
                    "FROM" => "Hikari- Notificaciones",
                    "FROMNAME" => "sistemas@hikari.pe",
                    "asunto" => "Actualización de la tarea - " . $objauxTarea->codigo,
                    "mensaje" => $mensaje,
                    "titulo" => "De: " . $objUsuario[$this->_idusuario_actual]["nombre"] . "<br>Para: " . $objUsuario[$idnotificacion]["nombre"],
                    "correo" => $objUsuario[$idnotificacion]["correo"]
                ));
                $this->registrarLectura($idTareaEstado, $idnotificacion,  $mensaje);
            }
        }

        if ($objauxTarea->idusuario_creador != $objauxTarea->idusuario_rol_creador) {
            $idnotificacion = $objauxTarea->idusuario_creador;
            enviarEmail(array(
                "FROM" => "Hikari- Notificaciones",
                "FROMNAME" => "sistemas@hikari.pe",
                "asunto" => "Actualización de la tarea - " . $objauxTarea->codigo,
                "mensaje" => $mensaje,
                "titulo" => "De: " . $objUsuario[$this->_idusuario_actual]["nombre"] . "<br>Para: " . $objUsuario[$idnotificacion]["nombre"],
                "correo" => $objUsuario[$idnotificacion]["correo"]
            ));
            $this->registrarLectura($idTareaEstado, $idnotificacion,  $mensaje);
        }
    }
    
}
