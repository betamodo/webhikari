<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Almacen extends MY_Controller {

    private $_fecha_actual;
    private $_idusuario_actual;
    private $_idlocal;
    private $_rol;

    function __construct() {
        parent::__construct();
        $this->_fecha_actual = date("Y-m-d H:i:s");
        $this->load->model('almacenmodel');
        $this->_idusuario_actual = $this->session->userdata('idusuario');
        $this->_rol = $this->session->userdata('rol');
        $this->_idlocal = $this->session->userdata('idlocal');
        $this->template->set_template('default');
        $this->load->helper(array('url', 'funciones', 'mysql_to_excel_helper'));
    }

    function index() {
        $this->login();
    }

    public function lista() {
        // Trayendo modelos
        $mensaje = $this->session->userdata('mensaje');
        if ($mensaje != "") {
            $mensaje = getHtmlMensaje(1, $mensaje);
        }
        $this->session->set_userdata('mensaje', "");

        $aListaAlmacen = $this->almacenmodel->getdataAlmacen("a.idalmacen > 0");
        $data['titulo'] = "Lista de almac�n";
        $data['mensaje'] = $mensaje;
        $data['aListaAlmacen'] = $aListaAlmacen;
        $botones = "";
        $botones .= '<a href = "' . base_url() . 'panel" class = "btn btn-sm btn-default" >Atras</a> ';
        $botones .= '<a href = "' . base_url() . 'almacen/nuevo" class = "btn btn-sm btn-default" >Nuevo</a> ';
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Almacenes", TRUE);
        $this->template->write_view('content', 'mantenimiento/almacen/lista', $data, TRUE);
        $this->template->render();
    }

    public function json_crear_almacen() {

        $nombre = $this->input->post("nombre");
        $idusuario = $this->input->post("idusuario");
        $estado = $this->input->post("estado");
        $msg_error = "";
        // Validar si existe el id de tarea
        $auxAlmacen = array();
        $oAlmacen = array();
        $auxAlmacen = $this->almacenmodel->getExiste("nombre", $nombre, 0, 1);

        if ($auxAlmacen->cantidad == 0) {
            $oAlmacen = array(
                "nombre" => $nombre,
                "idusuario_responsable" => $idusuario,
                "estado_registro" => $estado
            );
            $idAlmacen = $this->almacenmodel->guardar($oAlmacen);
            // Subimos los archivos si el usuario fue creado exitosamente
            if ($idAlmacen < 1) {
                $msg_error.="No se pudo registrar";
            }
        } else {
            $msg_error.="El nombre ingresado ya existe";
        }
        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "El almac�n se cre� exitosamente");
        }
        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    public function json_editar_almacen() {
        $idAlmacen = $this->input->post("idalmacen");
        $nombre = $this->input->post("nombre");
        $idusuario = $this->input->post("idusuario");
        $estado = $this->input->post("estado");

        $msg_error = "";
        // Validar si existe el id de tarea
        $auxAlmacen = array();
        $oAlmacen = array();
        $auxAlmacen = $this->almacenmodel->getExiste("nombre", $nombre, $idAlmacen, 1);
        if ($auxAlmacen->cantidad == 0) {
            $oAlmacen = array(
                "nombre" => $nombre,
                "idusuario_responsable" => $idusuario,
                "estado_registro" => $estado
            );
            $this->almacenmodel->editar($idAlmacen, $oAlmacen);
        } else {
            $msg_error.="El nombre ingresado ya existe";
        }
        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', "El almac�n se edit� exitosamente");
        }
        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    public function nuevo() {

        // Trayendo modelos
        $this->load->model('usuariomodel');
        $aUsuarios = $this->usuariomodel->getdata(array("idusuario", "nombre"), "rol = 2");
        $data['titulo'] = "Nuevo almac�n";
        $data['aUsuarios'] = $aUsuarios;
        $botones = "";
        $botones .= '<a href = "' . base_url() . 'almacen/lista" class = "btn btn-sm btn-default" >Atras</a> ';
        $this->template->add_js('media/js/almacen.js');
		$this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Nuevo local", TRUE);
        $this->template->write_view('content', 'mantenimiento/almacen/crear', $data, TRUE);
        $this->template->render();
    }

    public function editar($idAlmacen) {
        // Trayendo modelos
        $this->load->model('usuariomodel');
        $aUsuarios = $this->usuariomodel->getdata(array("idusuario", "nombre"), "rol = 2");

        $oAlmacen = $this->almacenmodel->getdata(array("*"), "idAlmacen = " . $idAlmacen, 1);
        if ($oAlmacen == FALSE) {
            $this->session->set_userdata('mensaje', "");
        }

        $data['titulo'] = "Editar almac�n";
        $data['oAlmacen'] = $oAlmacen;
        $data['aUsuarios'] = $aUsuarios;
        $botones = "";
		$botones .= '<a href = "' . base_url() . 'almacen/lista" class = "btn btn-sm btn-default" >Atras</a> ';
        $botones .= '<a href = "' . base_url() . 'almacen/nuevo" class = "btn btn-sm btn-default" >Nuevo</a> ';
        $this->template->add_js('media/js/almacen.js');
		$this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Editar almac�n", TRUE);
        $this->template->write_view('content', 'mantenimiento/almacen/editar', $data, TRUE);
        $this->template->render();
    }

}
