<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Empleado extends MY_Controller {

    private $_fecha_actual;
    private $_idusuario_actual;
    private $_idlocal;
    private $_rol;

    function __construct() {
        parent::__construct();
        $this->_fecha_actual = date("Y-m-d H:i:s");
        $this->load->model('empleadomodel');
		$this->load->model('contratomodel'); 		
        $this->_idusuario_actual = $this->session->userdata('idusuario');
        $this->_rol = $this->session->userdata('rol');
        $this->_idlocal = $this->session->userdata('idlocal');
        $this->template->set_template('default');
        $this->load->helper(array('url', 'funciones'));
    }

    function index() {
        $this->lista();
    }

    public function lista() {
        // Trayendo modelos
        $mensaje = $this->session->userdata('mensaje');
        if ($mensaje != "") {
            $mensaje = getHtmlMensaje(1, $mensaje);
        }
        $this->session->set_userdata('mensaje', "");

        $aListaEmpleados = $this->empleadomodel->getTodosEmpleadosContrato("e.estado_registro = 1",200);
        $data['titulo'] = "Lista de empleados";
        $data['mensaje'] = $mensaje;
        $data['aListaEmpleados'] = $aListaEmpleados;
        $botones = "";
        $botones .= '<a href = "' . base_url() . 'panel" class = "btn btn-sm btn-default" >Atras</a> ';
        $botones .= '<a href = "' . base_url() . 'empleado/nuevo" class = "btn btn-sm btn-default" >Nuevo</a> ';
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Empleados", TRUE);
        $this->template->write_view('content', 'mantenimiento/empleado/lista', $data, TRUE);
        $this->template->render();
    }

    public function json_crear_empleado() {

        $nombre = $this->input->post("nombre");        
        $direccion = $this->input->post("direccion");
		$dni = $this->input->post("dni");        
        $regimen = $this->input->post("regimen");
		$sexo = $this->input->post("sexo");        
        $estado = $this->input->post("estado");
		$sueldo = $this->input->post("sueldo");        
        $idlabor = $this->input->post("idlabor");
		$idlocal = $this->input->post("idlocal");
		$finicio = $this->input->post("finicio");
		$ffin = $this->input->post("ffin");
		$telefono = $this->input->post("telefono");        
        $email = $this->input->post("email");
		$lu_hora_inicio = $this->input->post("lu_hora_inicio");        
        $lu_hora_fin = $this->input->post("lu_hora_fin");
		$ma_hora_inicio = $this->input->post("ma_hora_inicio");        
        $ma_hora_fin = $this->input->post("ma_hora_fin");
		$mi_hora_inicio = $this->input->post("mi_hora_inicio");        
        $mi_hora_fin = $this->input->post("mi_hora_fin");
		$ju_hora_inicio = $this->input->post("ju_hora_inicio");        
		$ju_hora_fin = $this->input->post("ju_hora_fin");
		$vi_hora_inicio = $this->input->post("vi_hora_inicio");        
        $vi_hora_fin = $this->input->post("vi_hora_fin");
		$sa_hora_inicio = $this->input->post("sa_hora_inicio");        
        $sa_hora_fin = $this->input->post("sa_hora_fin");
		$do_hora_inicio = $this->input->post("do_hora_inicio");        
        $do_hora_fin = $this->input->post("do_hora_fin"); 
		
		$lunes=0;
		$martes=0;
		$miercoles=0;
		$jueves=0;
		$viernes=0;
		$sabado=0;
		$domingo=0;
		
		if($lu_hora_inicio > 0 && $lu_hora_fin > 0){
			$lunes=1;
		}
		if($ma_hora_inicio > 0 && $ma_hora_fin > 0){
			$martes=1;
		}
		if($mi_hora_inicio > 0 && $mi_hora_fin > 0){
			$miercoles=1;
		}
		if($ju_hora_inicio > 0 && $ju_hora_fin > 0){
			$jueves=1;
		}
		if($vi_hora_inicio > 0 && $vi_hora_fin > 0){
			$viernes=1;
		}
		if($sa_hora_inicio > 0 && $sa_hora_fin > 0){
			$sabado=1;
		}
		if($do_hora_inicio > 0 && $do_hora_fin > 0){
			$domingo=1;
		}
		
		$msg_error = "";
        // Validar si existe el id de tarea
        $auxEmpleado = array();
        $oEmpleado = array();
        $auxEmpleado = $this->empleadomodel->getExiste("rh_dni", $dni, 0, 1);

        if ($auxEmpleado->cantidad == 0) {
            $oEmpleado = array(
                "rh_nombre" => $nombre,
				"rh_dni" => $dni,
				"rh_sexo" => $sexo,
				"rh_telefono" => $telefono,
				"rh_correo" => $email,
                "rh_direccion" => $direccion,
				"estado_registro" => $estado
            );
            $idempleado = $this->empleadomodel->guardar($oEmpleado);
           
            if ($idempleado < 1) {
                $msg_error.="No se pudo registrar";
            }else{
				$oContrato=array();
				$oContrato=array(
				"idempleado" => $idempleado,
				"idlabor" => $idlabor,
				"idlocal" => $idlocal,
				"sueldo" => $sueldo,
				"lunes" => $lunes,
                "martes" => $martes,
				"miercoles" => $miercoles,
				"jueves" => $jueves,
				"viernes" => $viernes,
				"sabado" => $sabado,
				"domingo" => $domingo,
				"lu_hora_inicio" => $lu_hora_inicio,
				"lu_hora_fin" => $lu_hora_fin,
				"ma_hora_inicio" => $ma_hora_inicio,
				"ma_hora_fin" => $ma_hora_fin,
				"mi_hora_inicio" => $mi_hora_inicio,
				"mi_hora_fin" => $mi_hora_fin,
				"ju_hora_inicio" => $ju_hora_inicio,
				"ju_hora_fin" => $ju_hora_fin,
				"vi_hora_inicio" => $vi_hora_inicio,
				"vi_hora_fin" => $vi_hora_fin,
				"sa_hora_inicio" => $sa_hora_inicio,
				"sa_hora_fin" => $sa_hora_fin,
				"do_hora_inicio" => $do_hora_inicio,
				"do_hora_fin" => $do_hora_fin,
				"fecha_inicio" => getFechaEn2($finicio),
				"fecha_fin" => getFechaEn2($ffin)
				);
				$idcontrato = $this->contratomodel->guardar($oContrato);
				if($idcontrato < 0){
					$msj.="No se pudo registrar el contrato";
				}
			}
        } else {
            $msg_error.="El dni ingresado ya existe";
        }
        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', utf8_encode("El empleado se cre� exitosamente"));
        }
        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    public function json_editar_empleado() {

        $idempleado = $this->input->post("idempleado");
        $nombre = $this->input->post("nombre");        
        $direccion = $this->input->post("direccion");
		$dni = $this->input->post("dni");        
        $regimen = $this->input->post("regimen");
		$sexo = $this->input->post("sexo");        
        $estado = $this->input->post("estado");
		$sueldo = $this->input->post("sueldo");        
        $idlabor = $this->input->post("idlabor");
		$idlocal = $this->input->post("idlocal");
		$finicio = $this->input->post("finicio");
		$ffin = $this->input->post("ffin");
		$telefono = $this->input->post("telefono");        
        $email = $this->input->post("email");
		$lu_hora_inicio = $this->input->post("lu_hora_inicio");        
        $lu_hora_fin = $this->input->post("lu_hora_fin");
		$ma_hora_inicio = $this->input->post("ma_hora_inicio");        
        $ma_hora_fin = $this->input->post("ma_hora_fin");
		$mi_hora_inicio = $this->input->post("mi_hora_inicio");        
        $mi_hora_fin = $this->input->post("mi_hora_fin");
		$ju_hora_inicio = $this->input->post("ju_hora_inicio");        
		$ju_hora_fin = $this->input->post("ju_hora_fin");
		$vi_hora_inicio = $this->input->post("vi_hora_inicio");        
        $vi_hora_fin = $this->input->post("vi_hora_fin");
		$sa_hora_inicio = $this->input->post("sa_hora_inicio");        
        $sa_hora_fin = $this->input->post("sa_hora_fin");
		$do_hora_inicio = $this->input->post("do_hora_inicio");        
        $do_hora_fin = $this->input->post("do_hora_fin"); 
		
		$lunes=0;
		$martes=0;
		$miercoles=0;
		$jueves=0;
		$viernes=0;
		$sabado=0;
		$domingo=0;
		
		if($lu_hora_inicio > 0 && $lu_hora_fin > 0){
			$lunes=1;
		}
		if($ma_hora_inicio > 0 && $ma_hora_fin > 0){
			$martes=1;
		}
		if($mi_hora_inicio > 0 && $mi_hora_fin > 0){
			$miercoles=1;
		}
		if($ju_hora_inicio > 0 && $ju_hora_fin > 0){
			$jueves=1;
		}
		if($vi_hora_inicio > 0 && $vi_hora_fin > 0){
			$viernes=1;
		}
		if($sa_hora_inicio > 0 && $sa_hora_fin > 0){
			$sabado=1;
		}
		if($do_hora_inicio > 0 && $do_hora_fin > 0){
			$domingo=1;
		}
		
		$msg_error = "";
        // Validar si existe el id de tarea
        
			$oEmpleado = array();
			$oEmpleado = array(
					"rh_nombre" => $nombre,
					"rh_dni" => $dni,
					"rh_sexo" => $sexo,
					"rh_telefono" => $telefono,
					"rh_correo" => $email,
					"rh_direccion" => $direccion,
					"estado_registro" => $estado
			);
            $this->empleadomodel->editar($idempleado,$oEmpleado);
           $oContratoAux=$this->contratomodel->getdata(array("idcontrato"),"idempleado = ".$idempleado,1);
		   $oContrato=array();
				$oContrato=array(
				"idempleado" => $idempleado,
				"idlabor" => $idlabor,
				"idlocal" => $idlocal,
				"sueldo" => $sueldo,
				"lunes" => $lunes,
                "martes" => $martes,
				"miercoles" => $miercoles,
				"jueves" => $jueves,
				"viernes" => $viernes,
				"sabado" => $sabado,
				"domingo" => $domingo,
				"lu_hora_inicio" => $lu_hora_inicio,
				"lu_hora_fin" => $lu_hora_fin,
				"ma_hora_inicio" => $ma_hora_inicio,
				"ma_hora_fin" => $ma_hora_fin,
				"mi_hora_inicio" => $mi_hora_inicio,
				"mi_hora_fin" => $mi_hora_fin,
				"ju_hora_inicio" => $ju_hora_inicio,
				"ju_hora_fin" => $ju_hora_fin,
				"vi_hora_inicio" => $vi_hora_inicio,
				"vi_hora_fin" => $vi_hora_fin,
				"sa_hora_inicio" => $sa_hora_inicio,
				"sa_hora_fin" => $sa_hora_fin,
				"do_hora_inicio" => $do_hora_inicio,
				"do_hora_fin" => $do_hora_fin,
				"fecha_inicio" => getFechaEn2($finicio),
				"fecha_fin" => getFechaEn2($ffin)
				);
				$this->contratomodel->editar(intval($oContratoAux->idcontrato),$oContrato);
				
        if ($msg_error != "") {
            $this->session->set_userdata('mensaje', $msg_error);
        } else {
            $this->session->set_userdata('mensaje', utf8_encode("El empleado se modific� exitosamente"));
        }
        echo json_encode(array("msj" => $msg_error, "data" => array()));
    }

    public function nuevo() {
		$this->load->model('labormodel');
		$this->load->model('localmodel');
		$aLabores=$this->labormodel->getdata(array("*"),"estado_registro = 1");
		$aLocal=$this->localmodel->getdata(array("*"),"estado_registro = 1");
        $data['titulo'] = "Nuevo empleado";
		$data['aLabores'] = $aLabores;
		$data['aLocal'] = $aLocal;
        $botones = "";
        $botones .= '<a href = "' . base_url() . 'empleado/lista" class = "btn btn-sm btn-default" >Atras</a> ';
		$this->template->add_js('media/js/empleado.js');
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Nuevo empleado", TRUE);
        $this->template->write_view('content', 'mantenimiento/empleado/crear', $data, TRUE);
        $this->template->render();
    }

    public function editar($idempleado) {
        // Trayendo modelos
		$this->load->model('labormodel');
		$this->load->model('localmodel');
        $oEmpleado = $this->empleadomodel->getdata(array("*"), "idrh_empleado = " . $idempleado, 1);
        if ($oEmpleado == FALSE) {
            $this->session->set_userdata('mensaje', "No existe el tipo de documento");
        }else{
		$oContrato = $this->contratomodel->getdata(array("*"), "idempleado = " . $oEmpleado->idrh_empleado, 1);
		$aLabores=$this->labormodel->getdata(array("*"),"estado_registro = 1");
		$aLocal=$this->localmodel->getdata(array("*"),"estado_registro = 1");
        $data['aLabores'] = $aLabores;
		$data['aLocal'] = $aLocal;
		$data['titulo'] = "Editar empleado";
        $data['oEmpleado'] = $oEmpleado;
		$data['oContrato'] = $oContrato;		
		}
		
        
        $botones = "";
        $botones .= '<a href = "' . base_url() . 'empleado/lista" class = "btn btn-sm btn-default" >Atras</a> ';
        $botones .= '<a href = "' . base_url() . 'empleado/nuevo" class = "btn btn-sm btn-default" >Nuevo</a> ';
		$this->template->add_js('media/js/empleado.js');
        $this->template->write('botones', $botones, TRUE);
        $this->template->write('titleheader', "Editar empleado", TRUE);
        $this->template->write_view('content', 'mantenimiento/empleado/editar', $data, TRUE);
        $this->template->render();
    }

}
