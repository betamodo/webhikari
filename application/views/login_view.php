<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- include CSS & JS files -->
        <!-- CSS file -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/css/jNotify.jquery.css" media="screen" />

        <!-- jQuery files -->
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/jquery.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/clientes.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/jNotify.jquery.js"></script>

        <title></title>

    </head>
    <body>
        <button id="btn_mensaje">Mostrar Mensaje</button>
    </body>
</html>
