<div class="row">
    <div class="col-md-8">
        <span class="leyenda text-center">Actividades de la tarea</span>
        <hr>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <div class="row">   
            <div class="col-md-12 espacio5">
                <input type="text" id="txtnombre" class="form-control" placeholder="Nombre de la tarea">    
            </div>                
        </div><!--/row-->
        <div class="row">   
            <div class="col-md-6 espacio5">
                <div class="radio">
                    <label style="font-size: 1em;padding-left: 0px;">
                        <input type="radio" name="rbPrioridad" value="1">
                        <span class="cr"><i class="cr-icon fa fa-circle"></i></span>
                        Baja
                    </label>
                </div>
                <div class="radio">
                    <label style="font-size: 1em">
                        <input type="radio" name="rbPrioridad" value="2" checked="">
                        <span class="cr"><i class="cr-icon fa fa-circle"></i></span>
                        Media
                    </label>
                </div>
                <div class="radio">
                    <label style="font-size: 1em">
                        <input type="radio" name="rbPrioridad" value="3">
                        <span class="cr"><i class="cr-icon fa fa-circle"></i></span>
                        Alta
                    </label>
                </div>
            </div>
            <div class="col-md-6 espacio5">
                <select class="form-control" id="cbocategoria">
                    <option value="">Seleccione Categoría</option>
                    <?php
                    foreach ($aCategorias as $itemCategoria) {
                        ?>
                        <option value="<?php echo $itemCategoria->idcategoria ?>"><?php echo $itemCategoria->nombre; ?></option>
                        <?php
                    }
                    ?>
                </select>  
            </div><!--/span-->
        </div><!--/row-->

        <div class="row">   
            <?php
            $rol = $this->session->userdata('rol');
            $nombreAdmin = "¿Para el administrador?";
            if ($rol == 1) {
                $nombreAdmin = "¿Para mi?";
            }
            ?>
            <div class="col-md-6 espacio5">
                <div class="checkbox">
                    <label style="font-size: 1em; padding-left: 0px;">
                        <input type="checkbox" id="admin" value="1">
                        <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                        <?php echo $nombreAdmin; ?>
                    </label>
                </div>

            </div>                
            <?php
            if (count($aLocales) > 0) {
                ?>
                <div class="col-md-6 espacio5">

                    <select class="form-control" id="cbolocal">
                        <option value="">Seleccione local</option>
						<option value="17">Sin local, es para el gestor</option>
						<?php
                        foreach ($aLocales as $itemLocal) {
                            ?>
                            <option value="<?php echo $itemLocal->idlocal ?>"><?php echo $itemLocal->nombre; ?></option>
                            <?php
                        }
                        ?>
                    </select>

                </div>
                <?php
            }
            ?>
        </div><!--/row-->
        <div class="row">
            <div class="col-md-12 espacio5">
                <textarea id="txtdescripcion" class="form-control" rows="3" placeholder="Escribir detalle"></textarea>
            </div><!--/span-->                
        </div><!--/row-->  

        <div class="row">
            <div class="col-md-8">
                <button id="btnAgregarArchivo" class="btn btn-default classAdjuntar" type="submit">Añadir archivo</button>

                <div id="subir2" class="clearfix" style="padding-left: 7px;display: inline-block;margin-top: 9px;margin-left: -6px;">
                    <input type="hidden" id="hid_input_files" name="hid_input_files" value="file_0" />
                    <div id="remove_0" style="margin-right: 4px;" class="btn btn-sm btn-default btn_remover_files" data-id="0"><span data-id="file_0" class="glyphicon glyphicon-minus-sign btn_remover_files"></span></div>
                    <input type="file" name="file_0" id="file_0" class="custom-file-input">                        
                </div>
            </div>
        </div>
    </div><!--/span-->

    <div class="col-md-4">

    </div><!--/span-->       
</div><!--/row-->   

<div class="clearfix"></div>
<div class="row">
    <div class="col-sm-12 col-md-4 text-center">
        <button type="button" id="btnCrearTarea" class="btn btn-primary">Crear tarea</button>                
    </div>
    <div class="col-sm-12 col-md-4 text-center">
        <a href="<?php echo base_url() ?>tarea/lista" id="btnSalir" class="btn btn-link">Salir</a>
    </div>

</div>
