
<div id="busquedaAvanzada"></div>
<?php
echo $divmensaje;
$this->session->set_userdata('mensaje', "");
?>
<input type="hidden" id="idgrupo" value="<?php echo $idgrupo; ?>">
<input type="hidden" id="ids">
<div id="listaTareas">
    <ul class="list-group" id="contact-list2">
        <?php
        foreach ($aListaTareas as $itemTarea) {
            ?>
            <li class="list-group-item" style="margin-bottom: 7px;">
                <div class="col-xs-12 col-sm-6">
                    <div><span class="labelCodigo"><?php echo $itemTarea["codigo"] ?></span> | <span class="labelNomTarea pad3"><?php echo $itemTarea["nombre"] ?></span></div>
                    <div class="circulo" data-id="<?php echo $itemTarea["prioridad"]; ?>" style="background:<?php echo getColorPrioridad($itemTarea["prioridad"]) ?>;"></div>							
                    <?php
                    if ($this->session->userdata("rol") == 2 && $itemTarea["paraadmin"] == 0) {
                        if ($itemTarea["ultimoestado"] == 11 || $itemTarea["ultimoestado"] == 8) {
                            ?>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" data-codigo="<?php echo $itemTarea["codigo"] ?>" data-nombre="<?php echo $itemTarea["nombre"]; ?>" data-presu="<?php echo $itemTarea["presupuesto"]; ?>" data-id="<?php echo $itemTarea["idtarea"]; ?>" value="">
                                    <span class="cr"><i class="cr-icon glyphicon glyphicon-arrow-right"></i></span>                            
                                </label>
                            </div>
                            <?php
                        }
                    }
                    ?>
                    <?php
                    if ($this->session->userdata("rol") == 1 && $itemTarea["paraadmin"] == 1) {
                        if ($itemTarea["ultimoestado"] == 11 || $itemTarea["ultimoestado"] == 8) {
                            ?>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" data-codigo="<?php echo $itemTarea["codigo"] ?>" data-nombre="<?php echo $itemTarea["nombre"]; ?>" data-presu="<?php echo $itemTarea["presupuesto"]; ?>" data-id="<?php echo $itemTarea["idtarea"]; ?>" value="">
                                    <span class="cr"><i class="cr-icon glyphicon glyphicon-arrow-right"></i></span>                            
                                </label>
                            </div>
                            <?php
                        }
                    }
                    ?>
                    <?php
                    if ($this->session->userdata("rol") == 3 && $origen == "evaluar") {
                        if ($itemTarea["ultimoestado"] == 10) {
                            ?>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" data-nombre="<?php echo $itemTarea["nombre"]; ?>" data-presu="<?php echo $itemTarea["presupuesto"]; ?>" data-id="<?php echo $itemTarea["idtarea"]; ?>" value="">
                                    <span class="cr"><i class="cr-icon glyphicon glyphicon-arrow-right"></i></span>                            
                                </label>
                            </div>
                            <?php
                        }
                    }
                    ?>
                    <div class="labelNomTarea pad3"><img src="<?php echo $itemTarea["imagen"]; ?>"><span class="labelCodigo" style="font-size: 12px;"> <span><?php echo $itemTarea["nomestado"] ?></span> -  <span class="responsable"><?php echo getResponsable($itemTarea["ultimoestado"], $itemTarea["paraadmin"]) ?></span></span></div>
                    <div class="pad3"><span><?php echo $itemTarea["nomlocal"] ?></span> | <span><?php echo $itemTarea["nomcategoria"] ?></span> | <span><?php echo $itemTarea["nomresponsable"] ?></span></div>
                </div>
                <div class="col-xs-12 col-sm-4 numero" data-numero="2">
                    <?php
                    $nomCreador = $oUsuario[$itemTarea["idusuario_creador"]]["nombre"];
                    if (intval($itemTarea["idusuario"]) != 0) {
                        $nomCreador = $oUsuario[$itemTarea["idusuario"]]["nombre"];
                    }
                    ?>
                    <div><?php echo "<strong>CREADOR: </strong>" . $nomCreador; ?></div>
                    <div class="pad3" style="white-space: nowrap">F.REGISTRO: <?php echo $itemTarea["fregistro"] ?></div>
                    <div class="pad3" style="white-space: nowrap">F.ULT.MOD: <?php echo $itemTarea["factualizacion"] ?></div>
                </div>
                <div class="col-xs-12 col-sm-2">
                    <div class="text-left presu">
                        <?php
                        if ($this->session->userdata("rol") != 1) {
                            if ($itemTarea["presupuesto"] != "")
                                echo "S/. " . $itemTarea["presupuesto"];
                        }
                        ?>
                    </div>
                    <div class=" pull-right">
                        <div style="text-align: right;"><?php echo $itemTarea["tipo_transcurrido"] ?></div>
                        <div style="text-align: right;">
                            <?php echo $itemTarea["enlace"]; ?>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>                        
            </li> 

            <?php
        }
        ?>
    </ul>
</div>

<div class="row" style="display: none;">
    <div id="archivos" class="col-md-8">
        <input type="hidden" id="idsTareas">
        <input type="hidden" id="presuTareas">
        <button id="btnAdjuntar" class="btn btn-default classAdjuntar" type="submit">Adjuntar</button>
        <div id="subir" class="clearfix" style="padding-left: 7px;display: inline-block;margin-top: 9px;margin-left: -6px;">
            <input id="my_file_element" type="file" name="file_1" ><br clear="all"/>
            <div id="files_list"></div>                            
        </div>
    </div>
</div>

<!-- MODAL Opciones -->
<div id="myModal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" id="cabecera-modal">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 id="opciones-nomreq" class="modal-title">Enviar grupo de Presupuestos</h4>
            </div>
            <div id="opciones-contenido" class="modal-body">
                <div><input type="text" id="nomGrupo" class="form-control" placeholder="Nombre del Grupo"></div>
                <div id="panelentrada" style="  margin-top: 13px;margin-bottom: 13px;">

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" id="btnEnviarPresu" class="btn btn-sm btn-primary">Enviar Presupuesto</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<?php
//$this->session->set_userdata('filtro_local', "");
//$this->session->set_userdata('filtro_fecha_inicio', "");
//$this->session->set_userdata('filtro_fecha_fin', "");
//$this->session->set_userdata('filtro_estados', "");
//$this->session->set_userdata('filtro_monto_inicial', "");
//$this->session->set_userdata('filtro_monto_final', "");
//$this->session->set_userdata('filtro_monto_igual', "");
//$this->session->set_userdata('filtro_monto_ejecutor', "");
?>