
<?php
$idlocal = $this->session->userdata('filtro_local');
$finicio = $this->session->userdata('filtro_fecha_inicio');
$idestado = $this->session->userdata('filtro_estados');
$ffin = $this->session->userdata('filtro_fecha_fin');
$montoinicial = $this->session->userdata('filtro_monto_inicial');
$montofinal = $this->session->userdata('filtro_monto_final');
$montoigual = $this->session->userdata('filtro_monto_igual');
$idusuario = $this->session->userdata('filtro_monto_ejecutor');
?>
<div class="row">
    <div class="col-md-12">
        <span class="leyenda text-center">Búsqueda avanzada</span>
        <hr>
    </div>
</div>
<div class="row">
    <div class="col-sm-12 col-sm-3">
        <select id="select_local" class="form-control espacioControles" name="select_local">
            <?php
            $rol = $this->session->userdata('rol');
            if ($rol != 1) {
                ?>
                <option value="">Todos los locales</option>
                <?php
            }
            ?>
            <?php
            foreach ($aLocales as $itemLocal) {
                ?>
                <option value="<?php echo $itemLocal->idlocal; ?>" <?php if ($itemLocal->idlocal == $idlocal) echo "selected"; ?>><?php echo $itemLocal->nombre; ?></option>
                <?php
            }
            ?>
        </select>
    </div>
    <div class="col-sm-12 col-sm-3">
        <div class="input-group espacioControles">
            <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar"></span></span>
            <input type="text" readonly="" value="<?php echo $finicio ?>" id="finicio" class="form-control espacioControles" aria-describedby="basic-addon1" placeholder="Fecha inicio">            
        </div>

    </div>
    <div class="col-sm-12 col-sm-3">
        <div class="input-group espacioControles">
            <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar"></span></span>
            <input type="text" readonly="" id="ffin" value="<?php echo $ffin ?>" class="form-control espacioControles" aria-describedby="basic-addon1" placeholder="Fecha fin">            
        </div>
    </div>
    <div class="col-sm-12 col-sm-3">
        <select id="select_estado" class="form-control espacioControles" name="select_estado">
            <option value="" <?php if ($idestado == "") echo "selected"; ?>>Todos los estados</option>
            <option value="1" <?php if ($idestado == 1) echo "selected"; ?>>Por atender</option>
            <option value="2" <?php if ($idestado == 2) echo "selected"; ?>>Por ejecutar</option>
            <option value="3" <?php if ($idestado == 3) echo "selected"; ?>>Por corregir tarea</option>
            <option value="4" <?php if ($idestado == 4) echo "selected"; ?>>Por confirmar</option>
            <option value="5" <?php if ($idestado == 5) echo "selected"; ?>>Terminado</option>
            <option value="6" <?php if ($idestado == 6) echo "selected"; ?>>Por corregir ejecución</option>
            <!--            <option value="7">Evaluar Presupuesto</option>-->
            <option value="8" <?php if ($idestado == 8) echo "selected"; ?>>Por corregir ppto</option>
            <option value="9" <?php if ($idestado == 9) echo "selected"; ?>>Cancelado</option>
            <option value="10" <?php if ($idestado == 10) echo "selected"; ?>>Evaluar presupuesto</option>
            <option value="11" <?php if ($idestado == 11) echo "selected"; ?>>Preparar Ppto</option>
        </select>
    </div>

</div>
<div class="row">
    <?php
    if ($rol != 1) {
        ?>
        <div class="col-sm-12 col-sm-3">
            <div class="input-group espacioControles">
                <span class="input-group-addon" id="basic-addon10">S/. </span>
                <input type="text" id="montoinicial" class="form-control" value="<?php echo $montoinicial ?>" placeholder="Monto inicial"> 
            </div>
        </div>
        <div class="col-sm-12 col-sm-3">
            <div class="input-group espacioControles">
                <span class="input-group-addon" id="basic-addon20">S/. </span>
                <input type="text" id="montofinal" class="form-control" value="<?php echo $montofinal ?>" placeholder="Monto final"> 
            </div>
        </div>
        <div class="col-sm-12 col-sm-3">
            <div class="input-group espacioControles">
                <span class="input-group-addon" id="basic-addon30">S/. </span>
                <input type="text" id="montoigual" value="<?php echo $montoigual; ?>" class="form-control" placeholder="Monto igual a"> 
            </div>
        </div>  
        <?php
    }
    ?>

    <div class="col-sm-12 col-md-3">
        <select id="select_usuario" class="form-control espacioControles" name="select_usuario">
            <?php
            $rol = $this->session->userdata('rol');
            ?>
            <option value="">Todos los ejecutores</option>
            <?php
            ?>
            <?php
            foreach ($aGestores as $itemGestor) {
                ?>
                <option value="<?php echo $itemGestor->idusuario; ?>" <?php if ($itemGestor->idusuario == $idusuario) echo "selected"; ?>><?php echo $itemGestor->nombre; ?></option>
                <?php
            }
            ?>
        </select>
    </div>
    <div class="col-sm-12 col-md-1">
        <button id="btnBuscar" class="btn btn-sm btn-primary" style="width: 100%;display: inline-block;line-height: 22px;margin-bottom: 5px;">Buscar</button> 
    </div>
    <div class="col-sm-12 col-md-1">
        <button id="btnlimpiar" class="btn btn-sm btn-warning" style="width: 100%;line-height: 22px;">Limpiar</button>
    </div>
</div>
</div>
<br>
<hr>
<script>
    $("#btnlimpiar").click(function () {
        $("#select_local").val("");
        $("#finicio").val("");
        $("#ffin").val("");
        $("#montoinicial").val("");
        $("#montofinal").val("");
        $("#montoigual").val("");
    });
    $("#btnBuscar").click(function () {
        var idlocal = $("#select_local").val();
        var idestado = $("#select_estado").val();
        var finicio = $("#finicio").val();
        var ffin = $("#ffin").val();
        var montoinicial = $("#montoinicial").val();
        var montofinal = $("#montofinal").val();
        var montoigual = $("#montoigual").val();
        var idusuario = $("#select_usuario").val();

        var formData = new FormData();
        formData.append('idlocal', idlocal);
        formData.append('idestado', idestado);
        formData.append('finicio', finicio);
        formData.append('ffin', ffin);
        formData.append('montoinicial', montoinicial);
        formData.append('montofinal', montofinal);
        formData.append('montoigual', idlocal);
        formData.append('idusuario', idusuario);
        $("#listaTareas").html("<div class='cargando'></div>");
        $.post("json_buscar", {idusuario: idusuario, idestado: idestado, idlocal: idlocal, finicio: finicio, ffin: ffin, montoinicial: montoinicial, montofinal: montofinal, montoigual: montoigual}, function (data) {
            $("#listaTareas").html(data);
        });
    });
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
</script>
<script>
    $(document).ready(function () {
        $("#finicio").datepicker();
        $("#ffin").datepicker();
    });
</script>