<div id="mensajeaux"></div>
<?php
if ($divmensaje != "") {
    echo getHtmlMensaje(2, $divmensaje);
} else {
    ?>
    <div class="row">
        <div class="col-md-8">
            <span class="leyenda text-center">Actividades de la tarea</span>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="row">   
                <div class="col-md-12 espacio5">
                    <ul class="list-group" id="contact-list2" style="margin-bottom: 2px;">
                        <li class="list-group-item">
                            <div class="col-xs-12 col-sm-8">
                                <div><span class="labelCodigo"><?php echo $objTarea["codigo"] ?></span> | <span><?php echo $objTarea["nomlocal"] ?></span></div>

                                <div class="labelNomTarea pad3"><?php echo $objTarea["nombre"] ?></div>
                                <div class="pad3"><span><?php echo $objTarea["nomcategoria"] ?></span> | <span><?php echo $objTarea["nomresponsable"] ?></span></div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div><img src="<?php echo $objTarea["imagen"] ?>"><span class="actividad"><?php echo $objTarea["nomestado"] ?></span></div>
                                <div class="pad3">F.REGISTRO: <?php echo $objTarea["fregistro"] ?></div>
                                <div class="pad3">F.ULT.MOD: <?php echo $objTarea["factualizacion"] ?></div>	
                            </div>
                            <div class="clearfix"></div>                        
                        </li>  
                        <li class="list-group-item">
                            <div class="col-xs-12 col-sm-12">
                                <div class="pad3"><strong>Descripción:</strong></div>
                                <p><?php echo $objTarea["descripcion"]; ?></p>
                            </div>
                            <div class="clearfix"></div>
                        </li>
                        <?php
                        if ($aArchivosTarea != FALSE) {
                            ?>
                            <li class="list-group-item">
                                <div id="mntrs" class="clearfix">
                                    <?php
                                    foreach ($aArchivosTarea as $itemArchivoTarea) {
                                        $info = pathinfo(getcwd() . "/" . $itemArchivoTarea->url);
                                        $extension = $info['extension'];
                                        if (strtoupper($extension) == "JPG" || strtoupper($extension) == "JPEG" || strtoupper($extension) == "PNG" || strtoupper($extension) == "GIF") {
                                            ?>
                                            <div class="col-xs-12 col-sm-6 col-md-4">
                                                <a class="fancybox" rel="gallery1" href="<?php echo base_url() . "media/" . $itemArchivoTarea->url ?>">
                                                    <div class="clearfix thumbnail">
                                                        <div class="row">
                                                            <div class="col-xs-3">
                                                                <img width="50" height="50" src="<?php echo base_url() . "media/archivos/thumbs/" . substr(trim($itemArchivoTarea->url), 9) ?>" class="img-responsive">
                                                            </div>
                                                            <div class="col-xs-9">
                                                                <div>
                                                                    <p><?php echo substr(substr(trim($itemArchivoTarea->url), 9), 0, 10) . "." . $extension; ?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <?php
                                        } else {
                                            ?>
                                            <div class="col-xs-12 col-sm-6 col-md-4">
                                                <a class="fancybox" rel="gallery1" href="<?php echo base_url() . "media/" . $itemArchivoTarea->url ?>">
                                                    <div class="clearfix thumbnail">
                                                        <div class="row">
                                                            <div class="col-xs-3">
                                                                <img width="50" height="50" src="<?php echo base_url() . "media/img/" . $extension . ".png" ?>" class="img-responsive">
                                                            </div>
                                                            <div class="col-xs-9">
                                                                <div>
                                                                    <p><?php echo substr(substr(trim($itemArchivoTarea->url), 9), 0, 10) . "." . $extension; ?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>    
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </li>
                            <?php
                        }
                        ?>
                    </ul> 

                </div>                
            </div><!--/row-->

        </div><!--/span-->

        <div class="col-md-4">

        </div><!--/span-->       
    </div><!--/row-->   
    <div class="row">
        <div class="col-md-8">
            <span class="leyenda text-center">Actividades de la tarea</span>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="qa-message-list" id="wallmessages">
                <?php
                foreach ($aTareaEstados as $itemTareaEstado) {
                    if ($itemTareaEstado["imagen"] == "") {
                        $foto = "default.png";
                    } else {
                        $foto = $itemTareaEstado["imagen"];
                    }
                    ?>
                    <div class="message-item" id="m16">
                        <div class="message-inner">
                            <div class="message-head clearfix">
                                <div class="avatar pull-left"><a href="#"><img width="40" height="40" src="<?php echo base_url() . "media/archivos/avatars/thumbs/" . $foto ?>"></a></div>
                                <div class="pull-left">
                                    <div class="user-detail">
                                        <h5 class="handle"><?php echo $itemTareaEstado["nomestado"] ?></h5>
                                        <div class="post-meta">
                                            <span><?php echo $itemTareaEstado["fregistro"] . " - Duró: " . $itemTareaEstado["transcurrido"]; ?></span>
                                            <span><?php echo $itemTareaEstado["nomusuario"] ?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="pull-right">
                                    <span>
                                        <?php
                                        if ($this->session->userdata("rol") != 1) {
                                            echo "Ppto: S/. " . $itemTareaEstado["presupuesto"];
                                        } else {
                                            echo "";
                                        }
                                        ?>
                                    </span>
                                </div>
                            </div>
                            <div class="qa-message-content">
                                <?php echo $itemTareaEstado["comentario"]; ?>
                            </div>
                            <div class="contResp">
                                <div id="chat_respuestas_<?php echo $itemTareaEstado["idtarea_estado"]; ?>">
                                    <?php
                                    foreach ($itemTareaEstado["aRespuestas"] as $itemRespuesta) {
                                        ?>
                                        <div class="responder clearifx">
                                            <span class="spn-avatar pull-left media-object" href="#">
                                                <i class="glyphicon glyphicon-user"></i>
                                            </span>

                                            <div class="media-body">
                                                <b><?php echo $oUsuario[$itemRespuesta->idusuario_creador]["nombre"] ?></b>
                                                <p>
                                                    <?php echo $itemRespuesta->texto; ?>
                                                </p>
                                            </div>    
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div class="btn-resp clearfix">
                                    <a class="link-responder" data-id="<?php echo $itemTareaEstado["idtarea_estado"] ?>" href="javascript:void(0);">Responder</a>
                                </div>
                                <div id="form_respuesta_<?php echo $itemTareaEstado["idtarea_estado"] ?>"></div>
                            </div>
                            <?php
                            if ($itemTareaEstado["archivos"] != "") {
                                ?>
                                <div id="mntrs" class="clearfix">
                                    <?php
                                    $aArchivos = explode("|", trim($itemTareaEstado["archivos"], "|"));

                                    foreach ($aArchivos as $itemArchivo) {
                                        $info = pathinfo(getcwd() . "/" . $itemArchivo);
                                        $extension = $info['extension'];
                                        if (strtoupper($extension) == "JPG" || strtoupper($extension) == "JPEG" || strtoupper($extension) == "PNG" || strtoupper($extension) == "GIF") {
                                            ?>
                                            <div class="col-xs-12 col-sm-6 col-md-4">
                                                <a class="fancybox" rel="gallery1" href="<?php echo base_url() . "media/" . $itemArchivo ?>">
                                                    <div class="clearfix thumbnail">
                                                        <div class="row">
                                                            <div class="col-xs-3">
                                                                <img width="50" height="50" src="<?php echo base_url() . "media/archivos/thumbs/" . substr(trim($itemArchivo), 9) ?>" class="img-responsive">
                                                            </div>
                                                            <div class="col-xs-9">
                                                                <div>
                                                                    <p><?php echo substr(substr(trim($itemArchivo), 9), 0, 10) . "." . $extension; ?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <?php
                                        } else {
                                            ?>
                                            <div class="col-xs-12 col-sm-6 col-md-4">
                                                <a class="fancybox" rel="gallery1" href="<?php echo base_url() . "media/" . $itemArchivo ?>">
                                                    <div class="clearfix thumbnail">
                                                        <div class="row">
                                                            <div class="col-xs-3">
                                                                <img width="50" height="50" src="<?php echo base_url() . "media/img/" . $extension . ".png" ?>" class="img-responsive">
                                                            </div>
                                                            <div class="col-xs-9">
                                                                <div>
                                                                    <p><?php echo substr(substr(trim($itemArchivo), 9), 0, 10) . "." . $extension; ?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php
                }
                ?>

            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-md-8 espacio5">
            <input type="hidden" id="txtidtarea" value="<?php echo $objTarea["idtarea"]; ?>">
            <textarea id="txtdescripcion" class="form-control" rows="3" placeholder="Escribir detalle"></textarea>
        </div><!--/span-->                
    </div><!--/row-->  

    <div class="row">
        <div class="col-md-8">
            <button id="btnAgregarArchivo" class="btn btn-default classAdjuntar" type="submit">Añadir archivo</button>

            <div id="subir2" class="clearfix" style="padding-left: 7px;display: inline-block;margin-top: 9px;margin-left: -6px;">
                <input type="hidden" id="hid_input_files" name="hid_input_files" value="file_0" />
                <div id="remove_0" style="margin-right: 4px;" class="btn btn-sm btn-default btn_remover_files" data-id="0"><span data-id="file_0" class="glyphicon glyphicon-minus-sign btn_remover_files"></span></div>
                <input type="file" name="file_0" id="file_0" class="custom-file-input">                        
            </div>
        </div>
    </div>

    <div class="row" style="margin-top: 12px;">
        <div class="col-sm-12 col-md-4 text-center">
            <button type="button" id="btnConforme" data-id="1" class="btn btn-primary">Conforme</button>                
        </div>
        <div class="col-sm-12 col-md-4 text-center">
            <button type="button" id="btnNoConforme" data-id="0" class="btn btn-link">No conforme</button>
        </div>

    </div>
    <?php
}
?>