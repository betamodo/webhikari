<div class="row" style="margin-bottom: 12px;">
    <div class="col-md-9">
        <div style="font-weight: 700;">TAREA:</div>
        <div style="font-size: 12px;"><?php echo $objTarea->nombre; ?></div>
        
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <select id="select_estado" class="form-control espacioControles" name="select_estado">
            <option value="1" <?php if ($idestado == 1) echo "selected"; ?>>Por atender</option>
            <option value="2" <?php if ($idestado == 2) echo "selected"; ?>>Por ejecutar</option>
            <option value="3" <?php if ($idestado == 3) echo "selected"; ?>>Por corregir tarea</option>
            <option value="4" <?php if ($idestado == 4) echo "selected"; ?>>Por confirmar</option>
            <option value="5" <?php if ($idestado == 5) echo "selected"; ?>>Terminado</option>
            <option value="6" <?php if ($idestado == 6) echo "selected"; ?>>Por corregir ejecución</option>
            <option value="8" <?php if ($idestado == 8) echo "selected"; ?>>Por corregir ppto</option>
            <option value="9" <?php if ($idestado == 9) echo "selected"; ?>>Cancelado</option>
            <option value="10" <?php if ($idestado == 10) echo "selected"; ?>>Evaluar presupuesto</option>
            <option value="11" <?php if ($idestado == 11) echo "selected"; ?>>Preparar Ppto</option>
        </select>
    </div>
    <div class="col-md-4">
        <button id="btncambiaestado" class="btn btn-sm btn-default" data-idtarea="<?php echo $idtarea; ?>" style="padding: 8px;">Cambiar</button>
    </div>
</div>