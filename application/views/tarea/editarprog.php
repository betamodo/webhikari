<div class="row">
    <div class="col-md-8">
        <span class="leyenda text-center">Datos de la tarea</span>
        <hr>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <div class="row">   
            <div class="col-md-12 espacio5">
                <input type="hidden" id="txtidprogramacion" value="<?php echo $aProgramacion->idprogramacion; ?>" >    
                <input type="hidden" id="txtidtarea" value="<?php echo $aTareaProg->idtareaprog; ?>" >    
                <input type="text" id="txtnombre" value="<?php echo $aTareaProg->nombre; ?>" class="form-control" placeholder="Nombre de la tarea">    
            </div>                
        </div><!--/row-->
        <div class="row">   
            <div class="col-md-6 espacio5">
                <div class="radio">
                    <label style="font-size: 1em;padding-left: 0px;">
                        <input type="radio" name="rbPrioridad" value="1" <?php if ($aTareaProg->prioridad == 1) echo "checked"; ?>>
                        <span class="cr"><i class="cr-icon fa fa-circle"></i></span>
                        Baja
                    </label>
                </div>
                <div class="radio">
                    <label style="font-size: 1em">
                        <input type="radio" name="rbPrioridad" value="2" <?php if ($aTareaProg->prioridad == 2) echo "checked"; ?>>
                        <span class="cr"><i class="cr-icon fa fa-circle"></i></span>
                        Media
                    </label>
                </div>
                <div class="radio">
                    <label style="font-size: 1em">
                        <input type="radio" name="rbPrioridad" value="3" <?php if ($aTareaProg->prioridad == 3) echo "checked"; ?>>
                        <span class="cr"><i class="cr-icon fa fa-circle"></i></span>
                        Alta
                    </label>
                </div>
            </div>
            <div class="col-md-6 espacio5">
                <select class="form-control" id="cbocategoria">
                    <option value="">Seleccione Categoría</option>
                    <?php
                    foreach ($aCategorias as $itemCategoria) {
                        ?>
                        <option value="<?php echo $itemCategoria->idcategoria ?>" <?php if ($aTareaProg->idcategoria == $itemCategoria->idcategoria) echo "selected"; ?>><?php echo $itemCategoria->nombre; ?></option>
                        <?php
                    }
                    ?>
                </select>  
            </div><!--/span-->
        </div><!--/row-->

        <div class="row">   

            <div class="col-md-6 espacio5">

                <div class="checkbox">
                    <label style="font-size: 1em; padding-left: 0px;">
                        <input type="checkbox" id="admin" value="1" <?php if ($aTareaProg->paraadmin == 1) echo "checked"; ?>>
                        <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                        ¿Para el administrador?
                    </label>
                </div>

            </div>
            <?php
            if (count($aLocales) > 0) {
                ?>
                <div class="col-md-6 espacio5">

                    <select class="form-control" id="cbolocal">
                        <option value="">Seleccione local</option>
                        <?php
                        foreach ($aLocales as $itemLocal) {
                            ?>
                            <option value="<?php echo $itemLocal->idlocal ?>" <?php if ($aTareaProg->idlocal == $itemLocal->idlocal) echo "selected"; ?>><?php echo $itemLocal->nombre; ?></option>
                            <?php
                        }
                        ?>
                    </select>

                </div>
                <?php
            }
            ?>
        </div><!--/row-->
        <div class="row">
            <div class="col-md-12 espacio5">
                <textarea id="txtdescripcion" class="form-control" rows="3" placeholder="Escribir detalle"><?php echo $aTareaProg->descripcion; ?></textarea>
            </div><!--/span-->                
        </div><!--/row-->  

    </div><!--/span-->

    <div class="col-md-4">

    </div><!--/span-->       
</div><!--/row-->  

<br>

<div class="row">
    <div class="col-md-8">
        <span class="leyenda text-center">Datos de programación</span>
        <hr>
    </div>
</div>

<div class="row">
    <div class="col-md-8">
        <div class="row">   
            <div class="col-md-6 espacio5">
                <div class="input-group espacioControles">
                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar"></span></span>
                    <input type="text" readonly="" id="finicio" value="<?php echo getFechaEs2($aProgramacion->fecha_inicio); ?>" class="form-control espacioControles" aria-describedby="basic-addon1" placeholder="Fecha inicio">            
                </div>

            </div>
            <div class="col-md-6 espacio5">
                <div class="input-group espacioControles">
                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar"></span></span>
                    <input type="text" readonly="" id="ffin" value="<?php echo getFechaEs2($aProgramacion->fecha_fin); ?>" class="form-control espacioControles" aria-describedby="basic-addon1" placeholder="Fecha fin">            
                </div>
            </div>
        </div><!--/row-->

        <div class="row">   

            <div class="col-md-12 espacio5">

                <select class="form-control" id="cborepeticion">
                    <option value="">Seleccione repetición</option>
                    <option value="1" <?php if ($aProgramacion->repeticion == 1) echo "selected"; ?>>Diario</option>
                    <option value="2" <?php if ($aProgramacion->repeticion == 2) echo "selected"; ?>>Semanal</option>
                    <option value="3" <?php if ($aProgramacion->repeticion == 3) echo "selected"; ?>>Mensual</option>
                    <option value="4" <?php if ($aProgramacion->repeticion == 4) echo "selected"; ?>>Anual</option>
                </select>

            </div>

            <div class="row">   

                <div class="col-md-12 espacio5">
                    <div class="checkbox">
                        <label style="font-size: 1.5em">
                            <input id="lunes" type="checkbox" value="<?php echo $aProgramacion->lunes; ?>" <?php if ($aProgramacion->lunes == 1) echo "checked"; ?>>
                            <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                            LUNES
                        </label>
                    </div>
                    <div class="checkbox">
                        <label style="font-size: 1.5em">
                            <input id="martes" type="checkbox" value="<?php echo $aProgramacion->martes; ?>" <?php if ($aProgramacion->martes == 1) echo "checked"; ?>>
                            <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                            MARTES
                        </label>
                    </div>
                    <div class="checkbox">
                        <label style="font-size: 1.5em">
                            <input id="miercoles" type="checkbox" value="<?php echo $aProgramacion->miercoles; ?>" <?php if ($aProgramacion->miercoles == 1) echo "checked"; ?>>
                            <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                            MIERCOLES
                        </label>
                    </div>
                    <div class="checkbox">
                        <label style="font-size: 1.5em">
                            <input id="jueves" type="checkbox" value="<?php echo $aProgramacion->jueves; ?>" <?php if ($aProgramacion->jueves == 1) echo "checked"; ?>>
                            <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                            JUEVES
                        </label>
                    </div>                    

                    <div class="checkbox">
                        <label style="font-size: 1.5em">
                            <input id="viernes" type="checkbox" value="<?php echo $aProgramacion->viernes; ?>" <?php if ($aProgramacion->viernes == 1) echo "checked"; ?>>
                            <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                            VIERNES
                        </label>
                    </div>
                    <br>
                    <div class="checkbox">
                        <label style="font-size: 1.5em">
                            <input id="sabado" type="checkbox" value="<?php echo $aProgramacion->sabado; ?>" <?php if ($aProgramacion->sabado == 1) echo "checked"; ?>>
                            <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                            SABADO
                        </label>
                    </div>
                    <div class="checkbox">
                        <label style="font-size: 1.5em">
                            <input id="domingo" type="checkbox" value="<?php echo $aProgramacion->domingo; ?>" <?php if ($aProgramacion->domingo == 1) echo "checked"; ?>>
                            <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                            DOMINGO
                        </label>
                    </div>
                </div>   

            </div><!--/row-->

        </div><!--/row-->
        <ul class="list-group" id="contact-list2" style="margin-bottom: 2px;">
            <?php
            if ($aArchivos != FALSE) {
                ?>
                <li class="list-group-item">
                    <div id="mntrs" class="clearfix">
                        <?php
                        foreach ($aArchivos as $itemArchivoTarea) {
                            $info = pathinfo(getcwd() . "/" . $itemArchivoTarea->url);
                            $extension = $info['extension'];
                            if (strtoupper($extension) == "JPG" || strtoupper($extension) == "JPEG" || strtoupper($extension) == "PNG" || strtoupper($extension) == "GIF") {
                                ?>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <a class="fancybox" rel="gallery1" href="<?php echo base_url() . "media/" . $itemArchivoTarea->url ?>">
                                        <div class="clearfix thumbnail">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <img width="50" height="50" src="<?php echo base_url() . "media/archivos/thumbs/" . substr(trim($itemArchivoTarea->url), 9) ?>" class="img-responsive">
                                                </div>
                                                <div class="col-xs-9">
                                                    <div>
                                                        <p><?php echo substr(substr(trim($itemArchivoTarea->url), 9), 0, 10) . "." . $extension; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <?php
                            } else {
                                ?>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <a class="fancybox" rel="gallery1" href="<?php echo base_url() . "media/" . $itemArchivoTarea->url ?>">
                                        <div class="clearfix thumbnail">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <img width="50" height="50" src="<?php echo base_url() . "media/img/" . $extension . ".png" ?>" class="img-responsive">
                                                </div>
                                                <div class="col-xs-9">
                                                    <div>
                                                        <p><?php echo substr(substr(trim($itemArchivoTarea->url), 9), 0, 10) . "." . $extension; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>    
                                <?php
                            }
                        }
                        ?>
                    </div>
                </li>
                <?php
            }
            ?>
        </ul> 
        <hr>
        <div class="row">
            <div class="col-md-8">
                <button id="btnAgregarArchivo" class="btn btn-default classAdjuntar" type="submit">Añadir archivo</button>

                <div id="subir2" class="clearfix" style="padding-left: 7px;display: inline-block;margin-top: 9px;margin-left: -6px;">
                    <input type="hidden" id="hid_input_files" name="hid_input_files" value="file_0" />
                    <div id="remove_0" style="margin-right: 4px;" class="btn btn-sm btn-default btn_remover_files" data-id="0"><span data-id="file_0" class="glyphicon glyphicon-minus-sign btn_remover_files"></span></div>
                    <input type="file" name="file_0" id="file_0" class="custom-file-input">                        
                </div>
            </div>
        </div>
    </div><!--/span-->

    <div class="col-md-4">

    </div><!--/span-->       
</div><!--/row-->  




<div class="clearfix"></div>
<div class="row">
    <div class="col-sm-12 col-md-4 text-center">
        <button type="button" id="btnProgramar" class="btn btn-primary">Programar</button>                
    </div>
    <div class="col-sm-12 col-md-4 text-center">
        <a href="<?php echo base_url() ?>tarea/lista" id="btnSalir" class="btn btn-link">Salir</a>
    </div>

</div>
<script>
    $(document).ready(function () {
        $.datepicker.regional['es'] = {
            closeText: 'Cerrar',
            prevText: '<Ant',
            nextText: 'Sig>',
            currentText: 'Hoy',
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
            dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
            weekHeader: 'Sm',
            dateFormat: 'dd/mm/yy',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        };
        $.datepicker.setDefaults($.datepicker.regional['es']);
        $("#finicio").datepicker();
        $("#ffin").datepicker();
    });
</script>