
<div id="busquedaAvanzada"></div>
<?php
$this->session->set_userdata('mensaje', "");
?>
<div id="listaTareas">
    <ul class="list-group" id="contact-list2">
        <?php
        foreach ($aGrupos as $itemGrupo) {
            ?>
            <li class="list-group-item" style="margin-bottom: 7px;">
                <div class="col-xs-12 col-sm-3">

                    <div class="labelNomTarea"><?php echo $itemGrupo->nombre; ?></div>
                    <div class="labelCodigo"><?php echo $itemGrupo->nomcreador; ?></div>
                    <div><?php echo getFechaEs($itemGrupo->fecha_envio); ?></div>
                    <div><?php if ($itemGrupo->estado_registro == 3) echo '<span class="label label-danger">Evaluado</span>' ?></div>
                </div>
				<div class="col-xs-12 col-sm-2">
                    <div style="  text-decoration: underline;">ENVIADO</div>
                    <div><span class="labelCodigo" style="  font-size: 16px;"><?php echo intval($itemGrupo->cantidadtareas); ?></span> tarea(s)</div>
                    <div><span class="labelCodigo" style="  font-size: 17px;">S./ <?php echo doubleval($itemGrupo->montototal); ?></span></div>

                </div>
                <div class="col-xs-12 col-sm-2">
                    <div style="  text-decoration: underline;">FALTA ATENDER</div>
                    <div><span class="labelCodigo" style="  font-size: 16px;"><?php 
					echo intval($itemGrupo->cantidadtareas)-(intval($itemGrupo->cant_rechazados)+intval($itemGrupo->cantidad_aprobados));
					?></span> tarea(s)</div>
                    <div><span class="labelCodigo" style="  font-size: 17px;">S./ <?php 
					echo intval($itemGrupo->montototal)-(intval($itemGrupo->monto_rechazados)+intval($itemGrupo->monto_aprobado));
					 ?></span></div> 
                </div> 
				<div class="col-xs-12 col-sm-2">                    
                        <div style="  text-decoration: underline;">RECHAZADOS</div>
                        <div><span class="labelCodigo" style="  font-size: 16px;"><?php echo $itemGrupo->cant_rechazados; ?></span> tarea(s)</div>
                        <div><span class="labelCodigo" style="  font-size: 17px;">S./ <?php echo doubleval($itemGrupo->monto_rechazados); ?></span></div>                    
                </div> 
                <div class="col-xs-12 col-sm-2">
                    <?php if ($itemGrupo->estado_registro == 3) { ?>
                        <div style="  text-decoration: underline;">APROBADOS</div>
                        <div><span class="labelCodigo" style="  font-size: 16px;"><?php echo $itemGrupo->cantidad_aprobados; ?></span> tarea(s)</div>
                        <div><span class="labelCodigo" style="  font-size: 17px;">S./ <?php echo doubleval($itemGrupo->monto_aprobado); ?></span></div>
                    <?php } ?>
                </div> 
                <div class="col-xs-12 col-sm-1 text-right">
                    <?php if ($itemGrupo->estado_registro == 1) { ?>
                        <div><a href="<?php echo base_url() . "tarea/evaluargrupo/" . $itemGrupo->idgrupo; ?>" style="text-decoration:underline;">Evaluar</a></div>                                  
                    <?php } else {
                        ?>
                        <div><a href="<?php echo base_url() . "tarea/evaluargrupo/" . $itemGrupo->idgrupo; ?>" style="text-decoration:underline;">Ver</a></div>                                  
                    <?php }
                    ?>
                </div> 
                <div class="clearfix"></div>                        
            </li> 

            <?php
        }
        ?>
    </ul>
</div>

<div class="row" style="display: none;">
    <div id="archivos" class="col-md-8">
        <button id="btnAdjuntar" class="btn btn-default classAdjuntar" type="submit">Adjuntar</button>

        <div id="subir" class="clearfix" style="padding-left: 7px;display: inline-block;margin-top: 9px;margin-left: -6px;">
            <input id="my_file_element" type="file" name="file_1" ><br clear="all"/>
            <div id="files_list"></div>                            
        </div>
    </div>
</div>