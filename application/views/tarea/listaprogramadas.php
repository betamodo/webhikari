<div id="busquedaAvanzada"></div>
<div id="listaTareas">
    <ul class="list-group" id="contact-list2">
        <?php
        foreach ($aProgramacion as $itemProgramacion) {
            ?>
            <li class="list-group-item" style="margin-bottom: 7px;">
                <div class="col-xs-12 col-sm-5">
                    <div><span class="labelCodigo"><?php echo $itemProgramacion["codigo"]; ?></span> | <span><?php echo $itemProgramacion["nomlocal"] ?></span></div>
                    <div class="circulo" style="background:#CFCD4B;"></div>							
                    <div class="labelNomTarea pad3"><?php echo $itemProgramacion["nombre"] ?></div>
                    <div class="pad3"><span><?php echo $itemProgramacion["nomcategoria"] ?></span> | <span><?php echo $itemProgramacion["nomresponsable"] ?></span></div>
                    <div class="pad3"><span><?php echo $itemProgramacion["proveedor"] ?></span> | <span><?php echo "S/. " . doubleval($itemProgramacion["presupuesto"]) ?></span></div>
                </div>
                <div class="col-xs-12 col-sm-3 numero" data-numero="2">
                    <div class="pad3"><strong>ULT. EJEC.: </strong><?php echo $itemProgramacion["fultimaejecucion"] ?></div>
                    <div class="pad3"><strong>F.INICIO: </strong><?php echo $itemProgramacion["finicio"] ?></div>
                    <div class="pad3"><strong>F.FINAL: </strong><?php echo $itemProgramacion["ffin"] ?></div>
                </div>
                <div class="col-xs-12 col-sm-3 numero" data-numero="2">
                    <div class="pad3" style="white-space: nowrap"><strong>REPETICIÓN: </strong><?php echo getArrayRepeticion(intval($itemProgramacion["repeticion"])) ?></div>

                    <?php
                    $dias = "";
                    if ($itemProgramacion["repeticion"] == 1) {
                        ?>
                        <div class="pad3" style="white-space: nowrap"><strong>DIAS: </strong>
                            <?php
                            if ($itemProgramacion["lunes"] == 1) {
                                $dias.="LUN-";
                            }
                            if ($itemProgramacion["martes"] == 1) {
                                $dias.="MAR-";
                            }
                            if ($itemProgramacion["miercoles"] == 1) {
                                $dias.="MIE-";
                            }
                            if ($itemProgramacion["jueves"] == 1) {
                                $dias.="JUE-";
                            }
                            if ($itemProgramacion["viernes"] == 1) {
                                $dias.="VIE-";
                            }
                            if ($itemProgramacion["sabado"] == 1) {
                                $dias.="SAB-";
                            }
                            if ($itemProgramacion["domingo"] == 1) {
                                $dias.="DOM-";
                            }
                            echo trim($dias, "-");
                            ?></div>
                            <?php
                    }
                    ?>
                    <div>
                        <?php echo "<strong>PROX. F. PROG: </strong>".$itemProgramacion["fecha_siguiente"]; ?>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-1">
                    <div class=" pull-right">
                        <div style="text-align: right;">
                            <?php
                            if ($itemProgramacion["estado_registro"] == 1) {
                                echo "Activo";
                            } else {
                                echo "Inactivo";
                            }
                            ?>
                        </div>
                        <div>
                            <?php echo $itemProgramacion["enlace"]; ?>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>                        
            </li> 

            <?php
        }
        ?>
    </ul>
</div>

<div class="row" style="display: none;">
    <div id="archivos" class="col-md-8">
        <button id="btnAdjuntar" class="btn btn-default classAdjuntar" type="submit">Adjuntar</button>

        <div id="subir" class="clearfix" style="padding-left: 7px;display: inline-block;margin-top: 9px;margin-left: -6px;">
            <input id="my_file_element" type="file" name="file_1" ><br clear="all"/>
            <div id="files_list"></div>                            
        </div>
    </div>
</div>