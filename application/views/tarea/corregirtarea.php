<div id="mensajeaux"></div>
<?php
if ($divmensaje != "") {
    echo $divmensaje;
} else {
    ?>
    <div class="row">
        <div class="col-md-8">
            <span class="leyenda text-center">Información de la tarea</span>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="row">   
                <div class="col-md-12 espacio5">
                    <input type="text" id="txtnombre" value="<?php echo $objTarea["nombre"] ?>" class="form-control" placeholder="Nombre de la tarea">    
                </div>                
            </div><!--/row-->
            <div class="row">   
                <div class="col-md-6 espacio5">
                    <div class="radio">
                        <label style="font-size: 1em;padding-left: 0px;">
                            <input type="radio" name="rbPrioridad" value="1" <?php if ($objTarea["prioridad"] == 1) echo "checked"; ?>>
                            <span class="cr"><i class="cr-icon fa fa-circle"></i></span>
                            Baja
                        </label>
                    </div>
                    <div class="radio">
                        <label style="font-size: 1em">
                            <input type="radio" name="rbPrioridad" value="2" <?php if ($objTarea["prioridad"] == 2) echo "checked"; ?>>
                            <span class="cr"><i class="cr-icon fa fa-circle"></i></span>
                            Media
                        </label>
                    </div>
                    <div class="radio">
                        <label style="font-size: 1em">
                            <input type="radio" name="rbPrioridad" value="3" <?php if ($objTarea["prioridad"] == 3) echo "checked"; ?>>
                            <span class="cr"><i class="cr-icon fa fa-circle"></i></span>
                            Alta
                        </label>
                    </div>
                </div>
                <div class="col-md-6 espacio5">
                    <select class="form-control" id="cbocategoria">
                        <option value="">Seleccione Categoría</option>
                        <?php
                        foreach ($aCategorias as $itemCategoria) {
                            ?>
                            <option value="<?php echo $itemCategoria->idcategoria ?>" <?php if ($objTarea["idcategoria"] == $itemCategoria->idcategoria) echo "selected"; ?>><?php echo $itemCategoria->nombre; ?></option>
                            <?php
                        }
                        ?>
                    </select>  
                </div><!--/span-->
            </div><!--/row-->

            <div class="row">   
                <div class="col-md-6 espacio5">
                    <?php
                    $rol = $this->session->userdata('rol');
                    $nombreAdmin = "¿Para el administrador?";
                    if ($rol == 1) {
                        $nombreAdmin = "¿Para mi?";
                    }
                    ?>
                    <div class="checkbox">
                        <label style="font-size: 1em; padding-left: 0px;">
                            <input type="checkbox" id="admin" value="<?php echo $objTarea["paraadmin"]; ?>" <?php if ($objTarea["paraadmin"] == 1) echo "checked"; ?>>
                            <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                            <?php echo $nombreAdmin; ?>
                        </label>
                    </div>

                </div>

                <?php
                if (count($aLocales) > 0) {
                    ?>
                    <div class="col-md-6 espacio5">

                        <select class="form-control" id="cbolocal">
                            <option value="">Seleccione local</option>
                            <?php
                            foreach ($aLocales as $itemLocal) {
                                ?>
                                <option value="<?php echo $itemLocal->idlocal ?>" <?php if ($objTarea["idlocal"] == $itemLocal->idlocal) echo "selected"; ?>><?php echo $itemLocal->nombre; ?></option>
                                <?php
                            }
                            ?>
                        </select>

                    </div>
                    <?php
                }
                ?>
            </div><!--/row-->
            <?php
            $rol = $this->session->userdata('rol');
            $display = "block";
            if ($rol == 1) {
                $display = "none";
            }
            ?>
            <div class="row" style="display: <?php echo $display; ?>">   
                <div class="col-sm-12 col-md-8 espacio5">
                    <input class="form-control" id="txtproveedor" value="<?php echo $objTarea["proveedor"] ?>" placeholder="Proveedor">
                </div>
                <div class="col-sm-12 col-md-4 espacio5">
                    <input class="form-control" id="txtpresupuesto" value="<?php echo $objTarea["presupuesto"] ?>" placeholder="Monto de presupuesto">
                </div>
            </div><!--/row-->
            <div class="row">
                <div class="col-md-12 espacio5">
                    <input type="hidden" id="txtidtarea" value="<?php echo $objTarea["idtarea"]; ?>">
                    <textarea id="txtdescripcion" class="form-control" rows="3" placeholder="Escribir detalle"><?php echo trim($objTarea["descripcion"]) ?></textarea>
                </div><!--/span-->                
            </div><!--/row-->  
            <?php
            if ($aArchivosTarea != FALSE) {
                ?>
                <ul class="list-group" id="contact-list2" style="margin-bottom: 6px;">
                    <li class="list-group-item">
                        <div id="mntrs" class="clearfix">
                            <?php
                            foreach ($aArchivosTarea as $itemArchivoTarea) {
                                $info = pathinfo(getcwd() . "/" . $itemArchivoTarea->url);
                                $extension = $info['extension'];
                                if (strtoupper($extension) == "JPG" || strtoupper($extension) == "JPEG" || strtoupper($extension) == "PNG" || strtoupper($extension) == "GIF") {
                                    ?>
                                    <div class="col-xs-12 col-sm-6 col-md-4">
                                        <a class="fancybox" rel="gallery1" href="<?php echo base_url() . "media/" . $itemArchivoTarea->url ?>">
                                            <div class="clearfix thumbnail">
                                                <div class="row">
                                                    <div class="col-xs-3">
                                                        <img width="50" height="50" src="<?php echo base_url() . "media/archivos/thumbs/" . substr(trim($itemArchivoTarea->url), 9) ?>" class="img-responsive">
                                                    </div>
                                                    <div class="col-xs-9">
                                                        <div>
                                                            <p><?php echo substr(substr(trim($itemArchivoTarea->url), 9), 0, 10) . "." . $extension; ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <?php
                                } else {
                                    ?>
                                    <div class="col-xs-12 col-sm-6 col-md-4">
                                        <a class="fancybox" rel="gallery1" href="<?php echo base_url() . "media/" . $itemArchivoTarea->url ?>">
                                            <div class="clearfix thumbnail">
                                                <div class="row">
                                                    <div class="col-xs-3">
                                                        <img width="50" height="50" src="<?php echo base_url() . "media/img/" . $extension . ".png" ?>" class="img-responsive">
                                                    </div>
                                                    <div class="col-xs-9">
                                                        <div>
                                                            <p><?php echo substr(substr(trim($itemArchivoTarea->url), 9), 0, 10) . "." . $extension; ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>    
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </li>
                </ul>
                <?php
            }
            ?>

            <div class="row">
                <div class="col-md-8">
                    <button id="btnAgregarArchivo" class="btn btn-link classAdjuntar" type="submit">Añadir</button>

                    <div id="subir2" class="clearfix" style="padding-left: 7px;display: inline-block;margin-top: 9px;margin-left: -6px;">
                        <input type="file" name="file_0" id="file_0" class="custom-file-input">                        
                    </div>
                </div>
            </div>
        </div><!--/span-->

        <div class="col-md-4">

        </div><!--/span-->       
    </div><!--/row-->   
    <?php
    if (count($aTareaEstados) > 0) {
        ?>
        <div class="row">
            <div class="col-md-8">
                <span class="leyenda text-center">Actividades de la tarea</span>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="qa-message-list" id="wallmessages">
                    <?php
                    foreach ($aTareaEstados as $itemTareaEstado) {
                        if ($itemTareaEstado["imagen"] == "") {
                            $foto = "default.png";
                        } else {
                            $foto = $itemTareaEstado["imagen"];
                        }
                        ?>
                        <div class="message-item" id="m16">
                            <div class="message-inner">
                                <div class="message-head clearfix">
                                    <div class="avatar pull-left"><a href="#"><img width="40" height="40" src="<?php echo base_url() . "media/archivos/avatars/thumbs/" . $foto ?>"></a></div>
                                    <div class="pull-left">
                                        <div class="user-detail">
                                            <h5 class="handle"><?php echo $itemTareaEstado["nomestado"] ?></h5>
                                            <div class="post-meta">
                                                <span><?php echo $itemTareaEstado["fregistro"] . " - Duró: " . $itemTareaEstado["transcurrido"]; ?></span>
                                                <span><?php echo $itemTareaEstado["nomusuario"] ?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pull-right">
                                        <span>
                                            <?php
                                            if ($this->session->userdata("rol") != 1) {
                                                echo "Ppto: S/. " . $itemTareaEstado["presupuesto"];
                                            }
                                            ?>
                                        </span>
                                    </div>
                                </div>
                                <div class="qa-message-content">
                                    <?php echo $itemTareaEstado["comentario"]; ?>
                                </div>
                                <div class="contResp">
                                <div id="chat_respuestas_<?php echo $itemTareaEstado["idtarea_estado"]; ?>">
                                    <?php
                                    foreach ($itemTareaEstado["aRespuestas"] as $itemRespuesta) {
                                        ?>
                                        <div class="responder clearifx">
                                            <span class="spn-avatar pull-left media-object" href="#">
                                                <i class="glyphicon glyphicon-user"></i>
                                            </span>

                                            <div class="media-body">
                                                <b><?php echo $oUsuario[$itemRespuesta->idusuario_creador]["nombre"] ?></b>
                                                <p>
                                                    <?php echo $itemRespuesta->texto; ?>
                                                </p>
                                            </div>    
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div class="btn-resp clearfix">
                                    <a class="link-responder" data-id="<?php echo $itemTareaEstado["idtarea_estado"] ?>" href="javascript:void(0);">Responder</a>
                                </div>
                                <div id="form_respuesta_<?php echo $itemTareaEstado["idtarea_estado"] ?>"></div>
                            </div>
                                <?php
                                if ($itemTareaEstado["archivos"] != "") {
                                    ?>
                                    <div id="mntrs" class="clearfix">
                                        <?php
                                        $aArchivos = explode("|", trim($itemTareaEstado["archivos"], "|"));
                                        foreach ($aArchivos as $itemArchivo) {
                                            $info = pathinfo(getcwd() . "/" . $itemArchivo);
                                            $extension = $info['extension'];
                                            if (strtoupper($extension) == "JPG" || strtoupper($extension) == "JPEG" || strtoupper($extension) == "PNG" || strtoupper($extension) == "GIF") {
                                                ?>
                                                <div class="col-xs-12 col-sm-6 col-md-4">
                                                    <a class="fancybox" rel="gallery1" href="<?php echo base_url() . "media/" . $itemArchivo ?>">
                                                        <div class="clearfix thumbnail">
                                                            <div class="row">
                                                                <div class="col-xs-3">
                                                                    <img width="50" height="50" src="<?php echo base_url() . "media/archivos/thumbs/" . substr(trim($itemArchivo), 9) ?>" class="img-responsive">
                                                                </div>
                                                                <div class="col-xs-9">
                                                                    <div>
                                                                        <p><?php echo substr(substr(trim($itemArchivo), 9), 0, 10) . "." . $extension; ?></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                                <?php
                                            } else {
                                                ?>
                                                <div class="col-xs-12 col-sm-6 col-md-4">
                                                    <a class="fancybox" rel="gallery1" href="<?php echo base_url() . "media/" . $itemArchivo ?>">
                                                        <div class="clearfix thumbnail">
                                                            <div class="row">
                                                                <div class="col-xs-3">
                                                                    <img width="50" height="50" src="<?php echo base_url() . "media/img/" . $extension . ".png" ?>" class="img-responsive">
                                                                </div>
                                                                <div class="col-xs-9">
                                                                    <div>
                                                                        <p><?php echo substr(substr(trim($itemArchivo), 9), 0, 10) . "." . $extension; ?></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php
                    }
                    ?>

                </div>
            </div>

        </div>

    <?php } ?>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-sm-12 col-md-4 text-center">
            <button type="button" id="btnCorregirTarea" style="width: 100%" class="btn btn-primary">Corregir</button>                
        </div>
        <div class="col-sm-12 col-md-4 text-center">
            <a href="<?php echo base_url() ?>tarea/lista" id="btnSalir" class="btn btn-link">Salir</a>
        </div>

    </div>

    <?php
}
?>
