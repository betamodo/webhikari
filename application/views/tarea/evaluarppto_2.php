<div id="mensajeaux"></div>
<?php echo $divmensaje; ?>

<div class="row">
    <div class="col-md-8">
        <span class="leyenda text-center">Actividades de la tarea</span>
        <hr>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <div class="row">   
            <div class="col-md-12 espacio5">
                <ul class="list-group" id="contact-list2" style="margin-bottom: 2px;">
                    <li class="list-group-item">
                        <div class="col-xs-12 col-sm-8">
                            <div><span class="labelCodigo"><?php echo $objTarea["codigo"] ?></span> | <span><?php echo $objTarea["nomlocal"] ?></span></div>
                            
                            <div class="labelNomTarea pad3"><?php echo $objTarea["nombre"] ?></div>
                            <div class="pad3"><span><?php echo $objTarea["nomcategoria"] ?></span> | <span><?php echo $objTarea["nomresponsable"] ?></span></div>
                        </div>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar pull-left"><a href="#"><img width="40" height="40" src="<?php echo $objTarea["imagen"] ?>"></a></div>
                            <div class="pad3" style="white-space: nowrap">F.REGISTRO: <?php echo $objTarea["fregistro"] ?></div>
                            <div class="pad3" style="white-space: nowrap">F.ULT.MOD: <?php echo $objTarea["factualizacion"] ?></div>	
                        </div>
                        <div class="clearfix"></div>                        
                    </li>  
                    <li class="list-group-item">
                        <div class="col-xs-12 col-sm-12">
                            <div class="pad3"><strong>Descripción:</strong></div>
                            <p><?php echo $objTarea["descripcion"]; ?></p>
                        </div>
                        <div class="clearfix"></div>
                    </li>
                    <?php
                    if ($aArchivosTarea != FALSE) {
                        ?>
                        <li class="list-group-item">
                            <div id="mntrs" class="clearfix">
                                <?php
                                foreach ($aArchivosTarea as $itemArchivoTarea) {
                                    $info = pathinfo(getcwd() . "/" . $itemArchivoTarea->url);
                                    $extension = $info['extension'];
                                    ?>
                                    <div class="col-xs-12 col-sm-6 col-md-4">
                                        <a class="fancybox" rel="gallery1" href="<?php echo base_url() . "media/" . $itemArchivoTarea->url ?>">
                                            <div class="clearfix thumbnail">
                                                <div class="row">
                                                    <div class="col-xs-3">
                                                        <img width="50" height="50" src="<?php echo base_url() . "media/archivos/thumbs/" . substr(trim($itemArchivoTarea->url), 9) ?>" class="img-responsive">
                                                    </div>
                                                    <div class="col-xs-9">
                                                        <div>
                                                            <p><?php echo substr(substr(trim($itemArchivoTarea->url), 9), 0, 10) . "." . $extension; ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </li>
                        <?php
                    }
                    ?>
                </ul> 

            </div>                
        </div><!--/row-->

    </div><!--/span-->

    <div class="col-md-4">

    </div><!--/span-->       
</div><!--/row-->   
<div class="row">
    <div class="col-md-8">
        <span class="leyenda text-center">Actividades de la tarea</span>
        <hr>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <div class="qa-message-list" id="wallmessages">
            <?php
            foreach ($aTareaEstados as $itemTareaEstado) {
                ?>
                <div class="message-item" id="m16">
                    <div class="message-inner">
                        <div class="message-head clearfix">
                            <div class="avatar pull-left"><a href="./index.php?qa=user&qa_1=Oleg+Kolesnichenko"><img width="40" height="40" src="https://ssl.gstatic.com/accounts/ui/avatar_2x.png"></a></div>
                            <div class="user-detail">
                                <h5 class="handle"><?php echo $itemTareaEstado["nomestado"] ?></h5>
                                <div class="post-meta">
                                    <div class="asker-meta">
                                        <span class="qa-message-what"></span>
                                        <span class="qa-message-when">
                                            <span class="qa-message-when-data"><?php echo $itemTareaEstado["fregistro"] ?></span>
                                        </span>
                                        <span class="qa-message-who">
                                            <span class="qa-message-who-pad">por </span>
                                            <span class="qa-message-who-data"><?php echo $itemTareaEstado["nomusuario"] ?></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="qa-message-content">
                            <?php echo $itemTareaEstado["comentario"]; ?>
                        </div>
                        <?php
                        if ($itemTareaEstado["archivos"] != "") {
                            ?>
                            <div id="mntrs" class="clearfix">
                                <?php
                                $aArchivos = explode("|", trim($itemTareaEstado["archivos"], "|"));

                                foreach ($aArchivos as $itemArchivo) {
                                    $info = pathinfo(getcwd() . "/" . $itemArchivo);
                                    $extension = $info['extension'];
                                    ?>
                                    <div class="col-xs-12 col-sm-6 col-md-4">
                                        <a class="fancybox" rel="gallery1" href="<?php echo base_url() . "media/" . $itemArchivo ?>">
                                            <div class="clearfix thumbnail">
                                                <div class="row">
                                                    <div class="col-xs-3">
                                                        <img width="50" height="50" src="<?php echo base_url() . "media/archivos/thumbs/" . substr(trim($itemArchivo), 9) ?>" class="img-responsive">
                                                    </div>
                                                    <div class="col-xs-9">
                                                        <div>
                                                            <p><?php echo substr(substr(trim($itemArchivo), 9), 0, 10) . "." . $extension; ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <?php
            }
            ?>

        </div>
    </div>

</div>
<div class="row">
    <div class="col-md-8 espacio5">
        <input type="hidden" id="txtidtarea" value="<?php echo $objTarea["idtarea"]; ?>">
        <textarea id="txtdescripcion" class="form-control" rows="3" placeholder="Escribir detalle"></textarea>
    </div><!--/span-->                
</div><!--/row-->  

<div class="row">
    <div class="col-md-8">
        <button id="btnAgregarArchivo" class="btn btn-default classAdjuntar" type="submit">Añadir archivo</button>

        <div id="subir2" class="clearfix" style="padding-left: 7px;display: inline-block;margin-top: 9px;margin-left: -6px;">
            <input type="hidden" id="hid_input_files" name="hid_input_files" value="file_0" />
            <div id="remove_0" style="margin-right: 4px;" class="btn btn-sm btn-default btn_remover_files" data-id="0"><span data-id="file_0" class="glyphicon glyphicon-minus-sign btn_remover_files"></span></div>
            <input type="file" name="file_0" id="file_0" class="custom-file-input">                        
        </div>
    </div>
</div>
<div class="row" style="margin-top: 12px;">
    <div class="col-sm-12 col-md-3 text-center">
        <button type="button" id="btnEjecutar" data-id="1" class="btn btn-primary">Enviar para aprobación</button>                
    </div>
    <div class="col-sm-12 col-md-3 text-left">
        <button type="button" id="btnEjecutar" data-id="2" class="btn btn-primary">Ok, presupuestado</button>                
    </div>
    <div class="col-sm-12 col-md-3 text-left">
        <button type="button" id="btnSalir" data-id="0" class="btn btn-link">Rechazar</button>
    </div>

</div>
<br>
