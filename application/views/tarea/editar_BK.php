<div id="mensajeaux"></div>
<?php echo $divmensaje; ?>

<div class="row"><div class="col-md-4"></div><div class="col-md-4"></div></div>
<div class="row">
    <div class="col-md-8">
        <div class="row">   
            <div class="col-md-12 espacio5">
                <input type="text" id="txtnombre" class="form-control" placeholder="Nombre de la tarea">    
            </div>                
        </div><!--/row-->
        <div class="row">   
            <div class="col-md-6 espacio5">
                <div class="radio">
                    <label style="font-size: 1em;padding-left: 0px;">
                        <input type="radio" name="rbPrioridad" value="1">
                        <span class="cr"><i class="cr-icon fa fa-circle"></i></span>
                        Baja
                    </label>
                </div>
                <div class="radio">
                    <label style="font-size: 1em">
                        <input type="radio" name="rbPrioridad" value="2" checked="">
                        <span class="cr"><i class="cr-icon fa fa-circle"></i></span>
                        Media
                    </label>
                </div>
                <div class="radio">
                    <label style="font-size: 1em">
                        <input type="radio" name="rbPrioridad" value="3">
                        <span class="cr"><i class="cr-icon fa fa-circle"></i></span>
                        Alta
                    </label>
                </div>
            </div>
            <div class="col-md-6 espacio5">
                <select class="form-control" id="cbocategoria">
                    <option value="">Seleccione Categoría</option>
                    <?php
                    foreach ($aCategorias as $itemCategoria) {
                        ?>
                        <option value="<?php echo $itemCategoria->idcategoria ?>"><?php echo $itemCategoria->nombre; ?></option>
                        <?php
                    }
                    ?>
                </select>  
            </div><!--/span-->
        </div><!--/row-->

        <div class="row">   
            <?php
            $rol = $this->session->userdata('rol');
            if ($rol != 1) {
                ?>
                <div class="col-md-6 espacio5">

                    <div class="checkbox">
                        <label style="font-size: 1em; padding-left: 0px;">
                            <input type="checkbox" id="admin" value="1">
                            <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                            ¿Para el administrador?
                        </label>
                    </div>

                </div>
                <?php
            }
            ?>
            <?php
            if (count($aLocales) > 0) {
                ?>
                <div class="col-md-6 espacio5">

                    <select class="form-control" id="cbolocal">
                        <option value="">Seleccione local</option>
                        <?php
                        foreach ($aLocales as $itemLocal) {
                            ?>
                            <option value="<?php echo $itemLocal->idlocal ?>"><?php echo $itemLocal->nombre; ?></option>
                            <?php
                        }
                        ?>
                    </select>

                </div>
                <?php
            }
            ?>
        </div><!--/row-->
        <div class="row">
            <div class="col-md-12 espacio5">
                <textarea id="txtdescripcion" class="form-control" rows="3" placeholder="Escribir detalle"></textarea>
            </div><!--/span-->                
        </div><!--/row-->  

        <div class="row">
            <div id="archivos" class="col-md-8">
                <button id="btnAdjuntar" class="btn btn-default classAdjuntar" type="submit">Adjuntar</button>

                <div id="subir" class="clearfix" style="padding-left: 7px;display: inline-block;margin-top: 9px;margin-left: -6px;">
                    <input id="my_file_element" type="file" name="file_1" ><br clear="all"/>
                    <div id="files_list"></div>                            
                </div>
            </div>
        </div>
    </div><!--/span-->

    <div class="col-md-4">

    </div><!--/span-->       
</div><!--/row-->   

<div class="clearfix"></div>
<div class="row">
    <div class="col-sm-12 col-md-4 text-center">
        <button type="button" id="btnCrearTarea" class="btn btn-primary">Crear</button>                
    </div>
    <div class="col-sm-12 col-md-4 text-center">
        <button type="button" id="btnSalir" class="btn btn-link">Salir</button>
    </div>

</div>
