
<div id="busquedaAvanzada"></div>
<div id="listaTareas">
    <ul class="list-group" id="contact-list2">
        <?php
        foreach ($aListaTareas as $itemTarea) {
            ?>
            <li class="list-group-item" style="margin-bottom: 7px;">
                <div class="col-xs-12 col-sm-8">
                    <div><span class="labelCodigo"><?php echo $itemTarea["codigo"] ?></span> | <span><?php echo $itemTarea["nomlocal"] ?></span></div>
                    <div class="circulo" style="background:#CFCD4B;"></div>							
                    <div class="labelNomTarea pad3"><?php echo $itemTarea["nombre"] ?></div>
                    <div class="pad3"><span><?php echo $itemTarea["nomcategoria"] ?></span> | <span><?php echo $itemTarea["nomresponsable"] ?></span></div>
                </div>
                <div class="col-xs-12 col-sm-2 numero" data-numero="2">
                    <div><img src="<?php echo $itemTarea["imagen"]; ?>"><span class="actividad"> <?php echo $itemTarea["nomestado"] ?></span></div>
                    <div class="pad3" style="white-space: nowrap">F.REGISTRO: <?php echo $itemTarea["fregistro"] ?></div>
                    <div class="pad3" style="white-space: nowrap">F.ULT.MOD: <?php echo $itemTarea["factualizacion"] ?></div>
                </div>
                <div class="col-xs-12 col-sm-2">
                    <div class="text-left presu">
                        <?php
                        if ($itemTarea["presupuesto"] != "")
                            echo "S/. " . $itemTarea["presupuesto"];
                        ?>
                    </div>
                    <div class=" pull-right">
                        <div style="text-align: right;"><?php echo $itemTarea["tipo_transcurrido"] ?></div>
                        <div>
                            <?php echo $itemTarea["enlace"]; ?>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>                        
            </li> 

            <?php
        }
        ?>
    </ul>
</div>

<div class="row" style="display: none;">
    <div id="archivos" class="col-md-8">
        <button id="btnAdjuntar" class="btn btn-default classAdjuntar" type="submit">Adjuntar</button>

        <div id="subir" class="clearfix" style="padding-left: 7px;display: inline-block;margin-top: 9px;margin-left: -6px;">
            <input id="my_file_element" type="file" name="file_1" ><br clear="all"/>
            <div id="files_list"></div>                            
        </div>
    </div>
</div>