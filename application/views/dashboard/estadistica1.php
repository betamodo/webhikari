<div id = "listaTareas">
    <ul class = "list-group" id = "contact-list">
        <?php
        foreach ($mResultados as $key => $itemEstadistica) {
            ?>
            <li class="list-group-item" style="margin-bottom: 7px;">
                <div class="col-xs-12 col-md-2">
                    <div class="labelNomTarea pad3"><?php echo $objUsuarioAux[$key]["nombre"] ?></div>                    
                </div>
                <div class="col-xs-12 col-md-10">
                    <div class="row">
                        <?php
                        foreach ($itemEstadistica as $keyCantidad => $itemCantidad) {
                            ?>
                            <div class="col-xs-12 col-md-2 text-center">
                                <div><?php echo $aEstados[$keyCantidad][0]["nombre"]; ?></div>

                                <div class="labelNomTarea pad3 text-center"><a href="<?php echo base_url() . "panel/detallelista/" . $key . "/" . $keyCantidad ?>" target="_blank"><?php echo $itemCantidad; ?></a></div>                    
                            </div>                    
                            <?php
                        }
                        ?>
                    </div>
                     <div class="clearfix"></div> 
                </div>

                <div class="clearfix"></div>  
            </li> 
            <?php
        }
        ?>
    </ul>
</div>