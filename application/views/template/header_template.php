<!--
AUTOR: VICTOR REATEGUI PINEDO
-->
<?php
//***************************************************
// CAPTURAMOS EL MENU QUE TIENE PERMISO EL USUARIO
//***************************************************
$datamenu = $this->session->userdata('rsMenu');
$datasubmenu = $this->session->userdata('rsSubMenu');
$nomusuario = $this->session->userdata('nomusuario');
$nomempresa = $this->session->userdata('nomempresa');
$subtitulo = $this->session->userdata('subtitulo');
?>
<div>
    <div id="info_empresa">
        <div id="titulo_empresa"><?= strtoupper($nomempresa) ?></div>
        <div id="subtitulo_empresa"><?= $subtitulo ?></div>
    </div>
    <div id="separador_principal"></div>
    <div id="info_usuario">
        <div id="imagen_usuario"><img id="foto_usuario" title="dbLogin" src="<?php echo base_url(); ?>media/img/001.png" alt="" width="50"></div>
        <div id="datos_usuario"><?= $nomusuario ?></div>
        <div id="editar_perfil">Editar Perfil</div>
    </div>
    <div id="salir"><a style="color: white" href="<?php echo base_url()."usuario/login/cerrarsesion" ?>">Salir</a></div>
</div>
<form id="frmmenu" method="post">
    <input type="hidden" id="codmenu" name="codmenu" value=""/>
</form>
<div id="nav" style="
     top: 0;
     border-radius: 0;
     right: 0;
     left: 0;
     height: 34px !important;
     box-sizing: border-box;
     clear: both;
     padding:5px;
     border:1px solid #ddd;
     color: #555;
     margin-top: 48px;">

    <!-- ************ OPCIONES DEL MENÚ *********************** -->
    <?php
//    echo var_dump($datamenu);
//    echo '***********************************';
//    echo var_dump($datasubmenu);exit;

    foreach ($datamenu as $fila) {

    ?>
            <a href="#" class="easyui-menubutton" data-options="menu:'#civi<?= $fila->id ?>',iconCls:'icon-1'"><?= $fila->titulo ?></a>

    <?php
        
    }
    ?>

    <?php
    $filatmpidmenu = 0;
    $filatmpidsubmenu = 0;
    $contadorMenu = 0;
    $contador = 0;
    foreach ($datasubmenu as $fila) {
        if ($fila->padre != $filatmpidmenu) {
            if ($contador != 0) {
                echo '</div>';
            }
            echo '<div id="civi' . $fila->padre . '" style="width:150px;">';
        }
        $filatmpidmenu = $fila->padre;
        echo '<div><a class="link-controlador" data-id="' . $fila->id . '" data-url="' . $fila->urlcontrolador . '"  style="text-decoration:none; color:#333">' . $fila->titulo . '</a></div>';
        $contador++;
    }
    ?>
</div>

<!--<script>
    function ir(controlador,id){

    }
</script>-->

<script>
    $(function() {
        $( ".link-controlador" ).click(function(){
            var url= $(this).attr('data-url');
            var id= $(this).attr('data-id');
            $('#codmenu').attr('value',id);
            $('#frmmenu').attr('action',"../../"+url);
            $('#frmmenu').submit();
        });
    });
</script>
<!-- FIN DE OPCIONES DE MENÚ -->
