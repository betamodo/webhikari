<!-- ********************************** -->
<!-- PLANTILLA PARA EL TEMPLATE default -->
<!-- ********************************** -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
                    <meta name="description" content="">
                        <meta name="author" content="">
                            <link rel="icon" href="../../favicon.ico"/>

                            <title><?= $titleheader ?></title>

                            <!-- Bootstrap core CSS -->
                            <link href="<?php echo base_url() ?>media/css/bootstrap.min.css" rel="stylesheet"/>
                            <link href="<?php echo base_url() ?>media/css/custom.css" rel="stylesheet"/>
                            <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"/>						
                            <link href="<?php echo base_url() ?>media/fancybox/jquery.fancybox.css" rel="stylesheet" />
                            <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"/>
							<link href="http://req.pe/media/css/bootstrap-editable.css" rel="stylesheet">
                            <?= $_styles ?>
                            </head>

                            <body>
                                <!-- Preloader -->
                                <!--                                <div id="preloader">
                                                                    <div id="status">&nbsp;</div>
                                                                </div>-->
                                <nav class="navbar" style="border-bottom: 1px solid #e5e5e5;">
                                    <div class="container-fluid">
                                        <!-- Brand and toggle get grouped for better mobile display -->
                                        <div class="navbar-header">
                                            <a class="navbar-brand" href="#">
                                                <img id="imglogo" src="<?php echo base_url() ?>/media/img/logo_marca.png"  />
                                            </a>

                                            <!-- LLenando variable monto mímio -->
                                            <input type="hidden" id="monto_minimo" value="<?php echo $this->session->userdata("monto_minimo"); ?>"/>
                                            <input type="hidden" id="rol" value="<?php echo $this->session->userdata("rol"); ?>"/>
											<ul id="uSer" class="nav navbar-nav">
                                                <li class="dropdown">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                        <i class="glyphicon glyphicon-user" style="font-size: 22px;top: 6px;"></i>
                                                        <span class="profile-info">

                                                            <small><?php echo $this->session->userdata('nombre') . " | " . getNombreRol($this->session->userdata('rol')) ?></small>
                                                        </span>
                                                        <i class="glyphicon glyphicon-menu-down"></i>
                                                    </a>
                                                    <?php
                                                    $htmlCuentaLocal = $this->session->userdata('htmlcuentaslocales');
                                                    $htmlCuentaGestores = $this->session->userdata('htmlcuentasgestores');
                                                    $htmlcuentasactual = $this->session->userdata("htmlcuentasactual");
                                                    ?>
                                                    <ul class="dropdown-menu pull-right">
                                                        <?php
                                                        if ($htmlCuentaLocal != "") {
                                                            echo $htmlCuentaLocal;
                                                            ?>
                                                            <li class="divider"></li>
                                                            <?php
                                                        }
                                                        ?>
                                                        <?php
                                                        if ($htmlCuentaGestores != "") {
                                                            echo $htmlCuentaGestores;
                                                            ?>
                                                            <li class="divider"></li>
                                                            <?php
                                                        }
                                                        ?>

                                                        <?php
                                                        if ($htmlCuentaGestores != "" || $htmlCuentaLocal != "") {
                                                            echo $htmlcuentasactual;
                                                        }
                                                        ?>
                                                        <li>
                                                            <a href="<?php echo base_url() . "usuario/salir" ?>"><i class="glyphicon glyphicon-lock"></i> Cerrar sesión</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <!--<li class="pad">
                                                    <a href="#"><i class="glyphicon glyphicon-option-vertical"></i></a>
                                                </li>-->
                                            </ul>  
											<ul id="noti" class="nav navbar-nav">
                                                <li class="dropdown">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                        <i class="glyphicon glyphicon-comment" style="font-size: 22px;top: 6px;"></i>
                                                        <span class="profile-info">
															<small id="totalnoti"></small>
                                                        </span>                                                        
                                                    </a>
                                                    <ul class="dropdown-menu pull-right" style="min-width: 248px;border: 1px solid #AFAFB0;border-radius: 5px;">
                                                        <li>
                                                            <div id="divnoti"></div>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>

                                        <!-- Collect the nav links, forms, and other content for toggling -->

                                    </div><!-- /.container-fluid -->
                                </nav>
                                <div class="container-fluid main-container">

                                    <div class="col-md-12 content">
                                        <div class="panel panel-default" style="  margin-bottom: 68px;">
                                            <div class="panel-heading">
                                                <div class="clearfix">
                                                    <span style="  font-size: 19px;text-transform: uppercase;font-weight: 600;display: inline-block;  line-height: 28px;"><?php echo utf8_encode($titulo); ?></span>
                                                    <div id="botones" class="pull-right">
                                                        <?= $botones ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-body" id="panelFormulario">
                                                <?= $content ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--FOOTER-->
                                <div class="navbar navbar-fixed-bottom" role="navigation" id="footer">
                                    <div class="container-fluid">
                                        <div class="pull-left">
                                            <img src="<?php echo base_url() . "media/img/reqlogo.png" ?>" height="22" style="  margin-top: -4px;"/>                                                
                                        </div>
                                    </div>
                                </div>
                                <!--//FOOTER-->

                                <!-- Bootstrap core JavaScript
                                ================================================== -->
                                <!-- Placed at the end of the document so the pages load faster -->
                                <script src="<?php echo base_url() ?>media/js/jquery.min.js"></script>
                                <script src="<?php echo base_url() ?>media/js/bootstrap.min.js"></script>
                                <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
                                <script src="<?php echo base_url() ?>media/js/ie10-viewport-bug-workaround.js"></script>
                                <script src="<?php echo base_url() ?>media/js/custom.js"></script>
                                <script src="<?php echo base_url() ?>media/js/matenimiento.js"></script>

                                <script src="<?php echo base_url() ?>media/fancybox/jquery.fancybox.js"></script>
                                <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
								<script src="http://req.pe/media/js/bootstrap-editable.min.js"></script>
                                <!-- Preloader -->
                                <script type="text/javascript">
                                    //<![CDATA[
//                                    $(window).load(function () { // makes sure the whole site is loaded
//                                        $('#status').fadeOut(); // will first fade out the loading animation
//                                        $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
//                                        $('body').delay(350).css({'overflow': 'visible'});
//                                    })
                                    //]]>
                                </script>
                                <script>
                                    $.datepicker.regional['es'] = {
                                        closeText: 'Cerrar',
                                        prevText: '<Ant',
                                        nextText: 'Sig>',
                                        currentText: 'Hoy',
                                        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                                        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                                        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                                        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
                                        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
                                        weekHeader: 'Sm',
                                        dateFormat: 'dd/mm/yy',
                                        firstDay: 1,
                                        isRTL: false,
                                        showMonthAfterYear: false,
                                        yearSuffix: ''
                                    };
                                    $.datepicker.setDefaults($.datepicker.regional['es']);
                                </script>
                                <script>
                                    $(document).ready(function () {
										var myDate = new Date();
										var today = myDate;
                                        $('.fancybox').fancybox();
                                        $("#finicio").datepicker();
                                        $("#ffin").datepicker();
                                    });
									/* Traer la cantidad de notificaciones que tiene el usuario actual*/
									$.post("<?php echo base_url()."tarea/"?>json_totalnotificaciones", {idtarea: 0}, function (total) {
										$("#totalnoti").html(total);
									});
									/* Mostar detalladamento la actividad realizada*/
									$('#noti').on('show.bs.dropdown', function () {
									$("#divnoti").html("<div class='loading'></div>").load("<?php echo base_url()."tarea/"?>json_notificaciones");
									});
                                </script>
                                <?= $_scripts ?>
								
                            </body>
                            </html>
