<!-- ********************************** -->
<!-- PLANTILLA PARA EL TEMPLATE default -->
<!-- ********************************** -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <meta charset="utf-8" />
        <?= $_styles ?><!--cargamos los css-->
        <link type="text/css" href="<?php echo base_url(); ?>media/css/general.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/css/easyui.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/css/icon.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/css/demo.css"/>

<!--        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/css/themes/base/all.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/css/themes/base/all.css"/>-->


        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/jquery-1.6.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/general.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/datagrid-detailview.js"></script>

<!--        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/ui/core.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/ui/core.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/ui/widget.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/ui/mouse.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/ui/button.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/ui/draggable.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/ui/position.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/ui/resizable.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>media/js/ui/dialog.js"></script>-->


        <?= $_scripts ?><!--cargamos los js-->
        <title><?= $titleheader ?></title>
    </head>
    <body>
        <div id="wrapper">
            <div id="cabecera">
                <?= $header ?>
            </div>
            <div id="main">
                <div id="content">
                    <div id="barratitulo">
                        <img title="img_folder" src="http://localhost/erp/media/images/folder.png" alt="" width="28" style="float: left;margin: 5px;"></img>
                        <div style="float: left; font-size: 26px">|</div>
                        <div style="margin: 5px;float: left"><?= $title ?></div>
                        <div style="float: right">
                            <?= $sidebar ?>
                        </div>
                    </div>
                    <div id="div-busqueda" style="display: none">

                    </div>
                    <div class="contenedor_principal">
                        <?= $content ?>
                        </div>
                    </div>

                </div>
<!--            <div id="dialog-loading" class="easyui-dialog" title=" " closed="false" style="width:300px;height:200px;display: none">
                    <div style="margin-left: 111px;
margin-bottom: 12px;
margin-top: 18px;"><img id="" title="" src="../../media/images/loading.gif" alt=""/></div>
                <div style="margin-left: 73px;"><span style="float: left;
                               color: #4A99D8;
                               font-weight: bold;
                               font-size: 25px;">VRP</span>&nbsp;<span style="color: rgb(205, 218, 63);
                               font-size: 25px;"> SYSTEM</span></div>
                </div>-->
                <div id="footer">
                <?= $footer ?>
            </div>
        </div>
    </body>
</html>