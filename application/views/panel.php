<style>
    @media (max-width:900px){
        .list-group-item{
            display: block;
            margin-top: 9px;
        }

    }
    @media (min-width:768px){
        .list-group-item{
            display: inline-block;
        }
    }
    .list-group-item{

    }
    .labelNomTarea{
        font-size: 11px!important;
    }
    a:hover, a:focus {
        color: #495B79;
        text-decoration: underline;     
    }
</style>
<ul class="list-group" id="contact-list">
    <!--    <li class="list-group-item">
            <a href="<?php base_url() ?>tarea/lista">
                <div class="col-xs-12 col-sm-12">
                    <div style="font-size: 22px;">Tareas</div>
                    <div class="circulo" style="background:#CFCD4B;top: 7px;"></div>							
                    <div class="labelNomTarea pad3">Administración de tareas</div>            
                </div>
            </a>
            <div class="clearfix"></div>                        
        </li> -->
    <!--    <li class="list-group-item">
            <a href="<?php base_url() ?>panel/estadistica1">
                <div class="col-xs-12 col-sm-12">
                    <div style="font-size: 22px;">Módulo documentario</div>
                    <div class="circulo" style="background:#CFCD4B;top: 7px;"></div>							
                    <div class="labelNomTarea pad3">Matriz de Usuarios y estados</div>            
                </div>
            </a>
            <div class="clearfix"></div>                        
        </li>-->
    <li class="list-group-item">
        <a href="<?php base_url() ?>configuracion/lista">
            <div class="col-xs-12 col-sm-12">
                <div style="font-size: 22px;">Configuraciones</div>
                <div class="circulo" style="background:#CFCD4B;top: 7px;"></div>							
                <div class="labelNomTarea pad3">Crear, editar, eliminar</div>            
            </div>
        </a>
        <div class="clearfix"></div>                        
    </li>
    <li class="list-group-item">
        <a href="<?php base_url() ?>usuario/lista">
            <div class="col-xs-12 col-sm-12">
                <div style="font-size: 22px;">Usuarios</div>
                <div class="circulo" style="background:#CFCD4B;top: 7px;"></div>							
                <div class="labelNomTarea pad3">Crear, editar, eliminar</div>            
            </div>
        </a>
        <div class="clearfix"></div>                        
    </li>
	<li class="list-group-item">
        <a href="<?php base_url() ?>empleado/lista">
            <div class="col-xs-12 col-sm-12">
                <div style="font-size: 22px;">Empleados</div>
                <div class="circulo" style="background:#CFCD4B;top: 7px;"></div>							
                <div class="labelNomTarea pad3">Crear, editar, eliminar</div>            
            </div>
        </a>
        <div class="clearfix"></div>                        
    </li>
    <li class="list-group-item">
        <a href="<?php base_url() ?>local/lista">
            <div class="col-xs-12 col-sm-12">
                <div style="font-size: 22px;">Locales</div>
                <div class="circulo" style="background:#CFCD4B;top: 7px;"></div>							
                <div class="labelNomTarea pad3">Crear, editar, eliminar</div>            
            </div>
        </a>
        <div class="clearfix"></div>                        
    </li>
    <li class="list-group-item">
        <a href="<?php base_url() ?>categoria/lista">
            <div class="col-xs-12 col-sm-12">
                <div style="font-size: 22px;">Categorias</div>
                <div class="circulo" style="background:#CFCD4B;top: 7px;"></div>							
                <div class="labelNomTarea pad3">Crear, editar, eliminar</div>            
            </div>
        </a>
        <div class="clearfix"></div>                        
    </li>
	<li class="list-group-item">
        <a href="<?php base_url() ?>tipodoc/lista">
            <div class="col-xs-12 col-sm-12">
                <div style="font-size: 22px;">Tipo de doc.</div>
                <div class="circulo" style="background:#CFCD4B;top: 7px;"></div>							
                <div class="labelNomTarea pad3">Crear, editar, eliminar</div>            
            </div>
        </a>
        <div class="clearfix"></div>                        
    </li>
	<li class="list-group-item">
        <a href="<?php base_url() ?>almacen/lista">
            <div class="col-xs-12 col-sm-12">
                <div style="font-size: 22px;">Almacén</div>
                <div class="circulo" style="background:#CFCD4B;top: 7px;"></div>							
                <div class="labelNomTarea pad3">Crear, editar, eliminar</div>            
            </div>
        </a>
        <div class="clearfix"></div>                        
    </li>
	<li class="list-group-item">
        <a href="<?php base_url() ?>file/lista">
            <div class="col-xs-12 col-sm-12">
                <div style="font-size: 22px;">File</div>
                <div class="circulo" style="background:#CFCD4B;top: 7px;"></div>							
                <div class="labelNomTarea pad3">Crear, editar, eliminar</div>            
            </div>
        </a>
        <div class="clearfix"></div>                        
    </li>
	<li class="list-group-item">
        <a href="<?php base_url() ?>stand/lista">
            <div class="col-xs-12 col-sm-12">
                <div style="font-size: 22px;">Stand</div>
                <div class="circulo" style="background:#CFCD4B;top: 7px;"></div>							
                <div class="labelNomTarea pad3">Crear, editar, eliminar</div>            
            </div>
        </a>
        <div class="clearfix"></div>                        
    </li>
</ul>