<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html lang="es">
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta name="description" content=""/>
        <meta name="author" content="Saúl Paolo Pacheco Laguna"/>
        <link rel="icon" href="favicon.ico"/>

        <title>Hikari</title>
        <!-- Bootstrap core CSS -->
        <link href="<?php echo base_url(); ?>media/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="<?php echo base_url() ?>media/css/custom.css" rel="stylesheet"/>                
    </head>

    <body>

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <div class="account-wall">
                        <img class="profile-img" src="<?php echo base_url() ?>media/img/logo_hikari_login.png" alt="" />
                        <form class="form-signin">
                            <input type="text" id="nomusuario" class="form-control" placeholder="Ingrese usuario" required autofocus/>
                            <input type="password"  id="clave" class="form-control" placeholder="Ingrese clave" required/>
                            <div id="loginMsj" style="display: none;margin-bottom: 10px;" class="alert alert-danger fade in">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <strong>Error!</strong> <span id="msj">Acceso denegado.</span>
                            </div>
                            <button id="btnentrarlogin" class="btn btn-lg btn-primary btn-block">Ingresar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="<?php echo base_url(); ?>media/js/jquery.min.js"></script>
        <script src="<?php echo base_url() ?>media/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>media/js/login.js"></script>

    </body>
</html>
