<?php
if ($tipo == "texto") {
    ?>
    <input id="txtbuscador_filtro" class="form-control espacio5" value="">
    <?php
} elseif ($tipo == "tipodoc") {
    ?>
    <select class="form-control espacio5" id="txtbuscador_filtro">
        <?php
        foreach ($oTipoDoc as $itemTipodoc) {
            ?>
            <option value="<?php echo $itemTipodoc->idtipodoc; ?>" ><?php echo $itemTipodoc->nombre; ?></option>        
            <?php
        }
        ?>
    </select>
    <?php
} elseif ($tipo == "estado") {
    ?>
    <select class="form-control espacio5" id="txtbuscador_filtro">
        <option value="1">Recibido</option>
        <option value="2">En transporte</option>
        <option value="3">Almacenado</option>
        <option value="4">Derivado</option>
    </select>
    <?php
} elseif ($tipo == "fecha") {
    ?>
    <div class="row">
        <div class="col-sm-12 col-sm-6">
            <div class="input-group espacioControles">
                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar"></span></span>
                <input type="text" readonly="" id="finicio" class="form-control espacioControles" aria-describedby="basic-addon1" placeholder="Fecha inicio">            
            </div>
        </div>
        <div class="col-sm-12 col-sm-6">
            <div class="input-group espacioControles">
                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar"></span></span>
                <input type="text" readonly="" id="ffin" class="form-control espacioControles" aria-describedby="basic-addon1" placeholder="Fecha fin">            
            </div>
        </div>
    </div>
    <?php
}
?>
<script>
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
</script>
<script>
    $(document).ready(function () {
        $("#finicio").datepicker();
        $("#ffin").datepicker();
    });
</script>