<?php
$mensaje = $this->session->userdata('mensaje');
if ($mensaje != "") {
    getHtmlMensaje(2, $mensaje);
}
?>
<style>
    .item-ver{
        margin-bottom: 3px;
    }
    .item-ver strong{
        font-size: 14px;
    }
    .item-ver div{
        font-style: italic;
    }
</style>
<div id="mensajeaux"></div>
<div class="row">
    <div class="col-md-8">
        <input id="txtid" type="hidden" value="<?php echo $objDocumento["iddocumento"]; ?>">
        <span class="leyenda text-center">Datos del documento</span>
        <hr>
    </div>
</div>
<div class="row">
    <div class="col-md-8" style="border-bottom: 1px solid #ECECEC;margin-bottom: 3px;">
        <strong style="font-size: 18px;"><?php echo $objDocumento["codigo"]; ?></strong>        
    </div>
</div>

<div class="row">
    <div class="col-md-8">
        <div class="col-sm-12 col-md-4">
            <div class="item-ver">
                <strong>Remitente: </strong>
                <div><?php echo $objDocumento["origen"]; ?></div>
            </div>

            <div class="item-ver">
                <strong>Destinatario: </strong>
                <div><?php echo $objDocumento["destinatario"]; ?></div>
            </div>

            <div class="item-ver">
                <strong>Tipo doc: </strong>
                <div><?php echo $objDocumento["tipodoc"]; ?></div>
            </div>

            <div class="item-ver">
                <strong>Palabras claves: </strong>
                <?php
                $cadenaTags = "";
                foreach ($aTags as $itemtag) {
                    $cadenaTags.=$itemtag->nombre . ",";
                }
                ?>
                <div><?php echo trim($cadenaTags, ","); ?></div>
            </div>

        </div>
        <div class="col-sm-12 col-md-4">
            <div class="item-ver">
                <strong>Fecha Recep: </strong>
                <div><?php echo $objDocumento["fecha_recepcion"]; ?></div>
            </div>

            <div class="item-ver">
                <strong>Creador: </strong>
                <div><?php echo $objDocumento["nomcreador"]; ?></div>
            </div>

        </div>
        <div class="col-sm-12 col-md-4">
            <div class="item-ver">
                <strong>Estado: </strong>
                <div><?php echo $objDocumento["nomestado"]; ?></div>
            </div>        
        </div>
    </div>

</div>
<?php
if ($objDocumento["file"] != "") {
    ?>
    <div class="row">
        <div class="col-md-8">
            <span class="leyenda text-center">Datos del almacén</span>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="col-sm-12 col-md-4">
                <div class="item-ver">
                    <strong>File: </strong>
                    <div><?php echo $objDocumento["file"]; ?></div>
                </div> 
            </div>
            <div class="col-sm-12 col-md-4">
                <div class="item-ver">
                    <strong>Lugar Almacenaje: </strong>
                    <div><?php echo $objDocumento["nomalmacenaje"]; ?></div>
                </div> 
            </div>
            <div class="col-sm-12 col-md-4">
                <div class="item-ver">
                    <strong>Código: </strong>
                    <div><?php echo $objDocumento["box_estante"]; ?></div>
                </div>        
            </div>
        </div>

    </div>
    <?php
}
?>
<br>
<div class="row">
    <div class="col-sm-12 col-md-8">
        <?php
        if ($aArchivos != FALSE) {
            ?>
            <ul class="list-group" id="contact-list2" style="margin-bottom: 6px;">
                <li class="list-group-item">
                    <div id="mntrs" class="clearfix">
                        <?php
                        foreach ($aArchivos as $itemArchivo) {
                            $info = pathinfo(getcwd() . "/" . $itemArchivo->url);
                            $extension = $info['extension'];
                            if (strtoupper($extension) == "JPG" || strtoupper($extension) == "JPEG" || strtoupper($extension) == "PNG" || strtoupper($extension) == "GIF") {
                                ?>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <a class="fancybox" rel="gallery1" href="<?php echo base_url() . "media/" . $itemArchivo->url ?>">
                                        <div class="clearfix thumbnail">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <img width="50" height="50" src="<?php echo base_url() . "media/archivos/thumbs/" . substr(trim($itemArchivo->url), 9) ?>" class="img-responsive">
                                                </div>
                                                <div class="col-xs-9">
                                                    <div>
                                                        <p><?php echo substr(substr(trim($itemArchivo->url), 9), 0, 10) . "." . $extension; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <?php
                            } else {
                                ?>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <a class="fancybox" rel="gallery1" href="<?php echo base_url() . "media/" . $itemArchivo->url ?>">
                                        <div class="clearfix thumbnail">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <img width="50" height="50" src="<?php echo base_url() . "media/img/" . $extension . ".png" ?>" class="img-responsive">
                                                </div>
                                                <div class="col-xs-9">
                                                    <div>
                                                        <p><?php echo substr(substr(trim($itemArchivo->url), 9), 0, 10) . "." . $extension; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>    
                                <?php
                            }
                        }
                        ?>
                    </div>
                </li>
            </ul>
            <?php
        }
        ?>

    </div><!--/span-->    
</div><!--/row--> 
<div class="row">
    <div class="col-md-8">
        <span class="leyenda text-center">Datos del traslado</span>
        <hr>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <div class="col-sm-12 col-md-4">
            <div class="item-ver">
                <strong>Fecha de envío</strong>
                <div class="input-group espacioControles">
                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar"></span></span>
                    <input type="text" readonly="" id="ffin" class="form-control espacioControles" aria-describedby="basic-addon1" placeholder="Fecha de envío">            
                </div>
            </div> 
        </div>       
    </div>

</div>
<div class="row">
    <div class="col-md-8">
        <div class="col-sm-12 col-md-6">
            <div class="item-ver">
                <strong>Empresa de transpote</strong>
                <div><input id="txtempresa" class="form-control" placeholder="Empresa de transporte"></div>
            </div> 
        </div>
        <div class="col-sm-12 col-md-6">
            <div class="item-ver">
                <strong>Responsable: </strong>
                <div><input id="txtchofer" class="form-control" placeholder="Nombre del responsable"></div>
            </div> 
        </div>
    </div>

</div>
<div class="row">
    <div class="col-md-8">
        <div class="col-sm-12 col-md-6">
            <div class="item-ver">
                <strong>Tipo de Vehículo</strong>
                <div>
                    <select id="cbotipovechiculo" class="form-control">
                        <option value="">Seleccione tipo</option>
                        <option value="1">Carro</option>
                        <option value="2">Moto</option>
                        <option value="3">Avión</option>
                    </select>
                </div>
            </div> 
        </div>
        <div class="col-sm-12 col-md-6">
            <div class="item-ver">
                <strong>Identificador del vehículo: </strong>
                <div><input id="txtplaca" class="form-control" placeholder="Identificador del vehículo"></div>
            </div> 
        </div>
    </div>

</div>
<div class="row">
    <div class="col-md-8">
        <div class="col-sm-12 col-md-12">
            <div class="item-ver">
                <strong>Observación: </strong>
                <div><textarea id="txtdescripcion" class="form-control" rows="3" placeholder="Escribir detalle"></textarea></div>
            </div> 

        </div>
    </div>
</div><!--/row-->  
<br>
<div class="clearfix"></div>
<div class="row">
    <div class="col-sm-12 col-md-2 text-center">

    </div>
    <div class="col-sm-12 col-md-2 text-center">
        <button type="button" id="btnTransportar" class="btn btn-primary">Grabar</button>                
    </div>
    <div class="col-sm-12 col-md-2 text-center">
        <a href="<?php echo base_url() ?>documento/lista" id="btnSalir" class="btn btn-link">Salir</a>
    </div>

</div>
