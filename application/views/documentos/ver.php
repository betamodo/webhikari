<?php
$mensaje = $this->session->userdata('mensaje');
if ($mensaje != "") {
    getHtmlMensaje(2, $mensaje);
}
?>
<style>
    .item-ver{
        margin-bottom: 3px;
    }
    .item-ver strong{
        font-size: 14px;
    }
    .item-ver div{
        font-style: italic;
    }
</style>
<div id="mensajeaux"></div>
<div class="row">
    <div class="col-md-8">
        <span class="leyenda text-center">Datos del documento</span>
        <hr>
    </div>
</div>
<div class="row">
    <div class="col-md-8" style="margin-bottom: 3px;">
        <strong style="font-size: 18px;"><?php echo $objDocumento["codigo"]; ?></strong>
        <hr>
    </div>
</div>

<div class="row">
    <div class="col-md-8">
        <div class="col-sm-12 col-md-4">
            <div class="item-ver">
                <strong>Remitente: </strong>
                <div><?php echo $objDocumento["origen"]; ?></div>
            </div>

            <div class="item-ver">
                <strong>Destinatario: </strong>
                <div><?php echo $objDocumento["destinatario"]; ?></div>
            </div>

            <div class="item-ver">
                <strong>Tipo doc: </strong>
                <div><?php echo $objDocumento["tipodoc"]; ?></div>
            </div>

        </div>
        <div class="col-sm-12 col-md-4">
            <div class="item-ver">
                <strong>Fecha Recep: </strong>
                <div><?php echo $objDocumento["fecha_recepcion"]; ?></div>
            </div>

            <div class="item-ver">
                <strong>Creador: </strong>
                <div><?php echo $objDocumento["nomcreador"]; ?></div>
            </div>

        </div>
        <div class="col-sm-12 col-md-4">
            <div class="item-ver">
                <strong>Estado: </strong>
                <div><?php echo $objDocumento["nomestado"]; ?></div>
            </div>        
        </div>
    </div>

</div>

<div class="row">
    <div class="col-sm-12 col-md-8">
        <br>
        <hr>
        <?php
        if ($aArchivos != FALSE) {
            ?>
            <ul class="list-group" id="contact-list2" style="margin-bottom: 6px;">
                <li class="list-group-item">
                    <div id="mntrs" class="clearfix">
                        <?php
                        foreach ($aArchivos as $itemArchivo) {
                            $info = pathinfo(getcwd() . "/" . $itemArchivo->url);
                            $extension = $info['extension'];
                            if (strtoupper($extension) == "JPG" || strtoupper($extension) == "JPEG" || strtoupper($extension) == "PNG" || strtoupper($extension) == "GIF") {
                                ?>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <a class="fancybox" rel="gallery1" href="<?php echo base_url() . "media/" . $itemArchivo->url ?>">
                                        <div class="clearfix thumbnail">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <img width="50" height="50" src="<?php echo base_url() . "media/archivos/thumbs/" . substr(trim($itemArchivo->url), 9) ?>" class="img-responsive">
                                                </div>
                                                <div class="col-xs-9">
                                                    <div>
                                                        <p><?php echo substr(substr(trim($itemArchivo->url), 9), 0, 10) . "." . $extension; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <?php
                            } else {
                                ?>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <a class="fancybox" rel="gallery1" href="<?php echo base_url() . "media/" . $itemArchivo->url ?>">
                                        <div class="clearfix thumbnail">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <img width="50" height="50" src="<?php echo base_url() . "media/img/" . $extension . ".png" ?>" class="img-responsive">
                                                </div>
                                                <div class="col-xs-9">
                                                    <div>
                                                        <p><?php echo substr(substr(trim($itemArchivo->url), 9), 0, 10) . "." . $extension; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>    
                                <?php
                            }
                        }
                        ?>
                    </div>
                </li>
            </ul>
            <?php
        }
        ?>

    </div><!--/span-->    
</div><!--/row-->  
<?php
if ($oTransporte != false) {
    ?>
    <div class="row">
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">
                    <span class="leyenda text-center">Datos de transporte</span>
                    <hr>
                </div>
            </div>
            <div class="row">   
                <div class="col-md-4 espacio5">
                    <div class="item-ver">
                        <strong>Fecha de envío</strong>
                        <div><?php echo getFechaEs2($oTransporte->fecha_envio); ?></div>
                    </div>
                </div> 
                <div class="col-md-4 espacio5">
                    <div class="item-ver">
                        <strong>Modalidad</strong>
                        <div>
                            <?php
                            if ($oTransporte->modalidad == 1) {
                                echo "Contrato de empresa";
                            } else {
                                echo "Recojo en empresa";
                            }
                            ?>
                        </div>
                    </div>
                </div> 
                <div class="col-md-4 espacio5">
                    <div class="item-ver">
                        <strong>Responsable</strong>
                        <div><?php echo $oTransporte->responsable; ?></div>
                    </div>
                </div>                
            </div><!--/row-->
            <div class="row">
                <div class="col-md-12 espacio5">
                    <div class="item-ver">
                        <strong>Almacén destino</strong>
                        <div><?php echo $objDocumento["nomalmacen"]; ?></div>
                    </div> 
                </div>
            </div><!--/row--> 
            <div class="row">
                <div class="col-md-12 espacio5">
                    <div class="item-ver">
                        <strong>Comentario</strong>
                        <div><?php echo $oTransporte->comentario; ?></div>
                    </div> 
                </div>
            </div><!--/row--> 
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-8">
            <?php
            if ($aArchivosDocTransporte != FALSE) {
                ?>
                <ul class="list-group" id="contact-list2" style="margin-bottom: 6px;">
                    <li class="list-group-item">
                        <div id="mntrs" class="clearfix">
                            <?php
                            foreach ($aArchivosDocTransporte as $itemArchivo) {
                                $info = pathinfo(getcwd() . "/" . $itemArchivo->url);
                                $extension = $info['extension'];
                                if (strtoupper($extension) == "JPG" || strtoupper($extension) == "JPEG" || strtoupper($extension) == "PNG" || strtoupper($extension) == "GIF") {
                                    ?>
                                    <div class="col-xs-12 col-sm-6 col-md-4">
                                        <a class="fancybox" rel="gallery1" href="<?php echo base_url() . "media/" . $itemArchivo->url ?>">
                                            <div class="clearfix thumbnail">
                                                <div class="row">
                                                    <div class="col-xs-3">
                                                        <img width="50" height="50" src="<?php echo base_url() . "media/archivos/thumbs/" . substr(trim($itemArchivo->url), 9) ?>" class="img-responsive">
                                                    </div>
                                                    <div class="col-xs-9">
                                                        <div>
                                                            <p><?php echo substr(substr(trim($itemArchivo->url), 9), 0, 10) . "." . $extension; ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <?php
                                } else {
                                    ?>
                                    <div class="col-xs-12 col-sm-6 col-md-4">
                                        <a class="fancybox" rel="gallery1" href="<?php echo base_url() . "media/" . $itemArchivo->url ?>">
                                            <div class="clearfix thumbnail">
                                                <div class="row">
                                                    <div class="col-xs-3">
                                                        <img width="50" height="50" src="<?php echo base_url() . "media/img/" . $extension . ".png" ?>" class="img-responsive">
                                                    </div>
                                                    <div class="col-xs-9">
                                                        <div>
                                                            <p><?php echo substr(substr(trim($itemArchivo->url), 9), 0, 10) . "." . $extension; ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>    
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </li>
                </ul>
                <?php
            }
            ?>

        </div><!--/span-->    
    </div><!--/row-->
    <?php
}
?>
<?php
if ($objDocumento["file"] != "") {
    ?>
    <div class="row">
        <div class="col-md-8">
            <span class="leyenda text-center">Datos del almacén</span>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="col-sm-12 col-md-4">
                <div class="item-ver">
                    <strong>File: </strong>
                    <div><?php echo $objDocumento["file"]; ?></div>
                </div> 
            </div>
            <div class="col-sm-12 col-md-4">
                <div class="item-ver">
                    <strong>Sitio de Almacenaje: </strong>
                    <div><?php echo $objDocumento["nomalmacenaje"]; ?></div>
                </div> 
            </div>
            <div class="col-sm-12 col-md-4">
                <div class="item-ver">
                    <strong>Cod Box: </strong>
                    <div><?php echo $objDocumento["codbox"]; ?></div>
                </div>        
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-sm-12 col-md-8">
            <?php
            if ($aArchivosDocTransporte != FALSE) {
                ?>
                <ul class="list-group" id="contact-list2" style="margin-bottom: 6px;">
                    <li class="list-group-item">
                        <div id="mntrs" class="clearfix">
                            <?php
                            foreach ($aArchivosDocTransporte as $itemArchivo) {
                                $info = pathinfo(getcwd() . "/" . $itemArchivo->url);
                                $extension = $info['extension'];
                                if (strtoupper($extension) == "JPG" || strtoupper($extension) == "JPEG" || strtoupper($extension) == "PNG" || strtoupper($extension) == "GIF") {
                                    ?>
                                    <div class="col-xs-12 col-sm-6 col-md-4">
                                        <a class="fancybox" rel="gallery1" href="<?php echo base_url() . "media/" . $itemArchivo->url ?>">
                                            <div class="clearfix thumbnail">
                                                <div class="row">
                                                    <div class="col-xs-3">
                                                        <img width="50" height="50" src="<?php echo base_url() . "media/archivos/thumbs/" . substr(trim($itemArchivo->url), 9) ?>" class="img-responsive">
                                                    </div>
                                                    <div class="col-xs-9">
                                                        <div>
                                                            <p><?php echo substr(substr(trim($itemArchivo->url), 9), 0, 10) . "." . $extension; ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <?php
                                } else {
                                    ?>
                                    <div class="col-xs-12 col-sm-6 col-md-4">
                                        <a class="fancybox" rel="gallery1" href="<?php echo base_url() . "media/" . $itemArchivo->url ?>">
                                            <div class="clearfix thumbnail">
                                                <div class="row">
                                                    <div class="col-xs-3">
                                                        <img width="50" height="50" src="<?php echo base_url() . "media/img/" . $extension . ".png" ?>" class="img-responsive">
                                                    </div>
                                                    <div class="col-xs-9">
                                                        <div>
                                                            <p><?php echo substr(substr(trim($itemArchivo->url), 9), 0, 10) . "." . $extension; ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>    
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </li>
                </ul>
                <?php
            }
            ?>

        </div><!--/span-->    
    </div><!--/row-->
    <?php
}
?>
<br>
<?php
if ($aMovimiento != false) {
    ?>
    <div class="row">
        <div class="col-md-8">
            <span class="leyenda text-center">Datos del historial</span>
            <hr>
        </div>
    </div>
    <?php
    foreach ($aMovimiento as $itemMovimiento) {
        ?>
        <div class="row">
            <div class="col-md-8">
                <div class="qa-message-list" id="wallmessages">
                    <div class="message-item" id="m16">
                        <div class="message-inner">
                            <div class="message-head clearfix">
                                <div class="avatar pull-left"><a href="#"><img width="40" height="40" src="http://www.intranet.hikari.com.pe/media/archivos/avatars/thumbs/default.png"></a></div>
                                <div class="pull-left">
                                    <div class="user-detail">
                                        <h5 class="handle"><?php echo getnomEstadoDoc($itemMovimiento->estado) ?></h5>
                                        <div class="post-meta">
                                            <span><?php echo getFechaEs($itemMovimiento->fecha_creacion); ?></span>
                                            <span><?php echo $oUsuario[$itemMovimiento->idusuario_creador]["nombre"] ?></span>
                                        </div>
                                    </div>
                                </div>                        
                            </div>
                            <div class="qa-message-content"><?php echo $itemMovimiento->comentario; ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}
?>
<br>
<div class="clearfix"></div>

