<style>
    .item-ver{
        margin-bottom: 3px;
    }
    .item-ver strong{
        font-size: 14px;
    }
    .item-ver div{
        font-style: italic;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="item-ver text-center">
            <input type="hidden" id="iddoc" value="<?php echo $oDocumento->iddocumento; ?>">
            <strong><?php echo $oDocumento->codigo; ?></strong>
        </div> 
        <div class="item-ver">
            <strong>Asunto:</strong>
            <div><?php echo $oDocumento->asunto; ?></div>
        </div>         
        <div class="item-ver">
            <strong>De:</strong>
            <div><?php echo $oDocumento->remitente; ?></div>
        </div>
        <br>
        <hr>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div>
            <select  id="cbocategoria" class="form-control">
                <?php
                foreach ($aCategorias as $itemCategoria) {
                    ?>
                    <option value="<?php echo $itemCategoria->idcategoria; ?>" <?php if ($itemCategoria->idcategoria == $oDocumento->idcategoria) echo "selected"; ?>><?php echo $itemCategoria->nombre ?></option>
                    <?php
                }
                ?>
            </select>
        </div>
        <div class="espacio5">
            <textarea id="txtdescripcion" class="form-control" rows="3" placeholder="Mensaje de notificación"></textarea>
        </div>
    </div>
</div>