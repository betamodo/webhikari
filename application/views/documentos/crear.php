<?php 
if($this->session->userdata('mensaje') != ""){
?>
<div class="alert alert-danger fade in">
	<a href="#" class="close" data-dismiss="alert">&times;</a>
	<?php echo $this->session->userdata('mensaje');?>
</div>
<?php
}
?>
<div class="row">
    <div class="col-md-8">
        <span class="leyenda text-center">Datos del documento</span>
        <hr>
    </div>
</div>
<form action="<?php echo base_url()."documento/json_crear_documento"?>"  method="post" enctype="multipart/form-data">
	<div class="row">
		<div class="col-md-8">
			<!-- row/ Nro Doc y fecha de recepción-->
			<div class="row">   
				<div class="col-md-6 espacio5">
				<span><strong>Identificador</strong></span> 
					<input type="text" id="txtidentificador" name="txtidentificador" class="form-control" placeholder="Nro. documento">    
				</div>
				<div class="col-md-6 espacio5">
				<span><strong>Fecha de Recepción</strong></span> 
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar"></span></span>
						<input type="text" readonly="" id="finicio" name="frecepcion" class="form-control espacioControles" aria-describedby="basic-addon1" placeholder="Fecha recepción">            
					</div>
				</div>
			</div><!--/row-->

			<!-- row/ Origen-->
			<div class="row">   
				<div class="col-md-12 espacio5">
					<span><strong>Remitente</strong></span> 
					<input type="text" id="txtorigen" name="txtorigen" class="form-control" placeholder="Instituación o persona remitente">    
				</div>                
			</div><!--/row-->

			<div class="row">   
				<div class="col-md-6 espacio5">
				<span><strong>Área destinada</strong></span> 
					<select class="form-control" name="cbocategoria" id="cbocategoria">
						<option value="">Área destinada</option>
						<?php
						if ($aCategoria != false) {
							foreach ($aCategoria as $itemCategoria) {
								?>
								<option value="<?php echo $itemCategoria->idcategoria; ?>"><?php echo $itemCategoria->nombre; ?></option>                            
								<?php
							}
						}
						?>
					</select>
				</div>
				<div class="col-md-6 espacio5"> 
					<span><strong>Tipo de documento</strong></span> 			
					<select class="form-control" id="cbotipodoc" name="cbotipodoc">
						<option value="">Tipo doc.</option>
						<?php
						if ($aTipoDoc != false) {
							foreach ($aTipoDoc as $itemDoc) {
								?>
								<option value="<?php echo $itemDoc->idtipodoc; ?>"><?php echo $itemDoc->nombre; ?></option>                            
								<?php
							}
						}
						?>
					</select>
				</div><!--/span-->
			</div><!--/row-->

			<div class="row">   
				<div class="col-md-12 espacio5">
				<span><strong>Asunto</strong></span> 
					<input type="text" id="txtasunto" name="txtasunto" class="form-control" placeholder="Asunto">
				</div>                
			</div><!--/row-->

			<div class="row">
				<div class="col-md-6 espacio5">
					<strong>Almacén</strong>
					<select class="form-control" id="cboLugar" name="cboLugar">
						<option value="">Seleccione lugar de almacenamiento</option>
						<?php
						if ($aAlmacenes != false) {
							foreach ($aAlmacenes as $itemAlmacen) {
								?>
								<option value="<?php echo $itemAlmacen->idalmacen; ?>"><?php echo $itemAlmacen->nombre; ?></option>                            
								<?php
							}
						}
						?>

					</select>
				</div>
				<div class="col-md-6 espacio5">
					<strong>COD Box</strong>
					<input type="text" id="txtbox" name="txtbox" class="form-control" placeholder="">   
				</div>
			</div><!--/row-->  
			<div class="row">
				<div class="col-md-6 espacio5">
					<strong>File</strong>
						<select class="form-control" id="cbofile" name="cbofile">
							<option value="">Seleccione file</option>
							<?php
							if ($aFile != false) {
								foreach ($aFile as $itemFile) {
									?>
									<option value="<?php echo $itemFile->idfile; ?>"><?php echo $itemFile->codigo; ?></option>                            
									<?php
								}
							}
							?>
						</select>
				</div>
				<div class="col-md-6 espacio5">
						<strong style="display:none;">Stand</strong>
						<select class="form-control" style="display:none;" id="cbostand" name="cbostand">
							<option value="1">Seleccione stand</option>
						</select>
				</div>
			</div><!--/row-->
			<div class="row">
				<div class="col-md-12 espacio5">
				<span><strong>Comentario</strong></span>
					<textarea id="txtdescripcion" name="txtdescripcion" class="form-control" rows="3" placeholder="Escribir comentario"></textarea>
				</div><!--/span-->                
			</div><!--/row-->  
			
			<div class="row">
				<div class="col-md-8"> 
					<input type="file" name="archivo[]" id="archivo" class="custom-file-input" multiple>                         
				</div>
			</div>
		</div><!--/span-->

		<div class="col-md-4">

		</div><!--/span-->       
	</div><!--/row-->   

	<div class="clearfix"></div>
	<div class="row">
		<div class="col-sm-12 col-md-4 text-center">
			<button type="submit" id="btnCrearDoc" class="btn btn-primary">Crear tarea</button>                
		</div>
		<div class="col-sm-12 col-md-4 text-center">
			<a href="<?php echo base_url() ?>documento/lista" id="btnSalir" class="btn btn-link">Salir</a>
		</div>

	</div>
</form>
<?php
$this->session->set_userdata('mensaje', "");
?>
