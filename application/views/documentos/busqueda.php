<div class="row">
    <div class="col-md-12">
        <span class="leyenda text-center">Búsqueda avanzada</span>
        <hr>
    </div>
</div>
<div class="row">
    <div class="col-sm-12 col-sm-3">
        <select id="select_almacen" class="form-control espacioControles" name="select_almacen">
            <option value="">Todas los almacenes</option>
            <?php
            foreach ($aAlmacen as $itemAlmacen) {
                ?>
                <option value="<?php echo $itemAlmacen->idalmacen; ?>"><?php echo $itemAlmacen->nombre; ?></option>
                <?php
            }
            ?>
        </select>
    </div>
    <div class="col-sm-12 col-sm-3">
        <input type="text" id="bus_file" class="form-control" placeholder="Código del file">
    </div>
    <div class="col-sm-12 col-sm-3">
        <input type="text" id="bus_box" class="form-control" placeholder="Código box o Stand">
    </div>   
</div>

<div class="row">
    <div class="col-sm-12 col-sm-3">
        <select id="select_tipodoc" class="form-control espacioControles" name="select_tipodoc">
            <option value="">Todas los tipos</option>
            <?php
            foreach ($aTipodoc as $itemtipo) {
                ?>
                <option value="<?php echo $itemtipo->idtipo_doc; ?>"><?php echo $itemtipo->nombre; ?></option>
                <?php
            }
            ?>
        </select>
    </div>
    <div class="col-sm-12 col-sm-3">
        <input type="text" id="bus_codigo" class="form-control" placeholder="Código del documento">
    </div>
    <div class="col-sm-12 col-sm-3">
        <input type="text" id="bus_asunto" class="form-control" placeholder="Asunto del documento">
    </div>    
</div>
<div class="row">
    <div class="col-sm-12 col-sm-9">
        <input type="text" id="bus_descripcion" style="margin-bottom: 5px;" class="form-control" placeholder="Descripción del documento">
    </div>
</div>
<div class="row">
    <div class="col-sm-12 col-sm-6">
        <div class="input-group espacioControles">
            <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar"></span></span>
            <input type="text" readonly="" id="finicio" class="form-control espacioControles" aria-describedby="basic-addon1" placeholder="Fecha inicio">            
        </div>
    </div>
    <div class="col-sm-12 col-sm-6">
        <div class="input-group espacioControles">
            <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar"></span></span>
            <input type="text" readonly="" id="ffin" class="form-control espacioControles" aria-describedby="basic-addon1" placeholder="Fecha fin">            
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-1">
        <button id="btnBuscar" class="btn btn-sm btn-primary" style="width: 100%;display: inline-block;line-height: 22px;margin-bottom: 5px;">Buscar</button> 
    </div>
    <div class="col-sm-12 col-md-1">
        <button id="btnlimpiar" class="btn btn-sm btn-warning" style="width: 100%;line-height: 22px;">Limpiar</button>
    </div>
</div>

<br>
<hr>
<script>
    $("#btnlimpiar").click(function () {
        $("#select_local").val("");
        $("#finicio").val("");
        $("#ffin").val("");
        $("#montoinicial").val("");
        $("#montofinal").val("");
        $("#montoigual").val("");
    });
    $("#btnBuscar").click(function () {
        var idalmacen = $("#select_almacen").val();
        var file = $("#bus_file").val();
        var box = $("#bus_box").val();
        var idtipo = $("#select_tipodoc").val();
        var codigo = $("#bus_codigo").val();
        var asunto = $("#bus_asunto").val();
        var descripcion = $("#bus_descripcion").val();
        var finicio = $("#finicio").val();
        var ffin = $("#ffin").val();


        var formData = new FormData();
        formData.append('idalmacen', idalmacen);
        formData.append('file', file);
        formData.append('box', box);
        formData.append('idtipo', idtipo);
        formData.append('codigo', codigo);
        formData.append('asunto', asunto);
        formData.append('descripcion', descripcion);
        formData.append('finicio', finicio);
        formData.append('ffin', ffin);

        $("#listaTareas").html("<div class='cargando'></div>");
        $.post("json_buscar", {idalmacen: idalmacen, file: file, box: box, finicio: finicio, ffin: ffin, idtipo: idtipo, codigo: codigo, asunto: asunto, descripcion: descripcion}, function (data) {
            $("#listaTareas").html(data);
        });
    });
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
</script>
<script>
    $(document).ready(function () {
        $("#finicio").datepicker();
        $("#ffin").datepicker();
    });
</script>