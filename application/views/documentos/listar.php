<?php
$idalmacen = $this->session->userdata('filtro_idalmacen');
$file = $this->session->userdata('filtro_file');
$stand = $this->session->userdata('filtro_stand');
$estado = $this->session->userdata('filtro_estado');
$idtipo = $this->session->userdata('filtro_idtipo');
$codigo = $this->session->userdata('filtro_codigo');
$codbox = $this->session->userdata('filtro_codbox');
$asunto = $this->session->userdata('filtro_asunto');
$remitente = $this->session->userdata('filtro_remitente');
$ffin = $this->session->userdata('filtro_ffin');
$finicio = $this->session->userdata('filtro_finicio');
$idcategoria = $this->session->userdata('filtro_categoria');
?>
<style>
    .campo{
        margin-right: 10px;
        border: 1px solid #c3c3c3;
        padding: 3px;
        border-radius: 4px;
        display: inline-block;
        margin-bottom: 5px;
    }
    .delete_campo{
        cursor: pointer;
        color: red;
        font-weight: bold;
        padding: 2px;
    }
    legend{
        font-size: 13px;
        font-weight: 700;
        font-style: italic;
    }
</style>
<div id="busquedaAvanzada">
 <div class="row">
    <div class="col-md-12">
        <span class="leyenda text-center">Búsqueda avanzada</span>
        <hr>
    </div>
</div>
<div class="row">
    <div class="col-sm-12 col-sm-3">
        <select id="select_almacen" class="form-control espacioControles" name="select_almacen">
            <option value="">Todas los almacenes</option>
            <?php
            foreach ($aAlmacen as $itemAlmacen) {
                ?>
                <option value="<?php echo $itemAlmacen->idalmacen; ?>" <?php if($itemAlmacen->idalmacen==$idalmacen) echo "selected";?>><?php echo $itemAlmacen->nombre; ?></option>
                <?php
            }
            ?>
        </select>
    </div>
    <div class="col-sm-12 col-sm-3">
        <select id="bus_file" class="form-control espacioControles" name="bus_file">
            <option value="">Todas los files</option>
            <?php
			foreach ($aFiles as $itemFile) {
                ?>
                <option value="<?php echo $itemFile->idfile; ?>" <?php if($itemFile->idfile==$file) echo "selected";?>><?php echo $itemFile->codigo; ?></option>
                <?php
            }
            ?>
        </select>
    </div>
    <div class="col-sm-12 col-sm-3">
        <select id="bus_stand" class="form-control espacioControles" name="bus_stand">
            <option value="">Todas los stand</option>
            <?php
			foreach ($aStand as $itemStand) {
                ?>
                <option value="<?php echo $itemStand->idstand; ?>" <?php if($itemStand->idstand==$stand) echo "selected";?>><?php echo $itemStand->codigo; ?></option>
                <?php
            }
            ?>
        </select>
    </div> 
	<div class="col-sm-12 col-sm-3">
		<select class="form-control espacioControles" id="bus_estado">
			<option value="">Todos los estados</option>
			<option value="1" <?php if($estado==1) echo "selected";?>>Recibido</option>
			<option value="2" <?php if($estado==2) echo "selected";?>>En transporte</option>
			<option value="3" <?php if($estado==3) echo "selected";?>>Almacenado</option>
			<option value="4" <?php if($estado==4) echo "selected";?>>Derivado</option>
		</select>	
	</div> 
</div>

<div class="row">
    <div class="col-sm-12 col-sm-3">
        <select id="select_tipodoc" class="form-control espacioControles" name="select_tipodoc">
            <option value="">Todas los tipos</option>
            <?php
            foreach ($aTipodoc as $itemtipo) {
                ?>
                <option value="<?php echo $itemtipo->idtipodoc; ?>" <?php if($itemtipo->idtipodoc==$idtipo) echo "selected";?>><?php echo $itemtipo->nombre; ?></option>
                <?php
            }
            ?>
        </select>
    </div>
    <div class="col-sm-12 col-sm-3">
		<input type="text" id="bus_codigo" value="<?php echo $codigo;?>" class="form-control" placeholder="Código del documento">        
    </div>
	<div class="col-sm-12 col-sm-3">
        <input type="text" id="bus_codigobox" value="<?php echo $codbox;?>"  class="form-control" placeholder="COD Box">
    </div>
    <div class="col-sm-12 col-sm-3">
        <input type="text" id="bus_asunto" value="<?php echo $asunto;?>" class="form-control" placeholder="Asunto del documento">
    </div>    
</div>

<div class="row">
    <div class="col-sm-12 col-sm-3">
        <div class="input-group espacioControles">
            <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar"></span></span>
            <input type="text" readonly="" id="finicio"  value="<?php echo $finicio;?>" class="form-control espacioControles" aria-describedby="basic-addon1" placeholder="Fecha inicio recepción">            
        </div>
    </div>
    <div class="col-sm-12 col-sm-3">
        <div class="input-group espacioControles">
            <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar"></span></span>
            <input type="text" readonly="" id="ffin"  value="<?php echo $ffin;?>" class="form-control espacioControles" aria-describedby="basic-addon1" placeholder="Fecha fin recepción">            
        </div>
    </div>
	<div class="col-sm-12 col-sm-3">
	<input type="text" id="bus_remitente" value="<?php echo $remitente;?>" class="form-control" placeholder="Nombre del remitente">
	</div>
	<div class="col-sm-12 col-sm-3">
        <select id="select_cate" class="form-control espacioControles" name="select_tipodoc">
            <option value="">Todas las categorías</option>
            <?php
            foreach ($aCategorias as $itemcate) {
                ?>
                <option value="<?php echo $itemcate->idcategoria; ?>" <?php if($itemcate->idcategoria==$idcategoria) echo "selected";?>><?php echo $itemcate->nombre; ?></option>
                <?php
            }
            ?>
        </select>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-1">
        <button id="btnBuscar" class="btn btn-sm btn-primary" style="width: 100%;display: inline-block;line-height: 22px;margin-bottom: 5px;">Buscar</button> 
    </div>
    <div class="col-sm-12 col-md-1">
        <button id="btnlimpiar" class="btn btn-sm btn-warning" style="width: 100%;line-height: 22px;">Limpiar</button>
    </div>
</div>
</div>
<?php
$this->session->set_userdata('mensaje', "");
?>
<br>
<hr>

<div id="listaTareas">

</div>

<div class="row" style="display: none;">
    <div id="archivos" class="col-md-8">
        <button id="btnAdjuntar" class="btn btn-default classAdjuntar" type="submit">Adjuntar</button>

        <div id="subir" class="clearfix" style="padding-left: 7px;display: inline-block;margin-top: 9px;margin-left: -6px;">
            <input id="my_file_element" type="file" name="file_1" ><br clear="all"/>
            <div id="files_list"></div>                            
        </div>
    </div>
</div>