<?php
$mensaje = $this->session->userdata('mensaje');
if ($mensaje != "") {
    getHtmlMensaje(2, $mensaje);
}
?>
<div id="mensajeaux"></div>
<div class="row">
    <div class="col-md-8">
        <input type="hidden" id="txtiddoc" value="<?php echo $objDocumento->iddocumento; ?>">
        <span class="leyenda text-center">Datos del documento</span>
        <hr>
    </div>
</div>
<div class="row">
    <div class="col-md-4" style="margin-bottom: 3px;">
        <strong style="font-size: 18px;"><?php echo $objDocumento->codigo; ?></strong>

    </div>
    <div class="col-md-4" style="margin-bottom: 3px;">
        <strong style="font-size: 18px;"><?php echo getnomEstadoDoc($objDocumento->estado); ?></strong>
    </div>
    <hr>
</div>
<br>
<div class="row">
    <div class="col-md-8">
        <div class="row">  
            <div class="col-md-6 espacio5">
			<span><strong>Identificador</strong></span>
                <input type="text" id="txtidentificador" class="form-control" value="<?php echo $objDocumento->identificador; ?>" placeholder="Identificador del documento"> 
            </div>
            <div class="col-md-6 espacio5">
			<span><strong>Fecha de recepción</strong></span>
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar"></span></span>
                    <input type="text" readonly="" id="finicio" value="<?php echo getFechaEs2($objDocumento->fecha_recepcion); ?>" class="form-control espacioControles" aria-describedby="basic-addon1" placeholder="Fecha recepción">            
                </div>
            </div>                           
        </div><!--/row-->

        <div class="row">   
            <div class="col-md-12 espacio5">
				<span><strong>Remitente</strong></span>
                <input type="text" id="txtorigen" class="form-control" value="<?php echo $objDocumento->remitente; ?>" placeholder="Remitente del documento">    
            </div>                
        </div><!--/row-->
        <div class="row">   
            <div class="col-md-6 espacio5">
			<span><strong>Área destinada</strong></span>
                <select class="form-control" id="cbocategoria">
                    <option value="">Área destinada</option>
                    <?php
                    if ($aCategoria != false) {
                        foreach ($aCategoria as $itemCategoria) {
                            ?>
                            <option value="<?php echo $itemCategoria->idcategoria; ?>" <?php if ($itemCategoria->idcategoria == $objDocumento->idcategoria) echo "selected"; ?>><?php echo $itemCategoria->nombre; ?></option>                            
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="col-md-6 espacio5">   
				<span><strong>Tipo de documento</strong></span>			
                <select class="form-control" id="cbotipodoc">
                    <option value="">Seleccione tipo doc.</option>
                    <?php
                    if ($aTipoDoc != false) {
                        foreach ($aTipoDoc as $itemDoc) {
                            ?>
                            <option value="<?php echo $itemDoc->idtipodoc; ?>" <?php if ($itemDoc->idtipodoc == $objDocumento->idtipodoc) echo "selected"; ?>><?php echo $itemDoc->nombre; ?></option>                            
                            <?php
                        }
                    }
                    ?>
                </select>
            </div><!--/span-->
        </div><!--/row-->
        <div class="row">   
            <div class="col-md-12 espacio5">
			<span><strong>Asunto</strong></span>
                <input type="text" id="txtasunto" class="form-control" value="<?php echo $objDocumento->asunto; ?>" placeholder="Asunto">    
            </div> 
        </div><!--/row-->
        <div class="row">
            <div class="col-md-12 espacio5">
			<span><strong>Comentario</strong></span>
                <textarea id="txtdescripcion" class="form-control" rows="3" placeholder="Escribir detalle"><?php echo $objDocumento->comentario; ?></textarea>
            </div><!--/span-->                
        </div><!--/row-->  
        <?php
        if ($aArchivos != FALSE) {
            ?>
            <ul class="list-group" id="contact-list2" style="margin-bottom: 6px;">
                <li class="list-group-item">
                    <div id="mntrs" class="clearfix">
                        <?php
                        foreach ($aArchivos as $itemArchivo) {
                            $info = pathinfo(getcwd() . "/" . $itemArchivo->url);
                            $extension = $info['extension'];
                            if (strtoupper($extension) == "JPG" || strtoupper($extension) == "JPEG" || strtoupper($extension) == "PNG" || strtoupper($extension) == "GIF") {
                                ?>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <a class="fancybox" rel="gallery1" href="<?php echo base_url() . "media/" . $itemArchivo->url ?>">
                                        <div class="clearfix thumbnail">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <img width="50" height="50" src="<?php echo base_url() . "media/archivos/thumbs/" . substr(trim($itemArchivo->url), 9) ?>" class="img-responsive">
                                                </div>
                                                <div class="col-xs-9">
                                                    <div>
                                                        <p><?php echo substr(substr(trim($itemArchivo->url), 9), 0, 10) . "." . $extension; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <?php
                            } else {
                                ?>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <a class="fancybox" rel="gallery1" href="<?php echo base_url() . "media/" . $itemArchivo->url ?>">
                                        <div class="clearfix thumbnail">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <img width="50" height="50" src="<?php echo base_url() . "media/img/" . $extension . ".png" ?>" class="img-responsive">
                                                </div>
                                                <div class="col-xs-9">
                                                    <div>
                                                        <p><?php echo substr(substr(trim($itemArchivo->url), 9), 0, 10) . "." . $extension; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>    
                                <?php
                            }
                        }
                        ?>
                    </div>
                </li>
            </ul>
            <?php
        }
        ?>

        <!-- DATOS DE TRASLADO -->
        <?php
        if ($oTransporte != false) {
            ?>
            <div class="row">
                <div class="col-md-12">
                    <span class="leyenda text-center">Datos del traslado</span>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-sm-12 col-md-6">
                        <div class="item-ver">
                            <strong>Fecha de envío</strong>
                            <div class="input-group espacioControles">
                                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar"></span></span>
                                <input type="text" readonly="" id="ffin" value="<?php echo getFechaEs2($oTransporte->fecha_envio); ?>" class="form-control espacioControles" aria-describedby="basic-addon1" placeholder="Fecha de envío">            
                            </div>
                        </div> 
                    </div>
                    <div class="col-md-6 ">
                        <div class="item-ver">
                            <strong>Almacén</strong>
                            <select class="form-control" id="cboLugar">
                                <option value="">Seleccione lugar de almacenamiento</option>
                                <?php
                                if ($aAlmacenes != false) {
                                    foreach ($aAlmacenes as $itemAlmacen) {
                                        ?>
                                        <option value="<?php echo $itemAlmacen->idalmacen; ?>" <?php if ($objDocumento->idalmacen == $itemAlmacen->idalmacen) echo "selected"; ?>><?php echo $itemAlmacen->nombre; ?></option>                            
                                        <?php
                                    }
                                }
                                ?>

                            </select>
                        </div> 
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="col-sm-12 col-md-6">
                        <div class="item-ver">
                            <strong>Modalidad</strong>
                            <div>
                                <select id="cbomodalidad" class="form-control">
                                    <option value="">Seleccione modalidad</option>
                                    <option value="1" <?php if ($oTransporte->modalidad == 1) echo "selected"; ?>>Contrato de empresa</option>
                                    <option value="2" <?php if ($oTransporte->modalidad == 2) echo "selected"; ?>>Recojo en empresa</option>                        
                                </select>
                            </div>
                        </div> 
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="item-ver">
                            <strong>Responsable: </strong>
                            <div><input id="txtresponsable" class="form-control" value="<?php echo $oTransporte->responsable; ?>" placeholder="Nombre del responsable"></div>
                        </div> 
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-sm-12 col-md-12">
                        <div class="item-ver">
                            <strong>Observación: </strong>
                            <div><textarea id="txtdescripcion_transporte" class="form-control" rows="3" placeholder="Escribir detalle"><?php echo $oTransporte->comentario; ?></textarea></div>
                        </div> 

                    </div>
                </div>
            </div><!--/row-->  
            <?php
        }
        ?>
        <br>

        <!-- DATOS DE ALMACENAMIENTO -->
        <?php
        if (intval($objDocumento->idfile) > 0) {
            ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <span class="leyenda text-center">Datos de almacenamiento</span>
                            <hr>
                        </div>
                    </div>
                    <div class="row">   
                        <div class="col-sm-12 col-md-4">
                            <div class="item-ver">
                                <strong>Fecha de envío</strong>
                                <div class="input-group espacioControles">
                                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar"></span></span>
                                    <input type="text" readonly="" id="fauxiliar" value="<?php echo getFechaEs2($objDocumento->fecha_almacenamiento); ?>" class="form-control espacioControles" aria-describedby="basic-addon1" placeholder="Fecha de almacenamiento">            
                                </div>
                            </div> 
                        </div>    
                    </div><!--/row-->
                    <div class="row">   
                        <div class="col-md-4 espacio5">
                            <div class="item-ver">
                                <strong>File</strong>
                                <select class="form-control" id="cbofile">
                                    <option value="">Seleccione file</option>
                                    <?php
                                    if ($aFile != false) {
                                        foreach ($aFile as $itemFile) {
                                            ?>
                                            <option value="<?php echo $itemFile->idfile; ?>" <?php if ($objDocumento->idfile == $itemFile->idfile) echo "selected" ?>><?php echo $itemFile->codigo; ?></option>                            
                                            <?php
                                        }
                                    }
                                    ?>

                                </select>
                            </div> 
                        </div> 
                        <div class="col-md-4 espacio5">
                            <div class="item-ver">
                                <strong>Stand</strong>
                                <select class="form-control" id="cbostand">
                                    <option value="">Seleccione stand</option>
                                    <?php
                                    if ($aStand != false) {
                                        foreach ($aStand as $itemStand) {
                                            ?>
                                            <option value="<?php echo $itemStand->idstand; ?>" <?php if($itemStand->idstand==$objDocumento->idstand) echo "selected"; ?>><?php echo $itemStand->codigo; ?></option>                            
                                            <?php
                                        }
                                    }
                                    ?>

                                </select>
                            </div>                
                        </div>                
                        <div class="col-md-4 espacio5">
                            <div class="item-ver">
                                <strong>COD Box</strong>
                                <input type="text" id="txtbox" value="<?php echo $objDocumento->codbox; ?>" class="form-control" placeholder="">                    
                            </div>                
                        </div>                
                    </div><!--/row-->
                </div><!--/row-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-sm-12 col-md-12">
                            <div class="item-ver">
                                <strong>Observación: </strong>
                                <div><textarea id="txtdescripcion_almacen" class="form-control" rows="3" placeholder="Escribir detalle"><?php echo $objDocumento->comentario; ?></textarea></div>
                            </div> 

                        </div>
                    </div>
                </div><!--/row-->  
            </div><!--/row-->

            <?php
        }
        if ($aArchivosDocAlmacen != FALSE) {
            ?>
            <ul class="list-group" id="contact-list2" style="margin-bottom: 6px;">
                <li class="list-group-item">
                    <div id="mntrs" class="clearfix">
                        <?php
                        foreach ($aArchivosDocAlmacen as $itemArchivo) {
                            $info = pathinfo(getcwd() . "/" . $itemArchivo->url);
                            $extension = $info['extension'];
                            if (strtoupper($extension) == "JPG" || strtoupper($extension) == "JPEG" || strtoupper($extension) == "PNG" || strtoupper($extension) == "GIF") {
                                ?>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <a class="fancybox" rel="gallery1" href="<?php echo base_url() . "media/" . $itemArchivo->url ?>">
                                        <div class="clearfix thumbnail">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <img width="50" height="50" src="<?php echo base_url() . "media/archivos/thumbs/" . substr(trim($itemArchivo->url), 9) ?>" class="img-responsive">
                                                </div>
                                                <div class="col-xs-9">
                                                    <div>
                                                        <p><?php echo substr(substr(trim($itemArchivo->url), 9), 0, 10) . "." . $extension; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <?php
                            } else {
                                ?>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <a class="fancybox" rel="gallery1" href="<?php echo base_url() . "media/" . $itemArchivo->url ?>">
                                        <div class="clearfix thumbnail">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <img width="50" height="50" src="<?php echo base_url() . "media/img/" . $extension . ".png" ?>" class="img-responsive">
                                                </div>
                                                <div class="col-xs-9">
                                                    <div>
                                                        <p><?php echo substr(substr(trim($itemArchivo->url), 9), 0, 10) . "." . $extension; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>    
                                <?php
                            }
                        }
                        ?>
                    </div>
                </li>
            </ul>
            <?php
        }
        ?>
        <!--/FIN DE DATOS DE ALMACEN-->
        <br>
        <hr>

        <div class="row">
            <div class="col-md-8">
                <button id="btnAgregarArchivo" class="btn btn-default classAdjuntar" type="submit">Añadir archivo</button>

                <div id="subir2" class="clearfix" style="padding-left: 7px;display: inline-block;margin-top: 9px;margin-left: -6px;">
                    <input type="hidden" id="hid_input_files" name="hid_input_files" value="file_0" />
                    <div id="remove_0" style="margin-right: 4px;" class="btn btn-sm btn-default btn_remover_files" data-id="0"><span data-id="file_0" class="glyphicon glyphicon-minus-sign btn_remover_files"></span></div>
                    <input type="file" name="file_0" id="file_0" class="custom-file-input">                        
                </div>
            </div>
        </div>
    </div><!--/span-->

    <div class="col-md-4">

    </div><!--/span-->       
</div><!--/row-->   
<br>
<div class="clearfix"></div>
<div class="row">
    <div class="col-sm-12 col-md-2 text-center">

    </div>
    <div class="col-sm-12 col-md-2 text-center">
        <button type="button" id="btnEditarDoc" class="btn btn-primary">Editar documento</button>                
    </div>
    <div class="col-sm-12 col-md-2 text-center">
        <a href="<?php echo base_url() ?>documento/lista" id="btnSalir" class="btn btn-link">Salir</a>
    </div>

</div>
