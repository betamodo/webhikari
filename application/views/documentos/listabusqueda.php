
<?php
$rol = $this->session->userdata("rol");
$aCategoria[0]["nomcategoria"] = "";
$this->session->set_userdata('mensaje', "");
?>
<style>
.btncheck{
	position: absolute;
    left: -29px;
    top: -4px;
}
</style>
<ul class="list-group" id="contact-list2">
    <?php
    foreach ($aListaDocumentos as $itemDoc) {
        ?>
        <li class="list-group-item" style="margin-bottom: 7px;">			
            <div class="col-xs-12 col-sm-4"> 
			    <div><span class="labelCodigo"><?php echo $itemDoc->codigo; ?></span></div>
				<?php
				if ($rol == 6) {
				if($itemDoc->estado == 2 || $itemDoc->estado == 3){
				?>
				<div class="checkbox btncheck"> 
				  <label>
					<input type="checkbox" data-id="<?php echo $itemDoc->iddocumento;?>" data-codigo="<?php echo $itemDoc->codigo;?>" data-estado="<?php echo $itemDoc->estado;?>" value="0">
					<span class="cr"><i class="cr-icon glyphicon glyphicon-arrow-right"></i></span>            
				  </label>
				</div>
				<?php 
					}
				} 
				?>
				<div class="labelNomTarea pad3"><?php echo $itemDoc->asunto ?></div>
                <div><?php echo "<strong>De</strong>:<i>" . $itemDoc->remitente . "</i>"; ?></div>
                <div><?php echo "<strong>Para</strong>:<i>" . $aCategoria[intval($itemDoc->idcategoria)]["nomcategoria"] . "</i>"; ?></div>                    
            </div>
            <div class="col-xs-12 col-sm-3 numero" data-numero="2">
			<span class="labelCodigo">Descripción</span>
                <p><i><?php echo $itemDoc->comentario; ?></i></p>
            </div>
            <?php            
            if ($rol == 6) {
                $col = 2;
                ?>
                <div class="col-xs-12 col-sm-3 numero" data-numero="2">
                    <div><strong>LUGAR: </strong> <?php echo $itemDoc->nomalmacen ?></div>
                    <div><strong>FILE: </strong> <?php echo $aFiles[intval($itemDoc->idfile)]["codigo"] ?></div>
                    <div><strong>STAND: </strong> <?php echo $aStand[intval($itemDoc->idstand)]["codigo"] ?></div>
                    <div><strong>COD BOX: </strong><?php echo $itemDoc->codbox; ?></div>
                </div>
                <?php
            } else {
                $col = 5;
            }
            ?>
            <div class="col-xs-12 col-sm-<?php echo $col; ?>">
                <div class=" pull-right">
                    <div style="text-align: right;"><strong>F. RECEP: </strong><?php echo getFechaEs2($itemDoc->fecha_recepcion) ?></div>
                    <div style="text-align: right;"><?php echo getnomEstadoDoc($itemDoc->estado) ?></div>
                    <div class="pull-right">
                        <?php
                        echo getEnlaceDoc($itemDoc->estado, $itemDoc->iddocumento);
                        ?>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>                        
        </li> 

        <?php
    }
    ?>
</ul>

<div class="row" style="display: none;">
    <div id="archivos" class="col-md-8">
        <button id="btnAdjuntar" class="btn btn-default classAdjuntar" type="submit">Adjuntar</button>

        <div id="subir" class="clearfix" style="padding-left: 7px;display: inline-block;margin-top: 9px;margin-left: -6px;">
            <input id="my_file_element" type="file" name="file_1" ><br clear="all"/>
            <div id="files_list"></div>                            
        </div>
    </div>
</div>

<!-- MODAL DERIVACIÓN -->
<div id="myModal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" style="z-index: 99999;">
        <div class="modal-content">
            <div class="modal-header" id="cabecera-modal">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 id="titulo_modal" class="modal-title">Derivar documento</h4>
            </div>
            <div id="contenido_modal" class="modal-body">

            </div> 
            <div class="modal-footer" id="footer_modal">

                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" id="btnDerivardoc" class="btn btn-sm btn-primary">Enviar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<!-- MODAL  -->
<div id="dlgbox" class="modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" style="z-index: 99999;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 id="titulo_modal" class="modal-title text-center">Agregar a box</h4>
            </div>
            <div id="contenido_box" class="modal-body">
				
            </div>
            <div class="modal-footer" id="footer_box">
                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" id="btnmeterbox" class="btn btn-sm btn-primary">Enviar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>