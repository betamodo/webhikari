<?php
$mensaje = $this->session->userdata('mensaje');
if ($mensaje != "") {
    getHtmlMensaje(2, $mensaje);
}
?>
<div id="mensajeaux"></div>
<div class="row">
    <div class="col-md-8">
        <input type="hidden" id="txtiddoc" value="<?php echo $objDocumento->iddocumento; ?>">
        <span class="leyenda text-center">Datos del documento</span>
        <hr>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <div class="row">   
            <div class="col-md-12 espacio5">
                <input type="text" id="txtasunto" class="form-control" value="<?php echo $objDocumento->asunto; ?>" placeholder="Asunto">    
            </div>                
        </div><!--/row-->
        <div class="row">   
            <div class="col-md-12 espacio5">
                <input type="text" id="txtorigen" class="form-control" value="<?php echo $objDocumento->origen; ?>" placeholder="Origen del documento">    
            </div>                
        </div><!--/row-->
        <div class="row">   
            <div class="col-md-6 espacio5">
                <select class="form-control" id="cbotipodoc">
                    <?php
                    if ($aTipoDoc != false) {
                        foreach ($aTipoDoc as $itemDoc) {
                            ?>
                            <option value="<?php echo $itemDoc->idtipo_doc; ?>" <?php if ($itemDoc->idtipo_doc == $objDocumento->idtipodoc) echo "selected"; ?>><?php echo $itemDoc->nombre; ?></option>                            
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="col-md-6 espacio5">
                <input type="text" id="txtidentificador" class="form-control" value="<?php echo $objDocumento->identificador; ?>" placeholder="Identificador del documento"> 
            </div><!--/span-->
        </div><!--/row-->

        <div class="row">   
            <div class="col-md-12 espacio5">

                <ul id="myULTags" class="tags">
                    <!-- Existing list items will be pre-added to the tags. -->
                    <?php
                    $cadenaTags = "";
                    foreach ($aTags as $itemtag) {
                        ?>
                        <li data-value="<?php echo $itemtag->idtags; ?>"><?php echo $itemtag->nombre; ?></li>
                        <?php
                        $cadenaTags.=$itemtag->idtags . ",";
                    }
                    ?>
                </ul>
                <input type="hidden" id="text" name="tags" value="<?php echo trim($cadenaTags, ",") ?>"/>
            </div>
        </div><!--/row-->



        <div class="row">
            <div class="col-md-12">
                <span class="leyenda text-center">Datos de almacenamiento</span>
                <hr>
            </div>
        </div>
        <div class="row">   
            <div class="col-md-12 espacio5">
                <select class="form-control" id="cboLugar">
                    <option value="">Seleccione lugar de almacenamiento</option>
                    <?php
                    if ($aAlmacenes != false) {
                        foreach ($aAlmacenes as $itemAlmacen) {
                            ?>
                            <option value="<?php echo $itemAlmacen->idalmacen; ?>" <?php if ($itemAlmacen->idalmacen == $objDocumento->idalmacen) echo "selected"; ?>><?php echo $itemAlmacen->nombre; ?></option>                            
                            <?php
                        }
                    }
                    ?>

                </select>
            </div>                

        </div><!--/row-->
        <div class="row">   
            <div class="col-md-4 espacio5">
                <input type="text" id="txtfile" class="form-control" value="<?php echo $objDocumento->file; ?>" placeholder="Código del file">    
            </div> 
            <div class="col-md-4 espacio5">
                <select class="form-control" id="cboTipo">
                    <option>Seleccione tipo almacenamiento</option>
                    <option value="1" <?php if ($objDocumento->idtipo_almacen == 1) echo "selected"; ?>>STAND</option>
                    <option value="2" <?php if ($objDocumento->idtipo_almacen == 2) echo "selected"; ?>>BOX</option>
                </select>
            </div>                
            <div class="col-md-4 espacio5">
                <input type="text" id="txtbox" value="<?php echo $objDocumento->box_estante; ?>" disabled="" class="form-control" placeholder="">    
            </div>                
        </div><!--/row-->
        <br>
        <hr>
        <div class="row">
            <div class="col-md-12 espacio5">
                <textarea id="txtdescripcion" class="form-control" rows="3" placeholder="Escribir detalle"><?php echo $objDocumento->comentario; ?></textarea>
            </div><!--/span-->                
        </div><!--/row-->  
        <?php
        if ($aArchivos != FALSE) {
            ?>
            <ul class="list-group" id="contact-list2" style="margin-bottom: 6px;">
                <li class="list-group-item">
                    <div id="mntrs" class="clearfix">
                        <?php
                        foreach ($aArchivos as $itemArchivo) {
                            $info = pathinfo(getcwd() . "/" . $itemArchivo->url);
                            $extension = $info['extension'];
                            if (strtoupper($extension) == "JPG" || strtoupper($extension) == "JPEG" || strtoupper($extension) == "PNG" || strtoupper($extension) == "GIF") {
                                ?>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <a class="fancybox" rel="gallery1" href="<?php echo base_url() . "media/" . $itemArchivo->url ?>">
                                        <div class="clearfix thumbnail">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <img width="50" height="50" src="<?php echo base_url() . "media/archivos/thumbs/" . substr(trim($itemArchivo->url), 9) ?>" class="img-responsive">
                                                </div>
                                                <div class="col-xs-9">
                                                    <div>
                                                        <p><?php echo substr(substr(trim($itemArchivo->url), 9), 0, 10) . "." . $extension; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <?php
                            } else {
                                ?>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <a class="fancybox" rel="gallery1" href="<?php echo base_url() . "media/" . $itemArchivo->url ?>">
                                        <div class="clearfix thumbnail">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <img width="50" height="50" src="<?php echo base_url() . "media/img/" . $extension . ".png" ?>" class="img-responsive">
                                                </div>
                                                <div class="col-xs-9">
                                                    <div>
                                                        <p><?php echo substr(substr(trim($itemArchivo->url), 9), 0, 10) . "." . $extension; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>    
                                <?php
                            }
                        }
                        ?>
                    </div>
                </li>
            </ul>
            <?php
        }
        ?>
        <div class="row">
            <div class="col-md-8">
                <button id="btnAgregarArchivo" class="btn btn-default classAdjuntar" type="submit">Añadir archivo</button>

                <div id="subir2" class="clearfix" style="padding-left: 7px;display: inline-block;margin-top: 9px;margin-left: -6px;">
                    <input type="hidden" id="hid_input_files" name="hid_input_files" value="file_0" />
                    <div id="remove_0" style="margin-right: 4px;" class="btn btn-sm btn-default btn_remover_files" data-id="0"><span data-id="file_0" class="glyphicon glyphicon-minus-sign btn_remover_files"></span></div>
                    <input type="file" name="file_0" id="file_0" class="custom-file-input">                        
                </div>
            </div>
        </div>
    </div><!--/span-->

    <div class="col-md-4">

    </div><!--/span-->       
</div><!--/row-->   
<br>
<div class="clearfix"></div>
<div class="row">
    <div class="col-sm-12 col-md-2 text-center">

    </div>
    <div class="col-sm-12 col-md-2 text-center">
        <button type="button" id="btnEditarDoc" class="btn btn-primary">Editar documento</button>                
    </div>
    <div class="col-sm-12 col-md-2 text-center">
        <a href="<?php echo base_url() ?>documento/lista" id="btnSalir" class="btn btn-link">Salir</a>
    </div>

</div>
