<?php
$mensaje = $this->session->userdata('mensaje');
if ($mensaje != "") {
    getHtmlMensaje(2, $mensaje);
}
?>
<div id="mensajeaux"></div>
<div class="row">
    <div class="col-md-8">
        <input type="hidden" id="txtiddoc" value="<?php echo $objDocumento->iddocumento; ?>">
        <span class="leyenda text-center">Datos del documento</span>
        <hr>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <div class="row">   
            <div class="col-md-12 espacio5">
                <ul class="list-group" id="contact-list2" style="margin-bottom: 2px;">
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div><?php echo $objDocumento->codigo; ?></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div><?php echo $objDocumento->asunto; ?></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-4">
                                <div><?php echo $objDocumento->nomalmacen ?></div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div> <?php echo $objDocumento->file ?></div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <?php
                                $nombox = "STAND " . $objDocumento->box_estante;
                                if ($objDocumento->idtipo_almacen == 1) {
                                    $nombox = "BOX " . $objDocumento->box_estante;
                                }
                                ?>
                                <div><?php echo $nombox; ?></div>

                            </div>
                        </div>
                        <div class="clearfix"></div>                        
                    </li>                                     
                </ul> 

            </div>                
        </div><!--/row-->
        <div class="row">
            <div class="col-md-12">
                <ul class="list-group" id="contact-list3" style="margin-bottom: 2px;">
                    <li class="list-group-item">
                        <?php echo $objDocumento->comentario; ?>
                    </li>
                </ul>
            </div>
        </div>
    </div><!--/span-->

    <div class="col-md-4">

    </div><!--/span-->       
</div><!--/row-->   
<div class="row">
    <div class="col-md-8">
        <div class="row">   
            <div class="col-md-12 espacio5">

                <!-- Existing list items will be pre-added to the tags. -->
                <?php
                $cadenaTags = "";
                foreach ($aTags as $itemtag) {
                    ?>
                    <div class="item-tag"><?php echo $itemtag->nombre; ?></div>
                    <?php
                    $cadenaTags.=$itemtag->idtags . ",";
                }
                ?>
            </div>
        </div><!--/row-->
        <br>
        <div class="row">
            <div class="col-md-12">
                <span class="leyenda text-center">Datos de almacenamiento</span>
                <hr>
            </div>
        </div>
        <div class="row">   
            <div class="col-md-12 espacio5">
                <select class="form-control" id="cboLugar">
                    <option value="">Seleccione lugar de almacenamiento</option>
                    <?php
                    if ($aAlmacenes != false) {
                        foreach ($aAlmacenes as $itemAlmacen) {
                            ?>
                            <option value="<?php echo $itemAlmacen->idalmacen; ?>" <?php if ($itemAlmacen->idalmacen == $objDocumento->idalmacen) echo "selected"; ?>><?php echo $itemAlmacen->nombre; ?></option>                            
                            <?php
                        }
                    }
                    ?>

                </select>
            </div>                

        </div><!--/row-->
        <div class="row">   
            <div class="col-md-4 espacio5">
                <input type="text" id="txtfile" class="form-control" value="<?php echo $objDocumento->file; ?>" placeholder="Código del file">    
            </div> 
            <div class="col-md-4 espacio5">
                <select class="form-control" id="cboTipo">
                    <option>Seleccione tipo almacenamiento</option>
                    <option value="1" <?php if ($objDocumento->idtipo_almacen == 1) echo "selected"; ?>>STAND</option>
                    <option value="2" <?php if ($objDocumento->idtipo_almacen == 2) echo "selected"; ?>>BOX</option>
                </select>
            </div>                
            <div class="col-md-4 espacio5">
                <input type="text" id="txtbox" value="<?php echo $objDocumento->box_estante; ?>" disabled="" class="form-control" placeholder="">    
            </div>                
        </div><!--/row-->
        <br>
        <hr>
        <?php
        if ($aArchivos != FALSE) {
            ?>
            <ul class="list-group" id="contact-list2" style="margin-bottom: 6px;">
                <li class="list-group-item">
                    <div id="mntrs" class="clearfix">
                        <?php
                        foreach ($aArchivos as $itemArchivo) {
                            $info = pathinfo(getcwd() . "/" . $itemArchivo->url);
                            $extension = $info['extension'];
                            if (strtoupper($extension) == "JPG" || strtoupper($extension) == "JPEG" || strtoupper($extension) == "PNG" || strtoupper($extension) == "GIF") {
                                ?>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <a class="fancybox" rel="gallery1" href="<?php echo base_url() . "media/" . $itemArchivo->url ?>">
                                        <div class="clearfix thumbnail">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <img width="50" height="50" src="<?php echo base_url() . "media/archivos/thumbs/" . substr(trim($itemArchivo->url), 9) ?>" class="img-responsive">
                                                </div>
                                                <div class="col-xs-9">
                                                    <div>
                                                        <p><?php echo substr(substr(trim($itemArchivo->url), 9), 0, 10) . "." . $extension; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <?php
                            } else {
                                ?>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <a class="fancybox" rel="gallery1" href="<?php echo base_url() . "media/" . $itemArchivo->url ?>">
                                        <div class="clearfix thumbnail">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <img width="50" height="50" src="<?php echo base_url() . "media/img/" . $extension . ".png" ?>" class="img-responsive">
                                                </div>
                                                <div class="col-xs-9">
                                                    <div>
                                                        <p><?php echo substr(substr(trim($itemArchivo->url), 9), 0, 10) . "." . $extension; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>    
                                <?php
                            }
                        }
                        ?>
                    </div>
                </li>
            </ul>
            <?php
        }
        ?>
        
    </div><!--/span-->

    <div class="col-md-4">

    </div><!--/span-->       
</div><!--/row-->   
<br>
<div class="clearfix"></div>
<div class="row">
    <div class="col-sm-12 col-md-2 text-center">

    </div>
    <div class="col-sm-12 col-md-2 text-center">
        <button type="button" id="btnMoverDoc" class="btn btn-primary">Mover documento</button>                
    </div>
    <div class="col-sm-12 col-md-2 text-center">
        <a href="<?php echo base_url() ?>tarea/lista" id="btnSalir" class="btn btn-link">Salir</a>
    </div>

</div>
