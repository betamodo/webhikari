
<div id="busquedaAvanzada">
    <div class="row">
        <div class="col-md-12">
            <span class="leyenda text-center">Búsqueda avanzada</span>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-sm-3">
            <select id="select_almacen" class="form-control espacioControles" name="select_almacen">
                <option value="">Todas los almacenes</option>
                <?php
                foreach ($aAlmacen as $itemAlmacen) {
                    ?>
                    <option value="<?php echo $itemAlmacen->idalmacen; ?>"><?php echo $itemAlmacen->nombre; ?></option>
                    <?php
                }
                ?>
            </select>
        </div>
        <div class="col-sm-12 col-sm-3">
            <input type="text" id="bus_file" class="form-control" placeholder="Código del file">
        </div>
        <div class="col-sm-12 col-sm-3">
            <input type="text" id="bus_box" class="form-control" placeholder="Código box o Stand">
        </div>   
    </div>

    <div class="row">
        <div class="col-sm-12 col-sm-3">
            <select id="select_tipodoc" class="form-control espacioControles" name="select_tipodoc">
                <option value="">Todas los tipos</option>
                <?php
                foreach ($aTipodoc as $itemtipo) {
                    ?>
                    <option value="<?php echo $itemtipo->idtipo_doc; ?>"><?php echo $itemtipo->nombre; ?></option>
                    <?php
                }
                ?>
            </select>
        </div>
        <div class="col-sm-12 col-sm-3">
            <input type="text" id="bus_codigo" class="form-control" placeholder="Código del documento">
        </div>
        <div class="col-sm-12 col-sm-3">
            <input type="text" id="bus_asunto" class="form-control" placeholder="Asunto del documento">
        </div>    
    </div>
    <div class="row">
        <div class="col-sm-12 col-sm-9">
            <input type="text" id="bus_descripcion" style="margin-bottom: 5px;" class="form-control" placeholder="Descripción del documento">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-sm-3">
            <div class="input-group espacioControles">
                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar"></span></span>
                <input type="text" readonly="" id="finicio" class="form-control espacioControles" aria-describedby="basic-addon1" placeholder="Fecha inicio recepción">            
            </div>
        </div>
        <div class="col-sm-12 col-sm-3">
            <div class="input-group espacioControles">
                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar"></span></span>
                <input type="text" readonly="" id="ffin" class="form-control espacioControles" aria-describedby="basic-addon1" placeholder="Fecha fin recepción">            
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-1">
            <button id="btnBuscar" class="btn btn-sm btn-primary" style="width: 100%;display: inline-block;line-height: 22px;margin-bottom: 5px;">Buscar</button> 
        </div>
        <div class="col-sm-12 col-md-1">
            <button id="btnlimpiar" class="btn btn-sm btn-warning" style="width: 100%;line-height: 22px;">Limpiar</button>
        </div>
    </div>

</div>
<?php
$this->session->set_userdata('mensaje', "");
?>
<br>
<hr>

<div id="listaTareas">

</div>

<div class="row" style="display: none;">
    <div id="archivos" class="col-md-8">
        <button id="btnAdjuntar" class="btn btn-default classAdjuntar" type="submit">Adjuntar</button>

        <div id="subir" class="clearfix" style="padding-left: 7px;display: inline-block;margin-top: 9px;margin-left: -6px;">
            <input id="my_file_element" type="file" name="file_1" ><br clear="all"/>
            <div id="files_list"></div>                            
        </div>
    </div>
</div>