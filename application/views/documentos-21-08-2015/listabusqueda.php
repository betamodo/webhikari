
<?php
$this->session->set_userdata('mensaje', "");
?>

    <ul class="list-group" id="contact-list2">
        <?php
        foreach ($aListaDocumentos as $itemDoc) {
            ?>
            <li class="list-group-item" style="margin-bottom: 7px;">

                <div class="col-xs-12 col-sm-6">
                    <div><span class="labelCodigo"><?php echo $itemDoc->codigo; ?></span></div>
                    <div class="labelNomTarea pad3"><?php echo $itemDoc->asunto ?></div>
                    <p><i><?php echo $itemDoc->comentario;?></i></p>
                    <div class="pad3"><span><?php echo $itemDoc->origen; ?></span></div>
                </div>
                <div class="col-xs-12 col-sm-4 numero" data-numero="2">
                    <div><strong>LUGAR: </strong> <?php echo $itemDoc->nomalmacen ?></div>
                    <div><strong>FILE: </strong> <?php echo $itemDoc->file ?></div>
                    <?php
                    if ($itemDoc->idtipo_almacen == 1) {
                        ?>
                        <div class="pad3" style="white-space: nowrap"><strong>STAND: </strong><?php echo $itemDoc->box_estante ?></div>
                        <?php
                    } else {
                        ?>
                        <div class="pad3" style="white-space: nowrap"><strong>BOX: </strong><?php echo $itemDoc->box_estante ?></div>
                        <?php
                    }
                    ?>
                </div>
                <div class="col-xs-12 col-sm-2">
                    <div class=" pull-right">
                        <div style="text-align: right;"><strong>F. RECEP: </strong><?php echo getFechaEs2($itemDoc->fecha_recepcion) ?></div>
                        <div class="pull-right">
                            <a href="<?php echo base_url() . "documento/ver/" . $itemDoc->iddocumento ?>" class="btn btn-link">Ver</a>|<a href="<?php echo base_url() . "documento/editar/" . $itemDoc->iddocumento ?>" class="btn btn-link">Editar</a>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>                        
            </li> 

            <?php
        }
        ?>
    </ul>

<div class="row" style="display: none;">
    <div id="archivos" class="col-md-8">
        <button id="btnAdjuntar" class="btn btn-default classAdjuntar" type="submit">Adjuntar</button>

        <div id="subir" class="clearfix" style="padding-left: 7px;display: inline-block;margin-top: 9px;margin-left: -6px;">
            <input id="my_file_element" type="file" name="file_1" ><br clear="all"/>
            <div id="files_list"></div>                            
        </div>
    </div>
</div>