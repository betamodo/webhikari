<div class="row">
    <div class="col-md-8">
        <span class="leyenda text-center">Datos del documento</span>
        <hr>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <div class="row">   
            <div class="col-md-12 espacio5">
                <input type="text" id="txtasunto" class="form-control" placeholder="Asunto">    
            </div>                
        </div><!--/row-->
        <div class="row">   
            <div class="col-md-12 espacio5">
                <input type="text" id="txtorigen" class="form-control" placeholder="Origen del documento">    
            </div>                
        </div><!--/row-->
        <div class="row">   
            <div class="col-md-6 espacio5">
                <select class="form-control" id="cbotipodoc">
                    <?php
                    if ($aTipoDoc != false) {
                        foreach ($aTipoDoc as $itemDoc) {
                            ?>
                            <option value="<?php echo $itemDoc->idtipo_doc; ?>"><?php echo $itemDoc->nombre; ?></option>                            
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="col-md-6 espacio5">
                <input type="text" id="txtidentificador" class="form-control" placeholder="Identificador del documento"> 
            </div><!--/span-->
        </div><!--/row-->

        <div class="row">   
            <div class="col-md-12 espacio5">
                <input type="text" id="text" name="tags" value=""/>
            </div>
        </div><!--/row-->



        <div class="row">
            <div class="col-md-12">
                <span class="leyenda text-center">Datos de almacenamiento</span>
                <hr>
            </div>
        </div>
        <div class="row">   
            <div class="col-md-12 espacio5">
                <select class="form-control" id="cboLugar">
                    <option value="">Seleccione lugar de almacenamiento</option>
                    <?php
                    if ($aAlmacenes != false) {
                        foreach ($aAlmacenes as $itemAlmacen) {
                            ?>
                            <option value="<?php echo $itemAlmacen->idalmacen; ?>"><?php echo $itemAlmacen->nombre; ?></option>                            
                            <?php
                        }
                    }
                    ?>

                </select>
            </div>                

        </div><!--/row-->
        <div class="row">   
            <div class="col-md-4 espacio5">
                <input type="text" id="txtfile" class="form-control" placeholder="Código del file">    
            </div> 
            <div class="col-md-4 espacio5">
                <select class="form-control" id="cboTipo">
                    <option>Seleccione tipo almacenamiento</option>
                    <option value="1">STAND</option>
                    <option value="2">BOX</option>
                </select>
            </div>                
            <div class="col-md-4 espacio5">
                <input type="text" id="txtbox" disabled="" class="form-control" placeholder="">    
            </div>                
        </div><!--/row-->
        <br>
        <hr>
        <div class="row">
            <div class="col-md-12 espacio5">
                <textarea id="txtdescripcion" class="form-control" rows="3" placeholder="Escribir detalle"></textarea>
            </div><!--/span-->                
        </div><!--/row-->  

        <div class="row">
            <div class="col-md-8">
                <button id="btnAgregarArchivo" class="btn btn-default classAdjuntar" type="submit">Añadir archivo</button>

                <div id="subir2" class="clearfix" style="padding-left: 7px;display: inline-block;margin-top: 9px;margin-left: -6px;">
                    <input type="hidden" id="hid_input_files" name="hid_input_files" value="file_0" />
                    <div id="remove_0" style="margin-right: 4px;" class="btn btn-sm btn-default btn_remover_files" data-id="0"><span data-id="file_0" class="glyphicon glyphicon-minus-sign btn_remover_files"></span></div>
                    <input type="file" name="file_0" id="file_0" class="custom-file-input">                        
                </div>
            </div>
        </div>
    </div><!--/span-->

    <div class="col-md-4">

    </div><!--/span-->       
</div><!--/row-->   

<div class="clearfix"></div>
<div class="row">
    <div class="col-sm-12 col-md-4 text-center">
        <button type="button" id="btnCrearDoc" class="btn btn-primary">Crear tarea</button>                
    </div>
    <div class="col-sm-12 col-md-4 text-center">
        <a href="<?php echo base_url() ?>documento/lista" id="btnSalir" class="btn btn-link">Salir</a>
    </div>

</div>
