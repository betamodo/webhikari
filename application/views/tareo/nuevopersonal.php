<div class="row" style="margin-right: 0px;margin-left: 0px;">
    <div class="row">
        <div class="col-md-12">
            <span class="leyenda text-center">Actividades personales</span>            
        </div>
    </div>
    <div class="row ">
        <div class="col-sm-12 col-md-6">
            <input type="text" id="txtnombre" placeholder="Nombre" class="form-control espacio5">
        </div>
        <div class="col-sm-12 col-md-6">
            <input type="text" id="txtapellidos" placeholder="Apellidos" class="form-control espacio5">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-4">
            <input type="text" id="txtdni" placeholder="Doc. de identidad" class="form-control espacio5">
        </div>
        <div class="col-sm-12 col-md-2">
            <select id="cbosexo" class="form-control espacio5">
                <option value="">Seleccione sexo</option>
                <option value="1">M</option>
                <option value="0">F</option>
            </select>
        </div>
        <div class="col-sm-12 col-md-6">
            <select id="cboarea" class="form-control">
                <option value="">Seleccione área</option>
                <option value="1">Cocina</option>
                <option value="0">Mozo</option>
            </select>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <span class="leyenda text-center">Datos de horario</span>            
        </div>
    </div>
    <div class="row espacio5">
        <div class="col-sm-12 col-md-6">
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar"></span></span>
                <input type="text" readonly="" id="finicio" value="" class="form-control espacioControles" aria-describedby="basic-addon1" placeholder="Fecha de inicio">            
            </div>
        </div>
        <div class="col-sm-12 col-md-6">
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar"></span></span>
                <input type="text" readonly="" id="ffin" value="" class="form-control espacioControles" aria-describedby="basic-addon1" placeholder="Fecha cese">            
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-6">
            <input type="text" value="Local" class="form-control espacio5">
        </div>
        <div class="col-sm-12 col-md-3">
            <select id="cbohorainicio" class="form-control espacio5">
                <?php
                for ($i = 1; $i < 25; $i++) {
                    ?>
                    <option value="<?php echo $i; ?>"><?php echo $i; ?>:00:00</option>
                    <?php
                }
                ?>
            </select>
        </div>
        <div class="col-sm-12 col-md-3">
            <select id="cbohorafin" class="form-control">
                <?php
                for ($i = 1; $i < 25; $i++) {
                    ?>
                    <option value="<?php echo $i; ?>"><?php echo $i; ?>:00:00</option>
                    <?php
                }
                ?>
            </select>
        </div>
    </div>
    <div class="row">   
        <div class="col-md-12 espacio5">
            <div class="checkbox">
                <label style="font-size: 1.0em">
                    <input id="lunes" type="checkbox" value="0" >
                    <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                    Lunes
                </label>
            </div>
            <div class="checkbox">
                <label style="font-size: 1.0em">
                    <input id="martes" type="checkbox" value="0" >
                    <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                    MARTES
                </label>
            </div>
            <div class="checkbox">
                <label style="font-size: 1.0em">
                    <input id="miercoles" type="checkbox" value="0" >
                    <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                    MIERCOLES
                </label>
            </div>
            <div class="checkbox">
                <label style="font-size: 1.0em">
                    <input id="jueves" type="checkbox" value="0" >
                    <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                    JUEVES
                </label>
            </div>                    

            <div class="checkbox">
                <label style="font-size: 1.0em">
                    <input id="viernes" type="checkbox" value="0" >
                    <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                    VIERNES
                </label>
            </div>
            <div class="checkbox">
                <label style="font-size: 1.0em">
                    <input id="sabado" type="checkbox" value="0" >
                    <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                    SABADO
                </label>
            </div>
            <div class="checkbox">
                <label style="font-size: 1.0em">
                    <input id="domingo" type="checkbox" value="0" >
                    <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                    DOMINGO
                </label>
            </div>
        </div>   

    </div><!--/row-->
    <div class="row">
        <div class="col-md-12">
            <textarea id="txtdescripcion" class="form-control" placeholder="Escribe comentario"></textarea>
        </div>
    </div>
    <div class="row text-center">
        <div class="col-sm-12 col-md-4">

        </div>
        <div class="col-sm-12 col-md-2 espacio5">
            <button id="btnaddpersonal" style="width: 100%" class="btn btn-primary">Guardar</button>            
        </div>
        <div class="col-sm-12 col-md-2 espacio5">
            <a href="<?php base_url() . "tareo/listapersonal/" ?>" style="width: 100%"  class="btn btn-default">Salir</a>
        </div>
        <div class="col-sm-12 col-md-4">

        </div>
    </div>
</div>