<ul class="list-group" id="contact-list2">
    <?php
    foreach ($aLocales as $itemLocal) {
        ?>
        <li class="list-group-item" style="margin-bottom: 7px;">
            <div class="col-xs-12 col-sm-6">
                <div><span class="labelCodigo" style="font-size: 18px;"><?php echo $itemLocal->nombre; ?></span></div>                
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="text-left presu">
                </div>
                <div class="pull-right">
                    <div style="text-align: right;">
                        <a href="<?php echo base_url() . "tareo/listaprogramacion/" . $itemLocal->idlocal; ?>" class="btn btn-sm btn-link ">Programación</a> | 
                        <a href="<?php echo base_url() . "tareo/vertareo/" . $itemLocal->idlocal; ?>" class="btn btn-sm btn-link ">Ver tareo</a></div>
                </div>
            </div>
            <div class="clearfix"></div>                        
        </li> 
        <?php
    }
    ?>
</ul>
