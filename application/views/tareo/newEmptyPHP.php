<?php  
echo $this->doctype(); ?>
<!--[if lt IE 7 ]> <html lang="es" class="ie6"> <![endif]-->
<!--[if IE 7 ]> <html lang="es" class="ie7"> <![endif]-->
<!--[if IE 8 ]> <html lang="es" class="ie8"> <![endif]-->
<!--[if IE 9 ]> <html lang="es" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="es"> <!--<![endif]-->
<head> 
	<meta http-equiv="X-UA-Compatible" content="IE=9"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<?php echo $this->headTitle($this->translate('4Dental Web'))->setSeparator(' - ')->setAutoEscape(false) ?>

        <?php echo $this->headMeta()->appendName('viewport', 'width=device-width, initial-scale=1.0') ?>

        <!-- Le styles -->
        <?php
        echo $this->headLink(array('rel' => 'shortcut icon', 'type' => 'image/vnd.microsoft.icon', 'href' => $this->basePath() . '/images/favicon.ico'))
                ->prependStylesheet($this->basePath() . '/css/styles.css')
                ->prependStylesheet($this->basePath() . '/css/rcarousel.css')
				->prependStylesheet($this->basePath() . '/js/demo/owl.carousel.css')
				->prependStylesheet($this->basePath() . '/js/demo/owl.theme.default.css')
        ?>
		
<!-- IE6 "fix" for the close png image -->
<!--[if lt IE 7]>
<link type='text/css' href='css/basic_ie.css' rel='stylesheet' media='screen' />
<![endif]-->
 
        <!-- Scripts -->
        <?php
        echo $this->headScript()->prependFile($this->basePath() . '/js/html5.js', 'text/javascript', array('conditional' => 'lt IE 9',))
                ->prependFile($this->basePath() . '/js/demo/owl.carousel.js') 
                ->prependFile($this->basePath() . '/js/scripts.js')
                ->prependFile($this->basePath() . '/js/zoom-master/jquery.zoom.js')
                ->prependFile($this->basePath() . '/js/jquery.mCustomScrollbar.concat.min.js')
                ->prependFile($this->basePath() . '/js/jquery.scrollUp.js')
				->prependFile($this->basePath() . '/js/jquery.easing.min.js')
				->prependFile($this->basePath() . '/js/jquery.ui.rcarousel.js')
				->prependFile($this->basePath() . '/js/jquery.ui.widget.js')
				->prependFile($this->basePath() . '/js/jquery.ui.core.js')
                ->prependFile($this->basePath() . '/js/jquery-1.11.0.min.js')
        ?> 
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=true"></script>
		
	
	<script type="text/javascript">
	$( document ).ready(function() {
		$(".prod_img_thumb").click(function() {
			window.location.href = '<?php echo $this->basePath(); ?>/productos/detalle?id='+parseInt($(this).attr("data"));
		});
		$(".prod_img_thumb_search").click(function() {
			window.location.href = '<?php echo $this->basePath(); ?>/productos/detalle?id='+parseInt($(this).attr("data"));
		});
		
		// Remove the class of child and grandchild
		// This removes the CSS 'falback'
		$("#main_menu ul.child").removeClass("child");
		$("#main_menu ul.grandchild").removeClass("grandchild");
		
		// When a list item that contains an unordered list
		// is hovered on
		$("#main_menu li").has("ul").hover(function(){
			
			//Add a class of current and fade in the sub-menu
			$(this).addClass("current").children("ul").fadeIn();
			$(this).removeClass("current").children("div").stop(true, true).css("display", "block");
		}, function() {

			// On mouse off remove the class of current
			// Stop any sub-menu animation and set its display to none
			$(this).removeClass("current").children("ul").stop(true, true).css("display", "none");
			$(this).removeClass("current").children("div").stop(true, true).css("display", "none");
		});
		
		
		/*$(".div_productos_det").click(function() {
			$('#add_prod_modal').modal();
		});*/
		 
		$('#basic-modal .basic').click(function (e) {
			$('#basic-modal-content').modal();

			return false;
		});
		
		$(".div_productos_det").click(function() {
			window.location.href = '<?php echo $this->basePath(); ?>/productos/detalle?id='+parseInt($(this).attr("data"));
		});
		/*$('.div_productos_det').click(function (event) {
			event.preventDefault();
			var prod_id = $(this).attr('data'); 
			prod_id = prod_id.replace("prod-","");
			var position = $(this).position(); 
			$.get("<?php echo $this->basePath(); ?>/productos/detallelayout?id="+prod_id, function( data ) { 
					$('#layer_add_prod').html(data);
					$('#layer_add_prod').css("visibility","visible"); 
					$('#layer_add_prod').css("top",(position.top));
					$('#layer_add_prod').css("left",($('html').width()/2-$('#layer_add_prod').width()/2));
			}); 
		});*/
		
		$('.div_nov_prod_det').click(function (event) {
			event.preventDefault();
			var prod_id = $(this).attr('data'); 
			prod_id = prod_id.replace("prod-","");
			var position = $(this).position(); 
			
			$.get("<?php echo $this->basePath(); ?>/productos/detallelayout?id="+prod_id, function( data ) {
				
					$('#layer_add_prod').html(data);
					$('#layer_add_prod').css("visibility","visible"); 
					$('#layer_add_prod').css("top",(position.top));
					//$('#layer_add_prod').css("top",($('body').height()/2-$('#layer_add_prod').height()/2));
					$('#layer_add_prod').css("left",($('html').width()/2-$('#layer_add_prod').width()/2));
			}); 
		}); 
		
		$('.div_prod_destacado_det').click(function (event) {
			event.preventDefault();
			var prod_id = $(this).attr('data'); 
			prod_id = prod_id.replace("prod-","");
			var position = $(this).position(); 
			
			$.get("<?php echo $this->basePath(); ?>/productos/detallelayout?id="+prod_id, function( data ) {
				
					$('#layer_add_prod').html(data);
					$('#layer_add_prod').css("visibility","visible"); 
					$('#layer_add_prod').css("top",(position.top));
					//$('#layer_add_prod').css("top",($('body').height()/2-$('#layer_add_prod').height()/2));
					$('#layer_add_prod').css("left",($('html').width()/2-$('#layer_add_prod').width()/2));
			}); 
		}); 

		
		$('#main_container').click(function (event) {
			$('#layer_add_prod').css("visibility","hidden"); 
			
		});

                //preload frame cesta carrito
                $.get("<?php echo $this->basePath(); ?>/home/getcarritolayer", function( data ) {
                                $('#car-cotizar').html(data);
                });
                
                
		$('.compras').mouseenter(function (event) {
				var position = $(this).position();  
				//$('#car-cotizar_main').css("top",(position.top+30));                          
				//$('#car-cotizar_main').css("left",(1024-($('#car-cotizar').width()/2)));
                                
                                if($('#car-cotizar').css("display")=="none"){ 
                                    $.get("<?php echo $this->basePath(); ?>/home/getcarritolayer", function( data ) {
                                                    $('#car-cotizar').html(data);
                                    }); 
                                    $('#car-cotizar').css("display","block"); 
                                    $('#car-cotizar').css("display","block"); 
                                }/*else if($('#car-cotizar').css("visibility")=="visible"){
				
                                $('#car-cotizar_main').css("visibility","hidden"); 
				$('#car-cotizar').css("visibility","hidden"); */
			
			 
		});
		 
                $('#car-cotizar').mouseleave(function(e){
                
                    if($('#car-cotizar').css("display")=="block"){
				
                        $('#car-cotizar').css("display","none");
                        $('#car-cotizar').css("display","none");                
                        
                    }
                    
                })
                
                
                /*
                $('.btn_cotizar_menu').mouseleave(function(e){
                
                    if($('#car-cotizar').css("visibility")=="visible"){
				
                        $('#car-cotizar_main').css("visibility","hidden"); 
                        $('#car-cotizar').css("visibility","hidden");                
                        
                    }
                    
                })
                */
                
                
                
		$('.div_detproductos_cot').click(function (event) {
			cant = $("#txt_prod_cantidad").val();
			var prod_id = $(this).attr('data'); 
			prod_id = prod_id.replace("prod-","");
                        $('.div_detproductos_cot_agregado').show()
			$.get("<?php echo $this->basePath(); ?>/productos/actionproduct?act=1&id="+prod_id+"&cant="+cant, function( data ) {
				$('#cart_total_prod').html("("+data+")");
                                $('.div_detproductos_cot_agregado').html("Producto Agregado")
                                //$('.div_detproductos_cot_agregado').css('background-image', 'url(' + ../images/ico_boton_cotizar_agregado.gif + ')');                                
			});
		});
		
		$('#search_text').click(function (event) {
			$(this).val("");
		});
		
		$('#btn_search').click(function (event) {
			if($('#search_text').val()!=""){
				$('#form_busqueda').submit();
			}
		});
		
		$.get("<?php echo $this->basePath(); ?>/home/gettotalproductoscarro", function( data ) {
			$('#cart_total_prod').html("("+data+")"); 
		});
			
		/*$("div").click(function(e) {
			if($(this).hasClass("disablelink")) {
				return false;
			}
			$('#car-cotizar_main').css("visibility","hidden"); 
			$('#car-cotizar').css("visibility","hidden"); 
		});*/
		 

		$.scrollUp({
			scrollName: 'scrollUp', // Element ID
			scrollDistance: 300, // Distance from top/bottom before showing element (px)
			scrollFrom: 'top', // 'top' or 'bottom'
			scrollSpeed: 300, // Speed back to top (ms)
			easingType: 'linear', // Scroll to top easing (see http://easings.net/)
			animation: 'fade', // Fade, slide, none
			animationSpeed: 200, // Animation in speed (ms)
			scrollTrigger: false, // Set a custom triggering element. Can be an HTML string or jQuery object
			//scrollTarget: false, // Set a custom target element for scrolling to the top
			scrollText: '<img src="<?php echo $this->basePath(); ?>/img/boton_top.png" border="0">', // Text for element, can contain HTML
			scrollTitle: false, // Set a custom <a> title if required.
			scrollImg: false, // Set true to use image
			activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
			zIndex: 2147483647 // Z-Index for the overlay
		});
		return false;
	});
	</script>
</head>
<script>
  /*(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-8930341-48', 'jemaperu.com');
  ga('send', 'pageview');*/

</script>

<body>  
    
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

        
<div id="layer_add_prod"> 
</div>


	<div id="main">
		<header>
			<a href="<?php echo $this->basePath(); ?>/home" class="logo">
			<img src="<?php echo $this->basePath(); ?>/img/logo.png" border="0" width="180">
			</a>
			<div class="car">
				<div class="h_top">
					<!-- <form method="POST" id="buscar">
						<input type="search" name="buscar" class="buscar" placeholder="Buscar">
					</form> -->
					<div class="compras">
						<a href="/dev/4dental/public/productos/cotizacion" class="active">
							<span></span>
							<p>Productos a cotizar</p> 
							<span class="disablelink" id="cart_total_prod">(0)</span>
						</a>
					</div>
					<div id="car-cotizar">
						
					</div>
				</div>
				<div class="h_botton">
					<nav>
						<ul>
							<li><a href="<?php echo $this->basePath(); ?>/home" <?php if($this->page=="INICIO"){ echo 'class="activo"'; } ?>>INICIO</a></li>
							<li><a href="<?php echo $this->basePath(); ?>/home/quienessomos" <?php if($this->page=="QUIENES SOMOS"){ echo 'class="activo"'; } ?>>QUIÉNES SOMOS</a></li>
							<li><a href="<?php echo $this->basePath(); ?>/productos" <?php if($this->page=="PRODUCTOS"){ echo 'class="activo"'; } ?>>PRODUCTOS</a></li>
							<li><a href="<?php echo $this->basePath(); ?>/home/contacto" <?php if($this->page=="CONTACTO"){ echo 'class="activo"'; } ?>>CONTACTO</a></li>
						</ul>
					</nav>
				</div>
			</div>
		</header>
		
 
		<?php echo $this->content; ?>
	 
		<div class="footer_sub">
		<div id="foo_contat">
		<div>
			<div class="col-3"><img src="<?php echo $this->basePath(); ?>/img/ico_sobre_contactos.png"></div>
			<div class="col-3">
				<div class="info_contacto">
					<h3>CONTACTO</h3>
					<span>- USA: 13287 NW 5th Street, Plantation FL 33325.</span>
					<span>(01) 954 274 0713</span>
					<span>- PERU: Calle Madre Selva 251, Dpto 104, Surco, Lima.</span>
					<span>(0051) 986 619 319</span>
					<span>4dental.management@gmail.com</span>		
				</div>
			</div>
			<div class="col-3">
				<ul>
					<li><a href="#" class="facebook"></a></li>
					<li><a href="#" class="youtube"></a></li>
					<li><a href="#" class="pink"></a></li>
				</ul>
			</div>
			</div>
		</div>
		</div>
		<div class="foot">
		<footer>
			<span>©2014 4DENTAL PRODUCTOS. Todos los derechos reservados | Términios y condiciones</span>
			<span>Desarrollado por <a href="#" class="by">BETA.pe</a></span>
		</footer>
		</div>
	</div>
	
	 
	
</div>	<!-- /main_container -->
  
<?php echo $this->inlineScript() ?>

</div>
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=true"></script>
	
    </body>

</html>
