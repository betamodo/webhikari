
<div class="row">
    <div class="col-md-12">
        <div id="no-more-tables">
            <table class="col-md-12 table-bordered table-striped table-condensed cf">
                <thead class="cf">
                    <?php if ($empleadoContrato->horas_extras > 0): ?>
                    <tr>
                        <th class="text-center" colspan="8">
                            <div class="row">
                                <div class="col-md-3 pad-top-10" style="line-height: 38px;">Horas extras del contrato: <span id="horas-extras"><?php echo $empleadoContrato->horas_extras ?></span></div>
                                <div class="col-md-3 pad-top-10" style="line-height: 38px;">Horas para asignar disponibles: <span id="horas-extras-disponibles"></span></div>
                                <div class="col-md-3 pad-top-10" style="line-height: 38px;">
                                    <div class="col-md-4 col-md-offset-4">
                                        <div style="clear:none;display:inline;"><a href="#" id="selector-l"><span class="label label-success" style="font-size:100%">L</span></a></div>
                                        <div style="clear:none;display:inline;"><a href="#" id="selector-n"><span class="label label-danger" style="font-size:100%">N</span></a></div>
                                    </div>
                                </div>
                                <div class="col-md-3"><a class="btn btn-default disabled" href="#" role="button" id="guardar">Guardar</a></div>
                            </div>
                        </th>
                    </tr>
                    <?php else: ?>
                    <tr>
                        <th class="text-center" colspan="8">
                            <div class="row">
                                    <div class="col-md-2 col-md-offset-10"><a class="btn btn-default disabled" href="#" role="button" id="guardar">Guardar</a></div>
                            </div>
                        </th>
                    </tr>
                    <?php endif ?>
                    <tr>
                    <?php
                    // Despliega los nombres de las columnas
                    foreach ($aDias as $itemDias) {
                    ?>
                        <th class="text-center"><?php echo $itemDias; ?></th>
                    <?php
                    }
                    ?>
                    </tr>
                </thead>
                <tbody id="contenedor-drag">
                    <?php
                    $i = 0;
                    $htmlAntes = "";
                    $htmlDespues = "";
                    while ($i < 24) {
                        // Para cambiar el inicio de la hora que se muestra
                        if ($i < $horaInicio) {
                            $htmlAntes.='<tr class="text-center">';
                            // Hora
							$htmlAntes.='<td data-title="Hora">' . $i . ':00</td>';
                            // Para saber el indice del dia
                            $j = 0;
							foreach ($aDias as $keyAntes => $itemDias) {
								if($keyAntes != -1){
									$styleAntes="";
                                    if (
                                            ($oCantidades[$keyAntes][$i] > 0 && !isset($diasLibres[$keyAntes][$i])) ||
                                            ($oCantidades[$keyAntes][$i] <= 0 && isset($diasLibres[$keyAntes][$i]) && $diasLibres[$keyAntes][$i] == libresmodel::SUMAR_DIA)
                                        ) {
                                        // Trabaja si esta en su horario y no esta fijado en dias libres
                                        // o
                                        // Si no esta en su horario y esta fijado como sumar esa hora
                                        $styleAntes = ($oCantidades[$keyAntes][$i] <= 0 && isset($diasLibres[$keyAntes][$i]) && $diasLibres[$keyAntes][$i] == libresmodel::SUMAR_DIA) ? "success dia-sumado":"success horario-normal";
                                        $labelAntes="L";
                                    } else if(
                                            ($oCantidades[$keyAntes][$i] <= 0 && !isset($diasLibres[$keyAntes][$i])) ||
                                            ($oCantidades[$keyAntes][$i] > 0 && isset($diasLibres[$keyAntes][$i]) && $diasLibres[$keyAntes][$i] == libresmodel::RESTAR_DIA)
                                        ) {
                                        // No Trabaja si no esta en su horario y no esta fijado en dias libres
                                        // o
                                        // Si esta en su horario y esta fijado como restar esa hora
                                        $styleAntes = ($oCantidades[$keyAntes][$i] > 0 && isset($diasLibres[$keyAntes][$i]) && $diasLibres[$keyAntes][$i] == libresmodel::RESTAR_DIA) ? "danger horario-normal dia-restado":"danger";
                                        $labelAntes="N";
                                    }
                                    // $htmlAntes.='<td data-title="Domingo" class="numeric"><a href="javascript:void(0);"><span class="label label-'.$styleAntes.'">' . $labelAntes . '</span></a> </td>';
									$htmlAntes.='<td data-title="Domingo" class="numeric"><a class="arrastre"><span data-indicedia="'.$j.'" id="'.$keyAntes.'-'.$i.'" class="label label-'.$styleAntes.'">' . $labelAntes . '</span></a></td>';
                                $j++;                                    
								}									
							}                                
                            $htmlAntes.='</tr>';
                        } else {
                            $htmlDespues.='<tr class="text-center">';
							$htmlDespues.='<td data-title="Hora">' . $i . ':00</td>';
                            // Para saber el indice del dia
                            $j = 0;
							foreach ($aDias as $keyDespues => $itemDias) {
								if($keyDespues !=-1){
									// Configurando estilos	
									$styleDespues="";
                                    if (
                                            ($oCantidades[$keyDespues][$i] > 0 && !isset($diasLibres[$keyDespues][$i])) ||
                                            ($oCantidades[$keyDespues][$i] <= 0 && isset($diasLibres[$keyDespues][$i]) && $diasLibres[$keyDespues][$i] == libresmodel::SUMAR_DIA)
                                        ) {
                                        // Trabaja si esta en su horario y no esta fijado en dias libres
                                        // o
                                        // Si no esta en su horario y esta fijado como sumar esa hora
                                        $styleDespues = ($oCantidades[$keyDespues][$i] <= 0 && isset($diasLibres[$keyDespues][$i]) && $diasLibres[$keyDespues][$i] == libresmodel::SUMAR_DIA) ? "success dia-sumado":"success horario-normal";
                                        $labelDespues="L";
                                    } else if(
                                            ($oCantidades[$keyDespues][$i] <= 0 && !isset($diasLibres[$keyDespues][$i])) ||
                                            ($oCantidades[$keyDespues][$i] > 0 && isset($diasLibres[$keyDespues][$i]) && $diasLibres[$keyDespues][$i] == libresmodel::RESTAR_DIA)
                                        ) {
                                        // No Trabaja si no esta en su horario y no esta fijado en dias libres
                                        // o
                                        // Si esta en su horario y esta fijado como restar esa hora
                                        $styleDespues = ($oCantidades[$keyDespues][$i] > 0 && isset($diasLibres[$keyDespues][$i]) && $diasLibres[$keyDespues][$i] == libresmodel::RESTAR_DIA) ? "danger horario-normal dia-restado":"danger";
                                        $labelDespues = "N";
                                    }
                                    // $htmlDespues.='<td data-title="Domingo" class="numeric"><a href="javascript:void(0);"><span class="label label-'.$styleDespues.'">' . $labelDespues . '</span></a></td>';
									$htmlDespues.='<td data-title="Domingo" class="numeric"><a class="arrastre"><span data-indicedia="'.$j.'" id="'.$keyDespues.'-'.$i.'" class="label label-'.$styleDespues.'">' . $labelDespues . '</span></a></td>';
                                    $j++;                                    
								}									
							}
                            $htmlDespues.='</tr>';
                        }
                        $i++;
                    }
                    echo $htmlDespues . $htmlAntes;
                    ?>
                </tbody>
            </table>
        </div>        
    </div>
</div>
<script>
<?php
echo "var rangoHorario = ". json_encode($rangoHorario).";"; 
echo "var empleadoContrato = ". json_encode($empleadoContrato).";"; 
echo "var diasLibres = ". json_encode($diasLibresJ).";"; 
echo "var empleadoDescanso = ". json_encode($empleadoDescanso).";"; 
echo "var empleadoVacaciones = ". json_encode($empleadoVacaciones).";"; 
?>
</script>
