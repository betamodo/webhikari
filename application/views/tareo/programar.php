<style>
    input[name^=txtcantidad_]{
        margin-bottom: 5px; 
    }
</style> 
<form role="form" action="<?php echo base_url() . "tareo/grabarprogramacion" ?>" method="POST">
    <div class="row">
        <div class="col-md-3">
            <select id="cbolocal" name="cbolocal" class="form-control">  
                <option value="">Seleccione local</option>
                <?php
                foreach ($aLocales as $itemLocal) {
                    ?>
                    <option value="<?php echo $itemLocal->idlocal ?>"><?php echo $itemLocal->nombre ?></option>
                    <?php
                }
                ?>

            </select>    
        </div>
        <div class="col-md-3">
            <input type="hidden" name="idprogramacion" id="idprogramacion" value="0">
            <select id="cbolabor" name="cbolabor" class="form-control">
                <option value="">Seleccione labor</option>
                <?php
                foreach ($aArea as $itemArea) {
                    ?>
                    <option value="<?php echo $itemArea->idarea ?>"><?php echo $itemArea->nombre ?></option>
                    <?php
                }
                ?>
            </select>    
        </div>
        <div class="col-md-3">
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar"></span></span>
                <input type="text" readonly="" id="finicio"  name="finicio" value="" class="form-control espacioControles" aria-describedby="basic-addon1" placeholder="Fecha de inicio">            
            </div>
        </div>
        <div class="col-md-3">
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar"></span></span>
                <input type="text" readonly="" id="ffin" name="ffin" value="" class="form-control espacioControles" aria-describedby="basic-addon1" placeholder="Fecha cese">            
            </div>
        </div> 
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">

            <div id="no-more-tables">
                <table class="col-md-12 table-bordered table-striped table-condensed cf">
                    <thead class="cf">
                        <tr>
                            <?php
                            foreach ($aDias as $itemDias) {
                                ?>
                                <th class="text-center"><?php echo $itemDias; ?></th>
                                <?php
                            }
                            ?>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        $i = 0;
                        $htmlAntes = "";
                        $htmlDespues = "";
                        while ($i < 24) {
                            if ($i < $horaInicio) {
                                $htmlAntes.='<tr class="text-center">
                                <td data-title="Hora">' . $i . ':00</td>
                                <td data-title="Lunes" class="numeric"><input type="text" placeholder="Cantidad"  name="txtcantidad_1_' . $i . '" class="form-control"> <input type="text" placeholder="Monto"  name="txtmonto_1_' . $i . '" class="form-control"></td>
                                <td data-title="Martes" class="numeric"><input type="text" placeholder="Cantidad" name="txtcantidad_2_' . $i . '" class="form-control"> <input type="text" placeholder="Monto" name="txtmonto_2_' . $i . '" class="form-control"></td>
                                <td data-title="Miércoles" class="numeric"><input type="text" placeholder="Cantidad" name="txtcantidad_3_' . $i . '" class="form-control"> <input type="text" placeholder="Monto" name="txtmonto_3_' . $i . '" class="form-control"></td>
                                <td data-title="Jueves" class="numeric"><input type="text" placeholder="Cantidad" name="txtcantidad_4_' . $i . '" class="form-control"> <input type="text" placeholder="Monto" name="txtmonto_4_' . $i . '" class="form-control"></td>
                                <td data-title="Viernes" class="numeric"><input type="text" placeholder="Cantidad" name="txtcantidad_5_' . $i . '" class="form-control"> <input type="text" placeholder="Monto" name="txtmonto_5_' . $i . '" class="form-control"></td>
                                <td data-title="Sábado" class="numeric"><input type="text" placeholder="Cantidad" name="txtcantidad_6_' . $i . '" class="form-control"> <input type="text" placeholder="Monto" name="txtmonto_6_' . $i . '" class="form-control"></td>
                                <td data-title="Domingo" class="numeric"><input type="text" placeholder="Cantidad" name="txtcantidad_7_' . $i . '" class="form-control"> <input type="text" placeholder="Monto" name="txtmonto_7_' . $i . '" class="form-control"></td>
                            </tr>';
                            } else {
                                $htmlDespues.='<tr class="text-center">
                                <td data-title="Hora">' . $i . ':00</td>
                                <td data-title="Lunes" class="numeric"><input type="text" placeholder="Cantidad" name="txtcantidad_1_' . $i . '" class="form-control"> <input type="text" placeholder="Monto" name="txtmonto_1_' . $i . '" class="form-control"></td>
                                <td data-title="Martes" class="numeric"><input type="text" placeholder="Cantidad" name="txtcantidad_2_' . $i . '" class="form-control"> <input type="text" placeholder="Monto" name="txtmonto_2_' . $i . '" class="form-control"></td>
                                <td data-title="Miércoles" class="numeric"><input type="text" placeholder="Cantidad" name="txtcantidad_3_' . $i . '" class="form-control"> <input type="text" placeholder="Monto" name="txtmonto_3_' . $i . '" class="form-control"></td>
                                <td data-title="Jueves" class="numeric"><input type="text" placeholder="Cantidad" name="txtcantidad_4_' . $i . '" class="form-control"> <input type="text" placeholder="Monto" name="txtmonto_4_' . $i . '" class="form-control"></td>
                                <td data-title="Viernes" class="numeric"><input type="text" placeholder="Cantidad" name="txtcantidad_5_' . $i . '" class="form-control"> <input type="text" placeholder="Monto" name="txtmonto_5_' . $i . '" class="form-control"></td>
                                <td data-title="Sábado" class="numeric"><input type="text" placeholder="Cantidad" name="txtcantidad_6_' . $i . '" class="form-control"> <input type="text" placeholder="Monto" name="txtmonto_6_' . $i . '" class="form-control"></td>
                                <td data-title="Domingo" class="numeric"><input type="text" placeholder="Cantidad" name="txtcantidad_7_' . $i . '" class="form-control"> <input type="text" placeholder="Monto" name="txtmonto_7_' . $i . '" class="form-control"></td>
                            </tr>';
                            }
                            $i++;
                        }
                        echo $htmlDespues . $htmlAntes;
                        ?>

                    </tbody>
                </table>


            </div>
            <div class="text-center" style="margin-top: 11px;">
                <button type="submit" class="btn btn-primary">Grabar</button>
            </div>       
        </div>
    </div>
</form>