<div class="row">
    <div class="col-md-6 fechadiahora">
        <div><?php echo strtoupper($dia); ?></div>
		<input type="hidden" id="nomdia" value="<?php echo $dia;?>">
		<input type="hidden" id="hrcomun" value="<?php echo $hora; ?>">
        <div><?php echo $hora; ?>:00</div>
    </div>
    <div class="col-md-6 clasedia">
        <div><?php echo $total; ?></div>
    </div>
</div>
<hr>
<!-- DETALLE -->
<?php
$totalDinero = 0;
foreach ($aLabores as $itemLabor) {
    ?>
    <div class="row ">
        <div class="col-md-12 col-md-12 titulo_area_labor"><?php echo $itemLabor->nombre; ?></div>
    </div>
    <?php
    $aEmpleados = array();
    $sueldoXHora = 0;
    $aEmpleados = getEmpleadosBanAux($hora, $dia, $idlocal, $itemLabor->idlabor, $finicio, $ffin, 2, true);
    if ($aEmpleados != false) {
        foreach ($aEmpleados as $itemEmpleado) {
            $sueldoXHora = doubleval($itemEmpleado->sueldo) / (48 * 4);
            $totalDinero = $totalDinero + $sueldoXHora;
            ?>
            <div class="row">
                <div class="col-md-5 bodynombres">
                    <div><a href="<?php echo base_url()."tareo/verhorarioaux/".$itemEmpleado->idrh_empleado."/".getFechaFFormatTFormat("d/m/Y", "Y-m-d", $finicio)."/".getFechaFFormatTFormat("d/m/Y", "Y-m-d", $ffin) ?>" target="_blank" ><?php echo $itemEmpleado->rh_nombre; ?></a></div>
                </div>
				<div class="col-md-3 cboLaborSimu">
					<select class="form-control">
							<option data-id="<?php echo $itemEmpleado->idrh_empleado;?>" value="lunes" <?php if($dia == "lunes"){ echo "selected";}?>>Lunes</option>
							<option data-id="<?php echo $itemEmpleado->idrh_empleado;?>" value="martes" <?php if($dia == "martes"){ echo "selected";}?>>Martes</option>
							<option data-id="<?php echo $itemEmpleado->idrh_empleado;?>" value="miercoles" <?php if($dia == "miercoles"){ echo "selected";}?>>Miércoles</option>
							<option data-id="<?php echo $itemEmpleado->idrh_empleado;?>" value="jueves" <?php if($dia == "jueves"){ echo "selected";}?>>Jueves</option>
							<option data-id="<?php echo $itemEmpleado->idrh_empleado;?>" value="viernes" <?php if($dia == "viernes"){ echo "selected";}?>>Viernes</option>
							<option data-id="<?php echo $itemEmpleado->idrh_empleado;?>" value="sabado" <?php if($dia == "sabado"){ echo "selected";}?>>Sábado</option>
							<option data-id="<?php echo $itemEmpleado->idrh_empleado;?>" value="domingo" <?php if($dia == "domingo"){ echo "selected";}?>>Domingo</option>
					</select>
				</div>
				<div class="col-md-2 cboHoraSimu">
					<select class="form-control" >
							<option data-id="<?php echo $itemEmpleado->idrh_empleado;?>" value="-1">Libre</option>
							<?php 
							for($i=0; $i < 24; $i ++)
							{
								?>
								<option data-id="<?php echo $itemEmpleado->idrh_empleado;?>" value="<?php echo $i;?>" <?php if($i == $hora){ echo "selected";}?>><?php echo $i;?></option>
								<?php	
							}
							?>
					</select>
				</div>				
				<div class="col-md-2 text-center" style="line-height: 39px;">
                    <span style="padding-left: 31px;"><?php echo "S/. " . round($sueldoXHora, 3); ?></span>
                </div>
            </div>			
            <?php
        }
    } else {
        echo "No hay trabajadores";
    }
}
?>
<div class="row">
    <div class="col-md-12 col-md-6 bodynombres">
    </div>
    <div class="col-md-12 col-md-6" style="text-align: center;font-size: 15px;font-weight: bold;padding-top: 10px;">
        <div style="border-top: 1px solid;padding-right: 10px;">Total dinero: S/. <?php echo round($totalDinero, 3); ?></div>
    </div>

</div>
<!-- ./DETALLE -->