
<div class="row">
    <div class="col-md-12">

        <div id="no-more-tables">
            <table class="col-md-12 table-bordered table-striped table-condensed cf">
                <thead class="cf">
                    <tr>
                        <?php
						/*echo "<pre>";
						var_dump($aDias);
						echo "</pre>";
						exit; */
                        foreach ($aDias as $itemDias) {
                            ?>
                            <th class="text-center"><?php echo $itemDias; ?></th>
                            <?php
                        }
                        ?>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    $i = 0;
                    $htmlAntes = "";
                    $htmlDespues = "";
                    while ($i < 24) {
						
                        if ($i < $horaInicio) {
                            $htmlAntes.='<tr class="text-center">';
								$htmlAntes.='<td data-title="Hora">' . $i . ':00</td>';
								foreach ($aDias as $keyAntes => $itemDias) {
									if($keyAntes !=-1){
										$styleAntes="";
										if($oCantidades[$keyAntes][$i]>0){
											$styleAntes="success";
											$labelAntes="L";
										}else{
											$styleAntes="danger";
											$labelAntes="N";
										}
										$htmlAntes.='<td data-title="Domingo" class="numeric"><a href="javascript:void(0);"><span class="label label-'.$styleAntes.'">' . $labelAntes . '</span></a> </td>';
									}									
								}                                
                            $htmlAntes.='</tr>';
                        } else {
                            $htmlDespues.='<tr class="text-center">';
								$htmlDespues.='<td data-title="Hora">' . $i . ':00</td>'; 
								foreach ($aDias as $keyDespues => $itemDias) {
									if($keyDespues !=-1){
										// Configurando estilos	
										$styleDespues="";
										if($oCantidades[$keyDespues][$i]>0){
											$styleDespues="success";
											$labelDespues="L";
										}else{
											$styleDespues="danger";
											$labelDespues="N";
										}
										$htmlDespues.='<td data-title="Domingo" class="numeric"><a href="javascript:void(0);"><span class="label label-'.$styleDespues.'">' . $labelDespues . '</span></a></td>';
									}									
								}
                            $htmlDespues.='</tr>';
                        }
                        $i++;
                    }
                    echo $htmlDespues . $htmlAntes;
                    ?>

                </tbody>
            </table>
        </div>        
    </div>
</div>
