
<ul class="list-group" id="contact-list2">
    <?php
    if ($aProgPersonal != false) {
        foreach ($aProgPersonal as $itemProg) {
            ?>
            <li class="list-group-item clearfix" style="margin-bottom: 7px;">
                <div class="col-xs-12 col-sm-6">
                    <div><span class="labelCodigo" style="font-size: 18px;"><?php echo $itemProg->nombre; ?></span></div>
                    <div><span><?php echo getFechaEs2($itemProg->fecha_inicio); ?></span></div>
                    <div><span><?php echo getFechaEs2($itemProg->fecha_fin); ?></span></div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="text-left presu">
                    </div>
                    <div class="pull-right">
                        <div style="text-align: right;">
                            <a href="<?php echo base_url() . "tareo/editprogramar/" . $itemProg->idprogpersonal; ?>" class="btn btn-sm btn-link ">Editar</a>                         
                        </div>
                    </div>
                </div>
            </li> 
            <?php
        }
    }
    ?>
</ul>
