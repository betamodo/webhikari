<style>

.fechadiahora{
	font-size: 17px;
    font-weight: 800;
}
.clasedia{
	line-height: 1;
    font-size: 44px;
    font-weight: 700;
    text-align: center;
}
.titulo_area_labor{
	font-size: 16px;
	    font-weight: 800;
}
.bodynombres{
	padding-left: 28px;
	line-height: 39px;
}

</style>
<div class="row">
    <div class="col-md-3">
        <select id="cbolocal" name="cbolocal" class="form-control">  
            <option value="">Seleccione local</option>
            <?php
            foreach ($aLocales as $itemLocal) {
                ?>
                <option value="<?php echo $itemLocal->idlocal ?>"><?php echo $itemLocal->nombre ?></option>
                <?php
            }
            ?>

        </select>    
    </div>
    <div class="col-md-3">
        <input type="hidden" name="idprogramacion" id="idprogramacion" value="0">
        <select id="cbolabor" name="cbolabor" class="form-control">
            <option value="">Seleccione labor</option>
            <?php
            foreach ($aArea as $itemArea) {
                ?>
                <option value="<?php echo $itemArea->idarea ?>"><?php echo $itemArea->nombre ?></option>
                <?php
            }
            ?>
        </select>    
    </div>
    <div class="col-md-3">
        <div class="input-group">
            <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar"></span></span>
            <input type="text" readonly="" id="finicio"  name="finicio" value="" class="form-control espacioControles" aria-describedby="basic-addon1" placeholder="Fecha inicio">            
        </div>
    </div>
    <div class="col-md-3">
        <div class="input-group">
            <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar"></span></span>
            <input type="text" readonly="" id="ffin" name="ffin" value="" class="form-control espacioControles" aria-describedby="basic-addon1" placeholder="Fecha fin">            
        </div>
    </div> 
</div>
<div class="row">
    <div class="col-md-12">
        <button id="btnvertareo" style="margin-bottom: 5px;" class="btn btn-sm btn-warning">Buscar</button>
    </div>
</div>


    <div class="row">
        <div class="col-md-12"> 
            <!-- Nav tabs --><div class="card">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#divtareo" aria-controls="divtareo" role="tab" data-toggle="tab">Real</a></li>
                    <li role="presentation"><a href="#divtareoaux" aria-controls="divtareoaux" role="tab" data-toggle="tab">Simulación</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="divtareo">
                        
                    </div>
                    <div role="tabpanel" class="tab-pane" id="divtareoaux">
                        
                    </div>                    
                </div>
            </div>
        </div>
    </div>


<!-- Modal -->
<div class="modal fade" id="modaldetalle" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document" style="z-index: 99999;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Detalle de personal</h4>
      </div>
      <div class="modal-body" id="bodydetalletareo">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>      
      </div>
    </div>
  </div>
</div>

<!-- Modal AUX -->
<div class="modal fade bs-example-modal-lg" id="modaldetalleaux" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document" style="z-index: 99999;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Simulación</h4>
      </div>
      <div class="modal-body" id="bodydetalletareoaux">
        
      </div>
      <div class="modal-footer">
		<button id="btnGuardarSimulacion" class="btn btn-sm btn-primary" >Guardar cambios</button>
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>      
      </div>
    </div>
  </div>
</div>