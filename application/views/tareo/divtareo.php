
<?php
if($msj!=""){
	?>
	<div style="margin-bottom: 10px;margin-top: 12px;" class="alert alert-danger fade in">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <span id="msj"><?php echo $msj;?></span>
    </div>
	<?php
}else{
?>
<div class="text-center titulotareo">REAL</div>
<div class="row">
    <div class="col-md-12">

        <div id="no-more-tables">
            <table class="col-md-12 table-bordered table-striped table-condensed cf table-hover">
                <thead class="cf">
                    <tr>
                        <?php
						/*echo "<pre>";
						var_dump($aDias);
						echo "</pre>";
						exit; */
                        foreach ($aDias as $itemDias) {
                            ?>
                            <th class="text-center"><?php echo $itemDias["nombre"]; ?></th>
                            <?php
                        }
                        ?>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    $i = 0;
                    $htmlAntes = "";
                    $htmlDespues = "";
                    while ($i < 24) {
						
                        if ($i < $horaInicio) {
                            $htmlAntes.='<tr class="text-center">';
								$htmlAntes.='<td data-title="Hora">' . $i . ':00</td>';
								foreach ($aDias as $keyAntes => $itemDias) {
									if($keyAntes !=-1){
										$styleAntes="";
										if($oCantidades[$keyAntes][$i]==$oCantidadesPersonal[$keyAntes][$i]){
											$styleAntes="primary";
										}elseif($oCantidadesPersonal[$keyAntes][$i] > $oCantidades[$keyAntes][$i]){
											$styleAntes="danger";
										}else{
											$styleAntes="warning";
										}
										$htmlAntes.='<td data-title="Domingo" class="numeric"><a href="javascript:void(0);"><span class="label label-primary">' . $oCantidades[$keyAntes][$i] . '</span></a> | <a href="javascript:void(0);" class="'.$styleAntes.'"><span class="label label-'.$styleAntes.' dlgdetalle" data-dia="'.$itemDias["campo"].'" data-hora="'.$i.'" data-total="'.$oCantidadesPersonal[$keyAntes][$i].'">' . $oCantidadesPersonal[$keyAntes][$i] . '</span></a></td>';
									}									
								}                                
                            $htmlAntes.='</tr>';
                        } else {
                            $htmlDespues.='<tr class="text-center">';
								$htmlDespues.='<td data-title="Hora">' . $i . ':00</td>'; 
								foreach ($aDias as $keyDespues => $itemDias) {
									if($keyDespues !=-1){
										// Configurando estilos	
										$styleDespues="";
										if($oCantidades[$keyDespues][$i]==$oCantidadesPersonal[$keyDespues][$i]){
											$styleDespues="primary";
										}elseif($oCantidadesPersonal[$keyDespues][$i] > $oCantidades[$keyDespues][$i]){
											$styleDespues="danger";
										}else{
											$styleDespues="warning";
										}
										$htmlDespues.='<td data-title="Domingo" class="numeric"><a href="javascript:void(0);"><span class="label label-primary">' . $oCantidades[$keyDespues][$i] . '</span></a> | <a href="javascript:void(0);"><span class="label label-'.$styleDespues.' dlgdetalle"  data-dia="'.$itemDias["campo"].'" data-hora="'.$i.'" data-total="'.$oCantidadesPersonal[$keyDespues][$i].'">' . $oCantidadesPersonal[$keyDespues][$i] . '</span></a></td>';
									}									
								}
                            $htmlDespues.='</tr>';
                        }
                        $i++;
                    }
                    echo $htmlDespues . $htmlAntes;
                    ?>

                </tbody>
            </table>
        </div>        
    </div>
</div>
<?php
}
?>