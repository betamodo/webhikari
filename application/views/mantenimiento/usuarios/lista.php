<?php echo $mensaje; $this->session->set_userdata('mensaje', "");?>
<div id="listaTareas">
    <ul class="list-group" id="contact-list2">
        <?php
        foreach ($aListaUsuarios as $itemUsuario) {
            if ($itemUsuario->foto == "") {
                $foto = "default.png";
            } else {
                $foto = $itemUsuario->foto;
            }
            ?>
            <li class="list-group-item" style="margin-bottom: 7px;">
                <div class="col-xs-12 col-sm-1 col-md-1">
                    <img class="cuadrofoto" width="40" height="40" src="<?php echo base_url() . "media/archivos/avatars/thumbs/" . $foto; ?>">
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <div style="font-size: 14px;font-weight: bold;"><span><?php echo $itemUsuario->nombre; ?></span></div>                            
                    <div class="pad3"><span><?php echo getNombreRol($itemUsuario->rol); ?></span><span><?php if ($itemUsuario->nomlocal != "") echo "| " . $itemUsuario->nomlocal; ?></span></div>
                    <div><span><?php echo $itemUsuario->correo; ?></span></div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 pad3" data-numero="2">
                    <div>
                        <?php
                        if ($itemUsuario->estado_registro == 1) {
                            ?>
                            <span class="label label-success">Activo</span>
                            <?php
                        } else {
                            ?>
                            <span class="label label-danger">Inactivo</span>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="pad3" style="white-space: nowrap"><a href="<?php echo base_url() . "usuario/editar/" . $itemUsuario->idusuario ?>">Editar</a></div>                    
                </div>

                <div class="clearfix"></div>                        
            </li> 

            <?php
        }
        ?>
    </ul>
</div>