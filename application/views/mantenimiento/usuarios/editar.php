<?php
$mensaje = $this->session->userdata('mensaje');
if ($mensaje != "") {
    getHtmlMensaje(2, $mensaje);
}
?>
<div id="mensajeaux"></div>
<div class="row">
    <div class="col-md-8">
        <div class="row">
            <span class="leyenda text-center">Datos del usuario</span>       
            <hr>
        </div>
        <div class="row">   
            <div class="col-md-6 espacio5">
                <input type="hidden" id="txtusuario" value="<?php echo $objUsuario->idusuario; ?>">
                <input type="text" id="txtnombre" value="<?php echo $objUsuario->nombre; ?>" class="form-control" placeholder="Nombre completo">    
            </div> 
            <div class="col-md-6 espacio5">
                <input type="email" id="txtcorreo" value="<?php echo $objUsuario->correo; ?>" class="form-control" placeholder="Correo">    
            </div>
        </div><!--/row-->        
        <div class="row">   

            <div class="col-md-6 espacio5">
                <input type="text" id="txtusername" value="<?php echo $objUsuario->nomusuario; ?>" class="form-control" placeholder="Username">
            </div>
            <div class="col-md-6 espacio5">
                <input type="password" value="<?php echo $objUsuario->clave; ?>" id="txtclave" class="form-control" placeholder="Clave">
            </div>

        </div><!--/row-->
        <div class="row">   

            <div class="col-md-6 espacio5">
                <select class="form-control" id="cborol">
                    <?php if ($objUsuario->sa != 2) { ?>
                        <option value="">Seleccione rol</option>                    
                        <option value="1" <?php if ($objUsuario->rol == 1) echo "selected"; ?>>Administrador</option>                    
                        <option value="2" <?php if ($objUsuario->rol == 2) echo "selected"; ?>>Gestor</option>                    
                        <option value="3" <?php if ($objUsuario->rol == 3) echo "selected"; ?>>Director</option>                    
                        <option value="4" <?php if ($objUsuario->rol == 4) echo "selected"; ?>>Secretaria(o)</option>                    
                        <?php
                    }else {
                        ?>
                        <option value="5">Super Admin</option>
                        <?php
                    }
                    ?>
                </select>
            </div>
            <div class="col-md-6 espacio5">
                <select class="form-control" id="cboEstado">

                    <?php if ($objUsuario->sa != 2) { ?>
                        <option value="">Seleccione estado</option>      
                        <option value="1" <?php if ($objUsuario->estado_registro == 1) echo "selected"; ?>>Activo</option>                    
                        <option value="2" <?php if ($objUsuario->estado_registro == 2) echo "selected"; ?>>Inactivo</option>                    
                        <?php
                    }else {
                        ?>
                        <option value="1" <?php if ($objUsuario->estado_registro == 1) echo "selected"; ?>>Activo</option>                    
                        <?php
                    }
                    ?>
                </select>
            </div>

        </div><!--/row-->
        <div class="row">
            <div class="col-md-8">
                <button id="btnAgregarArchivo" class="btn btn-default classAdjuntar" style="display: none;" type="submit">Añadir archivo</button>
                <div id="subir2" class="clearfix" style="padding-left: 7px;display: inline-block;margin-top: 9px;margin-left: -6px;">
                    <input type="hidden" id="hid_input_files" name="hid_input_files" value="file_0" />
                    <div id="remove_0" style="display: none;margin-right: 4px;" class="btn btn-sm btn-default btn_remover_files" data-id="0"><span data-id="file_0" class="glyphicon glyphicon-minus-sign btn_remover_files"></span></div>
                    <input type="file" name="file_0" id="file_0" class="custom-file-input">                        
                </div>
            </div>
        </div>
    </div><!--/span-->

    <div class="col-md-2">
        <a href="#" class="thumbnail">
            <?php
            if ($objUsuario->foto == "") {
                $foto = "defaultg.png";
            } else {
                $foto = $objUsuario->foto;
            }
            ?>
            <img data-src="holder.js/100%x180" alt="100%x180" src="<?php echo base_url() . "media/archivos/avatars/" . $foto ?>" data-holder-rendered="true" style="height: 180px; width: 100%; display: block;">
        </a>
    </div><!--/span-->       
</div><!--/row-->   

<div class="clearfix"></div>
<div class="row">
    <div class="col-sm-12 col-md-4 text-center">
        <button type="button" id="btnEditarUsuario" class="btn btn-primary">Editar</button>                
    </div>
    <div class="col-sm-12 col-md-4 text-center">
        <a href="<?php echo base_url() ?>usuario/lista" id="btnSalir" class="btn btn-link">Salir</a>
    </div>

</div>
