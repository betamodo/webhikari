<div id="mensajeaux"></div>
<div class="row">
    <div class="col-md-8">
        <span class="leyenda text-center">Datos del usuario</span>       
        <hr>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <div class="row">   
            <div class="col-md-6 espacio5">
                <input type="text" id="txtnombre" class="form-control" placeholder="Nombre completo">    
            </div> 
            <div class="col-md-6 espacio5">
                <input type="email" id="txtcorreo" class="form-control" placeholder="Correo">    
            </div>
        </div><!--/row-->        
        <div class="row">   

            <div class="col-md-6 espacio5">
                <input type="text" id="txtusername" class="form-control" placeholder="Username">
            </div>
            <div class="col-md-6 espacio5">
                <input type="password" id="txtclave" class="form-control" placeholder="Clave">
            </div>

        </div><!--/row-->
        <div class="row">   

            <div class="col-md-6 espacio5">
                <select class="form-control" id="cborol">
                    <option value="">Seleccione rol</option>                    
                    <option value="1">Administrador</option>                    
                    <option value="2">Gestor</option>                    
                    <option value="3">Director</option>                    
                    <option value="4">Secretaria(o)</option>                    
                </select>
            </div>
            <div class="col-md-6 espacio5">
                <select class="form-control" id="cboEstado">
                    <option value="">Seleccione estado</option>                    
                    <option value="1">Activo</option>                    
                    <option value="2">Inactivo</option>                    
                </select>
            </div>

        </div><!--/row-->
        <div class="row">
            <div class="col-md-8">
                <button id="btnAgregarArchivo" class="btn btn-default classAdjuntar" style="display: none;" type="submit">Añadir archivo</button>
                <div id="subir2" class="clearfix" style="padding-left: 7px;display: inline-block;margin-top: 9px;margin-left: -6px;">
                    <input type="hidden" id="hid_input_files" name="hid_input_files" value="file_0" />
                    <div id="remove_0" style="display: none;margin-right: 4px;" class="btn btn-sm btn-default btn_remover_files" data-id="0"><span data-id="file_0" class="glyphicon glyphicon-minus-sign btn_remover_files"></span></div>
                    <input type="file" name="file_0" id="file_0" class="custom-file-input">                        
                </div>
            </div>
        </div>
    </div><!--/span-->

    <div class="col-md-4">

    </div><!--/span-->       
</div><!--/row-->   

<div class="clearfix"></div>
<div class="row">
    <div class="col-sm-12 col-md-4 text-center">
        <button type="button" id="btnCrearUsuario" class="btn btn-primary">Crear</button>                
    </div>
    <div class="col-sm-12 col-md-4 text-center">
        <a href="<?php echo base_url() ?>usuario/lista" id="btnSalir" class="btn btn-link">Salir</a>
    </div>

</div>
