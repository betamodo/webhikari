<?php
$mensaje = $this->session->userdata('mensaje');
if ($mensaje != "") {
    getHtmlMensaje(2, $mensaje);
}
?>
<div id="mensajeaux"></div>
<div class="row">
    <div class="col-md-8">
        <div class="row">
            <span class="leyenda text-center">Datos del almacén</span>       
            <hr>
        </div>
        <div class="row">  
            <div class="col-md-6 espacio5"> 
                <input type="hidden" id="txtidalmacen" value="<?php echo $oAlmacen->idalmacen; ?>">
                <input type="text" id="txtnombre" value="<?php echo $oAlmacen->nombre; ?>" class="form-control" placeholder="Nombre del almacén">    
            </div> 
            <div class="col-md-6 espacio5">
                <select class="form-control" id="cboestado">
                    <option value="">Seleccione estado</option>                    
                    <option value="1" <?php if ($oAlmacen->estado_registro == 1) echo "selected"; ?>>Activo</option>                    
                    <option value="2" <?php if ($oAlmacen->estado_registro == 2) echo "selected"; ?>>Inactivo</option>                    
                </select> 
            </div>
        </div><!--/row-->        
        <div class="row">   
            <div class="col-md-12 espacio5">
                <select class="form-control" id="cboUsuario">
                    <option value="">Seleccione responsable</option>  
                    <?php
                    if ($aUsuarios != false) {
                        foreach ($aUsuarios as $itemUsuario) {
                            ?>
                            <option value="<?php echo $itemUsuario->idusuario; ?>" <?php if ($oAlmacen->idusuario_responsable == $itemUsuario->idusuario) echo "selected"; ?>><?php echo $itemUsuario->nombre; ?></option>                    
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>            
        </div><!--/row-->       
    </div><!--/span-->

    <div class="col-md-2">

    </div><!--/span-->       
</div><!--/row-->   

<div class="clearfix"></div>
<div class="row">
    <div class="col-sm-12 col-md-4 text-center">
        <button type="button" id="btnEditarAlmacen" class="btn btn-primary">Editar</button>                
    </div>
    <div class="col-sm-12 col-md-4 text-center">
        <a href="<?php echo base_url() ?>categoria/lista" id="btnSalir" class="btn btn-link">Salir</a>
    </div>

</div>
