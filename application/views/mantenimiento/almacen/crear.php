<div class="row">
    <div class="col-md-8">
        <span class="leyenda text-center">Datos del almacén</span>       
        <hr>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <div class="row">   
            <div class="col-md-6 espacio5">
                <input type="text" id="txtnombre" class="form-control" placeholder="Nombre del almacén">    
            </div> 
            <div class="col-md-6 espacio5">
                <select class="form-control" id="cboEstado">
                    <option value="">Seleccione estado</option>                    
                    <option value="1">Activo</option>                
                    <option value="2">Inactivo</option>                
                </select>
            </div>
        </div><!--/row-->                
    </div><!--/span-->

    <div class="col-md-4">

    </div><!--/span-->       
</div><!--/row-->   
<div class="row">   
    <div class="col-md-8 espacio5">
        <select class="form-control" id="cboUsuario">
            <option value="">Seleccione responsable</option>  
            <?php
            if ($aUsuarios != false) {
                foreach ($aUsuarios as $itemUsuario) {
                    ?>
                    <option value="<?php echo $itemUsuario->idusuario; ?>"><?php echo $itemUsuario->nombre; ?></option>                    
                    <?php
                }
            }
            ?>
        </select>
    </div>            
</div><!--/row-->
<div class="clearfix"></div>
<div class="row">
    <div class="col-sm-12 col-md-4 text-center">
        <button type="button" id="btnCrearAlmacen" class="btn btn-primary">Crear</button>                
    </div>
    <div class="col-sm-12 col-md-4 text-center">
        <a href="<?php echo base_url() ?>categoria/lista" id="btnSalir" class="btn btn-link">Salir</a>
    </div>

</div>
