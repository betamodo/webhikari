<?php echo utf8_encode($mensaje); ?>
<div id="listaTareas">
    <ul class="list-group" id="contact-list2">
        <?php
        foreach ($aListaStand as $itemStand) {
            ?>
            <li class="list-group-item" style="margin-bottom: 7px;">
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <div  style="font-size: 14px;font-weight: bold;">NOMBRE</div>
                    <div><span><?php echo $itemStand->codigo; ?></span></div>
									
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div>
                        <?php
                        if ($itemStand->estado_registro == 1) {
                            ?>
                            <span class="label label-success">Activo</span>
                            <?php
                        } else {
                            ?>
                            <span class="label label-danger">Inactivo</span>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="pad3" style="white-space: nowrap"><a href="<?php echo base_url() . "stand/editar/" . $itemStand->idstand ?>">Editar</a></div>                    
                </div> 
                <div class="clearfix"></div>                        
            </li> 

            <?php
        }
        ?>
    </ul>
</div>