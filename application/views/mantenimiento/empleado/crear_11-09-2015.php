<div id="mensajeaux"></div>
<div class="row">
    <div class="col-md-8">
        <span class="leyenda text-center">Datos del usuario</span>       
        <hr>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
		<div class="row">   
            <div class="col-md-6 espacio5">
                <input type="text" id="txtnombre" class="form-control" placeholder="Nombre">    
            </div> 
            <div class="col-md-6 espacio5">
                <input type="text" id="txtapellidos" class="form-control" placeholder="Apellidos">    
            </div> 
        </div><!--/row-->
		<div class="row">   
            <div class="col-md-12 espacio5">
                <input type="text" id="txtdireccion" class="form-control" placeholder="<?php echo utf8_encode("Direcci�n")?>">    
            </div>
        </div><!--/row--> 
        <div class="row">   
			<div class="col-md-2 espacio5">
                <input type="text" id="txtdni" class="form-control" placeholder="DNI">
            </div>
			<div class="col-md-2 espacio5">
				<select class="form-control" id="cbosexo">
                    <option value="">Sexo</option>                    
                    <option value="1">M</option>                    
                    <option value="2">F</option>                   
                </select>
			</div>
			<div class="col-md-2 espacio5">
				<select class="form-control" id="cboregimen">
                    <option value=""><?php echo utf8_encode("R�gimen"); ?></option>                    
                    <option value="48">48 Hr.</option>                    
                    <option value="24">24 Hr.</option>                   
                </select>
                
            </div>
            <div class="col-md-2 espacio5">
				<input type="text" id="txtsueldo" class="form-control" placeholder="Sueldo">               
            </div>
			<div class="col-md-4 espacio5">
				<select class="form-control" id="cbolabor">
                    <option value="">Seleccione labor</option>                    
                    <?php
					foreach($aLabores as $itemLabor){
						?>
					<option value="<?php echo $itemLabor->idlabor?>"><?php echo $itemLabor->nombre;?></option> 
					<?php
					}
					?>     
                </select>
			</div>
        </div><!--/row-->
		<div class="row">   
            <div class="col-md-6 espacio5">
                 <input type="text" id="txttelefono" class="form-control" placeholder="<?php echo utf8_encode("Tel�fono")?>">  
            </div>
            <div class="col-md-6 espacio5">
                <input type="text" id="txtemail" class="form-control" placeholder="Email">  
            </div>

        </div><!--/row-->
		<div class="row">   
            <div class="col-md-3 espacio5">
				<strong>Lunes</strong>
				<select class="form-control" style="margin-bottom:4px;" id="txt_lu_hora_inicio">
					<option value="">Hr. Inicio</option>   
					<?php for($i=0;$i<24;$i++){?>
                    <option value="<?php echo $i;?>"><?php echo $i;?></option>
					<?php }?>
                </select>
                <select class="form-control" style="margin-bottom:4px;" id="txt_lu_hora_fin">
				<option value="">Hr. Fin</option>   
					<?php for($i=0;$i<24;$i++){?>
                    <option value="<?php echo $i;?>"><?php echo $i;?></option>
					<?php }?>
                </select>				
            </div>
            <div class="col-md-3 espacio5">
				<strong>Martes</strong>
                <select class="form-control" style="margin-bottom:4px;" id="txt_ma_hora_inicio">
					<option value="">Hr. Inicio</option>   
					<?php for($i=0;$i<24;$i++){?>
                    <option value="<?php echo $i;?>"><?php echo $i;?></option>
					<?php }?>
                </select>
                <select class="form-control" style="margin-bottom:4px;" id="txt_ma_hora_fin">
				<option value="">Hr. Fin</option>   
					<?php for($i=0;$i<24;$i++){?>
                    <option value="<?php echo $i;?>"><?php echo $i;?></option>
					<?php }?>
                </select>
            </div>
			<div class="col-md-3 espacio5">
			<strong><?php echo utf8_encode("Mi�rcoles");?></strong>
                <select class="form-control" style="margin-bottom:4px;" id="txt_mi_hora_inicio">
					<option value="">Hr. Inicio</option>   
					<?php for($i=0;$i<24;$i++){?>
                    <option value="<?php echo $i;?>"><?php echo $i;?></option>
					<?php }?>
                </select>
                <select class="form-control" style="margin-bottom:4px;" id="txt_mi_hora_fin">
				<option value="">Hr. Fin</option>   
					<?php for($i=0;$i<24;$i++){?>
                    <option value="<?php echo $i;?>"><?php echo $i;?></option>
					<?php }?>
                </select> 
            </div>
			<div class="col-md-3 espacio5">
				<strong><?php echo utf8_encode("Jueves");?></strong>
                <select class="form-control" style="margin-bottom:4px;" id="txt_ju_hora_inicio">
					<option value="">Hr. Inicio</option>   
					<?php for($i=0;$i<24;$i++){?>
                    <option value="<?php echo $i;?>"><?php echo $i;?></option>
					<?php }?>
                </select>
                <select class="form-control" style="margin-bottom:4px;" id="txt_ju_hora_fin">
				<option value="">Hr. Fin</option>   
					<?php for($i=0;$i<24;$i++){?>
                    <option value="<?php echo $i;?>"><?php echo $i;?></option>
					<?php }?>
                </select>
            </div>
        </div><!--/row-->
		<div class="row">   
            <div class="col-md-3 espacio5">
				<strong><?php echo utf8_encode("Viernes");?></strong>
                <select class="form-control" style="margin-bottom:4px;" id="txt_vi_hora_inicio">
					<option value="">Hr. Inicio</option>   
					<?php for($i=0;$i<24;$i++){?>
                    <option value="<?php echo $i;?>"><?php echo $i;?></option>
					<?php }?>
                </select>
                <select class="form-control" style="margin-bottom:4px;" id="txt_vi_hora_fin">
				<option value="">Hr. Fin</option>   
					<?php for($i=0;$i<24;$i++){?>
                    <option value="<?php echo $i;?>"><?php echo $i;?></option>
					<?php }?>
                </select>
            </div>
            <div class="col-md-3 espacio5">
				<strong><?php echo utf8_encode("S�bado");?></strong>
                <select class="form-control" style="margin-bottom:4px;" id="txt_sa_hora_inicio">
					<option value="">Hr. Inicio</option>   
					<?php for($i=0;$i<24;$i++){?>
                    <option value="<?php echo $i;?>"><?php echo $i;?></option>
					<?php }?>
                </select>
                <select class="form-control" style="margin-bottom:4px;" id="txt_sa_hora_fin">
				<option value="">Hr. Fin</option>   
					<?php for($i=0;$i<24;$i++){?>
                    <option value="<?php echo $i;?>"><?php echo $i;?></option>
					<?php }?>
                </select>
            </div>
			<div class="col-md-3 espacio5">
				<strong><?php echo utf8_encode("Domingo");?></strong>
                <select class="form-control" style="margin-bottom:4px;" id="txt_do_hora_inicio">
					<option value="">Hr. Inicio</option>   
					<?php for($i=0;$i<24;$i++){?>
                    <option value="<?php echo $i;?>"><?php echo $i;?></option>
					<?php }?>
                </select>
                <select class="form-control" style="margin-bottom:4px;" id="txt_do_hora_fin">
				<option value="">Hr. Fin</option>   
					<?php for($i=0;$i<24;$i++){?>
                    <option value="<?php echo $i;?>"><?php echo $i;?></option>
					<?php }?>
                </select>  	
            </div>
			
        </div><!--/row-->
		<div class="row">   
            <div class="col-md-6 espacio5">
                <div class="item-ver">
                <div class="input-group espacioControles">
                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar"></span></span>
                    <input type="text" readonly="" id="finicio" class="form-control espacioControles" aria-describedby="basic-addon1" placeholder="Fecha inicio">            
                </div>
				</div> 
            </div>
            <div class="col-md-6 espacio5">
               <div class="item-ver">
                <div class="input-group espacioControles">
                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar"></span></span>
                    <input type="text" readonly="" id="ffin" class="form-control espacioControles" aria-describedby="basic-addon1" placeholder="Fecha cese">            
                </div>
				</div>
            </div>

        </div><!--/row-->
        <div class="row">   

            <div class="col-md-6 espacio5">
                <select class="form-control" id="cbolocal">
                    <option value="">Seleccione local</option>                    
                    <?php
					foreach($aLocal as $itemLocal){
						?>
					<option value="<?php echo $itemLocal->idlocal?>"><?php echo $itemLocal->nombre;?></option> 
					<?php
					}
					?>     
                </select>
            </div>
            <div class="col-md-6 espacio5">
                <select class="form-control" id="cboEstado">
                    <option value="1">Activo</option>                    
                    <option value="2">Inactivo</option>                    
                </select>
            </div>

        </div><!--/row-->
        <div class="row">
            <div class="col-md-8">
                <button id="btnAgregarArchivo" class="btn btn-default classAdjuntar" style="display: none;" type="submit">A�adir archivo</button>
                <div id="subir2" class="clearfix" style="padding-left: 7px;display: inline-block;margin-top: 9px;margin-left: -6px;">
                    <input type="hidden" id="hid_input_files" name="hid_input_files" value="file_0" />
                    <div id="remove_0" style="display: none;margin-right: 4px;" class="btn btn-sm btn-default btn_remover_files" data-id="0"><span data-id="file_0" class="glyphicon glyphicon-minus-sign btn_remover_files"></span></div>
                    <input type="file" name="file_0" id="file_0" class="custom-file-input">                        
                </div>
            </div>
        </div>
    </div><!--/span-->

    <div class="col-md-4">

    </div><!--/span-->       
</div><!--/row-->   

<div class="clearfix"></div>
<div class="row">
    <div class="col-sm-12 col-md-4 text-center">
        <button type="button" id="btnCrearempleado" class="btn btn-primary">Crear</button>                
    </div>
    <div class="col-sm-12 col-md-4 text-center">
        <a href="<?php echo base_url() ?>usuario/lista" id="btnSalir" class="btn btn-link">Salir</a>
    </div>

</div>
