<div id="mensajeaux"></div>
<div class="row">
    <div class="col-md-8">
        <span class="leyenda text-center">Per&iacute;odo de vacaciones del usuario</span>       
        <hr>
    </div>
</div>
<div class="row" id="datos-enviar">
    <div class="col-md-8">
        <?php
        if (isset($idempleado)) {
        ?>
        <input type="hidden" name="id-empleado" id="id-empleado" value="<?php echo $idempleado; ?>">
        <?php    
        }
        ?>
        <div class="col-md-6">
            <div class="item-ver">
                <div class="input-group espacioControles">
                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar"></span></span>
                    <input type="text" readonly value="<?php echo isset($vacaciones->fecha_inicio) && !empty($vacaciones->fecha_inicio) ? $vacaciones->fecha_inicio:"";?>" id="fecha-inicio" name="fecha-inicio" class="form-control datepicker espacioControles" aria-describedby="basic-addon1" placeholder="Fecha Inicio">            
                    <span class="help-block"></span>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="item-ver">
                <div class="input-group espacioControles">
                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar"></span></span>
                    <input type="text" readonly value="<?php echo isset($vacaciones->fecha_fin) && !empty($vacaciones->fecha_fin) ? $vacaciones->fecha_fin:"";?>" id="fecha-fin" name="fecha-fin" class="form-control datepicker espacioControles" aria-describedby="basic-addon1" placeholder="Fecha Final">            
                    <span class="help-block"></span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-sm-12 col-md-4 text-center">
        <button type="button" id="btnGuardarVacaciones" class="btn btn-primary">Guardar</button>                
    </div>
    <div class="col-sm-12 col-md-4 text-center">
        <a href="<?php echo base_url() ?>empleado/lista" id="btnSalir" class="btn btn-link">Salir</a>
    </div>

</div>
