<div id="mensajeaux"></div>
<div class="row">
    <div class="col-md-8">
        <span class="leyenda text-center">Datos del usuario</span>       
        <hr>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
		<div class="row">   
            <div class="col-md-12 espacio5">
			<input type="hidden" id="txtidempleado" value="<?php echo $oEmpleado->idrh_empleado;?>">
                <input type="text" id="txtnombre" value="<?php echo $oEmpleado->rh_nombre;?>" class="form-control" placeholder="Nombre">    
            </div> 
            
        </div><!--/row-->
		<div class="row">   
            <div class="col-md-12 espacio5">
                <input type="text" id="txtdireccion" value="<?php echo $oEmpleado->rh_direccion;?>" class="form-control" placeholder="Dirección">    
            </div>
        </div><!--/row--> 
        <div class="row">   
			<div class="col-md-2 espacio5">
                <input type="text" id="txtdni" value="<?php echo $oEmpleado->rh_dni;?>" class="form-control" placeholder="DNI">
            </div>
			<div class="col-md-2 espacio5">
				<select class="form-control" id="cbosexo">
                    <option value="">Sexo</option>                    
                    <option value="1" <?php if($oEmpleado->rh_sexo==1) echo "selected";?>>M</option>                    
                    <option value="2" <?php if($oEmpleado->rh_sexo==2) echo "selected";?>>F</option>                   
                </select>
			</div>
			<div class="col-md-2 espacio5">
				<select class="form-control" id="cboregimen">
                    <option value=""><?php echo ("Régimen"); ?></option>                    
                    <option value="48">48 Hr.</option>                    
                    <option value="24">24 Hr.</option>                   
                </select>
                
            </div>
            <div class="col-md-2 espacio5">
				<input type="text" id="txtsueldo" value="<?php echo $oContrato->sueldo;?>" class="form-control" placeholder="Sueldo">               
            </div>
			<div class="col-md-4 espacio5">
				<select class="form-control" id="cbolabor">
                    <option value="">Seleccione labor</option>                    
                    <?php
					foreach($aLabores as $itemLabor){
						?>
					<option value="<?php echo $itemLabor->idlabor?>" <?php if($itemLabor->idlabor == $oContrato->idlabor) echo "selected"; ?>><?php echo $itemLabor->nombre;?></option> 
					<?php
					}
					?>     
                </select>
			</div>
        </div><!--/row-->
		<div class="row">   
            <div class="col-md-6 espacio5">
                 <input type="text" id="txttelefono" value="<?php echo $oEmpleado->rh_telefono;?>" class="form-control" placeholder="<?php echo ("Teléfono")?>">  
            </div>
            <div class="col-md-6 espacio5">
                <input type="text" id="txtemail"  value="<?php echo $oEmpleado->rh_correo;?>" class="form-control" placeholder="Email">  
            </div>

        </div><!--/row-->
		<div class="row">   
            <div class="col-md-3 espacio5">
				<strong>Lunes</strong>
				<select class="form-control" style="margin-bottom:4px;" id="txt_lu_hora_inicio">
					<option value="">Hr. Inicio</option>   
					<?php for($i=0;$i<24;$i++){?>
                    <option value="<?php echo $i;?>" <?php if($oContrato->lunes==1){if($i==$oContrato->lu_hora_inicio) echo "selected";}?>><?php echo $i;?></option>
					<?php }?>
                </select>
                <select class="form-control" style="margin-bottom:4px;" id="txt_lu_hora_fin">
				<option value="">Hr. Fin</option>   
					<?php for($i=0;$i<24;$i++){?>
                    <option value="<?php echo $i;?>" <?php if($oContrato->lunes==1){if($i==$oContrato->lu_hora_fin) echo "selected";}?>><?php echo $i;?></option>
					<?php }?>
                </select>				
            </div>
            <div class="col-md-3 espacio5">
				<strong>Martes</strong>
                <select class="form-control" style="margin-bottom:4px;" id="txt_ma_hora_inicio">
					<option value="">Hr. Inicio</option>   
					<?php for($i=0;$i<24;$i++){?>
                    <option value="<?php echo $i;?>" <?php if($oContrato->martes==1){if($i==$oContrato->ma_hora_inicio) echo "selected";}?>><?php echo $i;?></option>
					<?php }?>
                </select>
                <select class="form-control" style="margin-bottom:4px;" id="txt_ma_hora_fin">
				<option value="">Hr. Fin</option>   
					<?php for($i=0;$i<24;$i++){?>
                    <option value="<?php echo $i;?>" <?php if($oContrato->martes==1){if($i==$oContrato->ma_hora_fin) echo "selected";}?>><?php echo $i;?></option>
					<?php }?>
                </select>
            </div>
			<div class="col-md-3 espacio5">
			<strong><?php echo ("Miércoles");?></strong>
                <select class="form-control" style="margin-bottom:4px;" id="txt_mi_hora_inicio">
					<option value="">Hr. Inicio</option>   
					<?php for($i=0;$i<24;$i++){?>
                    <option value="<?php echo $i;?>" <?php if($oContrato->miercoles==1){if($i==$oContrato->mi_hora_inicio) echo "selected";}?>><?php echo $i;?></option>
					<?php }?>
                </select>
                <select class="form-control" style="margin-bottom:4px;" id="txt_mi_hora_fin">
				<option value="">Hr. Fin</option>   
					<?php for($i=0;$i<24;$i++){?>
                    <option value="<?php echo $i;?>" <?php if($oContrato->miercoles==1){if($i==$oContrato->mi_hora_fin) echo "selected";}?>><?php echo $i;?></option>
					<?php }?>
                </select> 
            </div>
			<div class="col-md-3 espacio5">
				<strong><?php echo ("Jueves");?></strong>
                <select class="form-control" style="margin-bottom:4px;" id="txt_ju_hora_inicio">
					<option value="">Hr. Inicio</option>   
					<?php for($i=0;$i<24;$i++){?>
                    <option value="<?php echo $i;?>" <?php if($oContrato->jueves==1){if($i==$oContrato->ju_hora_inicio) echo "selected";}?>><?php echo $i;?></option>
					<?php }?>
                </select>
                <select class="form-control" style="margin-bottom:4px;" id="txt_ju_hora_fin">
				<option value="">Hr. Fin</option>   
					<?php for($i=0;$i<24;$i++){?>
                    <option value="<?php echo $i;?>" <?php if($oContrato->jueves==1){if($i==$oContrato->ju_hora_fin) echo "selected";}?>><?php echo $i;?></option>
					<?php }?>
                </select>
            </div>
        </div><!--/row-->
		<div class="row">   
            <div class="col-md-3 espacio5">
				<strong><?php echo ("Viernes");?></strong>
                <select class="form-control" style="margin-bottom:4px;" id="txt_vi_hora_inicio">
					<option value="">Hr. Inicio</option>   
					<?php for($i=0;$i<24;$i++){?>
                    <option value="<?php echo $i;?>" <?php if($oContrato->viernes==1){if($i==$oContrato->vi_hora_inicio) echo "selected";}?>><?php echo $i;?></option>
					<?php }?>
                </select>
                <select class="form-control" style="margin-bottom:4px;" id="txt_vi_hora_fin">
				<option value="">Hr. Fin</option>   
					<?php for($i=0;$i<24;$i++){?>
                    <option value="<?php echo $i;?>" <?php if($oContrato->viernes==1){if($i==$oContrato->vi_hora_fin) echo "selected";}?>><?php echo $i;?></option>
					<?php }?>
                </select>
            </div>
            <div class="col-md-3 espacio5">
				<strong><?php echo ("Sábado");?></strong>
                <select class="form-control" style="margin-bottom:4px;" id="txt_sa_hora_inicio">
					<option value="">Hr. Inicio</option>   
					<?php for($i=0;$i<24;$i++){?>
                    <option value="<?php echo $i;?>" <?php if($oContrato->sabado==1){if($i==$oContrato->sa_hora_inicio) echo "selected";}?>><?php echo $i;?></option>
					<?php }?>
                </select>
                <select class="form-control" style="margin-bottom:4px;" id="txt_sa_hora_fin">
				<option value="">Hr. Fin</option>   
					<?php for($i=0;$i<24;$i++){?>
                    <option value="<?php echo $i;?>" <?php if($oContrato->sabado==1){if($i==$oContrato->sa_hora_fin) echo "selected";}?>><?php echo $i;?></option>
					<?php }?>
                </select>
            </div>
			<div class="col-md-3 espacio5">
				<strong><?php echo ("Domingo");?></strong>
                <select class="form-control" style="margin-bottom:4px;" id="txt_do_hora_inicio">
					<option value="">Hr. Inicio</option>   
					<?php for($i=0;$i<24;$i++){?>
                    <option value="<?php echo $i;?>" <?php if($oContrato->domingo==1){if($i==$oContrato->do_hora_inicio) echo "selected";}?>><?php echo $i;?></option>
					<?php }?>
                </select>
                <select class="form-control" style="margin-bottom:4px;" id="txt_do_hora_fin">
				<option value="">Hr. Fin</option>   
					<?php for($i=0;$i<24;$i++){?>
                    <option value="<?php echo $i;?>" <?php if($oContrato->domingo==1){if($i==$oContrato->do_hora_fin) echo "selected";}?>><?php echo $i;?></option>
					<?php }?>
                </select>  	
            </div>
			
        </div><!--/row-->
		<div class="row">   
            <div class="col-md-6 espacio5">
                <div class="item-ver">
                <div class="input-group espacioControles">
                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar"></span></span>
                    <input type="text" readonly="" value="<?php echo getFechaEs2($oContrato->fecha_inicio);?>" id="finicio" class="form-control espacioControles" aria-describedby="basic-addon1" placeholder="Fecha inicio">            
                </div>
				</div> 
            </div>
            <div class="col-md-6 espacio5">
               <div class="item-ver">
                <div class="input-group espacioControles">
                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar"></span></span>
                    <input type="text" readonly="" id="ffin" value="<?php echo getFechaEs2($oContrato->fecha_fin);?>" class="form-control espacioControles" aria-describedby="basic-addon1" placeholder="Fecha cese">            
                </div>
				</div>
            </div>

        </div><!--/row-->
        <div class="row">   

            <div class="col-md-6 espacio5">
                <select class="form-control" id="cbolocal">
                    <option value="">Seleccione local</option>                    
                    <?php
					foreach($aLocal as $itemLocal){
						?>
					<option value="<?php echo $itemLocal->idlocal?>" <?php if($itemLocal->idlocal==$oContrato->idlocal) echo "selected";?>><?php echo $itemLocal->nombre;?></option> 
					<?php
					}
					?>     
                </select>
            </div>
            <div class="col-md-6 espacio5">
                <select class="form-control" id="cboEstado">
                    <option value="1" <?php if($oEmpleado->estado_registro == 1) echo "selected";?>>Activo</option>                    
                    <option value="2" <?php if($oEmpleado->estado_registro == 2) echo "selected";?>>Inactivo</option>                    
                </select>
            </div>

        </div><!--/row-->
        <div class="row">
            <div class="col-md-8">
                <button id="btnAgregarArchivo" class="btn btn-default classAdjuntar" style="display: none;" type="submit">Añadir archivo</button>
                <div id="subir2" class="clearfix" style="padding-left: 7px;display: inline-block;margin-top: 9px;margin-left: -6px;">
                    <input type="hidden" id="hid_input_files" name="hid_input_files" value="file_0" />
                    <div id="remove_0" style="display: none;margin-right: 4px;" class="btn btn-sm btn-default btn_remover_files" data-id="0"><span data-id="file_0" class="glyphicon glyphicon-minus-sign btn_remover_files"></span></div>
                    <input type="file" name="file_0" id="file_0" class="custom-file-input">                        
                </div>
            </div>
        </div>
    </div><!--/span-->

    <div class="col-md-4">

    </div><!--/span-->       
</div><!--/row-->   

<div class="clearfix"></div>
<div class="row">
    <div class="col-sm-12 col-md-4 text-center">
        <button type="button" id="btnEditarempleado" class="btn btn-primary">Editar</button>                
    </div>
    <div class="col-sm-12 col-md-4 text-center">
        <a href="<?php echo base_url() ?>empleado/lista" id="btnSalir" class="btn btn-link">Salir</a>
    </div>

</div>
