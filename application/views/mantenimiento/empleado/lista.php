<?php echo $mensaje; $this->session->set_userdata('mensaje', "");?>
<div id="listaTareas">
    <ul class="list-group" id="contact-list2">
        <?php
        foreach ($aListaEmpleados as $itemEmpleado) {
            
            ?>
            <li class="list-group-item" style="margin-bottom: 7px;">
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <div style="font-size: 14px;font-weight: bold;"><span><?php echo $itemEmpleado->rh_nombre; ?></span></div>                            
                    <div class="pad3"><span><?php echo $itemEmpleado->rh_dni; ?></span></div>
                    <div><span><?php if($itemEmpleado->rh_sexo==1){echo "M";}else{ echo "F";}; ?></span></div>
                </div>
				<div class="col-xs-12 col-sm-4 col-md-4">
					 <div class="pad3">
                        <?php
                        if ($itemEmpleado->estado_registro == 1) {
                            ?>
                            <span class="label label-success">Activo</span>
                            <?php
                        } else {
                            ?>
                            <span class="label label-danger">Inactivo</span>
                            <?php
                        }
                        ?>
                    </div>
					<div class="pad3"><?php echo $itemEmpleado->rh_telefono; ?></div>
					<div><?php echo $itemEmpleado->rh_correo; ?></div>
				</div>
                <div class="col-xs-12 col-sm-4 col-md-4 pad3 text-right" data-numero="2">
                    <div class="pad3" style="white-space: nowrap"><a href="<?php echo base_url() . "tareo/verhorario/" . $itemEmpleado->idrh_empleado ?>" target="_blank">Ver horario</a></div>
                    <div class="pad3" style="white-space: nowrap"><a href="<?php echo base_url() . "empleado/editar/" . $itemEmpleado->idrh_empleado ?>">Editar</a></div>
                    <div class="pad3" style="white-space: nowrap"><a href="<?php echo base_url() . "empleado/vacaciones/" . $itemEmpleado->idrh_empleado ?>">Modificar per&iacute;odo de vacaciones</a></div>
                </div>

                <div class="clearfix"></div>                        
            </li> 

            <?php
        }
        ?>
    </ul>
</div>