<?php echo $mensaje; ?>
<div id="listaTareas">
    <ul class="list-group" id="contact-list2">
        <?php
        foreach ($aListaCategoria as $itemCategoria) {
            ?>
            <li class="list-group-item" style="margin-bottom: 7px;">
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <div  style="font-size: 14px;font-weight: bold;">NOMBRE</div>
                    <div><span><?php echo $itemCategoria->nombre; ?></span></div>                                                                    
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <div  style="font-size: 14px;font-weight: bold;">RESPONSABLE</div>
                    <div class="pad3"><span><?php echo $itemCategoria->nomresponsable; ?></span><span></span></div>
                </div> 
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div>
                        <?php
                        if ($itemCategoria->estado_registro == 1) {
                            ?>
                            <span class="label label-success">Activo</span>
                            <?php
                        } else {
                            ?>
                            <span class="label label-danger">Inactivo</span>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="pad3" style="white-space: nowrap"><a href="<?php echo base_url() . "categoria/editar/" . $itemCategoria->idcategoria ?>">Editar</a></div>                    
                </div> 
                <div class="clearfix"></div>                        
            </li> 

            <?php
        }
        ?>
    </ul>
</div>