<?php
$mensaje = $this->session->userdata('mensaje');
if ($mensaje != "") {
    getHtmlMensaje(2, $mensaje);
}
?>
<div id="mensajeaux"></div>
<div class="row">
    <div class="col-md-8">
        <div class="row">
            <span class="leyenda text-center">Datos del local</span>       
            <hr>
        </div>
        <div class="row">   
            <div class="col-md-6 espacio5">
                <input type="hidden" id="txtidtipodoc" value="<?php echo $oTipodoc->idtipodoc; ?>">
                <input type="text" id="txtnombre" value="<?php echo $oTipodoc->nombre; ?>" class="form-control" placeholder="Nombre completo">    
            </div> 
            <div class="col-md-6 espacio5">
                <select class="form-control" id="cboEstado">
                    <option value="">Seleccione estado</option>                    
                    <option value="1" <?php if ($oTipodoc->estado_registro == 1) echo "selected"; ?>>Activo</option>                    
                    <option value="2" <?php if ($oTipodoc->estado_registro == 2) echo "selected"; ?>>Inactivo</option>                    
                </select>
            </div>
        </div><!--/row-->                      
    </div><!--/span-->

    <div class="col-md-2">

    </div><!--/span-->       
</div><!--/row-->   

<div class="clearfix"></div>
<div class="row">
    <div class="col-sm-12 col-md-4 text-center">
        <button type="button" id="btnEditartipodoc" class="btn btn-primary">Editar</button>                
    </div>
    <div class="col-sm-12 col-md-4 text-center">
        <a href="<?php echo base_url() ?>tipodoc/lista" id="btnSalir" class="btn btn-link">Salir</a>
    </div>

</div>
