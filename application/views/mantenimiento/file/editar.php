<?php
$mensaje = $this->session->userdata('mensaje');
if ($mensaje != "") {
    getHtmlMensaje(2, $mensaje);
}
?>
<div id="mensajeaux"></div>
<div class="row">
    <div class="col-md-8">
        <div class="row">
            <span class="leyenda text-center">Datos del file</span>       
            <hr>
        </div>
        <div class="row">   
            <div class="col-md-6 espacio5">
                <input type="hidden" id="txtidfile" value="<?php echo $oFile->idfile; ?>">
                <input type="text" id="txtnombre" value="<?php echo $oFile->codigo; ?>" class="form-control" placeholder="Nombre completo">    
            </div> 
            <div class="col-md-6 espacio5">
                <select class="form-control" id="cboestado">
                    <option value="">Seleccione estado</option>                    
                    <option value="1" <?php if ($oFile->estado_registro == 1) echo "selected"; ?>>Activo</option>                    
                    <option value="2" <?php if ($oFile->estado_registro == 2) echo "selected"; ?>>Inactivo</option>                    
                </select>
            </div>
        </div><!--/row-->   
		<div class="row">
			<div class="col-md-12">
			<textarea id="txtdescripcion" placeholder="Escribe una descripción" class="form-control" rows="2"><?php echo $oFile->descripcion; ?></textarea>
			</div>
		</div>		
    </div><!--/span-->

    <div class="col-md-2">

    </div><!--/span-->       
</div><!--/row-->   

<div class="clearfix"></div>
<div class="row" style="margin-top: 5px;">
    <div class="col-sm-12 col-md-4 text-center">
        <button type="button" id="btnEditarfile" class="btn btn-primary">Editar</button>                
    </div>
    <div class="col-sm-12 col-md-4 text-center">
        <a href="<?php echo base_url() ?>file/lista" id="btnSalir" class="btn btn-link">Salir</a>
    </div>

</div>
