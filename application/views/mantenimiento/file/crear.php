<div class="row">
    <div class="col-md-8">
        <span class="leyenda text-center">Datos del file</span>       
        <hr>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <div class="row">   
            <div class="col-md-6 espacio5">
                <input type="text" id="txtnombre" class="form-control" placeholder="Código del file">    
            </div> 
            <div class="col-md-6 espacio5">
                <select class="form-control" id="cboestado">
                    <option value="">Seleccione estado</option>                    
                    <option value="1">Activo</option>                
                    <option value="2">Inactivo</option>                
                </select>
            </div>
        </div><!--/row-->                
    </div><!--/span-->

    <div class="col-md-4">

    </div><!--/span-->       
</div><!--/row-->  
<div class="row">
	<div class="col-md-8">
	<textarea id="txtdescripcion" placeholder="Escribe una descripción" class="form-control" rows="2"></textarea>
	</div>
</div> 
<div class="clearfix"></div>
<div class="row" style="margin-top:5px;">
    <div class="col-sm-12 col-md-4 text-center">
        <button type="button" id="btnCrearfile" class="btn btn-primary">Crear</button>                
    </div>
    <div class="col-sm-12 col-md-4 text-center">
        <a href="<?php echo base_url() ?>file/lista" id="btnSalir" class="btn btn-link">Salir</a>
    </div>

</div>
