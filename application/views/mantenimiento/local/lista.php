<?php echo $mensaje; ?>
<div id="listaTareas">
    <ul class="list-group" id="contact-list2">
        <?php
        foreach ($aListaLocales as $itemLocal) {
            ?>
            <li class="list-group-item" style="margin-bottom: 7px;">
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <div  style="font-size: 14px;font-weight: bold;">NOMBRE</div>
                    <div><span><?php echo $itemLocal->nombre; ?></span></div>                                                                    
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <div  style="font-size: 14px;font-weight: bold;">RESPONSABLE</div>
                    <div class="pad3"><span><?php echo $itemLocal->nomresponsable; ?></span><span></span></div>
                </div> 
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div>
                        <?php
                        if ($itemLocal->estado_registro == 1) {
                            ?>
                            <span class="label label-success">Activo</span>
                            <?php
                        } else {
                            ?>
                            <span class="label label-danger">Inactivo</span>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="pad3" style="white-space: nowrap"><a href="<?php echo base_url() . "local/editar/" . $itemLocal->idlocal ?>">Editar</a></div>                    
                </div> 
                <div class="clearfix"></div>                        
            </li> 

            <?php
        }
        ?>
    </ul>
</div>