<div id="busquedaAvanzada"></div>
<?php
echo $divmensaje;
$this->session->set_userdata('mensaje', "");
?>
<div id="listaTareas">
    <div class="row">
        <?php
        foreach ($aListaConfig as $itemConfig) {
            ?>

            <div class="col-lg-2">
                <div><label><?php echo $itemConfig->nombre ?></label></div>
                <div><input type="text" id="config_<?php echo $itemConfig->idconfiguracion; ?>" value="<?php echo $itemConfig->valor; ?>" class="form-control"></div>
                <div style="margin-top: 5px;"><button data-variable="<?php echo $itemConfig->variable; ?>" data-id="<?php echo $itemConfig->idconfiguracion; ?>" class="btn btn-primary dataconfig" style="width: 100%">Guardar</button></div>
            </div>

            <?php
        }
        ?>
    </div>
</div>

<div class="row" style="display: none;">
    <div id="archivos" class="col-md-8">
        <button id="btnAdjuntar" class="btn btn-default classAdjuntar" type="submit">Adjuntar</button>

        <div id="subir" class="clearfix" style="padding-left: 7px;display: inline-block;margin-top: 9px;margin-left: -6px;">
            <input id="my_file_element" type="file" name="file_1" ><br clear="all"/>
            <div id="files_list"></div>                            
        </div>
    </div>
</div>