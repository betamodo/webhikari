<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//si no existe la función invierte_date_time la creamos
if (!function_exists('validaSession')) {

    // Validar Session 
    function validaSession($tipo = "formulario") {
        $ci = & get_instance();
        $aData = array();
        switch ($tipo) {
            case "formulario":
                if (intval($ci->session->userdata('idusuario')) == 0) {
                    redirect(base_url(), 'refresh');
                } else {
                    $aData = array("msj" => "", "data" => array());
                }
                break;
            case "json":
                if (intval($ci->session->userdata('idusuario')) == 0) {
                    $aData = array("msj" => "session", "data" => array());
                } else {
                    $aData = array("msj" => "", "data" => array());
                }

                break;

            default:
                if (intval($ci->session->userdata('idusuario')) == 0) {
                    redirect(base_url(), 'refresh');
                } else {
                    $aData = array("msj" => "", "data" => array());
                }
                break;
        }
        return $aData;
    }

}

if (!function_exists('getEmpleadosBan')) {

    function getEmpleadosBan($hora="",$dia="",$idlocal="",$idarea="",$finicio="",$ffin="") {
       $ci = & get_instance();
	   $ci->load->model('empleadomodel');
	   $auxDiasPersonalizado= getIdDia();	   
	   $where="(".intval($hora)." BETWEEN c.".$auxDiasPersonalizado[$dia]["abrev"]."_hora_inicio AND c.".$auxDiasPersonalizado[$dia]["abrev"]."_hora_fin) and ".strtolower($dia)." = 1 and c.idlocal = ".$idlocal." and c.idlabor = ".$idarea." and ('".  getFechaEn2($finicio)."' >= c.fecha_inicio) and ( '".  getFechaEn2($ffin)."' <= c.fecha_fin)";
	   $oEmpleados = $ci->empleadomodel->getEmpleadosContrato($where);
	   return $oEmpleados;
    }

}

if (!function_exists('getEmpleadosBanAux')) {

    function getEmpleadosBanAux($hora="",$dia="",$idlocal="",$idarea="",$finicio="",$ffin="",$tipo=2) {
       $ci = & get_instance();
	   $ci->load->model('empleadomodel');
	   $ci->load->model('libresmodel'); 
	   $auxDiasPersonalizado= getIdDia();
	   $oLibres = $ci->libresmodel->getdata(array("GROUP_CONCAT(idempleado SEPARATOR ',') as ids"),"tipo = 1 and hora = ".$hora." and dia = '".$dia."'",1);
	   $whereLibres="";
	   
	   $oAumento = $ci->libresmodel->getdata(array("GROUP_CONCAT(idempleado SEPARATOR ',') as ids"),"tipo = 2 and hora = ".$hora." and dia = '".$dia."'",1);
	   $whereLibres="";
	   $whereIn="";
	   if($oLibres->ids != NULL){
			$whereLibres="AND c.idempleado NOT IN (".$oLibres->ids.")";
		}
	   
	   
	   if($tipo==2){
			if($oAumento->ids != NULL){
				//$whereIn=strtolower($dia)." = 1 and c.idlocal = ".$idlocal." and c.idlabor = ".$idarea." and ('".  getFechaEn2($finicio)."' >= c.fecha_inicio) and ( '".  getFechaEn2($ffin)."' <= c.fecha_fin) and c.idempleado IN (".$oAumento->ids.")";
				$whereIn="c.idlocal = ".$idlocal." and c.idlabor = ".$idarea." and ('".  getFechaEn2($finicio)."' >= c.fecha_inicio) and ( '".  getFechaEn2($ffin)."' <= c.fecha_fin) and c.idempleado IN (".$oAumento->ids.")"; 
			}
		   
			$where="(".intval($hora)." BETWEEN c.".$auxDiasPersonalizado[$dia]["abrev"]."_hora_inicio AND c.".$auxDiasPersonalizado[$dia]["abrev"]."_hora_fin) and ".strtolower($dia)." = 1 and c.idlocal = ".$idlocal." and c.idlabor = ".$idarea." and ('".  getFechaEn2($finicio)."' >= c.fecha_inicio) and ( '".  getFechaEn2($ffin)."' <= c.fecha_fin) ".$whereLibres;
			 
			$oEmpleados = $ci->empleadomodel->getEmpleadosContratoAux2($where,$whereIn); 
	   
	   }else{
		   
			if($oAumento->ids != NULL){
				$whereIn=strtolower($dia)." = 1 and c.idlocal = ".$idlocal." and l.idarea = ".$idarea." and ('".  getFechaEn2($finicio)."' >= c.fecha_inicio) and ( '".  getFechaEn2($ffin)."' <= c.fecha_fin) and c.idempleado IN (".$oAumento->ids.")";
			}
		   
			$where="(".intval($hora)." BETWEEN c.".$auxDiasPersonalizado[$dia]["abrev"]."_hora_inicio AND c.".$auxDiasPersonalizado[$dia]["abrev"]."_hora_fin) and ".strtolower($dia)." = 1 and c.idlocal = ".$idlocal." and l.idarea = ".$idarea." and ('".  getFechaEn2($finicio)."' >= c.fecha_inicio) and ( '".  getFechaEn2($ffin)."' <= c.fecha_fin) ".$whereLibres;
			
			$oEmpleados = $ci->empleadomodel->getEmpleadosContratoAux3($where,$whereIn); 
			
	   }
	   /*echo "<pre>";
	   var_dump($oEmpleados);
	   echo "</pre>";*/
	   return $oEmpleados;
    }

}

if (!function_exists('getHtmlMensaje')) {

    function getHtmlMensaje($idmensaje, $mensaje = "") {
        $divmensaje = "";
        switch ($idmensaje) {
            case 1:
                $divmensaje = ' <div class="alert alert-success fade in">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong></strong> ' . $mensaje . '
                </div>';

                break;
            case 2:
                $divmensaje = ' <div class="alert alert-danger fade in">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong></strong> ' . $mensaje . '</div>';
                break;
            default:
                $divmensaje = "";
                break;
        }
        return $divmensaje;
    }

}

if (!function_exists('getFechaEs')) {

    function getFechaEs($fecha) {
        date_default_timezone_set("America/Lima");
        return date("d/m/Y H:i:s", strtotime($fecha));
    }

}
if (!function_exists('getNombreBoxEstante')) {

    function getNombreBoxEstante($id) {
        $nombre = "";
        if ($id == 1) {
            $nombre = "BOX";
        } elseif ($id = 2) {
            $nombre = "STANT";
        }
        return $nombre;
    }

}

if (!function_exists('getNombreRol')) {

    function getNombreRol($idrol) {
        $nomrol = "";
        if ($idrol == 1) {
            $nomrol = "Administrador";
        } elseif ($idrol == 2) {
            $nomrol = "Gestor";
        } elseif ($idrol == 3) {
            $nomrol = "Director";
        } elseif ($idrol == 4) {
            $nomrol = "Secretaria";
        } elseif ($idrol == 5) {
            $nomrol = "Super admin";
        }
        return $nomrol;
    }

}

if (!function_exists('getColorPrioridad')) {

    function getColorPrioridad($prioridad) {

        if ($prioridad == 1) {
            $color = "#5cb85c";
        }if ($prioridad == 2) {
            $color = "#CFCD4B";
        }if ($prioridad == 3) {
            $color = "#ED2020";
        }
        return $color;
    }

}

if (!function_exists('getColorEstado')) {

    function getColorEstado($estado) {

        if ($estado == 1) {
            $color = "#5cb85c";
        } else {
            $color = "#CFCD4B";
        }
        return $color;
    }

}

if (!function_exists('getTiempoTranscurrido')) {

    function getTiempoTranscurrido($fecha1, $fecha2) {
        $datetime1 = date_create($fecha1);
        $datetime2 = date_create($fecha2);
        $fecha = date_diff($datetime1, $datetime2);
        $tiempoTranscurrido = "Ahora";
        if ($fecha->y > 0) {
            $tiempoTranscurrido = $fecha->y . " A ";
        }
        if ($fecha->m > 0) {
            $tiempoTranscurrido = $fecha->m . " M ";
        }
        if ($fecha->d > 0) {
            $tiempoTranscurrido = $fecha->d . " d ";
        }
        if ($fecha->h > 0) {
            $tiempoTranscurrido = $fecha->h . "h ";
        }
        if ($fecha->i > 0) {
            $tiempoTranscurrido = $fecha->i . "m ";
        }
        return $tiempoTranscurrido;
    }

}

if (!function_exists('getFechaEs2')) {

    function getFechaEs2($fecha) {
        date_default_timezone_set("America/Lima");
        return date("d/m/Y", strtotime($fecha));
    }

}
if (!function_exists('getNombrePrioridad')) {

    function getNombrePrioridad($idprioridad) {
        $nombre = "";
        if ($idprioridad == 1) {
            $nombre = "Baja";
        } elseif ($idprioridad == 2) {
            $nombre = "Media";
        } else {
            $nombre = "Alta";
        }
        return $nombre;
    }

}
if (!function_exists('getArrayRepeticion')) {

    function getArrayRepeticion($idrepeticion) {
        $textoRepeticion = "";
        switch ($idrepeticion) {
            case 1:
                $textoRepeticion = "Diario";
                break;
            case 2:
                $textoRepeticion = "Semanal";
                break;
            case 3:
                $textoRepeticion = "Mensual";
                break;
            case 4:
                $textoRepeticion = "Anual"; 
                break;
			case 5:
                $textoRepeticion = "Cada 2 meses";
                break;
			case 6:
                $textoRepeticion = "Cada 3 meses";
                break;
			case 7:
                $textoRepeticion = "Cada 4 meses";
                break;
			case 8:
                $textoRepeticion = "Cada 5 meses";
                break;
			case 9:
                $textoRepeticion = "Cada 6 meses";
                break;
			case 10:
                $textoRepeticion = "Cada 7 meses";
                break;
			case 11:
                $textoRepeticion = "Cada 8 meses";
                break;
			case 12:
                $textoRepeticion = "Cada 9 meses";
                break;
			case 13:
                $textoRepeticion = "Cada 10 meses";
                break;
			case 14:
                $textoRepeticion = "Cada 11 meses";
                break;

            default:
                $textoRepeticion = "";
                break;
        }
        return $textoRepeticion;
    }

}
if (!function_exists('getResponsable')) {

    function getResponsable($idestado = 0, $paraadmin = 0) {

        $responsable = "";
        switch ($idestado) {
            case 1:
                if ($paraadmin == 1) {
                    $responsable = "Administrador";
                } else {
                    $responsable = "Gestor";
                }
                break;
            case 2:
                if ($paraadmin == 1) {
                    $responsable = "Administrador";
                } else {
                    $responsable = "Gestor";
                }
                break;
            case 3:
                if ($paraadmin == 1) {
                    $responsable = "Gestor";
                } else {
                    $responsable = "Administrador";
                }

                break;
            case 4:
                if ($paraadmin == 1) {
                    $responsable = "Gestor";
                } else {
                    $responsable = "Administrador";
                }
                break;
            case 5:
                if ($paraadmin == 1) {
                    $responsable = "";
                } else {
                    $responsable = "";
                }
                break;
            case 6:
                if ($paraadmin == 1) {
                    $responsable = "Administrador";
                } else {
                    $responsable = "Gestor";
                }
                break;
            case 7:
                $responsable = "Director";
                break;
            case 8:
                if ($paraadmin == 1) {
                    $responsable = "Administrador";
                } else {
                    $responsable = "Gestor";
                }
                break;
            case 9:
                if ($paraadmin == 1) {
                    $responsable = "";
                } else {
                    $responsable = "";
                }
                break;
            case 10:
                $responsable = "Director";
                break;
            case 11:
                if ($paraadmin == 1) {
                    $responsable = "Administrador";
                } else {
                    $responsable = "Gestor";
                }
                break;
            default:
                $responsable = "VACIO";
                break;
        }
        return $responsable;
    }

}
if (!function_exists('getEstados')) {

    function getEstados() {
        $aEstados = array();
		$ci = & get_instance();
        $rol = intval($ci->session->userdata('rol'));
		if($rol==3){
			$aEstados[1][0] = array("nombre" => "Por atender", "gerundio" => "Registro de tarea", "imagen" => base_url() . "media/img/poratender.png", "rol" => "");
		}else{
			$aEstados[1][0] = array("nombre" => "Por atender", "gerundio" => "Editar tarea", "imagen" => base_url() . "media/img/poratender.png", "rol" => "");
		}
        
        $aEstados[1][1] = array("nombre" => "Por atender", "gerundio" => "Por atender (Ya evaluado)", "imagen" => base_url() . "media/img/poratender.png", "rol" => "");
        $aEstados[2][0] = array("nombre" => "En ejecución", "gerundio" => "Atender tarea - Aceptado", "imagen" => base_url() . "media/img/porejecutar.png", "rol" => "");
        $aEstados[2][1] = array("nombre" => "En ejecución", "gerundio" => "Evaluar presupuesto - Aprobado", "imagen" => base_url() . "media/img/porejecutar.png", "rol" => "");
        $aEstados[3][0] = array("nombre" => "Por corregir tarea", "gerundio" => "Atender tarea- Rechazado", "imagen" => base_url() . "media/img/corregir.png", "rol" => "");
        $aEstados[3][2] = array("nombre" => "Por corregir tarea", "gerundio" => "Atender tarea - A corregir tarea", "imagen" => base_url() . "media/img/corregir.png", "rol" => "");
        $aEstados[3][1] = array("nombre" => "Por corregir tarea", "gerundio" => "Atender tarea- Rechazado", "imagen" => base_url() . "media/img/corregir.png", "rol" => "");
        $aEstados[4][0] = array("nombre" => "Por confirmar", "gerundio" => "Ejecutar tarea", "imagen" => base_url() . "media/img/porconfirmar.png", "rol" => "");
        $aEstados[5][0] = array("nombre" => "Terminado", "gerundio" => "Confirmar tarea", "imagen" => base_url() . "media/img/terminado.png", "rol" => "");
        $aEstados[6][0] = array("nombre" => "Por corregir ejecución", "gerundio" => "Conformidad de tarea", "imagen" => base_url() . "media/img/corregir.png", "rol" => "");
        $aEstados[7][0] = array("nombre" => "Por evaluar presupuesto", "gerundio" => "Atender tarea", "imagen" => base_url() . "media/img/evaluarppto.png", "rol" => "");
        $aEstados[10][0] = array("nombre" => "Por evaluar presupuesto", "gerundio" => "Corregir ppto", "imagen" => base_url() . "media/img/evaluarppto.png", "rol" => "");
        $aEstados[10][1] = array("nombre" => "Por evaluar presupuesto", "gerundio" => "Enviar Presupuesto", "imagen" => base_url() . "media/img/evaluarppto.png", "rol" => "");
        $aEstados[11][0] = array("nombre" => "Por preparar ppto", "gerundio" => "Atender tarea", "imagen" => base_url() . "media/img/evaluarppto.png", "rol" => "");
        $aEstados[11][1] = array("nombre" => "Por preparar ppto", "gerundio" => "Atender tarea - Aprobado", "imagen" => base_url() . "media/img/evaluarppto.png", "rol" => "");
        $aEstados[8][0] = array("nombre" => "Por reevaluar ppto", "gerundio" => "Evaluar Presupuesto", "imagen" => base_url() . "media/img/editppto.png", "rol" => "");
        $aEstados[8][1] = array("nombre" => "Por reevaluar ppto", "gerundio" => "Evaluar Presupuesto - Rechazado", "imagen" => base_url() . "media/img/editppto.png", "rol" => "");
        $aEstados[9][0] = array("nombre" => "Rechazado", "gerundio" => "Rechazado", "imagen" => base_url() . "media/img/cancelado.png", "rol" => "");
        return $aEstados;
    }

}

if (!function_exists('getEnlace')) {

    // Validar Session 
    function getEnlace($idestado, $idtarea, $paraadmin = 0, $idusuario_creador = 0) {
        $ci = & get_instance();

        $enlace = "";
        $rol = intval($ci->session->userdata('rol'));
        switch ($rol) {
            case 1:
                if ($idestado == 1) {
                    if ($paraadmin == 1) {
                        $enlace = "<a href='" . base_url() . "tarea/poratender/" . $idtarea . "' class='subrayado'>Atender</a>";
                    } else {
                        $enlace = "<a href='" . base_url() . "tarea/editar/" . $idtarea . "' class='subrayado'>Editar</a>";
                    }
                } elseif ($idestado == 3) {
                    $enlace = "<a href='" . base_url() . "tarea/corregirtarea/" . $idtarea . "' class='subrayado'>Corregir tarea</a>";
                } elseif ($idestado == 4) {
                    $ci->load->model('usuariomodel');
                    $objUsuario = $ci->usuariomodel->getdata(array("rol"), "idusuario = " . $idusuario_creador, 1);
                    $rolCreador = $objUsuario->rol;
                    if ($paraadmin == 1) {
                        $enlace = "<a href='" . base_url() . "tarea/ver/" . $idtarea . "' class='subrayado'>Ver</a>";
                    } else {
                        if ($rolCreador == 3 && $rol == 1) {
                            $enlace = "<a href='" . base_url() . "tarea/confirmar/" . $idtarea . "' class='subrayado'>Confirmar ejecución</a>";
                        } else {
                            if ($rol == 1) {
                                $enlace = "<a href='" . base_url() . "tarea/confirmar/" . $idtarea . "' class='subrayado'>Confirmar ejecución</a>";
                            } else {
                                $enlace = "<a href='" . base_url() . "tarea/ver/" . $idtarea . "' class='subrayado'>Ver</a>";
                            }
                        }
                    }
                } elseif ($idestado == 2) {
                    if ($paraadmin == 1) {
                        $enlace = "<a href='" . base_url() . "tarea/ejecutar/" . $idtarea . "' class='subrayado'>Ejecutar</a>";
                    } else {
                        $enlace = "<a href='" . base_url() . "tarea/ver/" . $idtarea . "' class='subrayado'>Ver</a>";
                    }
                } elseif ($idestado == 6) {
                    if ($paraadmin == 1) {
                        $enlace = "<a href='" . base_url() . "tarea/corregir_ejecucion/" . $idtarea . "' class='subrayado'>Corregir Ejecución</a>";
                    } else {
                        $enlace = "<a href='" . base_url() . "tarea/ver/" . $idtarea . "' class='subrayado'>Ver</a>";
                    }
                } elseif ($idestado == 7) {
                    //$enlace = "<a href='" . base_url() . "tarea/ver/" . $idtarea . "' class='subrayado'>Ver</a>";
                    $enlace = "<a href='" . base_url() . "tarea/ver/" . $idtarea . "' class='subrayado'>Ver</a>";
                } elseif ($idestado == 11) {
                    if ($paraadmin == 1) {
                        $enlace = "<h5><span class='label label-warning'>Pend. Envío<span></h5> <a href='" . base_url() . "tarea/editar/" . $idtarea . "' class='subrayado'>Editar</a>";
                    } else {
                        $enlace = "<a href='" . base_url() . "tarea/ver/" . $idtarea . "' class='subrayado'>Ver</a>";
                    }
                } else {
                    $enlace = "<a href='" . base_url() . "tarea/ver/" . $idtarea . "' class='subrayado'>Ver</a>";
                }
                break;
            case 2:
                if ($idestado == 1) {
                    if ($paraadmin == 1) {
                        $enlace = "<a href='" . base_url() . "tarea/ver/" . $idtarea . "' class='subrayado'>Ver</a> | <a href='" . base_url() . "tarea/editar/" . $idtarea . "' class='subrayado'>Editar</a>";
                    } else {
							
						if($ci->session->userdata("idusuario")==$idusuario_creador){
							$enlace = "<a href='" . base_url() . "tarea/ver/" . $idtarea . "' class='subrayado'>Ver</a> | <a href='" . base_url() . "tarea/editar/" . $idtarea . "' class='subrayado'>Editar</a>";
						}else{
							$enlace = "<a href='" . base_url() . "tarea/poratender/" . $idtarea . "' class='subrayado'>Atender</a> | <a href='" . base_url() . "tarea/editar/" . $idtarea . "' class='subrayado'>Editar</a>";	
						}
                        //$enlace = "<a href='" . base_url() . "tarea/poratender/" . $idtarea . "' class='subrayado'>Atender</a> | <a href='" . base_url() . "tarea/editar/" . $idtarea . "' class='subrayado'>Editar</a>";
                    }
                } elseif ($idestado == 4) {
                    if ($paraadmin == 1) {
                        $ci->load->model('usuariomodel');
                        $objUsuario = $ci->usuariomodel->getdata(array("rol"), "idusuario = " . $idusuario_creador, 1);
                        $rolCreador = $objUsuario->rol;
                        if ($rolCreador == 1) {
                            $enlace = "<a href='" . base_url() . "tarea/confirmar/" . $idtarea . "' class='subrayado'>Confirmar</a> | <a href='" . base_url() . "tarea/editar/" . $idtarea . "' class='subrayado'>Editar</a>";
                        } else {
                            $enlace = "<a href='" . base_url() . "tarea/ver/" . $idtarea . "' class='subrayado'>Ver</a> | <a href='" . base_url() . "tarea/editar/" . $idtarea . "' class='subrayado'>Editar</a>";
                        }
                    } else {
						if($ci->session->userdata("idusuario")==$idusuario_creador){
								$enlace = "<a href='" . base_url() . "tarea/confirmar/" . $idtarea . "' class='subrayado'>Confirmar</a> | <a href='" . base_url() . "tarea/editar/" . $idtarea . "' class='subrayado'>Editar</a>";
							}else{
								$enlace = "<a href='" . base_url() . "tarea/ver/" . $idtarea . "' class='subrayado'>Ver</a> | <a href='" . base_url() . "tarea/editar/" . $idtarea . "' class='subrayado'>Editar</a>";
						}								
                    }
                } elseif ($idestado == 2) {
                    if ($paraadmin == 1) {
                        $enlace = "<a href='" . base_url() . "tarea/ver/" . $idtarea . "' class='subrayado'>Ver</a> | <a href='" . base_url() . "tarea/editar/" . $idtarea . "' class='subrayado'>Editar</a>";
                    } else {
						if($ci->session->userdata("idusuario")==$idusuario_creador){
							$enlace = "<a href='" . base_url() . "tarea/ver/" . $idtarea . "' class='subrayado'>Ver</a> | <a href='" . base_url() . "tarea/editar/" . $idtarea . "' class='subrayado'>Editar</a>";
						}else{
							$enlace = "<a href='" . base_url() . "tarea/ejecutar/" . $idtarea . "' class='subrayado'>Ejecutar</a> | <a href='" . base_url() . "tarea/editar/" . $idtarea . "' class='subrayado'>Editar</a>";
						}                        
                    }
                } elseif ($idestado == 6) {
                    if ($paraadmin == 1) {
                        $ci->load->model('usuariomodel');
                        $objUsuario = $ci->usuariomodel->getdata(array("rol"), "idusuario = " . $idusuario_creador, 1);
                        $rolCreador = $objUsuario->rol;
                        if ($rolCreador == 2) {
                            $enlace = "<a href='" . base_url() . "tarea/corregir_ejecucion/" . $idtarea . "' class='subrayado'>Corregir ejecución</a> | <a href='" . base_url() . "tarea/editar/" . $idtarea . "' class='subrayado'>Editar</a>";
                        } else {
                            $enlace = "<a href='" . base_url() . "tarea/ver/" . $idtarea . "' class='subrayado'>Ver</a>";
                        }
                    } else {
                        $enlace = "<a href='" . base_url() . "tarea/corregir_ejecucion/" . $idtarea . "' class='subrayado'>Corregir ejecución</a> | <a href='" . base_url() . "tarea/editar/" . $idtarea . "' class='subrayado'>Editar</a>";
                    }
                } elseif ($idestado == 7) {
                    $enlace = "<a href='" . base_url() . "tarea/ver/" . $idtarea . "' class='subrayado'>Ver</a>";
                } elseif ($idestado == 11) {
                    if ($paraadmin == 0) {
                        $enlace = "<h5><span class='label label-warning'>Pend. Envío<span></h5> <a href='" . base_url() . "tarea/editar/" . $idtarea . "' class='subrayado'>Editar</a>";
                    } else {
                        $enlace = "<a href='" . base_url() . "tarea/ver/" . $idtarea . "' class='subrayado'>Ver</a>";
                    }
                } elseif ($idestado == 8) {
                    $enlace = "<h5><span class='label label-warning'>Corregir Ppto.<span></h5> <a href='" . base_url() . "tarea/editar/" . $idtarea . "' class='subrayado'>Editar</a>";
                } elseif ($idestado == 9) {
                    $enlace = "<a href='" . base_url() . "tarea/ver/" . $idtarea . "' class='subrayado'>Ver</a>";
                } elseif ($idestado == 5) {
                    $enlace = "<a href='" . base_url() . "tarea/ver/" . $idtarea . "' class='subrayado'>Ver</a>";
                } else {
                    $enlace = "<a href='" . base_url() . "tarea/ver/" . $idtarea . "' class='subrayado'>Ver</a> | <a href='" . base_url() . "tarea/editar/" . $idtarea . "' class='subrayado'>Editar</a>";
                }
                break;
            case 3:
                if ($idestado == 7 || $idestado == 10) {
                    //$enlace = "<a href='" . base_url() . "tarea/evaluarppto/" . $idtarea . "' class='subrayado'>Evaluar Ppto</a>";
                    $enlace .= "<a href='" . base_url() . "tarea/ver/" . $idtarea . "' class='subrayado'>Ver</a>";
                    $enlace .= "  <h5><span class='label label-warning'>Evaluar Ppto<span></h5>";
                } elseif ($idestado == 4) {
					$ci->load->model('usuariomodel');
                    if ($paraadmin == 1) {
                        $objUsuario = $ci->usuariomodel->getdata(array("rol"), "idusuario = " . $idusuario_creador, 1);
                        $rolCreador = $objUsuario->rol;
                        if ($rolCreador == 3) {
                            $enlace = "<a href='" . base_url() . "tarea/confirmar/" . $idtarea . "' class='subrayado'>Confirmar</a>";
                        } else {
                            $enlace = "<a href='" . base_url() . "tarea/ver/" . $idtarea . "' class='subrayado'>Ver</a>";
                        }
                    } else {
						$objUsuario = $ci->usuariomodel->getdata(array("rol"), "idusuario = " . $idusuario_creador, 1);
                        $rolCreador = $objUsuario->rol;
                        if ($rolCreador == 3) {
                            $enlace = "<a href='" . base_url() . "tarea/confirmar/" . $idtarea . "' class='subrayado'>Confirmar</a>";
                        } else {
                            $enlace = "<a href='" . base_url() . "tarea/ver/" . $idtarea . "' class='subrayado'>Ver</a>";
                        }
                    }
                } else {
                    $enlace = "<a href='" . base_url() . "tarea/ver/" . $idtarea . "' class='subrayado'>Ver</a>";
                }
                $enlace .= " | <a href='" . base_url() . "tarea/movertarea/" . $idtarea . "' class='subrayado'>Mover</a>";
                break;

            default:
                break;
        }
        return $enlace;
    }

}

if (!function_exists('subirArchivos2')) {

    function subirArchivos2($Adjuntos = array(), $lugar = "") {
        $archivos = array();
        $ci = & get_instance();
        if ($lugar == "avatars") {
            $ruta = getcwd() . '/media/archivos/avatars/';
        } else {
            $ruta = getcwd() . '/media/archivos/';
        }

        $mensage = '';
        foreach ($Adjuntos['archivo']['error'] as $key => $error) {
				$NombreOriginal ="";
                
                    $NombreOriginal = $Adjuntos["archivo"]["name"][$key]; //Obtenemos el nombre original del archivo
                    $NombreOriginal = rand(1, 10000) . "_" . $NombreOriginal;
                    $temporal = $Adjuntos["archivo"]["tmp_name"][$key]; //Obtenemos la ruta Original del archivo
                    $Destino = $ruta . $NombreOriginal; //Creamos una ruta de destino con la variable ruta y el nombre original del archivo						
                    move_uploaded_file($temporal, $Destino); //Movemos el archivo temporal a la ruta especificada		
                
                if ($lugar == "avatars") {
                    resize($ruta, $NombreOriginal, 'thumbs', array("width" => 40, "height" => 40));
                    array_push($archivos, array("ruta" => "" . $NombreOriginal, "nombre" => $Adjuntos["archivo"]["name"][$key]));
                } else {
                    resize($ruta, $NombreOriginal, 'thumbs', array("width" => 50, "height" => 50));
                    array_push($archivos, array("ruta" => "archivos/" . $NombreOriginal, "nombre" => $Adjuntos["archivo"]["name"][$key]));
                }
        }
        return $archivos;
    }

}

if (!function_exists('subirArchivos')) {

    function subirArchivos($Adjuntos = array(), $lugar = "") {
        $archivos = array();
        $ci = & get_instance();
        if ($lugar == "avatars") {
            $ruta = getcwd() . '/media/archivos/avatars/';
        } else {
            $ruta = getcwd() . '/media/archivos/';
        }

        $mensage = '';
        foreach ($Adjuntos as $valor => $key) {
            if (substr($valor, 0, 7) == "archivo") {
                if ($key['error'] == UPLOAD_ERR_OK) {//Si el archivo se paso correctamente Ccontinuamos 
                    $NombreOriginal = $key['name']; //Obtenemos el nombre original del archivo
                    $NombreOriginal = rand(1, 10000) . "_" . $NombreOriginal;
                    $temporal = $key['tmp_name']; //Obtenemos la ruta Original del archivo
                    $Destino = $ruta . $NombreOriginal; //Creamos una ruta de destino con la variable ruta y el nombre original del archivo	

                    move_uploaded_file($temporal, $Destino); //Movemos el archivo temporal a la ruta especificada		
                }
                if ($key['error'] == '') { //Si no existio ningun error, retornamos un mensaje por cada archivo subido
                    if ($lugar == "avatars") {
                        resize($ruta, $NombreOriginal, 'thumbs', array("width" => 40, "height" => 40));
                        array_push($archivos, array("ruta" => "" . $NombreOriginal, "nombre" => $key['name']));
                    } else {
                        resize($ruta, $NombreOriginal, 'thumbs', array("width" => 50, "height" => 50));
                        array_push($archivos, array("ruta" => "archivos/" . $NombreOriginal, "nombre" => $key['name']));
                    }
                }
                if ($key['error'] != '') {//Si existio algún error retornamos un el error por cada archivo.
                    $mensage .= '-> No se pudo subir el archivo <b>' . $NombreOriginal . '</b> debido al siguiente Error: \n' . $key['error'];
                }
            }
        }
        return $archivos;
    }

}

if (!function_exists('resize')) {

    function resize($path, $name, $carpeta, $dimension) {
        $ci = & get_instance();
        $config = array();
        $config['image_library'] = 'gd2';
        $config['source_image'] = $path . $name; // le decimos donde esta la imagen que acabamos de subir
        $config['thumb_marker'] = '';
        $config['new_image'] = $path . $carpeta; // las nuevas imágenes se guardan en la carpeta "thumbs"
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = $dimension["width"];
        $config['height'] = $dimension["height"];

        $ci->load->library('image_lib');
        $ci->image_lib->clear();
        $ci->image_lib->initialize($config);
        $ci->image_lib->resize();
    }

}

if (!function_exists('enviarEmail')) {

    function enviarEmail($Datos) {
        $ci = & get_instance();

        $ci->load->library('My_PHPMailer');
        $url_logo = "logo_marca.png";
        $empresa = "Hikari";
        $html = '<table cellspacing="0" cellpadding="0" border="0" style="color:#333;background:#fff;padding:0;margin:0;width:100%;font:15px/1.25em \'Helvetica Neue\',Arial,Helvetica"> <tbody><tr width="100%"> <td valign="top" align="left" style="background:#f0f0f0;font:15px/1.25em \'Helvetica Neue\',Arial,Helvetica"> <table style="border:none;padding:0 18px;margin:50px auto;width:500px"> <tbody>' .
                '<tr width="100%" height="57"> <td valign="top" align="left" style="border-top-left-radius:4px;border-top-right-radius:4px;background:#fff;padding:12px 18px;text-align:center">' .
                '<img src="' . base_url() . '/media/img/' . $url_logo . '" title="' . $empresa . '" style="font-weight:bold;font-size:18px;color:#fff;vertical-align:top" class="CToWUd"> </td> </tr>' .
                '<tr width="100%"><td valign="top" align="left" style="border-bottom-left-radius:4px;border-bottom-right-radius:4px;background:#fff;padding:18px"> <h1 style="font-size:20px;margin:0;color:#333"> ' . $Datos["titulo"] . ' </h1>' .
                '<p style="font:15px/1.25em \'Helvetica Neue\',Arial,Helvetica;color:#333">' .
                '</p>' .
                $Datos["mensaje"] . '<br>Para revisar la tarea hacer <a href="' . base_url() . '" target="_blank">clic aquí</a>' .
                '</td> </tr>' .
                '<tr width="100%" height="30"> <td valign="top" align="left" style="border-top-left-radius:4px;border-top-right-radius:4px;background: #fff;/* padding:12px 18px; */text-align: left;padding-left: 14px;"><img height="30" width="60" src="http://req.pe/media/img/logomail.png" title="REQ" style="font-weight:bold;/* font-size:18px; */color:#fff;vertical-align:top" class="CToWUd">' .
                '</tbody> </table>';
        $mail = new PHPMailer();
        $mail->SetLanguage('es');
        $mail->CharSet = 'UTF-8';
        $mail->FromName = $Datos["FROM"];
        $mail->From = $Datos["FROMNAME"];
        $mail->Subject = $Datos["asunto"];
        $mail->AddAddress($Datos["correo"]);
        $mail->addBcc("vicrea@beta.pe");
        $mail->Body = $html;
        $mail->IsHTML(true);
        if (!$mail->Send()) {
            $aMensaje = array("msj" => "Correo no válido");
        } else {
            $aMensaje = array("msj" => "1");
        }
    }

}

if (!function_exists('getFechaEn2')) {

    function getFechaEn2($fecha) {
        if ($fecha != "" || $fecha != NULL || $fecha != "00/00/00") {
            list($dia, $mes, $anio) = explode("/", $fecha);
            return $anio . "-" . $mes . "-" . $dia;
        } else {
            return "ND";
        }
    }

}
if (!function_exists('getFechaEn')) {

    function getFechaEn($fecha) {
        date_default_timezone_set("America/Lima");
        return date("Y-m-d", strtotime($fecha));
    }

}

if (!function_exists('getProximoSemana')) {

    function getProximoSemana($fecha) {
        return date('Y-m-d', strtotime('+1 week', strtotime($fecha)));
    }

}
if (!function_exists('getProximoMes')) {

    function getProximoMes($fecha) {
		return date('Y-m-d', strtotime('+1 months', strtotime($fecha)));
    }
}

if (!function_exists('getProximo2Mes')) {
	
    function getProximo2Mes($fecha) {
        return date('Y-m-d', strtotime('+2 months', strtotime($fecha)));
    }
}

if (!function_exists('getProximo3Mes')) {
	
    function getProximo3Mes($fecha) {
        return date('Y-m-d', strtotime('+3 months', strtotime($fecha)));
    }
}

if (!function_exists('getProximoMesProgramado')) {
	
    function getProximoMesProgramado($fecha,$meses) {
		switch($meses){
			case 1:
				return date('Y-m-d', strtotime('+1 months', strtotime($fecha)));
				break;
			case 2:
				return date('Y-m-d', strtotime('+2 months', strtotime($fecha)));
				break;
			case 3:
				return date('Y-m-d', strtotime('+3 months', strtotime($fecha)));
				break;
			case 4:
				return date('Y-m-d', strtotime('+4 months', strtotime($fecha)));
				break;
			case 5:
				return date('Y-m-d', strtotime('+5 months', strtotime($fecha)));
				break;
			case 6:
				return date('Y-m-d', strtotime('+6 months', strtotime($fecha)));
				break;
			case 7:
				return date('Y-m-d', strtotime('+7 months', strtotime($fecha)));
				break;
			case 8:
				return date('Y-m-d', strtotime('+8 months', strtotime($fecha)));
				break;
			case 9:
				return date('Y-m-d', strtotime('+9 months', strtotime($fecha)));
				break;
			case 10:
				return date('Y-m-d', strtotime('+10 months', strtotime($fecha)));
				break;
			case 11:
				return date('Y-m-d', strtotime('+11 months', strtotime($fecha)));
				break;			
			default:
                break;
		}        
    }
}

if (!function_exists('getProximoAnio')) {

    function getProximoAnio($fecha) {
        return date('Y-m-d', strtotime('+1 year', strtotime($fecha)));
    }

}

if (!function_exists('getProximoDia')) {

    function getProximoDia($aDias, $fecha, $idDiaActual, $proximo = 0) {
        $fechaProxima = "";
        switch ($idDiaActual) {
            case 0:
                if ($aDias["domingo"] == 1) {
                    if ($proximo == 1) {
                        $proxFecha = $fecha;
                    } else {
                        $proxFecha = date("Y-m-d");
                    }
                } elseif ($aDias["lunes"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Monday", strtotime($fecha)));
                } elseif ($aDias["martes"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Tuesday", strtotime($fecha)));
                } elseif ($aDias["miercoles"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Wednesday", strtotime($fecha)));
                } elseif ($aDias["jueves"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Thursday", strtotime($fecha)));
                } elseif ($aDias["viernes"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Friday", strtotime($fecha)));
                } elseif ($aDias["sabado"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Sunday", strtotime($fecha)));
                }
                break;
            case 1:
                if ($aDias["lunes"] == 1) {
                    if ($proximo == 1) {
                        $proxFecha = $fecha;
                    } else {
                        $proxFecha = date("Y-m-d");
                    }
                } elseif ($aDias["martes"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Tuesday", strtotime($fecha)));
                } elseif ($aDias["miercoles"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Wednesday", strtotime($fecha)));
                } elseif ($aDias["jueves"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Thursday", strtotime($fecha)));
                } elseif ($aDias["viernes"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Friday", strtotime($fecha)));
                } elseif ($aDias["sabado"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Sunday", strtotime($fecha)));
                } elseif ($aDias["domingo"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Saturday", strtotime($fecha)));
                }
                break;
            case 2:

                if ($aDias["martes"] == 1) {
                    if ($proximo == 1) {
                        $proxFecha = $fecha;
                    } else {
                        $proxFecha = date("Y-m-d");
                    }
                } elseif ($aDias["miercoles"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Wednesday", strtotime($fecha)));
                } elseif ($aDias["jueves"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Thursday", strtotime($fecha)));
                } elseif ($aDias["viernes"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Friday", strtotime($fecha)));
                } elseif ($aDias["sabado"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Sunday", strtotime($fecha)));
                } elseif ($aDias["domingo"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Saturday", strtotime($fecha)));
                } elseif ($aDias["lunes"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Monday", strtotime($fecha)));
                }
                break;
            case 3:
                if ($aDias["miercoles"] == 1) {
                    if ($proximo == 1) {
                        $proxFecha = $fecha;
                    } else {
                        $proxFecha = date("Y-m-d");
                    }
                } elseif ($aDias["jueves"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Thursday", strtotime($fecha)));
                } elseif ($aDias["viernes"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Friday", strtotime($fecha)));
                } elseif ($aDias["sabado"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Sunday", strtotime($fecha)));
                } elseif ($aDias["domingo"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Saturday", strtotime($fecha)));
                } elseif ($aDias["lunes"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Monday", strtotime($fecha)));
                } elseif ($aDias["martes"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Tuesday", strtotime($fecha)));
                }
                break;
            case 4:
                if ($aDias["jueves"] == 1) {
                    if ($proximo == 1) {
                        $proxFecha = $fecha;
                    } else {
                        $proxFecha = date("Y-m-d");
                    }
                } elseif ($aDias["viernes"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Friday", strtotime($fecha)));
                } elseif ($aDias["sabado"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Sunday", strtotime($fecha)));
                } elseif ($aDias["domingo"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Saturday", strtotime($fecha)));
                } elseif ($aDias["lunes"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Monday", strtotime($fecha)));
                } elseif ($aDias["martes"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Tuesday", strtotime($fecha)));
                } elseif ($aDias["miercoles"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Wednesday", strtotime($fecha)));
                }
                break;
            case 5:
                if ($aDias["viernes"] == 1) {
                    if ($proximo == 1) {
                        $proxFecha = $fecha;
                    } else {
                        $proxFecha = date("Y-m-d");
                    }
                } elseif ($aDias["sabado"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Sunday", strtotime($fecha)));
                } elseif ($aDias["domingo"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Saturday", strtotime($fecha)));
                } elseif ($aDias["lunes"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Monday", strtotime($fecha)));
                } elseif ($aDias["martes"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Tuesday", strtotime($fecha)));
                } elseif ($aDias["miercoles"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Wednesday", strtotime($fecha)));
                } elseif ($aDias["jueves"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Thursday", strtotime($fecha)));
                }
                break;
            case 6:
                if ($aDias["sabado"] == 1) {
                    if ($proximo == 1) {
                        $proxFecha = $fecha;
                    } else {
                        $proxFecha = date("Y-m-d");
                    }
                } elseif ($aDias["domingo"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Saturday", strtotime($fecha)));
                } elseif ($aDias["lunes"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Monday", strtotime($fecha)));
                } elseif ($aDias["martes"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Tuesday", strtotime($fecha)));
                } elseif ($aDias["miercoles"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Wednesday", strtotime($fecha)));
                } elseif ($aDias["jueves"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Thursday", strtotime($fecha)));
                } elseif ($aDias["viernes"] == 1) {
                    $proxFecha = date("Y-m-d", strtotime("next Friday", strtotime($fecha)));
                }
                break;
            default:
                break;
        }
        return $proxFecha;
    }

}

if (!function_exists('getEsElDia')) {

    function getEsElDia($aDias = array(), $fecha = "") {
        if ($fecha == "") {
            $fecha = date("Y-m-d");
        }
        $ejecuta = 0;
        $lunes = $aDias["lunes"];
        $martes = $aDias["martes"];
        $miercoles = $aDias["miercoles"];
        $jueves = $aDias["jueves"];
        $viernes = $aDias["viernes"];
        $sabado = $aDias["sabado"];
        $domingo = $aDias["domingo"];
        $diaActual = date("w", strtotime($fecha));

        if ($diaActual == 0 && $domingo == 1) {
            $ejecuta = 1;
        }
        if ($diaActual == 1 && $lunes == 1) {
            $ejecuta = 1;
        }
        if ($diaActual == 2 && $martes == 1) {
            $ejecuta = 1;
        }
        if ($diaActual == 3 && $miercoles == 1) {
            $ejecuta = 1;
        }
        if ($diaActual == 4 && $jueves == 1) {
            $ejecuta = 1;
        }
        if ($diaActual == 5 && $viernes == 1) {
            $ejecuta = 1;
        }
        if ($diaActual == 6 && $sabado == 1) {
            $ejecuta = 1;
        }
        return $ejecuta;
    }

}

if (!function_exists('redireccionaAccion')) {

    // Validar Session 
	function redireccionaAccion($idestado, $idtarea, $paraadmin = 0, $idusuario_creador = 0,$idgrupo=0,$idusuario_ejecutor=0) {
        $ci = & get_instance();
        $idusuarioActual = intval($ci->session->userdata('idusuario'));
		$rol = intval($ci->session->userdata('rol'));
        switch ($rol) {
            case 1:
                if ($idestado == 1) {
                    if ($paraadmin == 1) {
						if($idusuarioActual== $idusuario_creador || $idusuarioActual==$idusuario_ejecutor){
							redirect(base_url() . "tarea/poratender/". $idtarea, 'refresh');
						}else{
							redirect(base_url() . "tarea/ver/". $idtarea, 'refresh');
						}
						
                    } else {
                        redirect(base_url() . "tarea/ver/". $idtarea, 'refresh');
                    }
                } elseif ($idestado == 3) {
					if($idusuarioActual== $idusuario_creador || $idusuarioActual==$idusuario_ejecutor){
						redirect(base_url() . "tarea/corregirtarea/". $idtarea, 'refresh');
					}else{
						redirect(base_url() . "tarea/ver/". $idtarea, 'refresh');
					}
					
                } elseif ($idestado == 4) {
                    $ci->load->model('usuariomodel');
                    $objUsuario = $ci->usuariomodel->getdata(array("rol"), "idusuario = " . $idusuario_creador, 1);
                    $rolCreador = $objUsuario->rol;
                    if ($paraadmin == 1) {
						redirect(base_url() . "tarea/ver/". $idtarea, 'refresh');
                    } else {
                        if ($rolCreador == 3 && $rol == 1) {
							if($idusuarioActual== $idusuario_creador || $idusuarioActual==$idusuario_ejecutor){
								redirect(base_url() . "tarea/confirmar/". $idtarea, 'refresh');
							}else{
								redirect(base_url() . "tarea/ver/". $idtarea, 'refresh');
							}
							
                        } else {
                            if ($rol == 1) {
								if($idusuarioActual== $idusuario_creador || $idusuarioActual==$idusuario_ejecutor){
									redirect(base_url() . "tarea/confirmar/". $idtarea, 'refresh');
								}else{
									redirect(base_url() . "tarea/ver/". $idtarea, 'refresh');
								}
                                
                            } else {
                                redirect(base_url() . "tarea/ver/". $idtarea, 'refresh');
                            }
                        }
                    }
                } elseif ($idestado == 2) {
                    if ($paraadmin == 1) {
						if($idusuarioActual== $idusuario_creador || $idusuarioActual==$idusuario_ejecutor){
							redirect(base_url() . "tarea/ejecutar/". $idtarea, 'refresh');
						}else{
							redirect(base_url() . "tarea/ver/". $idtarea, 'refresh');
						}
						
                    } else {
                        redirect(base_url() . "tarea/ver/". $idtarea, 'refresh');
                    }
                } elseif ($idestado == 6) {
                    if ($paraadmin == 1) {
						if($idusuarioActual== $idusuario_creador || $idusuarioActual==$idusuario_ejecutor){
							redirect(base_url() . "tarea/corregir_ejecucion/". $idtarea, 'refresh');                        
						}else{
							redirect(base_url() . "tarea/ver/". $idtarea, 'refresh');
						}						
                    } else {
						redirect(base_url() . "tarea/ver/". $idtarea, 'refresh');
                    }
                } elseif ($idestado == 7) {
                    redirect(base_url() . "tarea/ver/". $idtarea, 'refresh');                    
                } elseif ($idestado == 11) {
                    if ($paraadmin == 1) {
						// DEBERIA FILTRAR TODOS LOS PENDIENTES DE ENVIO
						redirect(base_url() . "tarea/lista", 'refresh');
                        //$enlace = "<h5><span class='label label-warning'>Pend. Envío<span></h5> <a href='" . base_url() . "tarea/editar/" . $idtarea . "' class='subrayado'>Editar</a>";
                    } else {
						redirect(base_url() . "tarea/ver/". $idtarea, 'refresh');                        
                    }
                } else {
					redirect(base_url() . "tarea/ver/". $idtarea, 'refresh');                    
                }
                break;
            case 2:
                if ($idestado == 1) {
                    if ($paraadmin == 1) {
						redirect(base_url() . "tarea/ver/". $idtarea, 'refresh');                        
                    } else {
						if($ci->session->userdata("idusuario")==$idusuario_creador){
							redirect(base_url() . "tarea/ver/". $idtarea, 'refresh');
						}else{
							if($idusuarioActual== $idusuario_creador || $idusuarioActual==$idusuario_ejecutor){
								redirect(base_url() . "tarea/poratender/". $idtarea, 'refresh');							
							}else{
								redirect(base_url() . "tarea/ver/". $idtarea, 'refresh');
							}
							
						}                        
                    }
                } elseif ($idestado == 4) {
                    if ($paraadmin == 1) {
                        $ci->load->model('usuariomodel');
                        $objUsuario = $ci->usuariomodel->getdata(array("rol"), "idusuario = " . $idusuario_creador, 1);
                        $rolCreador = $objUsuario->rol;
                        if ($rolCreador == 1) {
							if($idusuarioActual== $idusuario_creador || $idusuarioActual==$idusuario_ejecutor){
								redirect(base_url() . "tarea/confirmar/". $idtarea, 'refresh');
							}else{
								redirect(base_url() . "tarea/ver/". $idtarea, 'refresh');
							}
							
                        } else {
							redirect(base_url() . "tarea/ver/". $idtarea, 'refresh');                            
                        }
                    } else {
						if($ci->session->userdata("idusuario")==$idusuario_creador){
								if($idusuarioActual== $idusuario_creador || $idusuarioActual==$idusuario_ejecutor){
									redirect(base_url() . "tarea/confirmar/". $idtarea, 'refresh');
								}else{
									redirect(base_url() . "tarea/ver/". $idtarea, 'refresh');
								}								
							}else{
								redirect(base_url() . "tarea/ver/". $idtarea, 'refresh');
						}								
                    }
                } elseif ($idestado == 2) {
                    if ($paraadmin == 1) {
						redirect(base_url() . "tarea/ver/". $idtarea, 'refresh');                        
                    } else {
						if($ci->session->userdata("idusuario")==$idusuario_creador){
							redirect(base_url() . "tarea/ver/". $idtarea, 'refresh');							
						}else{
							if($idusuarioActual== $idusuario_creador || $idusuarioActual==$idusuario_ejecutor){
								redirect(base_url() . "tarea/ejecutar/". $idtarea, 'refresh');
							}else{
								redirect(base_url() . "tarea/ver/". $idtarea, 'refresh');				
							}
							
						}
                    }
                } elseif ($idestado == 6) {
                    if ($paraadmin == 1) {
                        $ci->load->model('usuariomodel');
                        $objUsuario = $ci->usuariomodel->getdata(array("rol"), "idusuario = " . $idusuario_creador, 1);
                        $rolCreador = $objUsuario->rol;
                        if ($rolCreador == 2) {
							if($idusuarioActual== $idusuario_creador || $idusuarioActual==$idusuario_ejecutor){
								redirect(base_url() . "tarea/corregir_ejecucion/". $idtarea, 'refresh');
							}else{
								redirect(base_url() . "tarea/ver/". $idtarea, 'refresh');
							}							
                        } else {
							redirect(base_url() . "tarea/ver/". $idtarea, 'refresh');             
                        }
                    } else {
						if($idusuarioActual== $idusuario_creador || $idusuarioActual==$idusuario_ejecutor){
							redirect(base_url() . "tarea/corregir_ejecucion/". $idtarea, 'refresh');
						}else{
							redirect(base_url() . "tarea/ver/". $idtarea, 'refresh');	
						}					
                    }
                } elseif ($idestado == 7) {
					redirect(base_url() . "tarea/ver/". $idtarea, 'refresh');
                } elseif ($idestado == 11) {
                    if ($paraadmin == 0) {
						// Está para verse
						redirect(base_url() . "tarea/lista", 'refresh');
                    } else {
						redirect(base_url() . "tarea/ver/". $idtarea, 'refresh');                        
                    }
                } elseif ($idestado == 8) {
					if($idusuarioActual== $idusuario_creador || $idusuarioActual==$idusuario_ejecutor){
						redirect(base_url() . "tarea/editar/". $idtarea, 'refresh');                    
					}else{
						redirect(base_url() . "tarea/ver/". $idtarea, 'refresh');                        
					}
					
                } elseif ($idestado == 9) {
					redirect(base_url() . "tarea/ver/". $idtarea, 'refresh');                        
                    
                } elseif ($idestado == 5) {
                    redirect(base_url() . "tarea/ver/". $idtarea, 'refresh');                        
                } else {
                    redirect(base_url() . "tarea/ver/". $idtarea, 'refresh');                        
                }
                break; 
            case 3:
                if ($idestado == 7 || $idestado == 10) {
                    redirect(base_url() . "tarea/evaluargrupo/". $idgrupo, 'refresh');
                } elseif ($idestado == 4) {
					$ci->load->model('usuariomodel');
                    if ($paraadmin == 1) {
                        $objUsuario = $ci->usuariomodel->getdata(array("rol"), "idusuario = " . $idusuario_creador, 1);
                        $rolCreador = $objUsuario->rol;
                        if ($rolCreador == 3) {
							redirect(base_url() . "tarea/confirmar/". $idtarea, 'refresh');
                        } else {
							redirect(base_url() . "tarea/ver/". $idtarea, 'refresh');
                        }
                    } else {
						$objUsuario = $ci->usuariomodel->getdata(array("rol"), "idusuario = " . $idusuario_creador, 1);
                        $rolCreador = $objUsuario->rol;
                        if ($rolCreador == 3) {
							redirect(base_url() . "tarea/confirmar/". $idtarea, 'refresh');
                        } else {
							redirect(base_url() . "tarea/ver/". $idtarea, 'refresh');
                        }
                    }
                } else {
					redirect(base_url() . "tarea/ver/". $idtarea, 'refresh');
                }                
                break;

            default:
                break;
        }
        return $enlace;
    }

}

if (!function_exists('getnomEstadoDoc')) {

    function getnomEstadoDoc($estado) {
        $nomestado = "";
        switch ($estado) {
            case 1:
                $nomestado = '<span class="label label-info" style="font-size: 11px;">En local</span>';
                break;
            case 2:
                $nomestado = '<span class="label label-warning" style="font-size: 11px;">En transporte</span>';
                break;
            case 3:
                $nomestado = '<span class="label label-success" style="font-size: 11px;">En Almacén</span>';
                break;
            case 4:
                $nomestado = '<span class="label label-primary" style="font-size: 11px;">En almacén - Derivado</span>';
                break;
			case 5:
                $nomestado = '<span class="label label-default" style="font-size: 11px;">Almacén externo</span>';
                break;				
            default:
                $nomestado = "";
                break;
        }
        return $nomestado;
    }

}

if (!function_exists('getEnlaceDoc')) {

    function getEnlaceDoc($estado, $iddocumento) {
        $enlaces = '';
        $ci = & get_instance();
        $rol = intval($ci->session->userdata('rol'));
        switch ($estado) {
            case 1:
                $enlaces='<a href="' . base_url() . 'documento/ver/' . $iddocumento . '" class="btn btn-link">Ver</a>';
                if ($rol != 6) {
                    $enlaces.='<a href="' . base_url() . 'documento/editar/' . $iddocumento . '" class="btn btn-link">Editar</a>';
                    $enlaces.='<a href="' . base_url() . 'documento/transportar/' . $iddocumento . '" class="btn btn-link">Transportar</a>';
                }
                break;
            case 2:
                $enlaces='<a href="' . base_url() . 'documento/ver/' . $iddocumento . '" class="btn btn-link">Ver</a>';
                if ($rol == 6) {
                    $enlaces.='<a href="' . base_url() . 'documento/editar/' . $iddocumento . '" class="btn btn-link">Editar</a>';
                    $enlaces.='<a href="' . base_url() . 'documento/almacenar/' . $iddocumento . '" class="btn btn-link">Almacenar</a>';
                }

                break;
            case 3:
                $enlaces='<a href="' . base_url() . 'documento/ver/' . $iddocumento . '" class="btn btn-link">Ver</a>';
                if ($rol == 6) {
                    $enlaces.='<a href="' . base_url() . 'documento/editar/' . $iddocumento . '" class="btn btn-link">Editar</a>';
                    $enlaces.='<a href="javascript:void(0);" data-id="' . $iddocumento . '" class="btn btn-link derivar">Derivar</a>';
                }

                break;
            case 4:
                $enlaces ='<a href="' . base_url() . 'documento/ver/' . $iddocumento . '" class="btn btn-link">Ver</a>';
                if ($rol == 6) {                    
                    $enlaces.='<a href="' . base_url() . 'documento/editar/' . $iddocumento . '" class="btn btn-link">Editar</a>';
                }
				
			case 5:
                $enlaces ='<a href="' . base_url() . 'documento/ver/' . $iddocumento . '" class="btn btn-link">Ver</a>';
                if ($rol == 6) {                    
                    $enlaces.='<a href="' . base_url() . 'documento/editar/' . $iddocumento . '" class="btn btn-link">Editar</a>';
                }
                break;

            default:
                $enlaces.="";
                break;
        }
        return $enlaces;
    }

}

if (!function_exists('getDiaSemanaPersonalizado')) {

    function getDiaSemanaPersonalizado() {		
		$aDia[1]=array("iddia"=>2,"nombre"=>"Lunes","campo"=>"lunes","abrev"=>"lu");
		$aDia[2]=array("iddia"=>3,"nombre"=>"Martes","campo"=>"martes","abrev"=>"ma");
		$aDia[3]=array("iddia"=>4,"nombre"=>"Miércoles","campo"=>"miercoles","abrev"=>"mi");
		$aDia[4]=array("iddia"=>5,"nombre"=>"Jueves","campo"=>"jueves","abrev"=>"ju");
		$aDia[5]=array("iddia"=>6,"nombre"=>"Viernes","campo"=>"viernes","abrev"=>"vi");
		$aDia[6]=array("iddia"=>7,"nombre"=>"Sábado","campo"=>"sabado","abrev"=>"sa");
		$aDia[7]=array("iddia"=>1,"nombre"=>"Domingo","campo"=>"domingo","abrev"=>"do");
		return $aDia;
    }

}

if (!function_exists('getIdDia')) {

    function getIdDia() {
		$aDia["lunes"]=array("iddia"=>1,"abrev"=>"lu");
		$aDia["martes"]=array("iddia"=>2,"abrev"=>"ma");
		$aDia["miercoles"]=array("iddia"=>3,"abrev"=>"mi");
		$aDia["jueves"]=array("iddia"=>4,"abrev"=>"ju");
		$aDia["viernes"]=array("iddia"=>5,"abrev"=>"vi");
		$aDia["sabado"]=array("iddia"=>6,"abrev"=>"sa");
		$aDia["domingo"]=array("iddia"=>7,"abrev"=>"do");		
		return $aDia;
    }

}

if (!function_exists('getDiaSemana')) {

    function getDiaSemana() {
		$aDia[0]=array("iddia"=>7,"nombre"=>"Domingo","campo"=>"domingo");
		$aDia[1]=array("iddia"=>1,"nombre"=>"Lunes","campo"=>"lunes");
		$aDia[2]=array("iddia"=>2,"nombre"=>"Martes","campo"=>"martes");
		$aDia[3]=array("iddia"=>3,"nombre"=>"Miércoles","campo"=>"miercoles");
		$aDia[4]=array("iddia"=>4,"nombre"=>"Jueves","campo"=>"jueves");
		$aDia[5]=array("iddia"=>5,"nombre"=>"Viernes","campo"=>"viernes");
		$aDia[6]=array("iddia"=>6,"nombre"=>"Sábado","campo"=>"sabado");		
		return $aDia;
    }

}
